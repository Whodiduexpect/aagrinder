# AAGRINDER CHANGELOG

This changelog contains some of the things that were added/removed/changed.

But not everything. Most changes in aagrinder are not something a casual
player would care about, so they are not included in this changelog.

For a more complete view, read the commit message history.



### November 2022

+ Added stick
+ Made snow visible behind player
* Changed water draining
- Removed cloud self-regeneration

### October 2022

+ Added /craft
+ Made water visible behind seagrass
+ Added fireflies
+ Biome is now displayed under coordinates
+ Added buckets
+ Added snow block
+ Made icicles grow
+ Use , and . keys for scrolling hotbar
+ Increased stack size
* Made stalagmite climbable
* Changed the effect of quicksand
* Made tree growth more easily understandable
* Snow no longer blocks you when on top of ladder
* Changed snowfall logic
- Removed "H for help" hint

### September 2022

* Made custom warp invisible and command-controlled
- Removed /sudo

### August 2022

+ Added custom warps (not yet obtainable)
+ Made hotbar clickable
+ Added pick block key
+ Added hunger
+ Made bushes farmabele
* Pony mod now uses two different fonts
* Made map view prettier

### July 2022

+ Added some spooky items
+ Added seagrass animation
+ Added bubble animation
+ Added land claiming (admin-only)
+ Added warp animations
+ Added warp warmup and cooldown
+ Added friends management
+ Added snow regeneration (snowfall)
+ Added snowflake animation
* Renamed portal to warp
* Decreased network usage
- Removed MySQL dependency

### June 2022

* Improved chest and crafting UI
* Made hotbar more interesting

### May 2022

* Improved chest and crafting UI

### April 2022

+ Made cloud regenerate on its own

### February 2022

* Made leaves non-solid
* Made chest climbable

### December 2021

+ Added hoppers

### November 2021

+ Added many debugging features
* Improved UI

### October 2021

+ Added stone grinder
* Improved inventory and crafting UI

### September 2021

+ Added new crafting menu
+ Added new recipes
+ Added Matrix bridge

### July 2021

+ Added new animations
+ Added inventory item limits
* Improved inventory UI
* Made seagrass more easily placable
* Made seagrass not destroy itself

### June 2021

* Changed climbing logic
* Changed ruby logic

### January 2021

+ Added new recipes
+ Added cloud animation
* Improved settings (/set)
* Improved signs
* Changed wire logic

### November 2020

- Removed digging cooldown

### October 2020

+ Added digging cooldown
* Changed graphite to chalk

### September 2020

+ Added wire logic
+ Added stone slab
+ Added trapdoor
+ Added ice
+ Added word wrapping in chat
+ Made fruit pickable without breaking
+ Added berry bushes
+ Added quicksand
+ Added inspector
+ Added new commands
+ Added graphite and magnetite
+ Added portal biome
* Improved movement logic
* Improved water logic
* Changed coral growth
* Improve tentacle growth

### August 2020

+ Enabled commands on login screen
* Improved movement logic

### July 2020

+ Added mixed forest biome
+ Added rainbow biome
+ Added grabber
+ Added water flow logic
* Multithreaded (faster) terrain generation
* Improved custom key mapping

### June 2020

+ Added tentacle growth
+ Added biome background colors
* Updated tree growth
* Changed coral growth

### April 2020

* Fix tree not growing next to block

### March 2020

+ Added grass and seagrass growth
+ Added coral death
+ Added basic wire logic
+ Added leaves falling animation
* Updated coral growth
* Updated recipes

### February 2020

+ Added map view
+ Added ? block
+ Added frame block
* Improved pony mod
* Updated recipes
- Fixed ladder bug

### December 2019

+ Added chest
* Made vines climbable

### November 2019

+ Added music
* Improved UI

### October 2019

+ Made player color changable
+ Made password changable
+ Added more items
+ Made aagrinder more hacking-resistant
* Decreased network usage

### September 2019

+ Added portals (warps)
+ Added basic water physics
+ Added coral
+ Added water
+ Added block animations
+ Added sprinting on ladder
* Changed ruby logic
* Decreased client RAM usage

### August 2019

+ Added colors in inventory
+ Allowed digging under player
+ Detach player from terrain
+ Added super grinder
+ Added hotbar
+ Added color variation throughout terrain
+ Added better biomes
+ Make leaves despawn
+ Added leaves animation
+ Added ghost
+ Added ruby
+ Added tree growth
+ Display other player names
* Improved mouse and keyboard controls
* Swapped mouse buttons
* Easier account registration
- Remove whisper logging

### July 2019

* Made camera not centered on player

### June 2019

+ A pretty login screen
+ Added sprinting
+ Added pony.html
+ Added support for color in chat

### November 2018

+ Added support for mouse in inventory
+ Added gravity
+ Added ladder logic
+ Added join/leave announcements
+ Added grass spinning

### September 2018

+ Added mouse controls
+ Added chatbox
+ Added placement
+ Added digging

### April 2018

- Replace MongoDB dependency with MySQL

### February 2018

+ Cursor mouse controls
+ Remember command history
+ Player colors
+ Color support
+ Ability to register from login screen
* Improved text editing

Progress before February 2018 was not tracked
