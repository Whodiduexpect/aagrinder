# >>> https://aagrinder.xyz <<<

![aagrinder](https://mazie.rocks/files/aagrinder-jungle.png "AAGRINDER")

# What is this???

- AAGRINDER is a text-based game
- AAGRINDER is a sandbox game
- AAGRINDER is a multiplayer game
- AAGRINDER is a browser game
- AAGRINDER is inspired by [Minecraft](https://www.minecraft.net) and [Terraria](https://terraria.org)

Uses this terrain generator: https://gitlab.com/MRAAGH/aagrinder-terrain

Note: You can't play AAGRINDER on touchscreen.

Wiki: http://wiki.aagrinder.xyz

Matrix: [#aagrinder:aagrinder.xyz](https://matrix.to/#/#aagrinder:aagrinder.xyz)

# Play on my server: https://aagrinder.xyz

# ... or [make your own server](https://wiki.aagrinder.xyz/index.php?title=Setting_up_a_server)

TL;DR server installation:
```
$ git clone https://gitlab.com/MRAAGH/aagrinder.git
$ cd aagrinder
$ npm install
$ git clone https://gitlab.com/MRAAGH/aagrinder-terrain.git
$ cd aagrinder-terrain
$ make
$ cd ..
$ npm start
```

Dependencies:
- node.js
- npm
  - bcryptjs
  - express
  - socket.io
- g++

This game is licensed under the GNU AGPL, which allows you to modify the game or use parts of it in your own projects.
However, the music is served with multiple separate licenses. See `public/audio/music/LICENSE`.
