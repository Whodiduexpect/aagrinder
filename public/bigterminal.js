// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


// Wrapper for terminal.
// Adds println, text wrapping, output history, scrolling, dynamic last line

"use strict";

const DEFAULT_COLOR = '#aaa';

class BigTerminal {
    constructor(terminal){
        this.terminal = terminal;
        this.lines = []; // lines of output
        this.visual = []; // 1 visual line corresponds to 1 line on screen
        // (though it's not neccesarily visible on screen)
        this.lineLimit = 100;
        this.scrollPos = 0; // index of the (visual) line at the top
        this.dynamic = ''; // line that's being typed
        this.visualDynamic = [];
    }

    println(text){
        const wasScrolledToEnd = this.isScrolledToEnd();
        this.lines.push(text);
        this.formatLine(text, this.visual);
        this.deleteOldLines();
        if(wasScrolledToEnd){
            this.scrollToEnd();
        }
        this.display();
    }

    clear() {
        this.lines = [];
        this.reformat();
        this.scrollToEnd();
    }

    setLineLimit(n) {
        this.lineLimit = n;
        this.deleteOldLines();
        this.display();
    }

    deleteOldLines() {
        // delete visual lines
        this.visual = this.visual.slice(
            Math.max(this.visual.length-this.lineLimit,0));
        // also delete logical lines
        // so they don't take up unnecessary ram.
        // Number of logical lines is always >=
        // number of visual lines.
        // So trimming them to the same number is ok
        this.lines = this.lines.slice(
            Math.max(this.lines.length-this.lineLimit,0));
    }

    modify(text, silent){
        this.dynamic = text;
        this.visualDynamic = [];
        this.formatLine(this.dynamic, this.visualDynamic);
        if(!silent) this.scrollToEnd();
        this.display();
    }

    // add typed line to lines
    commit(){
        this.println(this.dynamic);
        this.dynamic = '';
    }

    printTruncate(text){ // cut off instead of wrapping
        this.println(text.substring(0, this.terminal.width));
    }

    formatLine(line_, buffer){
        // make copy to leave original intact:
        const lines = AASTRING_DECODE_WITH_WRAPPING(
            line_, this.terminal.width, DEFAULT_COLOR)
        for (const line of lines) {
            buffer.push(line);
        }
        if (SETTINGS.s['gap']) {
            buffer.push({
                raw: '',
                color: [],
            });
        }
    }

    reformat(){ // wrap again (terminal size changed?)
        this.visual = [];
        for(const line of this.lines){
            this.formatLine(line, this.visual);
        }
        this.visualDynamic = [];
        this.formatLine(this.dynamic, this.visualDynamic);
        this.display();
    }

    display(){ // find visible section and display it
        const visibleLines = [];
        for(let i = 0; i < this.terminal.height && i + this.scrollPos < this.visual.length; i++){
            visibleLines.push(this.visual[i + this.scrollPos]);
        }
        // is there space left at the end for displaying the dynamic line?
        if(visibleLines.length < this.terminal.height){
            for(let j = 0; j < this.visualDynamic.length && visibleLines.length < this.terminal.height; j++){
                visibleLines.push(this.visualDynamic[j]);
            }
        }
        this.terminal.displayScreen(visibleLines);
    }

    scrollUp(n=1){
        this.scrollPos -= n;
        if(this.scrollPos < 0){
            this.scrollPos = 0;
        }
        this.display();
    }

    scrollDown(n=1){
        this.scrollPos += n;
        if(this.scrollPos > this.getScrollableLength()){
            this.scrollPos = this.getScrollableLength();
        }
        this.display();
    }

    scrollToEnd(){
        this.scrollPos = this.getScrollableLength();
        if(this.scrollPos < 0){
            this.scrollPos = 0;
        }
        this.display();
    }

    getScrollableLength() {
        return this.visual.length
            + this.visualDynamic.length
            - this.terminal.height;
    }

    isScrolledToEnd(){
        let endScrollPos = this.getScrollableLength();
        if(endScrollPos < 0){
            endScrollPos = 0;
        }
        return endScrollPos === this.scrollPos;
    }
}
