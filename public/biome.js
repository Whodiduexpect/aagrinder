// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


// generates a set of (almost) all sprites that appear in the game
// which is much more than can be found in sprites.js
// these sprites are adjusted to specific biomes

"use strict";

// :r! ../util/litwirecolors.py
const LITWIRECOLORS = [
    '#70082f',
    '#a14c76',
    '#a7547f',
    '#ad5d88',
    '#b36692',
    '#ba6e9b',
    '#c077a4',
    '#c680ad',
    '#cc88b6',
    '#d391bf',
    '#d999c8',
    '#dfa2d1',
    '#e5abda',
    '#ecb3e3',
    '#f2bcec',
    '#f8c5f5',
];

const POWLEVELS = '0123456789abcdef';

const WATERLEVELS = '0123456789abcdefghijklmnopqrstuv';

const BIOMES = {

    'a': { r:+0.0,g:+0.4,b:+0.0, sky:'#101008', short:'apple', name:'apple'},
    'A': { r:+0.0,g:+0.5,b:+0.0, sky:'#001810', short:'apleforst', name:'apple forest'},
    'j': { r:+0.4,g:+0.1,b:+0.0, sky:'#002000', short:'aplejungl', name:'apple jungle'},
    'p': { r:+0.0,g:+0.6,b:+0.0, sky:'#08140b', short:'apricot', name:'apricot'},
    'P': { r:+0.0,g:+0.7,b:+0.0, sky:'#04180b', short:'apriforst', name:'apricot forest'},
    'J': { r:+0.6,g:+0.1,b:+0.0, sky:'#002000', short:'jungle', name:'jungle'},
    't': { r:-0.3,g:+0.4,b:+0.5, sky:'#001020', short:'taiga', name:'taiga'},
    'T': { r:-0.3,g:+0.4,b:+0.5, sky:'#0b1014', short:'snowtaiga', name:'snowy taiga'},
    'd': { r:+0.5,g:-0.4,b:-0.4, sky:'#302020', short:'desert', name:'desert'},
    'r': { r:-0.5,g:-0.2,b:-0.2, sky:'#101010', short:'mountain', name:'mountain'},
    'i': { r:-0.3,g:+0.4,b:+0.5, sky:'#0b1014', short:'icemntn', name:'ice mountain'},
    'm': { r:-0.1,g:+0.4,b:+0.2, sky:'#001414', short:'mixforest', name:'mixed forest'},
    'C': { r:-1.5,g:-2.0,b:-2.0, sky:'#200005', short:'tentislnd', name:'tentacle island'},
    'y': { r:+4.0,g:+2.0,b:+4.0, sky:'#330030', short:'yayisland', name:'yay island'},
    'f': { r:+0.0,g:+0.0,b:+0.0, sky:'#000030', short:'flwrislnd', name:'flower island'},
    'u': { r:+0.0,g:+0.0,b:+0.0, sky:'#000000', short:'undergrnd', name:'underground'},
    'c': { r:+0.0,g:+0.0,b:+0.0, sky:'#000000', short:'spookcave', name:'cave'},
    'I': { r:+0.0,g:+0.0,b:+0.0, sky:'#000000', short:'ice cave', name:'ice cave'},
    'o': { r:+0.0,g:+0.0,b:+0.0, sky:'#000000', short:'ocean', name:'ocean'},
    's': { r:+0.0,g:+0.0,b:+0.0, sky:'#000b28', short:'sky', name:'sky'},
    'R': { r:+0.0,g:+0.0,b:+0.0, sky:'#050c2b', short:'rainbow', name:'rainbow'},
    'W': { r:-1.5,g:-2.0,b:-2.0, sky:'#200005', short:'warp', name:'warp'},

};

// old color generation code
// // openssl prime -generate -bits 9
// const r = Math.sin(x/227+y/107+0.1)+Math.sin(x/397-y/127+1.3)+Math.sin(x/457+y/239+2.1);
// const g = Math.sin(x/251+y/109+0.4)+Math.sin(x/419-y/193+1.5)+Math.sin(x/463+y/241+2.7);
// const b = Math.sin(x/389+y/113+0.7)+Math.sin(x/449-y/199+1.7)+Math.sin(x/491+y/397+2.9);

function MIX_COLORS_RGB(a, b, intensity) {
    let dR = intensity*b[0];
    dR = dR>0?Math.floor(dR):Math.ceil(dR);
    let dG = intensity*b[1];
    dG = dG>0?Math.floor(dG):Math.ceil(dG);
    let dB = intensity*b[2];
    dB = dB>0?Math.floor(dB):Math.ceil(dB);
    return [a[0]+dR, a[1]+dG, a[2]+dB];
}

function MIX_COLORS_HEX(a, b, intensity) {
    return (RGB2HEX(MIX_COLORS_RGB(HEX2RGB(a),HEX2RGB(b),intensity)));
}

function HEX2RGB(hex) {
    const r = parseInt(hex.substring(1,3), 16);
    const g = parseInt(hex.substring(3,5), 16);
    const b = parseInt(hex.substring(5,7), 16);
    return [r,g,b];
}

function RGB2HEX(rgb) {
    // sanitize
    for (let i = 0; i < 3; i++) {
        if (rgb[i] < 0) rgb[i] = 0;
        if (rgb[i] > 255) rgb[i] = 255;
        rgb[i] = rgb[i].toString(16);
        if (rgb[i].length < 2) rgb[i] = '0'+rgb[i];
    }
    // serialize
    return '#'+rgb[0]+rgb[1]+rgb[2];
}

function CONVERT_TO_BIOME(sprite, biome) {

    const newSprite = {
        char: sprite.char,
        color: sprite.color,
        sway: sprite.sway,
        misplace: sprite.misplace,
        blinkspeed: sprite.blinkspeed,
        secondchar: sprite.secondchar,
        dy: sprite.dy,
    };

    if (sprite.intensity > 0) {
        // color will be adjusted
        newSprite.color = RGB2HEX(MIX_COLORS_RGB(
            HEX2RGB(sprite.color),
            [biome.r, biome.g, biome.b],
            sprite.intensity));
    }

    return newSprite;
}

function CONVERT_TO_ALL_BIOMES(sprite) {
    const biomeSprite = {};
    for (const biomeKey of Object.keys(BIOMES)) {
        const biome = BIOMES[biomeKey];
        biomeSprite[biomeKey] = CONVERT_TO_BIOME(sprite, biome);
    }
    return biomeSprite;
}

const BIOME_SPRITES = {};

for (const key of Object.keys(SPRITES)) {
    const sprite = SPRITES[key];

    // colors of the wire blocks at different power levels
    if (key[0] === '=' || key === '%') {
        for (let i = 0; i < 16; i++) {
            const newKey = key + POWLEVELS[i];
            const newSprite = {
                char: sprite.char,
                color: LITWIRECOLORS[i],
                sway: sprite.sway,
                misplace: sprite.misplace,
                blinkspeed: sprite.blinkspeed,
            };
            BIOME_SPRITES[newKey] = CONVERT_TO_ALL_BIOMES(newSprite);
        }
    }

    else {
        // all other blocks
        BIOME_SPRITES[key] = CONVERT_TO_ALL_BIOMES(sprite);

        // special stuff for grass
        if (key[0] === '-') {
            BIOME_SPRITES[key]['y'].color = '#ca84a7';
        }
        if (key[0] === 'd') {
            BIOME_SPRITES[key]['t'].color = '#5b4623';
            BIOME_SPRITES[key]['T'].color = '#5b4623';
            BIOME_SPRITES[key]['i'].color = '#5b4623';
        }
    }
}

// flowing water colors:
for (let i = 0; i < WATERLEVELS.length; i++) {
    const key = '>'+WATERLEVELS[i];

    BIOME_SPRITES[key]['u'].color = '#22227f';
    BIOME_SPRITES[key]['c'].color = '#22227f';
    BIOME_SPRITES[key]['o'].color = '#22227f';

    // flowing water gradient
    for (const biomeKey of Object.keys(BIOMES)) {
        const biome = BIOMES[biomeKey];
        const oldColor = BIOME_SPRITES[key][biomeKey].color;
        const oldColorRGB = HEX2RGB(oldColor);
        const factor = i/32*0.75+0.25;
        // some mixture of black and water color
        const newColorRGB = MIX_COLORS_RGB(
            [0,0,0], oldColorRGB, factor);
        const newColor = RGB2HEX(newColorRGB);
        BIOME_SPRITES[key][biomeKey].color = newColor;
    }

}

// special overrides:
BIOME_SPRITES['*|']['y'].color = '#d0c8ce';
BIOME_SPRITES['w']['u'].color = '#22227f';
BIOME_SPRITES['w']['c'].color = '#22227f';
BIOME_SPRITES['w']['o'].color = '#22227f';
BIOME_SPRITES['WA']['C'].sway = true;
BIOME_SPRITES['WA']['W'].sway = true;
BIOME_SPRITES['-a']['o'].sway = true;
BIOME_SPRITES['-8']['o'].sway = true;
BIOME_SPRITES['-a']['o'].secondchar = {char: 'W', color: '#22227f', dx: 0, dy: 0, scale: 1};
BIOME_SPRITES['-8']['o'].secondchar = {char: 'W', color: '#22227f', dx: 0, dy: 0, scale: 1};
BIOME_SPRITES['B0']['u'].color = '#8c8c8c';
BIOME_SPRITES['B2']['u'].color = '#858585';
BIOME_SPRITES['B4']['u'].color = '#828282';

const yayd = '#eeeeee';

BIOME_SPRITES['d']['y'].color = yayd;
BIOME_SPRITES['d0']['y'].color = yayd;
BIOME_SPRITES['d1']['y'].color = yayd;
BIOME_SPRITES['d2']['y'].color = yayd;
BIOME_SPRITES['d3']['y'].color = yayd;
BIOME_SPRITES['d4']['y'].color = yayd;
BIOME_SPRITES['d5']['y'].color = yayd;
BIOME_SPRITES['d6']['y'].color = yayd;
BIOME_SPRITES['d7']['y'].color = yayd;

const yayB = '#b78eba';

BIOME_SPRITES['B']['y'].color = yayB;
BIOME_SPRITES['B0']['y'].color = yayB;
BIOME_SPRITES['B1']['y'].color = yayB;
BIOME_SPRITES['B2']['y'].color = yayB;
BIOME_SPRITES['B3']['y'].color = yayB;
BIOME_SPRITES['B4']['y'].color = yayB;
BIOME_SPRITES['B5']['y'].color = yayB;
BIOME_SPRITES['B6']['y'].color = yayB;
BIOME_SPRITES['B7']['y'].color = yayB;

BIOME_SPRITES['~']['R'].color = '#ffffff';
BIOME_SPRITES['w']['R'].rainbow = true;

const RAINBOW = [
    '#ff8000',
    '#ffb000',
    '#bfbf00',
    '#70bf00',
    '#00bfbf',
    '#00b0ff',
    '#0080ff',
    '#b000ff',
];

// water source rainbow color
for(let j = 0; j < 8; j++) {
    const key = 'w'+j;
    const rainbowColor = RAINBOW[j];
    const parentSprite = BIOME_SPRITES['w']['R'];
    const newSprite = {
        char: parentSprite.char,
        color: rainbowColor,
        sway: parentSprite.sway,
        misplace: parentSprite.misplace,
        blinkspeed: parentSprite.blinkspeed,
        dy: parentSprite.dy,
    };
    BIOME_SPRITES[key] = {'R': newSprite};
}

// flowing water rainbow colors:
for (let i = 0; i < WATERLEVELS.length; i++) {
    BIOME_SPRITES['>'+WATERLEVELS[i]]['R'].rainbow = true;
    const parentKey = '>'+WATERLEVELS[i];
    const parentSprite = BIOME_SPRITES[parentKey]['R'];
    for(let j = 0; j < 8; j++) {
        const key = parentKey+j;
        // flowing water gradient in rainbow color
        const rainbowColor = RAINBOW[j];
        const rainbowColorRGB = HEX2RGB(rainbowColor);
        const factor = i/32*0.75+0.25;
        // some mixture of black and rainbow color
        const newColorRGB = MIX_COLORS_RGB(
            [0,0,0], rainbowColorRGB, factor);
        const newColor = RGB2HEX(newColorRGB);
        const newSprite = {
            char: parentSprite.char,
            color: newColor,
            sway: parentSprite.sway,
            misplace: parentSprite.misplace,
            blinkspeed: parentSprite.blinkspeed,
            dy: parentSprite.dy,
        };
        BIOME_SPRITES[key] = {'R': newSprite};
    }
}





// make a version with all special effects deleted
const BIOME_SPRITES_NO_EFFECT = {};
for (const key of Object.keys(BIOME_SPRITES)) {
    const sprite = BIOME_SPRITES[key];
    const spriteNoEffect = {};
    for (const biomeKey of Object.keys(sprite)) {
        const biomeSprite = sprite[biomeKey];
        spriteNoEffect[biomeKey] = {
            char: biomeSprite.char,
            color: biomeSprite.color,
            secondchar: biomeSprite.secondchar,
        }
    }
    BIOME_SPRITES_NO_EFFECT[key] = spriteNoEffect;
}
