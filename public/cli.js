// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


// Wrapper for bigterminal.
// Adds a static and editable part to the dynamic line.
// Adds edit controls
// Adds command history
// Adds a mode with asterisks only (for passwords)

"use strict";

class Cli {
    constructor(bigterminal){
        this.bigterminal = bigterminal;
        this.static = '';
        this.hint = '';
        this.promptcolor = [];
        this.editable = '';
        this.enabled = false; // whether we are allowing the user to type and cursor blinking
        this.focused = true; // another condition for cursor blinking
        this.asterisks = false; // whether we are hiding the typed characters
        this.isCommand = false; // whether we are adding it to command history
        this.commandBackup = '';
        this.history = [];
        this.historyPos = 0;
        this.editPos = 0;
        this.cursorOn = false;
        this.pendingGetlineCallback = undefined;
        this.characterLimit = 0;
    }

    println(line, color = []){
        this.bigterminal.println(line, color);
    }

    clear() {
        this.bigterminal.clear();
    }

    setLineLimit(n) {
        this.bigterminal.setLineLimit(n);
    }

    getLines() {
        return this.bigterminal.lines;
    }

    focus(){
        this.focused = true;
        if (this.enabled) this.cursorOn = true;
        this.display(true);
    }

    blur(){
        this.focused = false;
        this.cursorOn = false;
        this.display(true);
    }

    clearCmd() {
        this.editable = '';
    }

    blink(){
        if (this.cursorOn){
            this.cursorOn = false;
            this.display(true); //silent
        }
        else{
            // only blink if enabled:
            if(this.enabled && this.focused){
                this.cursorOn = true;
                this.display(true); // silent
            }
        }
    }

    // prompt, aka enable
    async prompt(content, allowEmpty = false, characterLimit = 0, color = [], hint = ''){ // prompt the user to input something (and display static content)
        this.static = content;
        this.hint = hint;
        this.promptcolor = color;
        this.characterLimit = characterLimit;
        this.editable = '';
        this.enabled = true;
        this.isCommand = false;
        this.asterisks = false;
        this.editPos = 0;
        this.display();
        let result;
        if(allowEmpty){
            result = await this.getLine();
        }
        else{
            result = await this.getNonemptyLine();
        }
        this.characterLimit = 0;
        return result;
    }

    async promptCommand(content, silent, characterLimit = 0, color = [], hint = ''){ // gets added to command history
        this.static = content;
        this.hint = hint;
        this.promptcolor = color;
        this.characterLimit = characterLimit;
        this.editable = '';
        this.enabled = false; // command prompt does not automatically enable. TODO: there is probably a better solution for this rather than hardcoding it in here.
        this.isCommand = true;
        this.asterisks = false;
        this.editPos = 0;
        if(!silent) this.display();
        const result = await this.getLine(false);
        this.characterLimit = 0;
        return result;
    }

    async promptPassword(content, characterLimit = 0, color = [], hint = ''){ // not visible on screen
        this.static = content;
        this.hint = hint;
        this.promptcolor = color;
        this.characterLimit = characterLimit;
        this.editable = '';
        this.enabled = true;
        this.isCommand = false;
        this.asterisks = true;
        this.editPos = 0;
        this.display();
        const result = await this.getLine();
        this.characterLimit = 0;
        return result;
    }

    toAsterisks(line){
        let ast = '';
        for(let i = 0; i < line.length; i++){
            ast += '*';
        }
        return ast;
    }

    display(silent){
        let displayedEditable = this.asterisks ? this.toAsterisks(this.editable) : this.editable;

        if(this.cursorOn){
            const left = displayedEditable.slice(0, this.editPos);
            const right = displayedEditable.slice(this.editPos + 1, displayedEditable.length);
            displayedEditable = left + '█' + right;
        }
        else if(this.editPos === displayedEditable.length) {
            // cursor is not on
            // but if it was, it would be at the end.
            // in this case we add an underscore to prevent
            // a line wrapping blinking mess
            displayedEditable += ' ';
        }

        let color = this.promptcolor;
        if (this.editable === '' && this.hint.length > 0) {
            // add hint
            displayedEditable += '\\[#444\\]'+this.hint;
        }

        this.bigterminal.modify(this.static + displayedEditable, color, silent);
    }

    historyAppend(line) {
        // add to command history (if it makes sense)
        if (line === '') {
            return;
        }
        if (this.history.length === 0
            || line !== this.history[this.history.length - 1]) {
            // it's no dupe.
            // then it makes sense to add this to history.
            this.history.push(line);
        }
        // jump to the end of history
        this.historyPos = this.history.length;
    }

    // commit, aka apply and disable
    commit(){ // turn the currently typed text into a "command"
        const wasCommited = this.editable;
        if(this.asterisks){
            // hide them
            this.editable = '';
        }

        if(this.isCommand){
            this.historyAppend(this.editable);
        }
        else{
            // not a command. Let's display it on screen I guess
            this.cursorOn = false;
            this.display();
            this.bigterminal.commit();
        }

        // jump to the end of history
        this.historyPos = this.history.length;

        if(!(this.isCommand && this.editable === '')){
            this.static = '';
            this.editable = '';
            this.display();
        }
        this.enabled = false;
        this.isCommand = false;
        this.asterisks = false;
        return wasCommited;
    }

    handleKey(key){
        if(!this.enabled){
            return;
        }
        if(key.length !== 1){

            // It is not a simple key, such as 'A' or '~'

            switch(key){

                case 'Backspace':
                    this.backspace();
                    break;

                case 'Delete':
                    this.deletekey();
                    break;

                case 'PageUp':
                    this.bigterminal.scrollUp(this.bigterminal.terminal.height-2);
                    break;

                case 'PageDown':
                    this.bigterminal.scrollDown(this.bigterminal.terminal.height-2);
                    break;

                case 'Home':
                    this.home();
                    break;

                case 'End':
                    this.end();
                    // this.bigterminal.scrollToEnd();
                    break;

                case 'ArrowUp':
                    this.up();
                    break;

                case 'ArrowDown':
                    this.down();
                    break;

                case 'ArrowLeft':
                    this.left();
                    break;

                case 'ArrowRight':
                    this.right();
                    break;

                case 'Enter':
                    const result = this.commit();
                    if(this.pendingGetlineCallback){
                        this.pendingGetlineCallback(true, result);
                    }
                    return;
                default:
                    console.log('ignored key: ' + key);
            }
            // whatever it was, it is not going to be typed.
            return;
        }
        // now we'd type the key
        // BUT
        // maybe we're at the character limit?
        if (this.characterLimit > 0 && this.editPos >= this.characterLimit) {
            // uh oh
            return false;
        }
        // type this:
        const left = this.editable.slice(0, this.editPos);
        const right = this.editable.slice(this.editPos, this.editable.length);
        this.editable = left + key + right;
        this.editPos++;
        this.cursorOn = true;
        this.display();
        return false;
    }

    setEditable(text) {
        this.editable = text;
        this.editPos = text.length;
        this.display();
    }

    deletekey(){
        if(this.editPos < this.editable.length){
            const left = this.editable.slice(0, this.editPos);
            const right = this.editable.slice(this.editPos + 1, this.editable.length);
            this.editable = left + right;
            this.cursorOn = true;
            this.display();
        }
    }

    backspace(){
        if(this.editPos > 0){
            const left = this.editable.slice(0, this.editPos - 1);
            const right = this.editable.slice(this.editPos, this.editable.length);
            this.editable = left + right;
            this.editPos--;
            this.cursorOn = true;
            this.display();
        }
    }

    left(){
        if(this.editPos > 0){
            this.editPos--;
            this.cursorOn = true;
            this.display();
        }
    }

    right(){
        if(this.editPos < this.editable.length){
            this.editPos++;
            this.cursorOn = true;
            this.display();
        }
    }

    home(){
        this.editPos = 0;
        this.cursorOn = true;
        this.display();
    }

    end(){
        this.editPos = this.editable.length;
        this.cursorOn = true;
        this.display();
    }

    up(){
        if(!this.isCommand){
            // history makes no sense atm
            return;
        }
        if(this.historyPos === 0){
            // nothing is up
            return;
        }
        if(this.historyPos === this.history.length){
            // we are at the current command. Back it up.
            this.commandBackup = this.editable;
        }
        this.historyPos--;
        this.editable = this.history[this.historyPos];
        this.editPos = this.editable.length;
        this.display();
    }

    down(){
        if(!this.isCommand){
            // history makes no sense atm
            return;
        }
        if(this.historyPos === this.history.length){
            // nothing is down
            return;
        }
        this.historyPos++;
        if(this.historyPos === this.history.length){
            // this is the current command
            this.editable = this.commandBackup;
        }
        else{
            this.editable = this.history[this.historyPos];
        }
        this.editPos = this.editable.length;
        this.display();
    }

    getLine(){
        return new Promise((resolve, reject)=>{
            // prepare the callback:
            this.pendingGetlineCallback = (ok, line)=>{
                if(ok){
                    this.pendingGetlineCallback = undefined;
                    return resolve(line);
                }
                else{
                    this.pendingGetlineCallback = undefined;
                    return reject();
                }
            };
            // the above doesn't actually do anything yet.
            // Instead, it waits for other parts of the
            // code to run this.pendingGetlineCallback
        });
    }

    async getNonemptyLine(enable = true){
        var line;
        do{
            this.enabled = enable;
            line = await this.getLine();
        }while(line.length === 0);
        return line;
    }

    abort(){
        if(this.pendingGetlineCallback){
            this.pendingGetlineCallback(false, '');
            this.pendingGetlineCallback = undefined;
        }
        this.enabled = false;
    }

    unpause(){
        this.enabled = true;
    }

    pause(){
        this.enabled = false;
    }

    scrollToEnd() {
        this.bigterminal.scrollToEnd();
    }
}
