// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Main client script file.
 * I try to put code elsewhere.
 */

"use strict";

if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
}

// keycodes used in the default bindings:
const BLOCKED_KEYCODES = [8, 9, 32, 33, 34, 35, 36, 37, 38, 39, 40, 111, 222];
// specific keys that we want to prevent browser from interpreting:
const BLOCKED_KEYS = ['/',"'",'+','-'];
const CLI_SIZE = 0.3;
const CLI_MIN_SIZE = 18;
const MIN_CANVAS_WIDTH = 200;

let mycanvas;
let cliterminal;
let guiterminal;
let bigterminal;
let cli;
const keys = new Keys();
let login;
let socket;
let playerName;

const gamefont = '20px AagrinderFont, "DejaVu Sans Mono", "Lucida Console", Monospace';
const chatfont = '20px AagrinderChatFont, "DejaVu Sans Mono", "Lucida Console", Monospace';

document.addEventListener("DOMContentLoaded", function () {
    // socket = io.connect();
    // socket = io.connect('http://localhost:8080');
    // socket = io('https://aagrinder.xyz', {secure: true});
    socket = io(undefined, {secure: true});
    mycanvas = document.getElementById('terminal');
    cliterminal = new Terminal(mycanvas,chatfont,10,10,0);
    guiterminal = new Terminal(mycanvas,gamefont,10,10,10);
    bigterminal = new BigTerminal(cliterminal);
    cli = new Cli(bigterminal);
    login = new Login(cli, guiterminal, socket, keys);

    //Align everything for the first time:
    onResize(undefined);

    // bigterminal.println('connecting...', Array(13).fill('#555'));


    setInterval(()=>cli.blink(), 500);

    window.addEventListener('keydown', onKeydown, false);
    window.addEventListener('keyup', onKeyup, false);

    window.addEventListener('mousedown', onMouseupdown, false);
    window.addEventListener('mouseup', onMouseupdown, false);

    window.addEventListener('focus', event=>login.focus(), false);
    window.addEventListener('blur', event=>login.blur(), false);

    window.addEventListener('resize', onResize, false);
});

function onKeydown(e) {
    if (BLOCKED_KEYCODES.indexOf(e.keyCode) > -1
        || BLOCKED_KEYS.indexOf(e.key) > -1) {
        e.preventDefault(); //Prevent some of the browser's key bindings
    }
    cli.handleKey(e.key);
    keys.handleKeyDown(e.keyCode);
    login.handleKeyDown(e.keyCode);
}

function onKeyup(e) {
    if (BLOCKED_KEYCODES.indexOf(e.keyCode) > -1
        || BLOCKED_KEYS.indexOf(e.key) > -1) {
        e.preventDefault(); //Prevent some of the browser's key bindings
    }
    keys.handleKeyUp(e.keyCode);
    login.handleKeyUp(e.keyCode);
}

function onMouseupdown(e){
    switch(e.buttons){
        case 0:
            keys.handleMbUp(1);
            keys.handleMbUp(2);
            keys.handleMbUp(3);
            break;
        case 1:
            keys.handleMbDown(1);
            keys.handleMbUp(2);
            keys.handleMbUp(3);
            break;
        case 2:
            keys.handleMbUp(1);
            keys.handleMbDown(2);
            keys.handleMbUp(3);
            break;
        case 3:
            keys.handleMbDown(1);
            keys.handleMbDown(2);
            keys.handleMbUp(3);
            break;
        case 4:
            keys.handleMbUp(1);
            keys.handleMbUp(2);
            keys.handleMbDown(3);
            break;
        case 5:
            keys.handleMbDown(1);
            keys.handleMbUp(2);
            keys.handleMbDown(3);
            break;
        case 6:
            keys.handleMbUp(1);
            keys.handleMbDown(2);
            keys.handleMbDown(3);
            break;
        case 7:
            keys.handleMbDown(1);
            keys.handleMbDown(2);
            keys.handleMbDown(3);
            break;
    }
}

// Browser window resize
function onResize(e) {
    const w = window.innerWidth;
    const h = window.innerHeight;
    if(w > MIN_CANVAS_WIDTH){ // only apply resize if the canvas is reasonably big
        mycanvas.width = w;
        mycanvas.height = h;
        const charWidth = Math.floor((mycanvas.width - 2 * PADDING) / CHAR_WIDTH) - 1;
        let charWidthLeft = Math.floor(charWidth * CLI_SIZE);
        if (charWidthLeft < CLI_MIN_SIZE && charWidthLeft > CLI_MIN_SIZE/2) {
            charWidthLeft = CLI_MIN_SIZE;
        }
        const charWidthRight = charWidth - charWidthLeft;
        const charHeight = Math.floor((mycanvas.height - 2 * PADDING) / LINE_SPACING);
        cliterminal.resize(charWidthLeft - 2, charHeight, 0);
        guiterminal.resize(charWidthRight+2, charHeight+2, charWidthLeft, -1);
        bigterminal.reformat();
        bigterminal.scrollToEnd();
    }
}

function ajax(rawdata, url) {
    return new Promise((resolve, reject) => {
        let data = Object.getOwnPropertyNames(rawdata).map(
            a => a+'='+encodeURIComponent(rawdata[a])).join('&');

        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    resolve(JSON.parse(xhr.responseText));
                }
                else {
                    console.log('xmlhttprequest failed with status', xhr.status);
                    reject();
                }
            }
        };
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(data);
    });
}

