// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * The Commands class in this file contains logic
 * for all client-side commands.
 * If a command is not recognized as a client command,
 * it is sent to the server.
 */

"use strict";

// these commands are allowed on login screen
const LOGINSCREENCOMMANDS = [
    '/layout',
    '/map',
    '/unmap',
    '/unmapallkeys',
    '/set',
    '/resetallsettings',
    '/prompt',
    '/hello',
    '/help',
    '/h',
    '/?',
    '/ca',
    '/cl',
    '/clear',
    '/copylast',
    '/copyall',
    '/verify',
];

class Commands {

    // commands class gets reference to entire game object
    // and has access to everything
    constructor(game, cli, settings, layout, music) {
        this.game = game;
        this.cli = cli;
        this.settings = settings;
        this.layout = layout;
        this.music = music;
    }

    async executeCommand(typed) {

        // split by whitespace in the command
        const args = typed.split(/ +/);

        // is on login screen?
        if (this.game === null) {
            // yes, is on login screen.
            // check if command allowed on login screen
            if (!LOGINSCREENCOMMANDS.includes(args[0])) {
                // no, not allowed here
                this.cli.println('Unknown command. Try /help');
                return;
            }
        }

        switch(args[0]){

            case '/map':
                // needs 1 argument
                if (args.length < 2) {
                    // missing argument
                    this.cli.println('Usage: /map <function>\\[n\\]'
                        +'Available functions:\\[n\\]'+ACTIONS.join(', '));
                    break;
                }
                // check if valid action
                if (!ACTIONS.includes(args[1])) {
                    // invalid
                    this.cli.println('Available functions:\\[n\\]'
                        +ACTIONS.join(', '));
                    break;
                }
                // request key to be recorded:
                this.cli.println('\\[#fff\\]Press button for '+args[1]+' ...');
                const key = await this.game.recordKey();
                // prevent this key from being used elsewhere:
                this.game.keys.clearFresh();
                this.game.keys.handleKeyUp(key);
                // check if can map
                if (!this.layout.keyCanMap(key)) {
                    // no
                    this.cli.println('can\'t change this key');
                    break;
                }
                // save the mapping
                this.layout.mapKey(key, args[1]);
                this.cli.println('mapping '+key+'->'+args[1]
                    +' saved in localstorage');
                this.layout.saveLayout();
                break;

            case '/unmap':
                if (args.length !== 1) {
                    this.cli.println('/unmap does not take an argument!');
                    break;
                }
                // request key to be recorded:
                this.cli.println('\\[#fff\\]Press button to unmap ...');
                const unkey = await this.game.recordKey();
                // prevent this key from being used elsewhere:
                this.game.keys.clearFresh();
                this.game.keys.handleKeyUp(unkey);
                // check if can unmap
                if (!this.layout.keyCanMap(unkey)) {
                    // no
                    this.cli.println('can\'t unmap this key');
                    break;
                }
                // save the mapping
                this.layout.unmapKey(unkey);
                this.cli.println('unmapped key '+unkey+'.');
                this.layout.saveLayout();
                break;

            case '/unmapallkeys':
                this.layout.changeLayout('emptylayout');
                this.cli.println('all keys were unmapped!');
                this.layout.saveLayout();
                break;

            case '/layout':
                // needs 1 argument
                if (args.length < 2) {
                    // missing argument
                    this.cli.println('Usage: /layout <name>\\[n\\]'
                        +'Available layouts: '
                        +Object.keys(LAYOUTS).join(', '));
                    break;
                }
                // check if valid layout
                if (typeof(LAYOUTS[args[1]]) === 'undefined') {
                    // invalid
                    this.cli.println('Available layouts: '
                        +Object.keys(LAYOUTS).join(', '));
                    break;
                }
                // change the layout
                this.layout.changeLayout(args[1]);
                this.cli.println('layout '+args[1]+' saved in localstorage');
                this.layout.saveLayout();
                break;

            case '/hello':
                this.cli.println('Hi there, nice to meet you! ^~^');
                break;

            case '/pos':
                const coords =
                    'block: ' + this.game.player.x + ', ' + this.game.player.y
                    + '; chunk: ' + (this.game.player.x >> 8)
                    + ', ' + (this.game.player.y >> 8)
                    + '; biome: ' + BIOMES[this.game.map.biomeAt(
                        this.game.player.x, this.game.player.y)].name;
                this.cli.println(coords);
                break;

            case '/pop':
                this.game.playPop();
                break;

            case '/passwd': case '/password':
                const currentpassword = await this.cli.promptPassword('Current password: ');

                const checkresult = await ajax({
                    name: playerName,
                    password: currentpassword,
                },
                    '/api/users/checkpass',
                );
                if (!checkresult.success) {
                    this.cli.println(checkresult.message);
                    break;
                }

                const newpassword = await this.cli.promptPassword('New password: ');
                const retypenewpassword = await this.cli.promptPassword('Retype new password: ');
                if (newpassword !== retypenewpassword) {
                    this.cli.println('Sorry, passwords do not match.\\[n\\]Password unchanged.');
                    break;
                }

                const changeresult = await ajax({
                    name: playerName,
                    oldpassword: currentpassword,
                    newpassword: newpassword,
                },
                    '/api/users/changepass',
                );
                if (!changeresult.success) {
                    this.cli.println('\\[#733\\]'+changeresult.message);
                    break;
                }
                else {
                    this.cli.println(changeresult.message);
                }

                break;

            case '/prompt':
                const promptedcommand = prompt();
                if (promptedcommand === null) {
                    break;
                }
                await this.executeCommand(promptedcommand);
                this.cli.historyAppend(promptedcommand);
                break;

            case '/set':
                if (args.length === 1) {
                    this.settings.printhelp(this.cli);
                    break;
                }
                const setresult = this.settings.set(args);
                if (!setresult) {
                    this.cli.println('Invalid setting. Run /set');
                    break;
                }
                // if it was just a query
                if (setresult.q) {
                    // only display the result of the query
                    this.cli.println(setresult.name + ' = ' + setresult.value);
                    break;
                }
                this.settings.saveSettings();
                // TODO: if this.game === null,
                // filter out settings which have
                // no immediate effect on login screen
                switch (setresult.name) {
                    case 'music':
                        if (setresult.eq) {
                            // set volume
                            this.music.resetVolume();
                            this.cli.println('Volume set.')
                        }
                        else if(setresult.value) {
                            if (this.game === null) {
                                // login screen
                                this.music.loginMusic()
                            }
                            else {
                                this.music.startMusic();
                            }
                        }
                        else {
                            this.music.stopMusic();
                            this.cli.println('Music disabled.')
                        }
                        break;
                    case 'listmusic':
                        if (setresult.value) {
                            this.cli.println('Music will be listed.');
                        } else {
                            this.cli.println('Music will not be listed.');
                        }
                        break;
                    case 'maxchatlines':
                        this.cli.setLineLimit(setresult.value);
                        break;
                    case 'gap':
                        this.cli.bigterminal.reformat();
                        break;
                    case 'twirt':
                        if (setresult.value) {
                            this.cli.println('Cricket noises are back.');
                        } else {
                            this.cli.println('Crickets are gone.');
                        }
                        break;
                    case '???':
                        if (setresult.value) {
                            this.cli.println('???');
                        } else {
                            this.cli.println('...');
                        }
                        break;

                }
                break;

            case '/resetallsettings':
                this.settings.setAllDefaultSettings();
                this.settings.clearLocalStorage();
                break;

            case '/credits':
                this.game.creditsIndex = 0;
                this.game.creditsIndexMod = 0;
                break;

            case '/bye': case '/exit': case '/quit': case '/leave':
                this.game.socket.disconnect();
                this.game.socket.connect();
                break;

            case '/clear':
                this.cli.clear();
                break;

            case '/copylast': case '/cl': {
                if (typeof navigator.clipboard === 'undefined') {
                    this.cli.println('\\[#733\\]Clipboard unavailable!');
                    break;
                }
                const lines = this.cli.getLines();
                const content = AASTRING_DECODE(lines[lines.length-1])
                    .map(l=>l.raw).join('\n');
                navigator.clipboard.writeText(content)
                .then(() => {
                    this.cli.println('\\[#373\\]Copied to clipboard!');
                }, () => {
                    this.cli.println('\\[#733\\]Failed to copy!');
                });
                break;
            }

            case '/copyall': case '/ca':{
                if (typeof navigator.clipboard === 'undefined') {
                    this.cli.println('\\[#733\\]Clipboard unavailable!');
                    break;
                }
                const lines = this.cli.getLines();
                const content = lines.map(li=>
                    AASTRING_DECODE(li).map(l=>l.raw).join('\n')
                ).join('\n')
                navigator.clipboard.writeText(content)
                .then(() => {
                    this.cli.println('\\[#373\\]Copied to clipboard!');
                }, () => {
                    this.cli.println('\\[#733\\]Failed to copy!');
                });
                break;
            }

            case '/verify': {
                const code = 169-20*7+'KcIJV'+(11+83)+'dWqW4g';
                this.cli.println('Your verification code is:\\[n\\]\\[#ff0\\]'+code);
                if (typeof navigator.clipboard !== 'undefined') {
                    navigator.clipboard.writeText(code)
                    .then(() => {
                        this.cli.println('\\[#373\\]Copied to clipboard!');
                    });
                }
                break;
            }

            case '/log': {
                if (!IS_DEBUGLOG_CLIENT) {
                    this.cli.println('Not supported. Try debug.html');
                    break;
                }
                const log = this.game.syncher.debugLog.join('\n');
                console.log(log);
                this.cli.println('debug log was printed to console');
                break;
            }

            case '/help': case '/h': case '/?':
                // different depending on whether logged in
                if (this.game !== null) {
                    this.cli.println(
                        'press H for a list of buttons'
                    );
                    // also send to server and get list of server commands
                    this.game.socket.emit('chat', {message: typed});
                }
                else {
                    this.cli.println('available commands: '
                        +LOGINSCREENCOMMANDS.sort().join(' '));
                }
                break;

            default:
                // no known client command, but might be a server command.
                // commands are sent over chat endpoint
                this.game.socket.emit('chat', {message: typed});
        }

    }

}
