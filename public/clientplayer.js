// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A class for holding data about the player who is logged in,
 * along with a few client-side actions the user can trigger.
 */

"use strict";

const CURSOR_TOLERANCE_BEYOND_REACH_X = 6;
const CURSOR_TOLERANCE_BEYOND_REACH_Y = 4;
const CURSOR_TOLERANCE_BEYOND_INVENTORY_X = 6;
const PLAYER_MAXFOOD = 1000;

class Player {
    constructor(color, settings){
        this.settings = settings;
        this.mousex = 0;
        this.mousey = 0;
        this.x = 0;
        this.y = 0;
        this.cursorx = 0;
        this.cursory = 0;
        this.reach = 0;
        this.jump = 0;
        this.food = 0;
        this.maxjump = 0;
        this.gamemode = 0;
        this.digx = 0;
        this.digy = 0;
        this.digtime = 0;
        this.digduration = 0;
        this.name = '';
        this.color = color;
        this.inventory = new Inventory({}, 35);
        this.chest = {x:0, y:0, inventory:new Inventory()};
        this.positionList = {};
        this.animation = false;
        this.blockanimations = [];
        this.delanis = [];  // new item deletion animations
        this.friends = {friend:[], pending:[], requests:[]};
        this.TOTALWARPCOOLDOWN = 100;
        this.warpCooldown = 0;
        this.warpWarmup = 0;
        this.detectorBlinkSpeed = 0;
        this.followBlinkSpeed = 0;
        this.visibleDetectors = [];
        this.visibleGhosts = [];
    }

    cursorUp(){
        if(this.cursory < this.reach){
            this.cursory++;
        }
    }

    cursorDown(){
        if(this.cursory > -this.reach){
            this.cursory--;
        }
    }

    cursorRight(){
        if(this.cursorx < this.reach){
            this.cursorx++;
        }
    }

    cursorLeft(){
        if(this.cursorx > -this.reach){
            this.cursorx--;
        }
    }

    cursorSet(x, y){

        const prevx = this.cursorx;
        const prevy = this.cursory;

        // fix cursor being slightly beyond reach:

        if(x <= this.reach+CURSOR_TOLERANCE_BEYOND_REACH_X && x > this.reach){
            x = this.reach;
        }

        if(x >= -this.reach-CURSOR_TOLERANCE_BEYOND_REACH_X && x < -this.reach){
            x = -this.reach;
        }

        if(y <= this.reach+CURSOR_TOLERANCE_BEYOND_REACH_Y && y > this.reach){
            y = this.reach;
        }

        if(y >= -this.reach-CURSOR_TOLERANCE_BEYOND_REACH_Y && y < -this.reach){
            y = -this.reach;
        }

        if(x <= this.reach && x >= -this.reach && y <= this.reach && y >= -this.reach){
            this.cursorx = x;
            this.cursory = y;
        }

        return prevx !== this.cursorx || prevy !== this.cursory;

    }

}
