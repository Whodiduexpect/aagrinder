// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */



/*
 * Synching client with server.
 * All state-changing things must go through here, except client-only state.
 * Syncher sends local events to the server, together with an id.
 * Receives from the server events caused by other players and the
 * server itself.
 * 
 * Syncher keeps local events in a stack (client events have known IDs).
 * When server events are received, it might belong somewhere in the middle
 * of the stack rather than the end, due to delays in network communication.
 * 
 * Conveniently, the server always specifies the ID of the last client-side
 * event that happened before this server event.
 * The client-side Syncher corrects order of events like this:
 * > search the event stack for the event id specified by the server
 * > undo actions from the event stack up to that point
 * > apply server event
 * > redo the actions that have been undone,
 *   while taking in account the changes caused by the server event
 * 
 * Local events can become invalid when attempting to reapply them.
 * However, we keep them in the stack even if they don't do anything.
 * Because these events (that were already transmitted to the server)
 * may not be discarded until the server confirms their existence
 * by specifying some later event ID.
 * 
 * If a re-applied event really has no effect, the server will simply
 * ignore it but take note of the event ID anyway.
 */

"use strict";

class Syncher {
    constructor(map, player, invComponent, chestComponent, hotbarInvComponent, socket, cli){
        this.map = map;
        this.player = player;
        this.invComponent = invComponent;
        this.chestComponent = chestComponent;
        this.hotbarInvComponent = hotbarInvComponent;
        this.eventStack = []; // a list of events which may need to get undone
        this.branch = 0;
        this.socket = socket;
        this.cli = cli;
        this.nextTickId = 0;
        this.tickLogic = sharedTickLogic;
        this.actionFunctions = sharedActionFunctions;
        this.debugLog = [];
        this.blockChanges = undefined;
        this.createdTileEntities = [];
        this.deletedTileEntities = [];
    }

    createView(silent=false){
        return new View(this, this.player, this.invComponent, this.chestComponent, silent);
    }

    clientAction(actionName, data, silent=false){
        if(!this.actionFunctions[actionName]) return false;
        const view = this.createView(silent)
        const success = this.actionFunctions[actionName](view, data) !== false;
        return success;
    }

    // similar to executeRawAction, but inverse
    undoRawAction(changes, eventId) {
        if (IS_DEBUGLOG_CLIENT) {
            this.debugLog.push(this.getDebugState());
            this.debugLog.push('['+eventId+' UNDO] '+JSON.stringify(changes));
        }
        // undo every block change
        for(let i = changes.b.length-1; i >= 0; i--){
            const blockChange = changes.b[i];
            this.map.setBlock(blockChange.x, blockChange.y, blockChange.p);
        }
        for (const deletedEntity of changes.de) {
            this.map.createTileEntity(deletedEntity);
        }
        for (const createdEntity of changes.ce) {
            this.map.deleteTileEntityAt(createdEntity.x, createdEntity.y);
        }
        // undo movement and basic stuff
        this.player.x = changes.px;
        this.player.y = changes.py;
        this.player.jump = changes.pj;
        this.player.food = changes.pf;
        this.player.digx = changes.pdx;
        this.player.digy = changes.pdy;
        this.player.digtime = changes.pdt;
        this.player.digduration = changes.pdd;
        // undo every inventory change
        // this is a simple backup
        // (it used to be relative and more complicated)
        this.player.inventory.state = JSON.parse(changes.invbackup);
        this.invComponent.updateSlots();
        this.chestComponent.updateSlots();
    }

    checkIfTickMadeAnyChages(savedState) {
        if (savedState.b.length > 0) return true;
        if (savedState.ce.length > 0) return true;
        if (savedState.de.length > 0) return true;
        if (savedState.px !== this.player.x) return true;
        if (savedState.py !== this.player.y) return true;
        if (savedState.pj !== this.player.jump) return true;
        if (savedState.pf !== this.player.food) return true;
        if (savedState.pdx !== this.player.digx) return true;
        if (savedState.pdy !== this.player.digy) return true;
        if (savedState.pdt !== this.player.digtime) return true;
        if (savedState.pdd !== this.player.digduration) return true;
        if (savedState.invbackup
            !== JSON.stringify(this.player.inventory.state)) return true;
        return false;
    }

    getDebugState() {
        // get the current player state as a string
        // (for debug log)
        return '  state: '+JSON.stringify({
            px: this.player.x,
            py: this.player.y,
            pj: this.player.jump,
            pf: this.player.food,
            pdx: this.player.digx,
            pdy: this.player.digy,
            pdt: this.player.digtime,
            pdd: this.player.digduration,
            inv: this.player.inventory.state
        });
    }

    executeTick(tick, redo=false, existingTickId) {

        // remember state before tick
        const savedState = {
            px: this.player.x, // previous player position
            py: this.player.y, // previous player position
            pj: this.player.jump, // previous player position
            pf: this.player.food, // previous player hunger
            pdx: this.player.digx, // previous block being dug
            pdy: this.player.digy, // previous block being dug
            pdt: this.player.digtime, // time spent digging block
            pdd: this.player.digduration, // total time needed to dig
            invbackup: JSON.stringify(this.player.inventory.state),
        };

        const debugState = this.getDebugState();

        // start keeping track of blocks and entities changed in this tick
        this.blockChanges = [];
        this.createdTileEntities = [];
        this.deletedTileEntities = [];

        // this is an ugly workaround for preventing tick modifications.
        // Normally, tick is modified by ticklogic, filtering unneeded fields
        // but for redo, we don't want this. Instead, we want to keep
        // the tick like it was when it was sent to the server.
        let tickBackup;
        if (redo) {
            tickBackup = JSON.stringify(tick);
        }

        // execute the tick by following tick logic
        // (block changes are recorded by the View)
        this.tickLogic(tick, (actionName, data) =>
            this.clientAction(actionName, data, redo));

        // now the terrain changes are known and can be saved
        savedState.b = this.blockChanges;
        savedState.ce = this.createdTileEntities;
        savedState.de = this.deletedTileEntities;

        let tickId;
        if (redo) {
            tickId = existingTickId;
            tick = JSON.parse(tickBackup);
        }
        else {
            // is an original action
            tickId = this.nextTickId;
            this.nextTickId++;
            this.nextTickId %= 100000;
        }

        // ignore tick if it is empty, original, and had no effect.
        // however, if it was a redo tick and had no effect,
        // it still needs to be put in the event stack,
        // because you can't just leave stuff out while
        // time traveling. There is a chance that the next
        // time we undo and redo this same tick, it will
        // suddenly become valid again. So we need to keep
        // it around.
        // (because, of course, it was already sent to server earlier)
        if (!redo && Object.keys(tick).length === 0
            && !this.checkIfTickMadeAnyChages(savedState)) {
            // ignore this tick
        }
        else {
            // save event to event stack
            this.eventStack.push({
                id: tickId,
                tick: tick,
                undo: savedState,
            });
            if (IS_DEBUGLOG_CLIENT) {
                this.debugLog.push(debugState);
                this.debugLog.push(
                    '['+tickId+(redo?' REDO':'')+'] '+JSON.stringify(tick));
            }
            // console.log("local event", JSON.stringify(tick));
            // if it's original, also send to server.
            // But if it is a redo, we are obviously
            // not sending it to server again.
            if (!redo) {
                // console.log('was sent to server');
                const emittedTick = {
                    t: tick,
                    i: tickId,
                };
                this.socket.emit('t', emittedTick);
            }
        }
    }

    serverAction(data){

        // console.log("server event", data);

        // apply block updates
        if(data.b){
            for(const y of Object.getOwnPropertyNames(data.b)){
                for(const x of Object.getOwnPropertyNames(data.b[y])){
                    this.map.setBlock(x, y, data.b[y][x]);
                }
            }
        }

        // apply tile entity updates
        // first delete, then create
        // (modification is implemented as a combination
        // of deletion and creation)
        if(data.de){
            for(const pos of data.de){
                this.map.deleteTileEntityAt(pos.x, pos.y);
            }
        }
        if(data.ce){
            for(const tileEntity of data.ce){
                this.map.createTileEntity(tileEntity);
            }
        }

        // apply chunk updates
        if(data.c){
            for(let i = 0; i < data.c.length; i++){
                const err = this.map.loadChunk(
                    data.c[i].x,
                    data.c[i].y,
                    data.c[i].t,
                    data.c[i].b,
                    data.c[i].e,
                    data.c[i].c,
                );
                if (err !== 0) {
                    this.cli.println('\\[#f00\\]Corrupted world data. Proceed at your own risk.');
                    console.warn('Corrupted ('+err+') terrain string at chunk '+data.c[i].x+', '+data.c[i].y);
                }
            }
        }

        if (data.u) {
            for (const {x, y} of data.u) {
                this.map.unloadChunk(x, y);
            }
        }

        // animation?
        if ("animation" in data) {
            // start animation
            this.player.prevx = this.player.x;
            this.player.prevy = this.player.y;
            this.player.animation = data.animation;
        }

        // get block animations
        if ("ba" in data) {
            this.player.blockanimations = this.player.blockanimations.concat(data.ba);
        }

        // deletion animations
        if ("delanis" in data) {
            this.player.delanis = data.delanis;
        }

        // apply player movement
        if("px" in data){
            this.player.x = data.px;
        }
        if("py" in data){
            this.player.y = data.py;
        }

        // apply player hunger
        if("food" in data){
            this.player.food = data.food;
        }

        // validate player position
        if("vx" in data && this.player.x !== data.vx
            || "vy" in data && this.player.y !== data.vy
        ){
            this.cli.println('\\[#f00\\]Corrupted session. Please reload.');
            const err = 'Player position ('+this.player.x+','+this.player.y+') does not match validation ('+data.vx+','+data.vy+')';
            if (IS_DEBUGLOG_CLIENT) {
                this.debugLog.push(this.getDebugState());
                this.debugLog.push('[ERROR] '+err);
            }
            console.log(err);
        }

        // validate player hunger
        if ("vf" in data && this.player.food !== data.vf) {
            this.cli.println('\\[#f00\\]Corrupted session. Please reload.');
            const err = 'Player hunger ('+this.player.food+') does not match validation ('+data.vf+')';
            if (IS_DEBUGLOG_CLIENT) {
                this.debugLog.push(this.getDebugState());
                this.debugLog.push('[ERROR] '+err);
            }
            console.log(err);
        }

        // validate player inventory
        if("vinv" in data
            && JSON.stringify(this.player.inventory.state)
                !== JSON.stringify(data.vinv)
        ){
            this.cli.println('\\[#f00\\]Corrupted session. Please reload.');
            const err = 'Player inventory ('+JSON.stringify(this.player.inventory.state)+') does not match validation ('+JSON.stringify(data.vinv)+')';
            if (IS_DEBUGLOG_CLIENT) {
                this.debugLog.push(this.getDebugState());
                this.debugLog.push('[ERROR] '+err);
            }
            console.log(err);
        }

        // apply player reach
        if("reach" in data){
            this.player.reach = data.reach;
        }

        // apply player maxjump
        if("maxjump" in data){
            this.player.maxjump = data.maxjump;
            this.player.jump = data.maxjump;
        }

        // apply player gamemode
        if("gamemode" in data){
            this.player.gamemode = data.gamemode;
        }

        // apply player name
        if ("playername" in data) {
            this.player.name = data.playername;
        }

        // apply player color
        if("color" in data){
            this.player.color = data.color;
            this.cli.println('\\[#'+this.player.color+'\\]P = '+YOU_STRING);
        }

        if("entertochat" in data){
            this.cli.println('[H for help]\\[n\\][enter to chat]');
        }
        else if("gamemode" in data){
            // only in this case print an announcement for gamemode
            // because this means it was not the very first update
            this.cli.println('gamemode updated');
        }

        // apply inventory
        if("inv" in data){
            this.player.inventory.state = data.inv;
            this.invComponent.updateSlots();
            this.chestComponent.updateSlots();
        }

        // apply other player positions
        if ("p" in data) {
            for (const otherPlayer of data.p) {
                if ("x" in otherPlayer) {
                    // moving
                    this.player.positionList[otherPlayer.n] = {
                        x: otherPlayer.x,
                        y: otherPlayer.y,
                        c: otherPlayer.c,
                    };
                }
                else {
                    // removing
                    delete this.player.positionList[otherPlayer.n];
                }
            }
        }

        // update visible detectors
        if ("detectors" in data) {
            this.player.visibleDetectors = data.detectors;
        }

        // update ghosts
        if ("g" in data) {
            this.player.visibleGhosts = data.g;
        }

        // inventory detector blink
        if ("dbs" in data) {
            this.player.detectorBlinkSpeed = data.dbs;
        }

        // inventory follow blink
        if ("fbs" in data) {
            this.player.followBlinkSpeed = data.fbs;
        }

        // secret chest data
        if ("ch" in data) {
            if (data.ch === false) {
                // chest is being deleted
                this.player.chestOpen = false;
            }
            else {
                this.player.chest.x = data.ch.x;
                this.player.chest.y = data.ch.y;
                this.player.chest.inventory.loadFromJSON(data.ch.d);
                if (data.ch.T === true) {
                    // freshly opened chest
                    this.invComponent.show();
                    this.invComponent.focus();
                    this.chestComponent.show();
                    this.chestComponent.updateSlots();
                    this.chestComponent.cursorSet(0);
                    this.chestComponent.blur();
                }
                this.chestComponent.updateSlots();
            }
        }

        if ('an' in data) {
            this.cli.println(data.an);
        }

        // friends update (if applicable)
        if("friends" in data){
            this.player.friends = data.friends;
            if (this.player.friends.requests.length > 0) {
                this.cli.println('\\[#373\\]You have friend requests! Press Tab');
            }
        }

        if ('hotbar' in data) {
            this.hotbarInvComponent.slots = data.hotbar;
        }

    }

    serverEvent(event){
        if (IS_DEBUGLOG_CLIENT) {
            this.debugLog.push(' ... '+event.l+' received here');
        }
        const undoneTicks = this.rollback(event.l);
        if (IS_DEBUGLOG_CLIENT) {
            // Log server event but filter out giant chunk updates
            this.debugLog.push(this.getDebugState());
            this.debugLog.push('server['+event.l+'] '+JSON.stringify(event,
                (key, val) => (typeof val === 'string' && val.length > 1000)
                ? '<data>' : val));
        }
        this.serverAction(event);
        this.eventStack = [];
        this.fastforward(undoneTicks);
    }

    rollback(index){
        // console.log('rollback to',index)
        const undoneTicks = [];
        let found = false;
        for(let i = this.eventStack.length-1; i >= 0; i--){
            const event = this.eventStack[i];
            // console.log('ch ', event.i)
            // console.log(event.i, '===', index, '?');
            if(event.id === index){
                // stop undoing because specified index was found
                found = true;
                break;
            }
            // console.log('undo');
            // put it in the list so it's possible to redo it later
            undoneTicks.push({tick:event.tick, id:event.id});
            // with the modified implementation,
            // we keep track of changes for the whole tick,
            // so we only need to undoRawAction once
            this.undoRawAction(event.undo, event.id);
        }
        return undoneTicks.reverse();
    }

    fastforward(undoneTicks){
        for(const tick of undoneTicks){
            // console.log('redo', tick);
            const redoSuccess = this.executeTick(tick.tick, true, tick.id);
        }
    }
}


// View is a layer of abstraction to allow the same
// game logic code to be used both on server and client.
//
// This is the client-side View implementation.
// It keeps track of block changes made by tick actions.
// The remaining changes (player position, inventory)
// are handled by the Syncher directly, by taking a snapshot.
//
// These changes need to be recorded so they can be undone later
// when we want to roll back time to synchronize with the server.
//
// Note that the server has no need for such recording
// because actions are never undone on the server.
class View {

    constructor(syncher, player, invComponent, chestComponent, silent=false){
        this.syncher = syncher;
        this.player = player;
        this.invComponent = invComponent;
        this.chestComponent = chestComponent;
        this.isServer = false;
        this.silent = silent;  // enable or disable visual feedback
    }

    setBlock(x, y, b, blockUpdate=false){
        let prev = this.syncher.map.getBlock(x, y);
        // record this event so it can be undone later:
        this.syncher.blockChanges.push({x:x,y:y,p:prev});
        this.syncher.map.setBlock(x, y, b);
        // (even if blockUpdate is true, it doesn't matter,
        // because block update doesn't do anything on client)
    }

    addAnimation(x, y, animationduration){
        // animation disabled?
        if (this.silent) return;
        // animations are stored in player
        // which is probably not the "best place"
        // but I think it's fine
        this.player.blockanimations.push(
            {x:x,y:y,b:this.getBlock(x,y),a:animationduration});
    }

    getBlock(x, y){
        return this.syncher.map.getBlock(x,y);
    }

    isPlayer(x, y){
        if (this.player.x === x && this.player.y === y) return true;
        for (const key of Object.keys(this.player.positionList)) {
            const p = this.player.positionList[key];
            if (p.x === x && p.y === y) {
                return true;
            }
        }
        return false;
    }

    playerPosChanged() {
        // nothing special to do on the client
    }

    getClaim(x, y){
        return this.syncher.map.getClaim(x, y);
    }

    checkIfFriend(otherAccountName) {
        for (const friend of this.player.friends.friends) {
            if (friend.name === otherAccountName) return true;
        }
        return false;
    }

    gainItem(item, count=1, data_or_index){
        this.player.inventory.gainItem(item, count, data_or_index);
        this.invComponent.updateSlots();
    }

    refreshInventorySlots() {
        this.invComponent.updateSlots();
    }

    enqueueWire(x, y) {
        // wires do nothing on the client
    }

    prepareWaterUpdate(x, y) {
        // no water updates on the client
    }

    getTileEntityAt(x, y) {
        return this.syncher.map.getTileEntityAt(x, y);
    }

    createTileEntity(data) {
        // this should never be called on the client anyway.
        console.log('BUG!');
    }

    playerCreateTileEntity(data) {
        this.syncher.createdTileEntities.push({x:data.x, y:data.y});
        this.syncher.map.createTileEntity(data);
    }

    deleteTileEntityAt(x, y) {
        // this should never be called on the client anyway.
        console.log('BUG!');
    }

    playerDeleteTileEntityAt(x, y) {
        const tiles = this.getTileEntityAt(x, y);
        if (tiles.length === 1) {
            this.syncher.deletedTileEntities.push(tiles[0]);
        } else {
            console.log('Unable to record deleted entity in event stack: '
                + 'incorrect number of entities: ' + tiles.length);
        }
        this.syncher.map.deleteTileEntityAt(x, y);
    }

    attemptCreateCustomWarp(x, y, code, scope) {
        // this should never be called on the client anyway.
        console.log('BUG!');
    }

    notifyPrint(message) { // TODO: everything that uses this should use aastring format
        this.syncher.cli.println(message);
    }

}
