// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * An extended version of InventoryComponent which
 * additionaly has a list of craftable items that
 * automatically updates based on recipes from Inventory
 */

"use strict";

class CraftComponent extends InventoryComponent {
    constructor(inventory, playerInventory, name, xpos=0, totalHeight){
        super(inventory, name, xpos);
        this.playerInventory = playerInventory;  // readonly!
        this.craftableRecipes = [];
        this.totalHeight = totalHeight;
        this.recipeSlots = totalHeight - 1 - inventory.total_slots;
        this.isRecipeCursor = false;
        this.recipeCursor = 0;
        this.isRecipeHover = false;
        this.recipeHover = 0;
    }

    updateSlots() {
        super.updateSlots();
        this.craftableRecipes = [];
        for (let r = 0; r < Inventory.recipes.length; r++) {
            if (this.inventory.couldCraft(r)) {
                this.craftableRecipes.push(r);
            }
        }
        if (this.isRecipeHover) {
            if (this.craftableRecipes.length === 0) {
                this.isRecipeHover = false;
            } else if (this.recipeHover >= this.craftableRecipes.length) {
                this.isRecipeHover = false;
            }
        }
        if (this.isRecipeCursor) {
            if (this.craftableRecipes.length === 0) {
                this.isRecipeCursor = false;
            } else if (this.recipeCursor >= this.craftableRecipes.length) {
                this.recipeCursor = this.craftableRecipes.length-1;
            }
        }
    }

    getVisualHeight() {
        return 5;
    }

    curRecipe() {
        if (this.isRecipeHover) return this.craftableRecipes[this.recipeHover];
        return this.craftableRecipes[this.recipeCursor];
    }

    handleClick(x, y) {
        this.isRecipeHover = false;
        // try to click inventory normally
        if (super.handleClick(x, y)) {
            // ok click was in normal inventory
            this.isRecipeCursor = false;
            return true;
        }
        // otherwise try clicking on recipes
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        if (local.x > 0 && local.x < 1+lineWidth
            && local.y > this.inventory.total_slots + 1
            && local.y < this.inventory.total_slots + 2 + this.craftableRecipes.length) {
            this.isRecipeCursor = true;
            this.recipeCursor = local.y-2-this.inventory.total_slots;
            return true;
        }
        return false;
    }

    cursorUp() {
        this.isRecipeHover = false;
        if (this.isRecipeCursor) {
            this.recipeCursor--;
            if (this.recipeCursor < 0) {
                this.isRecipeCursor = false;
                this.cursor = this.slots.length-1;
            }
        } else if (this.cursor === 0
                && this.craftableRecipes.length > 0) {
            // jump from first inventory pos to last recipe
            this.isRecipeCursor = true;
            this.recipeCursor = this.craftableRecipes.length-1;
        } else {
            super.cursorUp();
        }
    }

    cursorDown() {
        this.isRecipeHover = false;
        if (this.isRecipeCursor) {
            this.recipeCursor++;
            if (this.recipeCursor >= this.craftableRecipes.length) {
                this.isRecipeCursor = false;
                this.cursor = 0;
            }
        } else if (this.cursor === this.slots.length-1
                && this.craftableRecipes.length > 0) {
            // jump from last inventory pos to first recipe
            this.isRecipeCursor = true;
            this.recipeCursor = 0;
        } else {
            super.cursorDown();
        }
    }

    blur() {
        super.blur();
        // unfocus recipe list when unfocusing crafting window
        this.isRecipeHover = false;
        this.isRecipeCursor = false;
        this.recipeCursor = 0;
    }

    hide() {
        super.hide();
        // clear crafting window when closing crafting window
        this.inventory.state = {};
        this.updateSlots();
        this.cursor = 0;
    }

    getFrame() {
        const frame_extra = super.getFrame(false);
        const frame = frame_extra.f;
        const extraSprites = frame_extra.e;
        // last row is no longer last row
        frame.pop()
        frame.push(this.compact ? '├─ ↓ ─┤' : '├────── ↓↓ ──────┤');
        // add recipe list
        for (let i = 0; i < this.recipeSlots; i++) {
            if (i >= this.craftableRecipes.length) {
                // no more items to show
                // only display "no recipes" if there's items in the menu
                if (this.slots.length > 0 && i === 0) {
                    // actually no items at all
                    if (this.compact) {
                        frame.push('│     │');
                        frame.push('│ no  │');
                        frame.push('│recip│');
                        i+=2;
                    } else {
                        frame.push('│                │');
                        frame.push('│   no recipes   │');
                        i+=1;
                    }
                }
                else {
                    if (this.compact) {
                        frame.push('│     │');
                    } else {
                        frame.push('│                │');
                    }
                }
            }
            else {
                // there is a recipe to show
                const recipe = Inventory.recipes[this.craftableRecipes[i]];
                const item_code = recipe.get.item;
                const count = recipe.get.amount;
                const item_name = Inventory.item_short_names[item_code];

                const line = '│  ' +
                    (this.compact ? '' : (item_name.toString()+'               ')
                        .substring(0, 11)
                    )+
                    (count.toString()+'   ').substring(0, 3) + '│';
                frame.push(line);
                extraSprites.push({
                    dx: 1,
                    dy: i+1+this.inventory.total_slots+1,
                    sprite: BIOME_SPRITES_NO_EFFECT[Inventory.item2blockPlain(item_code)]['A'],
                });
            }
        }
        frame.push(this.compact ? '└─────┘' : '└────────────────┘');
        return {f: frame, e: extraSprites};
    }

    nthSlotNumber(n) {
        if (!this.isRecipeHover && !this.isRecipeCursor) return '';
        const recipe = Inventory.recipes[this.curRecipe()];
        const item_code = this.slots[n].i;
        for (const need of recipe.need) {
            if (need.item === item_code) {
                if (this.playerInventory.canSpend(need.item, need.amount)) {
                    return need.amount.toString();
                } else {
                    return '\\[#733\\]'+need.amount.toString()+'\\[/\\]';
                }
            }
        }
        return '';
    }

    getCursorScreenPos() {
        if (this.isRecipeCursor) {
            return this.localToScreen(1,
                1 + this.inventory.total_slots + 1 + this.recipeCursor);
        }
        else return super.getCursorScreenPos();
    }

    getHoverScreenPos(x, y) {
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        if (local.x >= 0 && local.x < 2+lineWidth
            && local.y >= 6 && local.y < this.craftableRecipes.length+6) {
            return this.localToScreen(1, 1 + local.y);
        }
        return super.getHoverScreenPos(x, y);
    }

    previewRecipeByMouseHover(x, y) {
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        if (local.x >= 0 && local.x < 2+lineWidth
            && local.y >= 6 && local.y < this.craftableRecipes.length+6) {
            this.isRecipeHover = true;
            this.recipeHover = local.y-6;
            return;
        }
        this.isRecipeHover = false;
    }

}
