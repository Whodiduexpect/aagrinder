// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Where all the in-game components fit together.
 * The in-game components include the world, the player,
 * the controls and the chatbox among other things.
 * The login screen does not belong here.
 * The Game object is created on login.
 */

"use strict";

class Game{
    constructor(
        cli,
        guiterminal,
        socket,
        keys,
        settings,
        layout,
        music,
        commands
    ){
        this.socket = socket;
        this.cli = cli;
        this.settings = SETTINGS;
        this.layout = layout;
        this.music = music;
        this.player = new Player(undefined, this.settings);

        this.map = new Map();

        this.guiterminal = guiterminal;

        this.invComponent = new InventoryComponent(
            this.player.inventory, 'inventory', -11, this.socket);
        this.chestComponent = new InventoryComponent(
            this.player.chest.inventory, 'chest', +12);
        this.craftComponent = new CraftComponent(
            new Inventory({}, 5), this.player.inventory, 'crafting', +12, 20);
        this.deleteComponent = new InventoryComponent(
            new Inventory({}, 5), 'delete', +12);

        this.hotbarInvComponent = new HotbarInventoryComponent(+12);
        this.hotbarComponent = new HotbarComponent(
            this.hotbarInvComponent, this.player);

        this.helpComponent = new HelpComponent();

        this.tablistComponent = new TablistComponent(this.player, (text)=>this.prefillChatbox(text));

        this.syncher = new Syncher(
            this.map,
            this.player,
            this.invComponent,
            this.chestComponent,
            this.hotbarInvComponent,
            this.socket,
            this.cli
        );

        this.gui = new Gui(
            guiterminal,
            this.map,
            this.player,
            this.hotbarComponent,
            this.invComponent,
            this.chestComponent,
            this.craftComponent,
            this.deleteComponent,
            this.helpComponent,
            this.tablistComponent,
            this.hotbarInvComponent,
            this.settings
        );

        this.guiAnimation = new GuiAnimation(
            guiterminal,
            this.map,
            this.player
        );
        this.guiCredits = new GuiCredits(guiterminal);
        this.commands = commands;
        this.active = false;
        this.inChatbox = false;
        this.keys = keys; // holds current states of keyboard
        this.pendingKeyCallback = undefined; // used for key recording

        this.player.mousex = 0;
        this.player.mousey = 0;
        this.player.usingmousecontrols = false;

        this.preparedCraft = null;

        this.animationRunning = false;

        this.creditsIndex = -1;
        this.creditsIndexMod = 0;

        this.promptBlockx = 0;
        this.promptBlocky = 0;
        this.selectedQuestionMark = '';
        this.hasCancellableCliPrompt = false;

        this.itemToPickNextTick = null;

        // socket event handlers
        this.socket.on('t', this.socketTHandler = data=>this.onSocketTerrainUpdate(data, socket));
        this.socket.on('chat', this.socketChatHandler = data=>this.onSocketChat(data, socket));

        // resize event handlers in terminal
        this.guiterminal.addResizeEventListener((w,h)=>{this.invComponent.resizeTerminal(w,h)});
        this.guiterminal.addResizeEventListener((w,h)=>{this.chestComponent.resizeTerminal(w,h)});
        this.guiterminal.addResizeEventListener((w,h)=>{this.craftComponent.resizeTerminal(w,h)});
        this.guiterminal.addResizeEventListener((w,h)=>{this.deleteComponent.resizeTerminal(w,h)});
        this.guiterminal.addResizeEventListener((w,h)=>{this.hotbarComponent.resizeTerminal(w,h)});
        this.guiterminal.addResizeEventListener((w,h)=>{this.helpComponent.resizeTerminal(w,h)});
        this.guiterminal.addResizeEventListener((w,h)=>{this.hotbarInvComponent.resizeTerminal(w,h)});
        this.guiterminal.addResizeEventListener((w,h)=>{this.tablistComponent.resizeTerminal(w,h)});
        this.guiterminal.addResizeEventListener((w,h)=>{this.redisplay()});

        // mouse event handlers
        window.addEventListener('mousemove', this.mouseMoveHandler = e=>this.handleMouse(e), false);
        window.addEventListener('mousedown', this.mouseDownHandler = e=>this.handleMouseDown(e), false);
        window.addEventListener('mouseup', this.mouseUpHandler = e=>this.handleMouseUp(e), false);
        window.addEventListener('wheel', this.wheelHandler = e=>this.handleWheel(e), false);

        // keyboard has no event handlers here. It is polled by the game loop.

        // chatbox starts waiting for something to be typed
        // (new thread is created here)
        this.chatboxLoop();

        this.audio_pop = new Audio('audio/pop2.ogg');
        this.popTimeout = false;

        this.lastHorizontalMove = 0;

        // this.digCooldown = 0;

    }

    kill(){
        // important: turn off all the sockets because this game object is redundant!
        this.socket.off('t', this.socketTHandler);
        this.socket.off('chat', this.socketChatHandler);
        // also window events
        window.removeEventListener('mousemove', this.mouseMoveHandler);
        window.removeEventListener('mousedown', this.mouseDownHandler);
        window.removeEventListener('mouseup', this.mouseUpHandler);
        window.removeEventListener('wheel', this.wheelHandler);

        // also stop music.
        this.music.stopMusic();
    }

    onSocketTerrainUpdate(data){
        this.syncher.serverEvent(data);
    }

    onSocketChat(data){
        if(this.active){
            if (data.timestamp === true) {
                this.printWithTimestamp(data.message);
            }
            else {
                // without timestamp
                this.cli.println(data.message);
            }
            if (data.notify === true && this.settings.s['twirt']) {
                this.playPop();
            }
        }
    }

    printWithTimestamp(message, color) {
        const date = new Date();
        const hour = date.getHours();
        let min = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        const timestamp = hour + ':' + min + ' ';
        message = '\\[#444\\]'+timestamp+'\\[/\\]' + message;
        this.cli.println(message);
    }

    playPop() {
        if (this.popTimeout === true) {
            // was already a pop a second ago.
            return;
        }
        this.popTimeout = true;
        if (this.audio_pop.paused) {
            this.audio_pop.play();
        }
        else {
            this.audio_pop.currentTime = 0;
        }
        setTimeout(() => this.popTimeout = false, 1000);
    }

    // grabbing key for !map
    recordKey(){
        return new Promise((resolve, reject)=>{
            // prepare the callback for this event:
            this.pendingKeyCallback = key=>{
                this.pendingKeyCallback = undefined;
                return resolve(key);
            }
        });
    }

    async chatboxLoop(){
        let exitloop = false;
        while(!exitloop){
            const typed = await this.cli.promptCommand('> ', false, 10000, ['#444'])
                .catch(error => exitloop = true);
            if (exitloop) continue;
            this.keys.clearFresh();
            if(/^\//.test(typed)){
                // is a command (client or server)
                await this.commands.executeCommand(typed);
            }
            else{
                // is chat
                this.socket.emit('chat', {message: typed});
            }
            // stop focusing the cli
            this.blurChatbox();
        }
    }

    focus(){
        // window focused
        if(this.inChatbox){
            this.cli.focus();
        }
        else{
            this.gui.focus();
        }
    }

    blur(){
        // window unfocused
        this.gui.blur();
        this.cli.blur();
    }

    focusChatbox() {
        this.inChatbox = true;
        this.cli.focus();
        this.cli.unpause();
        this.cli.scrollToEnd();
    }

    blurChatbox() {
        this.inChatbox = false;
        this.cli.blur();
        this.cli.pause();
        if (this.hasCancellableCliPrompt) {
            cli.println('cancelled.');
            this.cli.abort();
            this.chatboxLoop();
            this.hasCancellableCliPrompt = false;
        }
    }

    handleKeyDown(keycode){

        // top priority to complete ignorance
        // (when game is not active)
        if(!this.active){
            return;
        }

        // next by priority is key recording.
        // if recording keys, don't do anything else.
        if(this.pendingKeyCallback){
            // resolve the callback with the key that was just pressed
            this.pendingKeyCallback(keycode);
            return;
        }

        // after that, priority goes to chatbox
        if(this.inChatbox){
            if (this.layout.mappings[keycode] === 'escape') {
                this.blurChatbox();
            }
            return;
        }

        // is help visible?
        if (this.helpComponent.helpvisible > 0) {
            // is it the help key again?
            if (this.layout.mappings[keycode] === 'help') {
                // show more help
                this.helpComponent.helpvisible = 2;
            }
            else {
                // hide help because key was pressed
                this.helpComponent.helpvisible = 0;
            }
            // skip because help is visible
            return;
        }

        // these bindings work the same whether or not inventory is open:
        switch (this.layout.mappings[keycode]) {

            case 'help':
                this.helpComponent.helpvisible = 1;
                return;

            case 'chat':
                this.focusChatbox();
                return;

            case 'command':
                this.focusChatbox();
                this.cli.clearCmd();
                this.cli.handleKey('/');
                return;

            case 'hotbarnext':
                this.hotbarInvComponent.cursorUpWrap();
                this.redisplay();
                return;

            case 'hotbarprev':
                this.hotbarInvComponent.cursorDownWrap();
                this.redisplay();
                return;

            case 'hotkey0': this.hotkeyPressed(0); return;
            case 'hotkey1': this.hotkeyPressed(1); return;
            case 'hotkey2': this.hotkeyPressed(2); return;
            case 'hotkey3': this.hotkeyPressed(3); return;
            case 'hotkey4': this.hotkeyPressed(4); return;
            case 'hotkey5': this.hotkeyPressed(5); return;
            case 'hotkey6': this.hotkeyPressed(6); return;
            case 'hotkey7': this.hotkeyPressed(7); return;
            case 'hotkey8': this.hotkeyPressed(8); return;
            case 'hotkey9': this.hotkeyPressed(9); return;
        }

        // is inventory visible?
        if (this.invComponent.isShown() && this.invComponent.focused) {
            // these keys get special function in inventory
            switch (this.layout.mappings[keycode]) {
                case 'up': case 'cursorup':
                    this.usingmousecontrols = false;
                    this.invComponent.cursorUp();
                    this.redisplay();
                    break;
                case 'down': case 'cursordown':
                    this.usingmousecontrols = false;
                    this.invComponent.cursorDown();
                    this.redisplay();
                    break;
                case 'left': case 'cursorleft':
                    this.usingmousecontrols = false;
                    break;
                case 'right': case 'cursorright':
                    this.usingmousecontrols = false;
                    if (this.chestComponent.isShown()) {
                        // switch to chest
                        if (this.chestComponent.slots < 1) break;
                        this.invComponent.blur();
                        this.chestComponent.focus();
                    } else if (this.craftComponent.isShown()) {
                        // switch to crafting
                        if (this.craftComponent.slots < 1) break;
                        this.invComponent.blur();
                        this.craftComponent.focus();
                    } else if (this.deleteComponent.isShown()) {
                        // switch to delete menu
                        if (this.deleteComponent.slots < 1) break;
                        this.invComponent.blur();
                        this.deleteComponent.focus();
                    } else if (this.hotbarInvComponent.isShown()) {
                        // switch to hotbarInv menu
                        if (this.hotbarInvComponent.slots < 1) break;
                        this.invComponent.blur();
                        this.hotbarInvComponent.focus();
                    }
                    this.redisplay();
                    break;
                case 'craft':
                    this.usingmousecontrols = false;
                    // do not allow switching from delete menu to crafting
                    if (this.deleteComponent.isShown()) break;
                    // toggle crafting menu
                    if (this.craftComponent.isShown()) {
                        this.invComponent.hide();
                        this.craftComponent.hide();
                    } else {
                        this.chestComponent.hide();
                        this.hotbarInvComponent.hide();
                        this.craftComponent.show();
                        this.invComponent.focus();
                    }
                    break;
                case 'jump':
                    this.usingmousecontrols = false;
                    const shift = this.keys.getFullKeyStates()[16];
                    if (this.chestComponent.isShown()) {
                        this.sendItemToChest(shift, false);
                    } else if (this.craftComponent.isShown()) {
                        this.sendItemToCraft(shift, false);
                    } else if (this.deleteComponent.isShown()) {
                        this.sendItemToDelete(shift, false);
                    } else if (this.hotbarInvComponent.isShown()) {
                        this.sendItemToHotbar();
                    }
                    break;
                case 'inventory': case 'escape':
                    // keys that exit inventory
                    this.invComponent.hide();
                    this.hotbarInvComponent.hide();
                    this.chestComponent.hide();
                    this.craftComponent.hide();
                    this.deleteComponent.hide();
                    // item deletion happens when inventory closed
                    this.commitItemDeletion();
                    this.redisplay();
                    break;
            }
            return;
        }

        // is chest UI visible?
        if (this.chestComponent.isShown() && this.chestComponent.focused) {

            // these keys get special function in chest
            switch (this.layout.mappings[keycode]) {
                case 'up': case 'cursorup':
                    this.usingmousecontrols = false;
                    this.chestComponent.cursorUp();
                    this.redisplay();
                    break;
                case 'down': case 'cursordown':
                    this.usingmousecontrols = false;
                    this.chestComponent.cursorDown();
                    this.redisplay();
                    break;
                case 'left': case 'cursorleft':
                    this.usingmousecontrols = false;
                    // only if there are items in inventory
                    if (this.invComponent.slots < 1) break;
                    this.chestComponent.blur();
                    this.invComponent.focus();
                    this.redisplay();
                    break;
                case 'right': case 'cursorright':
                    this.usingmousecontrols = false;
                    break;
                case 'craft':
                    this.usingmousecontrols = false;
                    this.chestComponent.hide();
                    this.hotbarInvComponent.hide();
                    this.craftComponent.show();
                    this.invComponent.focus();
                    break;
                case 'jump':
                    this.usingmousecontrols = false;
                    const shift = this.keys.getFullKeyStates()[16];
                    this.sendFromChestToInv(shift);
                    break;

                case 'inventory': case 'escape':
                    // keys that exit inventory
                    this.chestComponent.hide();
                    this.invComponent.hide();
                    this.redisplay();
                    break;

            }
            return;
        }

        // is crafting UI visible?
        if (this.craftComponent.isShown() && this.craftComponent.focused) {

            // these keys get special function in crafting
            switch (this.layout.mappings[keycode]) {
                case 'up': case 'cursorup':
                    this.usingmousecontrols = false;
                    this.craftComponent.cursorUp();
                    this.redisplay();
                    break;
                case 'down': case 'cursordown':
                    this.usingmousecontrols = false;
                    this.craftComponent.cursorDown();
                    this.redisplay();
                    break;
                case 'left': case 'cursorleft':
                    this.usingmousecontrols = false;
                    this.craftComponent.blur();
                    this.invComponent.focus();
                    this.redisplay();
                    break;
                case 'right': case 'cursorright':
                    this.usingmousecontrols = false;
                    break;
                case 'jump':
                    this.usingmousecontrols = false;
                    const shift = this.keys.getFullKeyStates()[16];
                    this.sendFromCraftToInv(shift);
                    break;

                case 'inventory': case 'escape': case 'craft':
                    // keys that exit inventory
                    this.craftComponent.hide();
                    this.invComponent.hide();
                    this.redisplay();
                    break;

            }
            return;
        }

        // is item deletion UI visible?
        if (this.deleteComponent.isShown() && this.deleteComponent.focused) {

            // these keys get special function in delete menu
            switch (this.layout.mappings[keycode]) {
                case 'up': case 'cursorup':
                    this.usingmousecontrols = false;
                    this.deleteComponent.cursorUp();
                    this.redisplay();
                    break;
                case 'down': case 'cursordown':
                    this.usingmousecontrols = false;
                    this.deleteComponent.cursorDown();
                    this.redisplay();
                    break;
                case 'left': case 'cursorleft':
                    this.usingmousecontrols = false;
                    // only if there are items in inventory
                    if (this.invComponent.slots < 1) break;
                    this.deleteComponent.blur();
                    this.invComponent.focus();
                    this.redisplay();
                    break;
                case 'right': case 'cursorright':
                    this.usingmousecontrols = false;
                    break;
                case 'craft':
                    break;
                case 'jump':
                    this.usingmousecontrols = false;
                    const shift = this.keys.getFullKeyStates()[16];
                    this.sendFromDeleteToInv(shift);
                    break;

                case 'inventory': case 'escape': case 'delete':
                    // keys that exit inventory
                    this.invComponent.hide();
                    this.deleteComponent.hide();
                    // item deletion happens when inventory closed
                    this.commitItemDeletion();
                    this.redisplay();
                    break;

            }
            return;
        }

        // is hotbar inventory UI visible?
        if (this.hotbarInvComponent.isShown() && this.hotbarInvComponent.focused) {

            // these keys get special function in hotbarInv menu
            switch (this.layout.mappings[keycode]) {
                case 'up': case 'cursorup':
                    this.usingmousecontrols = false;
                    this.hotbarInvComponent.cursorUp();
                    this.redisplay();
                    break;
                case 'down': case 'cursordown':
                    this.usingmousecontrols = false;
                    this.hotbarInvComponent.cursorDown();
                    this.redisplay();
                    break;
                case 'left': case 'cursorleft':
                    this.usingmousecontrols = false;
                    // only if there are items in inventory
                    if (this.invComponent.slots < 1) break;
                    this.hotbarInvComponent.blur();
                    this.invComponent.focus();
                    this.redisplay();
                    break;
                case 'right': case 'cursorright':
                    this.usingmousecontrols = false;
                    break;
                case 'craft':
                    break;
                case 'jump':
                    this.usingmousecontrols = false;
                    this.sendFromHotbarToInv();
                    break;

                case 'inventory': case 'escape':
                    // keys that exit inventory
                    this.invComponent.hide();
                    this.hotbarInvComponent.hide();
                    // item deletion happens when inventory closed
                    this.commitItemDeletion();
                    this.redisplay();
                    break;

            }
            return;
        }

        switch (this.layout.mappings[keycode]) {

            case 'togglemap':
                this.gui.overviewshown = !this.gui.overviewshown;
                break;

            case 'zoomout':
                if (this.gui.overviewshown) {
                    this.gui.overviewzoom *= 2;
                    if (this.gui.overviewzoom > 16) {
                        this.gui.overviewzoom = 16;
                    }
                }
                else {
                    // also enters overview,
                    // but always at lowest level
                    this.gui.overviewshown = true;
                    this.gui.overviewzoom = 2;
                    this.redisplay();
                }
                break;

            case 'zoomin':
                if (this.gui.overviewshown) {
                    this.gui.overviewzoom /= 2;
                    if (this.gui.overviewzoom < 2) {
                        this.gui.overviewzoom = 2;
                        // zooming brings out out of the overview
                        this.gui.overviewshown = false;
                    }
                }
                break;

            case 'inventory':
                // open inventory
                this.gui.overviewshown = false;
                this.invComponent.show();
                this.hotbarInvComponent.show();
                this.invComponent.focus();
                break;

            case 'craft':
                // open inventory and crafting
                this.gui.overviewshown = false;
                this.invComponent.show();
                this.invComponent.focus();
                this.craftComponent.show();
                break;

            case 'delete':
                // open inventory and deletion menu
                this.gui.overviewshown = false;
                this.invComponent.show();
                this.invComponent.focus();
                this.deleteComponent.show();
                break;

            case 'escape':
                this.gui.overviewshown = false;
                break;

        }
    }

    hotkeyPressed(nkey) {
        this.hotbarInvComponent.useHotkey(nkey);
        this.redisplay();
    }

    rememberHotbarOnServer() {
        this.socket.emit('h', this.hotbarInvComponent.slots);
    }

    sendItemToChest(all=false, half=false) {
        const item_code = this.invComponent.getSelected();
        // abort if fist is selected
        if (item_code === null) return;
        let amount = 1;
        if (all) {
            amount = Math.min(
                this.invComponent.curSlotAmount(),
                this.chestComponent.inventory.howManyCanGain(item_code));
        }
        if (half) {
            amount = Math.min(
                Math.ceil(this.invComponent.curSlotAmount()/2),
                this.chestComponent.inventory.howManyCanGain(item_code));
        }
        if (this.chestComponent.inventory.canGain(item_code, +amount)
            && this.invComponent.inventory.canGain(item_code, -amount)
            && amount > 0
        ) {
            this.socket.emit('i', {
                x: this.player.chest.x,
                y: this.player.chest.y,
                i: item_code,
                n: amount,
                d: this.invComponent.getCursorItemSlotIndex(),
            });
        }
    }


    sendItemToCraft(all=false, half=false) {
        const item_code = this.invComponent.getSelected();
        // abort if fist is selected
        if (item_code === null) return;
        // abort if don't have item
        if (this.invComponent.curSlotAmount() < 1) return;
        // special behavior for wood
        if (item_code[0] === 'H') {
            // remove every type of wood from the crafting menu
            for (const w of ['a','i','l','p','v','b','r','u']) {
                const wood = 'H' + w;
                this.craftComponent.inventory.setItemAmount(wood, 0);
            }
        }
        // special behavior for stick
        if (item_code[0] === '|') {
            // remove every type of wood from the crafting menu
            for (const s of ['|','/','\\']) {
                const stick = '|' + s;
                this.craftComponent.inventory.setItemAmount(stick, 0);
            }
        }
        if (this.craftComponent.inventory.canGain(item_code, +1)
            && this.invComponent.inventory.numItems(item_code)
            >= this.craftComponent.inventory.numItems(item_code)+1
        ) {
            this.craftComponent.inventory.setItemAmount(item_code,
                1, null);
            this.craftComponent.updateSlots();
        }
    }

    sendItemToDelete(all=false, half=false) {
        const item_code = this.invComponent.getSelected();
        // abort if fist is selected
        if (item_code === null) return;
        if (Inventory.isDataItem(item_code)) {
            this.cli.println('\\[#a33\\]can not delete');
            return;
        }
        let amount = 1;
        if (all) {
            amount = Math.min(
                this.invComponent.curSlotAmount(),
                this.deleteComponent.inventory.howManyCanGain(item_code));
        }
        if (half) {
            amount = Math.min(
                Math.ceil(this.invComponent.curSlotAmount()/2),
                this.deleteComponent.inventory.howManyCanGain(item_code));
        }
        if (this.deleteComponent.inventory.canGain(item_code, amount)
            && this.invComponent.numItems(item_code) >= amount
            && amount > 0
        ) {
            this.deleteComponent.inventory.gainItem(item_code, amount, null);
            this.deleteComponent.updateSlots();
            this.invComponent.pretendGainNonDataItem(item_code, -amount);
            this.invComponent.updateSlots();
        }
    }

    sendItemToHotbar() {
        const item_code = this.invComponent.getSelected();
        // abort if fist is selected
        if (item_code === null) return;
        // check if item exists in hotbar
        for (let i = 0; i < this.hotbarInvComponent.slots.length; i++) {
            if (this.hotbarInvComponent.slots[i] === item_code) {
                // disallow duplicate hotbar entries
                return;
            }
        }
        // find the first empty slot in hotbar
        for (let i = 0; i < this.hotbarInvComponent.slots.length; i++) {
            if (this.hotbarInvComponent.slots[i] === null) {
                this.hotbarInvComponent.slots[i] = item_code;
                this.rememberHotbarOnServer();
                break;
            }
        }
    }

    sendFromHotbarToInv() {
        const cursor = this.hotbarInvComponent.cursor;
        // nothing special, just remove this item from hotbar
        // because it was clicked or whatever
        this.hotbarInvComponent.slots[cursor] = null;
        this.rememberHotbarOnServer();
    }

    sendFromChestToInv(all=false, half=false) {
        const item_code = this.chestComponent.getSelected();
        if (item_code === null) return;
        let amount = 1;
        if (all) {
            amount = Math.min(
            this.chestComponent.curSlotAmount(),
            this.invComponent.inventory.howManyCanGain(item_code));
        }
        if (half) {
            amount = Math.min(
            Math.ceil(this.chestComponent.curSlotAmount()/2),
            this.invComponent.inventory.howManyCanGain(item_code));
        }
        if (this.chestComponent.inventory.canGain(item_code, -amount)
            && this.invComponent.inventory.canGain(item_code, +amount)
            && amount > 0
        ) {
            this.socket.emit('i', {
                x: this.player.chest.x,
                y: this.player.chest.y,
                i: item_code,
                n: -amount,
                d: this.chestComponent.getCursorItemSlotIndex(),
            });
        }
    }

    sendFromCraftToInv(all=false) {
        if (this.craftComponent.isRecipeCursor) {
            // jump was pressed on a recipe
            const recipe_index = this.craftComponent.curRecipe();
            const amount = all
                ? this.invComponent.inventory.howManyCanCraft(recipe_index)
                : 1;
            if (this.invComponent.inventory.canCraft(recipe_index, amount)) {
                this.preparedCraft = {r: recipe_index, n: amount};
            }
        } else {
            // jump was pressed on non-recipe item
            const item_code = this.craftComponent.getSelected();
            if (item_code === null) return;
            if (this.craftComponent.curSlotAmount() < 1) return;
            this.craftComponent.inventory.gainItemNoShenanigans(item_code, -1, 0);
            this.craftComponent.updateSlots();
        }
    }

    sendFromDeleteToInv(all=false, half=false) {
        const item_code = this.deleteComponent.getSelected();
        // abort if fist is selected (TODO: make this impossible)
        if (item_code === null) return;
        let amount = 1;
        if (all) {
            amount = this.deleteComponent.curSlotAmount();
        }
        if (half) {
            amount = Math.ceil(this.deleteComponent.curSlotAmount()/2);
        }
        if (!this.deleteComponent.inventory.canGain(item_code, -amount)) return;
        this.deleteComponent.inventory.gainItem(item_code, -amount, null);
        this.deleteComponent.updateSlots();
        this.invComponent.pretendGainNonDataItem(item_code, amount);
        this.invComponent.updateSlots();
    }

    commitItemDeletion() {
        const data = this.deleteComponent.inventory.state;
        if (Object.keys(data).length > 0) {
            this.socket.emit('del', data);
        }
        // clear the deletion preview in main inventory
        this.invComponent.clearPretend();
        this.invComponent.updateSlots();
        // clear the deletion menu
        this.deleteComponent.inventory.state = {};
        this.deleteComponent.updateSlots();
    }

    handleKeyUp(keycode){
    }

    prefillChatbox(text) {
        this.cli.setEditable(text);
        this.focusChatbox();
    }

    gameTick(){
        const fullKeyStates = this.keys.getFullKeyStates();
        const fullMbStates = this.keys.getFullMbStates();
        const fullFreshKeys = this.keys.freshKeys;
        fullKeyStates[2001] = fullMbStates[1];
        fullKeyStates[2002] = fullMbStates[2];
        fullKeyStates[2003] = fullMbStates[3];
        fullFreshKeys[2001] = this.keys.freshMbs[1]
        fullFreshKeys[2002] = this.keys.freshMbs[2];
        fullFreshKeys[2003] = this.keys.freshMbs[3];

        if(!this.active){
            return;
        }
        // handle tick here;

        const tick = {};

        this.tablistComponent.hide();

        if (this.itemToPickNextTick !== null) {
            this.invComponent.selectItemById(this.itemToPickNextTick);
            this.itemToPickNextTick = null;
        }

        // pass the prepared crafting action
        if (this.preparedCraft) {
            tick.c = this.preparedCraft;
            const recipe = Inventory.recipes[this.preparedCraft.r];
            this.itemToPickNextTick = recipe.get.item;
            this.preparedCraft = null;
        }

        if(!this.inChatbox && !this.helpComponent.helpvisible){

            // iterate over defined keys
            for (const i in fullKeyStates) {

                // skip if fake property
                if (!fullKeyStates.hasOwnProperty(i)) continue;

                // skip if not pressed
                if (!fullKeyStates[i]) continue;

                // make tab list visible (before checking if inventory open)
                if (this.layout.mappings[i] === 'tablist') {
                    this.tablistComponent.show();
                }

                // is any inventory UI visible?
                if (this.invComponent.isShown()) {
                    continue;
                }
                switch (this.layout.mappings[i]) {

                    case 'down':
                        tick.d = 1;
                        break;

                    case 'left':
                        tick.l = 1;
                        this.lastHorizontalMove = -1;
                        break;

                    case 'right':
                        tick.r = 1;
                        this.lastHorizontalMove = 1;
                        break;

                    case 'jump': case 'up':
                        tick.u = 1;
                        tick.j = 1;
                        break;

                    case 'sprint':
                        tick.s = 2;
                        if (this.player.gamemode === 2) {
                            tick.s = this.settings.eqs['spectatorspeed'];
                        }
                        break;

                    case 'cursorup':
                        this.player.cursorUp();
                        this.usingmousecontrols = false;
                        break;

                    case 'cursordown':
                        this.player.cursorDown();
                        this.usingmousecontrols = false;
                        break;

                    case 'cursorleft':
                        this.player.cursorLeft();
                        this.usingmousecontrols = false;
                        break;

                    case 'cursorright':
                        this.player.cursorRight();
                        this.usingmousecontrols = false;
                        break;

                    case 'dig':
                        if (this.maybeCollectDetector()) break;
                        if (this.maybeCollectSSPOOKY()) break;
                        // special case for "block inspector" tool
                        if (this.hotbarInvComponent.getSelected() === 'sp'
                            && this.player.inventory.hasItem('sp')
                        ) {
                            // needs to be a fresh key
                            if (!fullFreshKeys[i]) continue;
                            // activate block inspector
                            const x = this.player.x + this.player.cursorx;
                            const y = this.player.y + this.player.cursory;
                            const block = this.map.getBlock(x, y);
                            const item = Inventory.block2item(block);
                            const item_name = Inventory.item_names[item];
                            const msg = ''+block+'/'+item+'/'+item_name;
                            cli.println('\\[#684043\\]'+msg);
                            const tiles = this.map.getTileEntityAt(x, y);
                            for (const tile of tiles) {
                                const msg = JSON.stringify(tile);
                                cli.println('\\[#684043\\]'+msg);
                            }
                            break;
                        }
                        if (this.hotbarInvComponent.getSelected() !== null
                            && this.hotbarInvComponent.getSelected()[0] === 'f'
                            && this.invComponent.curSlotAmount() > 0) {
                            const x = this.player.x + this.player.mousex;
                            const y = this.player.y + this.player.mousey;
                            const block = this.map.getBlock(x, y);
                            // eat overrides dig
                            if (block[0] === 'f'
                                || (block[0] === 'W' && block[1] === 'f')) {
                                // skip eat and dig instead.
                                // (we allow digging food when holding food)
                            } else {
                                tick.e = this.hotbarInvComponent.getSelected();
                                break;
                            }
                        }
                        this.diggment(tick);
                        break;

                    case 'place':
                        if (this.hotbarInvComponent.getSelected() !== null) {
                            this.placement(tick);
                        }
                        if (fullFreshKeys[i]) {
                            if (this.maybeCollectDetector()) break;
                            if (this.maybeCollectSSPOOKY()) break;
                            // interactment
                            tick.I = {
                                x: this.player.cursorx,
                                y: this.player.cursory,
                                i: this.hotbarInvComponent.getSelected(),
                            };
                        }
                        break;

                    case 'pick':
                        const x = this.player.x + this.player.mousex;
                        const y = this.player.y + this.player.mousey;
                        const block = this.map.getBlock(x, y);
                        const item = Inventory.block2item(block);
                        // TODO: interact with hotbar instead of inv
                        if (item === ' ') {
                            // TODO: attempt to select nothing
                            // (only works if there is an empty slot)
                        } else {
                            if (!this.invComponent.selectItemById(item)
                                && this.player.gamemode !== 0) {
                                tick.M = item;
                                this.itemToPickNextTick = item;
                            }
                        }
                        break;

                }
            }
        }
        // check if prompt for ? finished:
        if (this.selectedQuestionMark !== '') {
            tick.P = {
                x: this.promptBlockx,
                y: this.promptBlocky,
                i: this.selectedQuestionMark,
            }
            this.selectedQuestionMark = '';
        }
        // check if on ice:
        if ((this.map.getBlock(this.player.x, this.player.y) === 'Wi'
            || this.map.getBlock(this.player.x, this.player.y-1) === 'Wc')
            && this.player.gamemode === 0
        ) {
            if (this.lastHorizontalMove === 1 && tick.l !== 1) {
                tick.r = 1;
            }
            else if (this.lastHorizontalMove === -1 && tick.r !== 1) {
                tick.l = 1;
            }
        }

        if (this.player.warpWarmup === 20) {
            tick.W = 1;
            this.player.warpCooldown = this.player.TOTALWARPCOOLDOWN;
        }

        if (this.usingmousecontrols) {
            this.snapCursorToMouse();
        }

        this.syncher.executeTick(tick);

        if (this.usingmousecontrols) {
            this.snapCursorToMouse();
        }

        this.keys.clearFresh();

        if (this.player.warpCooldown > 0 &&
            this.map.isBlockLoaded(this.player.x, this.player.y)) {
            this.player.warpCooldown--;
        }

        if (this.player.warpCooldown === 0
            && this.map.getBlock(this.player.x, this.player.y-1) === 'WW'
            && this.map.getBlock(this.player.x, this.player.y+5) === 'R1') {
            if (this.player.warpWarmup < 20) {
                this.player.warpWarmup++;
            }
        } else {
            this.player.warpWarmup = 0;
        }

        this.redisplay();

        // TODO: this paragraph is all temporary and bad code.
        if (this.player.gamemode === 0
            && this.map.getBlock(this.player.x, this.player.y) === 'WO') {
            if (!this.player.onTentacle) {
                this.cli.println('\\[#aa3\\]Tentacle grabs you!');
                this.player.onTentacle = true;
            }
        }
        else {
            this.player.onTentacle = false;
        }

    }

    maybeCollectDetector() {
        // check if player's cursor is on a detector
        const wx = this.player.x + this.player.cursorx;
        const wy = this.player.y + this.player.cursory;
        for (const detector of this.player.visibleDetectors) {
            if (wx !== detector.x || wy !== detector.y) continue;
            // the click was on a detector
            const follow = this.player.inventory.hasItem('Wo')
                && this.hotbarInvComponent.getSelected() === 'Wo';
            this.socket.emit('iclickeddetector', {
                x: wx,
                y: wy,
                follow: follow,
            });
            return true;
        }
        return false;
    }

    maybeCollectSSPOOKY() {
        // need to be holding detector
        if (!this.player.inventory.hasItem('E')) return false;
        if (this.hotbarInvComponent.getSelected() !== 'E') return false;
        // check if player's cursor is on a ghost
        const wx = this.player.x + this.player.cursorx;
        const wy = this.player.y + this.player.cursory;
        for (const ghost of this.player.visibleGhosts) {
            if (wx !== ghost.x || wy !== ghost.y) continue;
            this.socket.emit('iclickedsspooky', {
                x: wx,
                y: wy,
            });
            return true;
        }
        return false;
    }

    snapCursorToMouse() {
        if (this.player.cursorSet(
            this.player.mousex, // + this.player.camx - this.player.x,
            this.player.mousey, // + this.player.camy - this.player.y,
        )){
            this.usingmousecontrols = true;
        }
    }

    redisplay(){
        if (this.usingmousecontrols) {
            this.snapCursorToMouse();
        }

        if (this.creditsIndex > -1) {
            if (Math.floor(this.creditsIndex/20)%3 === 0) {
                this.creditsIndexMod += 2;
            }
            const successDisplay = this.guiCredits.display(this.creditsIndexMod);
            this.creditsIndex++;
            if (!successDisplay) {
                this.creditsIndex = -1;
                this.creditsIndexMod = 0;
            }
        }
        else if (this.player.animation) {
            // skip regular displaying
            if (!this.animationRunning) {
                // start the animation since it's not running already.
                this.animationRunning = true;
                const finishx = this.player.x;
                const finishy = this.player.y;
                this.guiAnimation.ghostAnimation(this.player.prevx, this.player.prevy, this.player.x, this.player.y).then(result => {
                    this.player.animation = false;
                    this.animationRunning = false;
                    this.player.x = finishx;
                    this.player.y = finishy;
                });
            }
        }
        else {
            this.gui.display();
        }
    }

    handleMouse(e){
        const xy = this.guiterminal.pixelToChar(e.clientX, e.clientY);
        this.player.mousex = xy.x;
        this.player.mousey = xy.y;
        this.snapCursorToMouse();
    }

    handleMouseDown(e){

        // if recording keys, don't do anything else.
        if(this.pendingKeyCallback){
            // resolve the callback with the key that was just pressed
            let keycode = 2001;
            if (e.buttons > 1) {
                keycode = 2002;
            }
            if (e.buttons > 3) {
                keycode = 2003;
            }
            this.pendingKeyCallback(keycode);
            return;
        }

        // check if click is in chat box
        const xy = this.guiterminal.pixelToChar(
            e.clientX, e.clientY, false);
        if (xy.x < 0 && !this.inChatbox) {
            this.focusChatbox();
            return;
        }
        // or out of chat box
        if (xy.x >= 0 && this.inChatbox) {
            this.blurChatbox();
            return;
        }

        const x = xy.x;
        const y = xy.y;

        if (this.hotbarComponent.isClickWithin(x, y)) {
            this.hotbarComponent.handleClick(x, y)
            // An ugly hack to prevent regular function on this click:
            this.keys.mbStates = [];
            this.keys.freshMbs = [];
            this.keys.freshMbList = [];
            return;
        }

        if (this.tablistComponent.isShown()) {
            if (this.tablistComponent.handleClick(x, y)) {
                return;
            }
        }

        if (this.invComponent.isShown()){
            if (this.invComponent.isClickWithin(x, y)) {
                if (this.invComponent.slots < 1) return;
                this.invComponent.focus();
                this.chestComponent.blur();
                this.craftComponent.blur();
                this.hotbarInvComponent.blur();
                if (this.invComponent.handleClick(x, y)) {
                    const shift = this.keys.getFullKeyStates()[16];
                    const rmb = e.buttons > 1;
                    if (this.chestComponent.isShown()) {
                        this.sendItemToChest(shift, rmb);
                    } else if (this.craftComponent.isShown()) {
                        this.sendItemToCraft(shift, rmb);
                    } else if (this.deleteComponent.isShown()) {
                        this.sendItemToDelete(shift, rmb);
                    } else if (this.hotbarInvComponent.isShown()) {
                        this.sendItemToHotbar();
                    }
                }
            } else if (this.chestComponent.isShown()){
                if (this.chestComponent.isClickWithin(x, y)) {
                    if (this.chestComponent.slots < 1) return;
                    this.chestComponent.focus();
                    this.invComponent.blur();
                    if (this.chestComponent.handleClick(x, y)) {
                        const shift = this.keys.getFullKeyStates()[16];
                        const rmb = e.buttons > 1;
                        this.sendFromChestToInv(shift, rmb);
                    }
                }
            } else if (this.craftComponent.isShown()){
                if (this.craftComponent.isClickWithin(x, y)) {
                    if (this.craftComponent.slots < 1) return;
                    this.craftComponent.focus();
                    this.invComponent.blur();
                    if (this.craftComponent.handleClick(x, y)) {
                        const shift = this.keys.getFullKeyStates()[16];
                        this.sendFromCraftToInv(shift);
                    }
                }
            } else if (this.deleteComponent.isShown()){
                if (this.deleteComponent.isClickWithin(x, y)) {
                    if (this.deleteComponent.slots < 1) return;
                    this.deleteComponent.focus();
                    this.invComponent.blur();
                    if (this.deleteComponent.handleClick(x, y)) {
                        const shift = this.keys.getFullKeyStates()[16];
                        const rmb = e.buttons > 1;
                        this.sendFromDeleteToInv(shift, rmb);
                    }
                }
            } else if (this.hotbarInvComponent.isShown()){
                if (this.hotbarInvComponent.isClickWithin(x, y)) {
                    if (this.hotbarInvComponent.slots < 1) return;
                    this.hotbarInvComponent.focus();
                    this.invComponent.blur();
                    if (this.hotbarInvComponent.handleClick(x, y)) {
                        this.sendFromHotbarToInv();
                    }
                }
            }
        }

    }

    handleMouseUp(e){
    }

    handleWheel(e){
        const xy = this.guiterminal.pixelToChar(
            e.clientX, e.clientY, false);
        if (xy.x < 0) {
            // when mouse over chatbox, scroll chatbox
            if(e.deltaY > 0){
                this.cli.bigterminal.scrollDown(3);
            }
            else {
                this.cli.bigterminal.scrollUp(3);
            }
            return;
        }
        else if (this.invComponent.isShown()
            && this.invComponent.isClickWithin(xy.x, xy.y)) {
            // when mouse over inventory, scroll inventory UI
            if(e.deltaY > 0){
                this.invComponent.scrollDown();
            }
            else{
                this.invComponent.scrollUp();
            }
        }
        else if (this.chestComponent.isShown()
            && this.chestComponent.isClickWithin(xy.x, xy.y)) {
            // when mouse over chest, scroll chest UI
            if(e.deltaY > 0){
                this.chestComponent.scrollDown();
            }
            else{
                this.chestComponent.scrollUp();
            }
        }
        else if (this.deleteComponent.isShown()
            && this.deleteComponent.isClickWithin(xy.x, xy.y)) {
            // when mouse over DELETE, scroll deletion UI
            if(e.deltaY > 0){
                this.deleteComponent.scrollDown();
            }
            else{
                this.deleteComponent.scrollUp();
            }
        }
        else if (this.craftComponent.isShown()
            && this.craftComponent.isClickWithin(xy.x, xy.y)) {
            // no scrolling action in crafting UI
        }
        else if (this.hotbarComponent.isClickWithin(xy.x, xy.y)) {
            // when mouse over hotbar, scroll hotbar
            if(e.deltaY > 0){
                this.hotbarInvComponent.cursorDownWrap();
            }
            else{
                this.hotbarInvComponent.cursorUpWrap();
            }
        }
        else if (!this.invComponent.isShown()) {
            // when inventory is closed, scroll hotbar by default
            if(e.deltaY > 0){
                this.hotbarInvComponent.cursorDownWrap();
            }
            else{
                this.hotbarInvComponent.cursorUpWrap();
            }
        }
        this.redisplay();
    }

    diggment(tick) {
        if (tick.D) {
            // skip because this is already in the tick
            return;
        }
        tick.D = {
            x: this.player.cursorx,
            y: this.player.cursory,
        };
        const selected = this.hotbarInvComponent.getSelected();
        if (selected === 'G0'
            && this.player.inventory.hasItem('G0')) {
            tick.D.G = 1; // with grinder
            tick.D.s = this.invComponent.getCursorItemSlotIndex();
        }
        else if (selected === 'G#'
            && this.player.inventory.hasItem('G#')) {
            tick.D.G = 2; // with SUPER GRINDER
            tick.D.s = this.invComponent.getCursorItemSlotIndex();
        }
        else if (selected === 'Gy'
            && this.player.inventory.hasItem('Gy')) {
            tick.D.G = 3; // with yay grinder
            tick.D.s = this.invComponent.getCursorItemSlotIndex();
        }
        else if (selected === 'GB'
            && this.player.inventory.hasItem('GB')) {
            tick.D.G = 4; // with stone grinder
            tick.D.s = this.invComponent.getCursorItemSlotIndex();
        }
        else if (selected === 'GH'
            && this.player.inventory.hasItem('GH')) {
            tick.D.G = 5; // with wooden grinder
            tick.D.s = this.invComponent.getCursorItemSlotIndex();
        }
    }

    placement(tick) {
        if (tick.P) {
            // skip because this is already in the tick
            return;
        }
        const selected = this.hotbarInvComponent.getSelected();
        tick.P = {
            x: this.player.cursorx,
            y: this.player.cursory,
            i: selected,
        }
        if (selected && selected[0] === 'G') {
            // grinder also needs slot data when placed
            tick.P.s = this.invComponent.getCursorItemSlotIndex();
        }
        // when placing ?, prompt for block:
        if (tick.P.i === '?'
            && this.player.inventory.hasItem('?')) {
            const x = this.player.x + this.player.cursorx;
            const y = this.player.y + this.player.cursory;
            const space = this.map.getBlock(x, y)
            if(space === ' ' || space === 'w') {
                // need to abort whatever's going on in the chatbox
                this.cli.abort();
                // now focus it
                this.focusChatbox();
                this.promptBlockx = tick.P.x;
                this.promptBlocky = tick.P.y;
                this.hasCancellableCliPrompt = true;
                // display the prompt for block
                this.cli.prompt('pick block: ', false, 1).then(a => {
                    if (a.length > 0) {
                        this.selectedQuestionMark = '?' + a[0];
                    }
                    // start the chatbox loop again now
                    this.hasCancellableCliPrompt = false;
                    this.chatboxLoop();
                    this.blurChatbox();
                });
            }
        }
        // when placing sign, prompt for content:
        if (tick.P.i === 'si'
            && this.player.inventory.hasItem('si')) {
            const x = this.player.x + this.player.cursorx;
            const y = this.player.y + this.player.cursory;
            const space = this.map.getBlock(x, y)
            if (space === ' '
                || space === 'w'
                || space === '~'
                || space === 'sn'
                || space[0] === '>') {
                // need to abort whatever's going on in the chatbox
                this.cli.abort();
                // now focus it
                this.focusChatbox();
                this.promptBlockx = tick.P.x;
                this.promptBlocky = tick.P.y;
                this.hasCancellableCliPrompt = true;
                // display the prompt for block
                this.cli.prompt('sign: ', false, 100).then(a => {
                    if (a.length > 0) {
                        this.selectedQuestionMark = 'si' + a;
                    }
                    // start the chatbox loop again now
                    this.hasCancellableCliPrompt = false;
                    this.chatboxLoop();
                    this.blurChatbox();
                });
            }
        }
        // when placing custom warp, prompt for code:
        if (tick.P.i === 'WV'
            && this.player.inventory.hasItem('WV')) {
            // need to abort whatever's going on in the chatbox
            this.cli.abort();
            // now focus it
            this.focusChatbox();
            this.promptBlockx = tick.P.x;
            this.promptBlocky = tick.P.y;
            this.hasCancellableCliPrompt = true;
            // display the prompt for block
            this.cli.prompt('warp code: ', false, 100).then(code => {
                this.cli.prompt('who can use? owner/friends/all/cancel: ', true, 100).then(s => {
                    const scope = s[0].toLowerCase();
                    const scopeOk = ['o', 'f', 'a'].includes(scope);
                    if (code.length > 0 && scopeOk) {
                        this.selectedQuestionMark = 'WV' + scope + code;
                    } else {
                        this.cli.println('cancelled.');
                    }
                    // start the chatbox loop again now
                    this.hasCancellableCliPrompt = false;
                    this.chatboxLoop();
                    this.blurChatbox();
                });
            });
            // but don't actually try to place the WV block
            delete tick.P;
        }
    }

}
