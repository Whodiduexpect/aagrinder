// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


// Wrapper for terminal
// Displays the game view.
// Displays the section of the world that is visible
// and overlays GUI elements such as coordinates, hotbar, inventory, cursor

// Plain CPU rendering. Nothing fancy.

"use strict";

const GUI_FRAME_COLOR = '#aaaaaa';
const GUI_SLIGHT_COLOR = '#666666';
const GUI_TEXT_COLOR = '#aaaaaa';
// const CURSOR_COLOR = '#209090';
const CURSOR_COLOR = '#7f7f7f';

const LEAF_LIFETIME = 50;
const WIREPARTICLE_LIFETIME = 20;
const SNOWFLAKE_LIFETIME = 50;
const FIREFLY_LIFETIME = 500;
const DROPLET_FALL_LIFETIME = [];
for (let i = 0; i < 10; i++) {
    for (let j = 0; j < i+1; j++) {
        DROPLET_FALL_LIFETIME.push(i)
    }
}
DROPLET_FALL_LIFETIME.push(10);

class Gui {
    constructor(terminal, map, player, hotbarComponent,
        invComponent, chestComponent, craftComponent, deleteComponent,
        helpComponent, tablistComponent, hotbarInvComponent, settings){
        this.terminal = terminal;
        this.map = map;
        this.player = player;
        this.invComponent = invComponent;
        this.chestComponent = chestComponent;
        this.craftComponent = craftComponent;
        this.deleteComponent = deleteComponent;
        this.hotbarComponent = hotbarComponent;
        this.helpComponent = helpComponent;
        this.hotbarInvComponent = hotbarInvComponent;
        this.tablistComponent = tablistComponent;
        this.components = [
            invComponent,
            chestComponent,
            craftComponent,
            deleteComponent,
            hotbarComponent,
            helpComponent,
            hotbarInvComponent,
            tablistComponent,
        ];
        this.settings = settings;
        this.time = 0;
        this.leaves = [];
        this.snowflakes = [];
        this.droplets = [];
        this.rubyparticles = [];
        this.bubbles = [];
        this.fireflies = [];
        this.wireparticles = [];
        this.curBgColor = "#000000";
        this.overviewshown = false;
        this.overviewzoom = 2;
        this.delanis = [];
    }

    static drawFrame(buffer, frame, frameX, frameY, defaultColor = GUI_FRAME_COLOR){
        for(const i in frame){
            const decoded = AASTRING_DECODE(frame[i], defaultColor)[0];
            for (let j = 0; j < decoded.raw.length; j++) {
                const char = decoded.raw[j];
                if (char === '@') continue;
                const color = decoded.color[j];
                const x = parseInt(j)+frameX;
                const y = parseInt(i)+frameY;
                if (y<0 || y>=buffer.length) continue;
                buffer[y][x] = {char: char, color: color};
            }
        }
    }

    static drawFormatted(buffer, lines, frameX, frameY) {
        for (const i in lines) {
            const line = lines[i];
            for (const j in line.color) {
                const char = line.raw[j];
                const color = line.color[j];
                const x = parseInt(j)+frameX;
                const y = parseInt(i)+frameY;
                if (y<0 || y>=buffer.length) continue;
                buffer[y][x] = {char: char, color: color};
            }
        }
    }

    static drawSpritesRelative(buffer, frameX, frameY, sprites) {
        for (const sp of sprites) {
            const y = frameY+sp.dy;
            if (y < 0 || y >= buffer.length) continue;
            buffer[y][frameX+sp.dx] = sp.sprite;
        }
    }

    static makeSpookySpite(block, biome, curbg, dx, dy) {
        const rawSprite = BIOME_SPRITES[block][biome];
        let shade = 1 - (dx*dx+4*dy*dy)*0.0009;
        if (shade < 0) shade = 0;
        return {
            char: rawSprite.char,
            color: MIX_COLORS_HEX(curbg, rawSprite.color, shade),
        };
    }

    drawInvCursor(buffer, cursorPos, slight=false) {
        if (!cursorPos) return;
        const x = cursorPos.x;
        const y = cursorPos.y;
        if (y < 0 || y >= buffer.length) return;

        if (slight) {
            this.terminal.displayCharacter(x+1, y, '▇', GUI_SLIGHT_COLOR);
            return;
        }
        if (buffer[y][x].char === '▇') {
            this.terminal.displayCharacter(x+1, y, '▇', GUI_FRAME_COLOR);
        } else {
            this.terminal.displayCharacter(x, y, '▇', GUI_FRAME_COLOR);
            this.terminal.displayCharacter(x+1, y, '▇', GUI_FRAME_COLOR);
            if (typeof buffer[y][x].secondchar !== 'undefined') {
                const secondchar = buffer[y][x].secondchar;
                this.terminal.displayCharacterScaled(
                    x + secondchar.dx,
                    y + secondchar.dy,
                    secondchar.scale,
                    secondchar.char,
                    secondchar.color);
            }
            this.terminal.displayCharacter(x, y, buffer[y][x].char, '#000');
        }
    }

    drawGenericCursor(buffer, x, y) {
        if (y < 0 || y >= buffer.length) return;
        this.terminal.displayCharacter(x, y, '▇', buffer[y][x].color);
        this.terminal.displayCharacter(x, y, buffer[y][x].char, '#000');
    }

    drawTablistCursor(buffer, cursorPos) {
        if (!cursorPos) return;
        const x = cursorPos.x;
        const y = cursorPos.y;
        for (let dx = 0; dx < 6; dx++) {
            this.drawGenericCursor(buffer, x+dx, y);
        }
    }

    drawPlayer(sx, sy, color, buffer) {
        const w = this.terminal.width;
        const h = this.terminal.height;
        if (sx<0 || sx>=w || sy<0 || sy>=h) return false;
        const prevBlock = buffer[h - sy - 1][sx];
        const playerSprite = {char:'P', color:'#'+color};
        if (prevBlock !== undefined
            && prevBlock.char === 'P'
            && prevBlock.secondchar !== undefined) {
            playerSprite.secondchar = prevBlock.secondchar;
        }
        else if (prevBlock !== undefined
            && (prevBlock.char === '.' || prevBlock.char === '▁')) {
            playerSprite.secondchar = {
                char: prevBlock.char,
                color: prevBlock.color,
                dx: 0, dy: 0, scale: 1};
            if (prevBlock.dy !== undefined) {
                playerSprite.secondchar.dy = -prevBlock.dy;
            }
        }
        buffer[h - sy - 1][sx] = playerSprite;
    }

    display(){
        this.time++;

        if (this.overviewshown) {
            this.displayOverview();
            return;
        }

        const currentBiome = this.map.biomeAt(
            this.player.x, this.player.y);
        const newBgColor = BIOMES[currentBiome].sky;
        const rgbcur = HEX2RGB(this.curBgColor);
        const rgbnew = HEX2RGB(newBgColor);
        const diff = [
            rgbcur[0] - rgbnew[0],
            rgbcur[1] - rgbnew[1],
            rgbcur[2] - rgbnew[2],
        ];
        // epic trick: add 90% of current color to biome color
        // instead of adding 10% of biome color to current color
        // and this way we avoid issues with rounding
        // console.log(rgbnew, diff, MIX_COLORS_RGB(rgbnew, diff, 0.9));
        this.curBgColor = RGB2HEX(MIX_COLORS_RGB(rgbnew, diff, 0.9));

        if (!this.settings.s['???']) {
            this.terminal.clearScreen();
            this.terminal.setBackground(this.curBgColor);
        }

        const w = this.terminal.width;
        const h = this.terminal.height;
        const left = this.player.x - Math.floor(w / 2);
        const top = this.player.y - Math.ceil(h / 2) + 1;
        const buffer = [];

        // render the terrain / world / map
        for(let y = 0; y < h; y++){
            buffer[h - y - 1] = [];
            for(let x = 0; x < w; x++){
                const worldx = left + x;
                const worldy = top + y;

                if (this.settings.s['chunkbordervisible'] &&
                    (worldx % 256 === 0 || worldy % 256 === 0)) {
                    buffer[h - y - 1][x] = {char: '▖', color: '#f00'};
                    continue;
                }

                const biome = this.map.biomeAt(worldx, worldy);
                const block = this.map.getBlock(worldx, worldy);

                // priority to # because this is not a normal sprite
                if (block[0] === '#') {
                    buffer[h - y - 1][x] = Gui.makeSpookySpite(
                        block, biome, this.curBgColor, x-w/2, y-h/2);
                }
                // WW is animated when on cooldown
                else if (block === 'WW' && this.player.warpCooldown > 0) {
                    const shade = Math.cos((
                        Math.log(this.player.TOTALWARPCOOLDOWN-this.player.warpCooldown,2)
                        -Math.log(this.player.TOTALWARPCOOLDOWN,2)
                    )*8)*-0.5+0.5;
                    const color = MIX_COLORS_HEX(
                        BIOME_SPRITES['WW']['A'].color,
                        '#9c001b',
                        shade);
                    buffer[h - y - 1][x] = {
                        char: 'W',
                        color: color,
                    };
                }
                else if (block === 'sn') {  // sometimes snow is swaying
                    const blockBelow = this.map.getBlock(worldx, worldy-1);
                    const biomeSpritesUnder = BIOME_SPRITES[blockBelow];
                    let snowType = 'sn'
                    if (biomeSpritesUnder) {
                        const spriteUnder = biomeSpritesUnder[biome];
                        const isSwaying = spriteUnder !== undefined && spriteUnder.sway;
                        snowType = isSwaying ? 'snsway' : 'sn';
                    }
                    const sprite = BIOME_SPRITES[snowType][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block === '|/') {
                    const type = this.map.getBlock(worldx-1, worldy) === 'WO'
                        && this.map.getBlock(worldx+1, worldy) === 'WO'
                        ? '|/a' : '|/';
                    const sprite = BIOME_SPRITES[type][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block === 'WO') {
                    const type = this.map.getBlock(worldx-1, worldy) === '|/'
                        && this.map.getBlock(worldx-2, worldy) === 'WO'
                        || this.map.getBlock(worldx+1, worldy) === '|/'
                        && this.map.getBlock(worldx+2, worldy) === 'WO'
                        ? 'WON' : 'WO';
                    const sprite = BIOME_SPRITES[type][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block[0] === '=') {
                    const trimBlock = block.substr(0,3);
                    let sprite = BIOME_SPRITES[trimBlock][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block[0] === ':') {
                    const trimBlock = block.substr(0,2);
                    let sprite = BIOME_SPRITES[trimBlock][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                // TODO: check this first and abort if air. then just pick different sprites
                else if(BIOME_SPRITES[block]){ // fails for air
                    if (block === 'B' || block === 'd') {
                        // miscolored B and d
                        // openssl prime -generate -bits 16

                        const wx = x + left;
                        const wy = y + top;

                        const which_B = ((wx*
                            4250351207
                        )%
                            51283
                            +(wy*
                                4190530093
                            )%
                            49811
                        )%8;

                        buffer[h - y - 1][x] = BIOME_SPRITES[block+Math.abs(which_B)][biome];
                    }
                    else {
                        let sprite = BIOME_SPRITES[block][biome];
                        if (sprite.rainbow) {
                            const wx = x + left;
                            const which_rainbow = (wx%8+8)%8;
                            sprite = BIOME_SPRITES[block+which_rainbow][biome];
                        }
                        buffer[h - y - 1][x] = sprite;
                    }
                }
            }
        }

        // try spawning new leaf particle
        // or droplet particle
        // or warp particle
        // or snowflake particle
        // or bubble particle
        for (let i = 0; i < 100; i++){
            // TODO: instead of checking 100 random blocks,
            // use warp tile entity information
            const sx = Math.floor(Math.random()*(w+20)-10);
            const sy = Math.floor(Math.random()*(h+20)-10);
            const worldx = left + sx;
            const worldy = top + sy;
            const block = this.map.getBlock(worldx, worldy);
            if (block === 'WW') {
                const rubyblock = this.map.getBlock(worldx, worldy+6);
                if (rubyblock === 'R1') {
                    this.rubyparticles.push({
                        wx: worldx,
                        wy: worldy+6,
                        age: 0,
                        dir: Math.random()-0.5,
                    });
                }
            }
            // the remaining iterations (above 10)
            // stop here, only checking warp
            if (i >= 10) continue;
            if (block[0] === 'A'
                || block[0] === 'a'
                || (block[0] === 'W' && block[1] === 'a')) {
                const biome = this.map.biomeAt(worldx, worldy);
                this.leaves.push({
                    wx: worldx,
                    wy: worldy,
                    color: BIOME_SPRITES[block][biome].color,
                    age: 0,
                    rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                });
            }
            else if (block === 'WT') {
                const blockbelow = this.map.getBlock(worldx, worldy-1);
                if (blockbelow === ' ') {
                    // will spawn water droplet
                    // but need to measure fall distance first
                    let j;
                    for (j = 1; j < DROPLET_FALL_LIFETIME.length; j++) {
                        if (this.map.getBlock(worldx, worldy-j) !== ' ') {
                            break;
                        }
                    }
                    // ok spawn
                    this.droplets.push({
                        x: worldx,
                        y: worldy,
                        age: 0,
                        rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                        falllifetime: DROPLET_FALL_LIFETIME[j],
                    });
                }
            }
            else if (block[0] === '-' || block[0] === '|') {  // firefly
                // abort firefly if too many fireflies
                if (this.fireflies.length > 3) continue;
                // abort firefly if wrong biome
                const biome = this.map.biomeAt(worldx, worldy);
                if (biome !== 'a'
                    && biome !== 'A'
                    && biome !== 'p'
                    && biome !== 'P'
                    && biome !== 'm'
                    && biome !== 'f') continue;
                // abort firefly if no dirt below
                const someBlockBelow = this.map.getBlock(
                    worldx+Math.floor(Math.random()*7-3),
                    worldy+Math.floor(Math.random()*7-3)-5);
                if (someBlockBelow !== 'd') continue;
                // abort firefly if no space
                const blockAboveL = this.map.getBlock(worldx-1, worldy+2);
                if (blockAboveL !== ' ' && blockAboveL[0] !== '-') continue;
                const blockAboveR = this.map.getBlock(worldx+1, worldy+2);
                if (blockAboveR !== ' ' && blockAboveR[0] !== '-') continue;
                // abort firefly if no leaf above (A*, a* or Wa*)
                let yes_firefly = false;
                for (let l = 0; l < 5; l++) {
                    const someBlockAbove = this.map.getBlock(
                        worldx+Math.floor(Math.random()*9-4),
                        worldy+Math.floor(Math.random()*7-3)+6);
                    if (someBlockAbove[0] === 'A'
                        || someBlockAbove[0] === 'a'
                        || someBlockAbove[0] === 'W'
                            && someBlockAbove[1] === 'a') {
                        yes_firefly = true;
                        break;
                    }
                }
                if (!yes_firefly) continue;

                this.fireflies.push({
                    wx: worldx,
                    wy: worldy+2,
                    age: 0,
                });
            }
            else if (block[0] === '=' && block[2] !== '0') {
                // wire particle
                this.wireparticles.push({
                    wx: worldx,
                    wy: worldy,
                    age: 0,
                    dirx: Math.random()-0.5,
                    diry: Math.random()-0.5,
                    rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                    char: block[3],
                });
            }
            // stop here, to make bubble more rare
            if (i >= 6) continue;
            if (block === 'w') {  // bubble in ocean biome
                const biome = this.map.biomeAt(worldx, worldy);
                if (biome !== 'o') continue;
                if (this.map.getBlock(worldx, worldy+1) !== 'w') continue;
                if (this.map.getBlock(worldx, worldy-1) === 'w'
                    && sy > 0) continue;
                this.bubbles.push({
                    wx: worldx,
                    wy: worldy,
                    kill: false,
                });
            }
            // stop here, to make snowflake more rare
            if (i >= 1) continue;
            if (Math.random() > 0.3) continue;
            if (block === ' ') {  // snowflake in snow biome
                const biome = this.map.biomeAt(worldx, worldy);
                if (biome !== 'i'
                    && biome !== 'I'
                    && biome !== 'T') continue;
                if (this.map.getBlock(worldx-3, worldy-2) !== ' ') continue;
                if (this.map.getBlock(worldx-6, worldy-4) !== ' ') continue;
                this.snowflakes.push({
                    wx: worldx,
                    wy: worldy,
                    age: 0,
                    rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                });
            }
        }

        for (const delani of this.player.delanis) {
            for (const item in delani.d) {
                const num = delani.d[item].c;
                let repeat = 1;
                if (num > 1) repeat = 2;
                if (num > 10) repeat = 3;
                if (num > 100) repeat = 4;
                if (num > 1000) repeat = 5;
                const block = Inventory.item2blockPlain(item);
                const sprite = BIOME_SPRITES_NO_EFFECT[block]['A'];
                for (let i = 0; i < repeat; i++) {
                    this.delanis.push({
                        age: 0,
                        wx: delani.x,
                        wy: delani.y,
                        vx: (Math.random()-0.5),
                        vy: (0.7+Math.random()*0.4),
                        rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                        char: sprite.char,
                        color: sprite.color,
                    });
                }
            }
        }
        this.player.delanis = [];

        if (this.player.warpWarmup > 0) {
            // additional ruby particle
            this.rubyparticles.push({
                wx: this.player.x,
                wy: this.player.y+5-this.player.warpWarmup*0.1,
                age: 0,
                dir: Math.random()-0.5,
            });
        }

        // render animated blocks
        for (const ani of this.player.blockanimations) {
            // skip if delayed
            // (note: this code works fine if ani.d is not defined)
            if (ani.d > 0) continue;
            const x = ani.x;
            const y = ani.y;
            if (x >= left && x < left+w && y >= top && y < top+h) {
                // animated block is within screen! display!
                const biome = this.map.biomeAt(x, y);
                if (ani.b[0] === '#') {
                    buffer[h - (y-top) -1][x-left] = Gui.makeSpookySpite(
                        ani.b, biome, this.curBgColor, x-left-w/2, y-top-h/2);
                    continue;
                }
                if (ani.b === ' ') {
                    buffer[h - (y-top) -1][x-left] = undefined;
                    continue;
                }
                buffer[h - (y-top) -1][x-left] = BIOME_SPRITES[ani.b][biome];
            }
        }
        this.player.blockanimations = this.player.blockanimations.map(
            ani=>{ani.a--; ani.d--; return ani;}).filter(ani=>ani.a>0);

        // render and update bubble particles
        for (const bubble of this.bubbles) {
            const sx = bubble.wx - left;
            const sy = bubble.wy - top;
            if (sx < 0 || sx >= w || sy < 0 || sy >= h) {
                // bubble left the screen
                bubble.kill = true;
                continue;
            }
            // display bubble
            buffer[h-sy-1][sx] = BIOME_SPRITES['bubble']['o'];
            // bubble rises up
            bubble.wy += 2;
            if (this.map.getBlock(bubble.wx, bubble.wy-1) !== 'w'
                || this.map.getBlock(bubble.wx, bubble.wy) !== 'w') {
                bubble.kill = true;
            }
        }
        this.bubbles = this.bubbles.filter(b=>!b.kill);

        // render detectors
        for (const detector of this.player.visibleDetectors) {
            if (this.time % 4 < 1) break;  // blink
            const sx = detector.x - left;
            const sy = h - (detector.y-top) -1;
            // which one to display?
            const follow = this.player.inventory.hasItem('Wo')
                && this.invComponent.selected === 'Wo';
            if (follow) {
                // skip other decorations if at edge of screen
                if (sx < 1 || sx >= w-1 || sy < 1 || sy >= h-1) continue;
                const color = BIOME_SPRITES['E']['a'].color;
                buffer[sy][sx] = {char:'O', color:color};
                buffer[sy+1][sx+1] = {char:'\\', color:color};
                buffer[sy-1][sx+1] = {char:'/', color:color};
                buffer[sy+1][sx-1] = {char:'/', color:color};
                buffer[sy-1][sx-1] = {char:'\\', color:color};
            } else {
                // skip displaying if not within screen
                if (sx < 0 || sx >= w || sy < 0 || sy >= h) continue;
                buffer[sy][sx] = BIOME_SPRITES['E']['a'];
            }
        }

        // warp warmup animation
        if (this.player.warpWarmup > 0) {
            const sx = this.player.x - left;
            const sy = this.player.y - top + 5;
            if (sx>=0 && sx<w && sy>=0 && sy<h) {
                buffer[h - sy - 1][sx] = {char:' '};
            }
        }

        // render ghosts
        for (const g of this.player.visibleGhosts) {
            if (!this.player.inventory.hasItem('E')) break;
            if (this.invComponent.selected !== 'E') break;
            const sx = g.x - left;
            const sy = g.y - top;
            if (sx>=0 && sx<w && sy>=0 && sy<h) {
                const sprite = Gui.makeSpookySpite(
                        'Ws', 'a', this.curBgColor, sx-w/2, sy-h/2);
                buffer[h - sy - 1][sx] = sprite;
            }
        }

        // render other players
        for (const key of Object.keys(this.player.positionList)) {
            const p = this.player.positionList[key];
            const sx = p.x - left;
            const sy = p.y - top;
            this.drawPlayer(sx, sy, p.c, buffer);
        }

        // render my player
        {
            const sx = this.player.x - left;
            const sy = this.player.y - top;
            this.drawPlayer(sx, sy, this.player.color, buffer);
        }

        // get world pos of mouse
        const mousescreenx = this.player.mousex + Math.floor(w/2);
        const mousescreeny = this.player.mousey + Math.ceil(h/2)-1;
        const mouseworldx = left + mousescreenx;
        const mouseworldy = top + mousescreeny;
        const mouseblock = this.map.getBlock(mouseworldx, mouseworldy);

        // display other player name
        for (const key of Object.keys(this.player.positionList)) {
            const p = this.player.positionList[key];
            if (p.x === mouseworldx && p.y === mouseworldy || this.tablistComponent.isShown()) {
                const frame = [key];
                Gui.drawFrame(buffer, frame,
                    mousescreenx-mouseworldx+p.x+1,
                    -this.player.mousey + Math.floor(h/2) + mouseworldy-p.y-1,
                    '#'+p.c);
            }
        }

        // display warp target
        if (mouseblock === 'WW') {
            const tileEntity = this.map.getTileEntityAt(mouseworldx, mouseworldy);
            if (tileEntity.length > 0){
                const warpx = tileEntity[0].d.tx;
                const warpy = tileEntity[0].d.ty;
                let label = 'Warp to ' + warpx + ', ' + warpy
                if (this.map.getBlock(mouseworldx, mouseworldy+6) !== 'R1') {
                    label += ' (unpowered)';
                }
                const frame = [label];
                Gui.drawFrame(buffer, frame, mousescreenx+1, -this.player.mousey + Math.floor(h/2)-1, '#404040');
            }
        }

        // display sign text
        if (mouseblock === 'si') {
            const tileEntity = this.map.getTileEntityAt(mouseworldx, mouseworldy);
            if (tileEntity.length > 0){
                const formatted = [];
                const lines = AASTRING_DECODE_WITH_WRAPPING(
                    tileEntity[0].d.t, 20, '#ccc');
                Gui.drawFormatted(buffer, lines, mousescreenx+1, -this.player.mousey + Math.floor(h/2)-1);
            }
        }

        // render coordinates
        if(w > 11 && h > 8){
            // render coordinates frame
            const textX = this.player.x.toString();
            const textY = this.player.y.toString();
            const textB = BIOMES[currentBiome].short;
            const width = Math.max(textX.length, textY.length, 6);
            const border = new Array(width+4).join('─');
            const frame =
                [
                    border+'┐',
                    'x: ' + (textX+'                                              ').substring(0, width) + '│',
                    'y: ' + (textY+'                                              ').substring(0, width) + '│',
                    (textB+'                                              ').substring(0, width+3) + '│',
                    border+'┘',
                ]
            Gui.drawFrame(buffer, frame, 0, 0, GUI_FRAME_COLOR);
        }

        // before rendering craft, update mouse hover in craft
        if (this.craftComponent.isShown()) {
            this.craftComponent.previewRecipeByMouseHover(
                mousescreenx, h - mousescreeny - 2);
        }

        // render all shown UI components
        for (const component of this.components) {
            if (component.isShown()) {
                component.drawOnBuffer(buffer, this.time);
            }
        }

        // display all of this on screen
        for(let y = 0; y < h; y++){
            for(let x = 0; x < w; x++){
                if(buffer[y][x]){
                    let posx = x;
                    let posy = y;
                    if (typeof buffer[y][x].secondchar !== 'undefined') {
                        const secondchar = buffer[y][x].secondchar;
                        this.terminal.displayCharacterScaled(
                            posx + secondchar.dx,
                            posy + secondchar.dy,
                            secondchar.scale,
                            secondchar.char,
                            secondchar.color);
                    }
                    if (buffer[y][x].sway) {
                        // animated leaves
                        posx += Math.sin(x/10+y/10+this.time*0.03+1)*0.15
                            +Math.sin(x/3+y/3+this.time*0.1+2)*0.05;
                        posy += Math.sin(x/10+this.time*0.03+2)*0.08
                            +Math.sin(x/3+this.time*0.1+3)*0.02;
                    }
                    if (buffer[y][x].blinkspeed !== undefined) {
                        let shade = 0;
                        const baseColor = '#333333';
                        const addColor = '#bbbbbb';
                        switch (buffer[y][x].blinkspeed) {
                            case 1:
                                // blink slowly
                                shade = Math.cos(this.time*0.314)*0.5+0.5;
                                break;
                            case 2:
                                // blink faster
                                shade = Math.cos(this.time*0.628)*0.5+0.5;
                                break;
                            case 3:
                                // blink very fast
                                shade = Math.cos(this.time*1.256)*0.5+0.5;
                                break;
                            case 4:
                                // blink super fast
                                shade = this.time % 2 ? 1 : 0.3
                                break;
                        }
                        const newColor = MIX_COLORS_HEX(baseColor, addColor, shade);
                        buffer[y][x].color = newColor;
                    }
                    if (buffer[y][x].misplace !== undefined) {
                        // misplaced t
                        const wx = x + left;
                        const wy = top + h - y;

                        const which_t = ((wx*
                            4250351207
                        )%
                            51283
                            +(wy*
                                4190530093
                            )%
                            49811
                        )%8;

                        posx += (Math.abs(which_t) - 4)/32
                            * buffer[y][x].misplace;
                    }
                    if (typeof buffer[y][x].dy !== 'undefined') {
                        posy -= buffer[y][x].dy;
                    }
                    this.terminal.displayCharacter(
                        posx,
                        posy,
                        buffer[y][x].char,
                        buffer[y][x].color);
                }
            }
        }

        // display cursor overlayed
        if (this.tablistComponent.isShown()) {
            const cursorPos = this.tablistComponent.getHoverScreenPos(
                mousescreenx, h - mousescreeny - 1);
            this.drawTablistCursor(buffer, cursorPos);
        }
        if(!this.helpComponent.isShown()){
            if (this.invComponent.isShown()) {
                // Does not need to be focused to show mouse hover effect
                this.drawInvCursor(buffer,
                    this.invComponent.getHoverScreenPos(
                        mousescreenx, h - mousescreeny - 2), true);
                // Needs to be focused to show the real item cursor
                if (this.invComponent.focused) {
                    this.drawInvCursor(buffer,
                        this.invComponent.getCursorScreenPos());
                }
            }
            if (this.chestComponent.isShown()) {
                // Does not need to be focused to show mouse hover effect
                this.drawInvCursor(buffer,
                    this.chestComponent.getHoverScreenPos(
                        mousescreenx, h - mousescreeny - 2), true);
                // Needs to be focused to show the real item cursor
                if (this.chestComponent.focused) {
                    this.drawInvCursor(buffer,
                        this.chestComponent.getCursorScreenPos());
                }
            }
            if (this.craftComponent.isShown()) {
                // Does not need to be focused to have mouse hover effect
                this.drawInvCursor(buffer,
                    this.craftComponent.getHoverScreenPos(
                        mousescreenx, h - mousescreeny - 2), true);
                // Needs to be focused to show the real item cursor
                if (this.craftComponent.focused) {
                    this.drawInvCursor(buffer,
                        this.craftComponent.getCursorScreenPos());
                }
            }
            if (this.deleteComponent.isShown()) {
                // Does not need to be focused to show mouse hover effect
                this.drawInvCursor(buffer,
                    this.deleteComponent.getHoverScreenPos(
                        mousescreenx, h - mousescreeny - 2), true);
                // Needs to be focused to show the real item cursor
                if (this.deleteComponent.focused) {
                    this.drawInvCursor(buffer,
                        this.deleteComponent.getCursorScreenPos());
                }
            }
            if (this.hotbarInvComponent.isShown()) {
                // Does not need to be focused to have mouse hover effect
                this.drawInvCursor(buffer,
                    this.hotbarInvComponent.getHoverScreenPos(
                        mousescreenx, h - mousescreeny - 2), true);
                // Needs to be focused to show the real item cursor
                if (this.hotbarInvComponent.focused) {
                    this.drawInvCursor(buffer,
                        this.hotbarInvComponent.getCursorScreenPos());
                }
            }
            if (!this.invComponent.isShown()) {
                // No inventories are being shown.
                // ░ █ ▇
                const x = Math.floor(w / 2) + this.player.cursorx;
                const y = h - Math.ceil(h / 2) - this.player.cursory;
                let char = '▇';
                if (this.player.digtime > 0) char = '▓';
                if (this.player.digtime > this.player.digduration*0.35) char = '▒';
                if (this.player.digtime > this.player.digduration*0.7) char = '░';
                if (!buffer[y]) {
                    // cursor is in an invalid spot
                }
                else if (!buffer[y][x]) {
                    // cursor in air

                    this.terminal.displayCharacter(x, y, char,'#333');
                }
                else if (buffer[y][x].char === '▀') {
                    // cursor on sign is completely different
                    char = '▀';
                    let color = '#aaa';
                    if (this.player.digtime > 0) {char = '░'; color = this.curBgColor}
                    if (this.player.digtime > this.player.digduration*0.35) {char = '▒'; color = this.curBgColor}
                    if (this.player.digtime > this.player.digduration*0.7) {char = '▓'; color = this.curBgColor}
                    this.terminal.displayCharacter(x, y, char, color);
                }
                else if (buffer[y][x].char === '▇') {
                    // cursor on frame
                    this.terminal.displayCharacter(x, y, char, '#aaa');
                }
                else {
                    // cursor on something else.
                    this.terminal.displayCharacter(x, y, char,
                        buffer[y][x].color);
                    let posx = x;
                    let posy = y;
                    if (typeof buffer[y][x].secondchar !== 'undefined') {
                        const secondchar = buffer[y][x].secondchar;
                        this.terminal.displayCharacterScaled(
                            posx + secondchar.dx,
                            posy + secondchar.dy,
                            secondchar.scale,
                            secondchar.char,
                            secondchar.color);
                    }
                    if (buffer[y][x].sway) {
                        // animated leaves
                        posx += Math.sin(x/10+y/10+this.time*0.03+1)*0.15
                            +Math.sin(x/3+y/3+this.time*0.1+2)*0.05;
                        posy += Math.sin(x/10+this.time*0.03+2)*0.08
                            +Math.sin(x/3+this.time*0.1+3)*0.02;
                    }
                    if (buffer[y][x].misplace !== undefined) {
                        // misplaced t
                        const wx = x + left;
                        const wy = top + h - y;

                        const which_t = ((wx*
                            4250351207
                        )%
                            51283
                            +(wy*
                                4190530093
                            )%
                            49811
                        )%8;

                        posx += (Math.abs(which_t) - 4)/32
                            * buffer[y][x].misplace;
                    }
                    if (typeof buffer[y][x].dy !== 'undefined') {
                        posy -= buffer[y][x].dy;
                    }
                    this.terminal.displayCharacter(
                        posx,
                        posy,
                        buffer[y][x].char,
                        '#000');
                }
            }
        }
        // particles
        for (const leaf of this.leaves) {
            const leafx = leaf.wx - left;
            const leafy = leaf.wy - top;
            this.terminal.displayCharacterRotated(
                leafx + leaf.age*-0.2,
                h-leafy-1 + leaf.age*0.05 + Math.sin(leaf.age*0.2)*0.2,
                leaf.age*-0.1*leaf.rot,
                (1-leaf.age/LEAF_LIFETIME)*0.8,
                'A',
                leaf.color
            );
        }
        for (const snowflake of this.snowflakes) {
            const snowflakex = snowflake.wx - left;
            const snowflakey = snowflake.wy - top;
            this.terminal.displayCharacterRotated(
                snowflakex + snowflake.age*-0.2,
                h-snowflakey-1 + snowflake.age*0.05 + Math.sin(snowflake.age*0.2)*0.2,
                snowflake.age*-0.1*snowflake.rot,
                (1 - Math.cos(snowflake.age/SNOWFLAKE_LIFETIME*6.28))*0.2,
                '*',
                '#ffffff'
            );
        }
        for (const wp of this.wireparticles) {
            const wpx = wp.wx - left;
            const wpy = wp.wy - top;
            this.terminal.displayCharacterRotated(
                wpx + wp.age*0.3 * wp.dirx,
                h-wpy-1 + wp.age*0.2 * wp.diry,
                wp.age*-0.1*wp.rot,
                (1-wp.age/WIREPARTICLE_LIFETIME)*0.7,
                wp.char,
                '#c077a4',
            );
        }
        for (const firefly of this.fireflies) {
            const fireflyx = firefly.wx - left;
            const fireflyy = firefly.wy - top;
            this.terminal.displayCharacterAlpha(
                fireflyx + Math.sin(firefly.age*0.3) + Math.sin(firefly.age*0.04)*2,
                h-fireflyy-1 + Math.sin(firefly.age*0.2) - Math.sin(firefly.age*0.05),
                (1 - Math.pow(Math.cos(firefly.age/FIREFLY_LIFETIME*6.28)/2+0.5,2))*0.5,
                '.',
                '#fcc63c',
            );
        }
        for (const droplet of this.droplets) {
            droplet.y -= droplet.age;
            const dropletx = droplet.x - left;
            const droplety = droplet.y - top;
            this.terminal.displayCharacterSafe(
                dropletx,
                h-droplety-1,
                '.',
                "#5555aa",
            );
        }
        for (const ruby of this.rubyparticles) {
            const rubyx = ruby.wx - left;
            const rubyy = ruby.wy - top;
            this.terminal.displayCharacterRotated(
                rubyx + (-ruby.age+ruby.age*ruby.age*0.05)*ruby.dir*0.5,
                h-rubyy-1 - ruby.age*-0.31,
                0,
                (1-ruby.age/50)*0.8,
                '*',
                BIOME_SPRITES['R1'][currentBiome].color
            );
        }
        if (this.player.warpWarmup > 0) {
            const a = this.player.warpWarmup;
            const b = 0.005 * a * a + (a > 15 ? (a-15)*0.7 : 0);
            const wx = this.player.x;
            const wy = this.player.y + 5 - b;
            this.terminal.displayCharacter(
                wx - left,
                h - (wy-top) - 1,
                'R',
                BIOME_SPRITES['R1'][currentBiome].color
            );
        }
        for (const delani of this.delanis) {
            const delanix = delani.wx - left;
            const delaniy = delani.wy - top;
            this.terminal.displayCharacterRotated(
                delanix + delani.age*delani.vx,
                h-delaniy-1 - Math.sin(delani.age*0.5)*delani.vy,
                delani.age*-0.3*(delani.rot),
                (1-delani.age/8),
                delani.char,
                delani.color
            );
        }
        // particle death
        this.leaves = this.leaves.map(
            l=>{l.age++; return l;}).filter(l=>l.age<LEAF_LIFETIME);
        this.snowflakes = this.snowflakes.map(
            s=>{s.age++; return s;}).filter(s=>s.age<SNOWFLAKE_LIFETIME);
        this.wireparticles = this.wireparticles.map(
            p=>{p.age++; return p;}).filter(p=>p.age<WIREPARTICLE_LIFETIME);
        this.fireflies = this.fireflies.map(
            s=>{s.age++; return s;}).filter(s=>s.age<FIREFLY_LIFETIME);
        this.droplets = this.droplets.map(
            d=>{d.age++; return d;})
            .filter(d=>d.age<d.falllifetime);
        this.rubyparticles = this.rubyparticles.map(
            r=>{r.age++; return r;}).filter(r=>r.age<20);
        this.delanis = this.delanis.map(
            d=>{d.age++; return d;}).filter(d=>d.age<8);
    }

    // TODO: combine this with display()
    displayOverview(){
        const scale = this.overviewzoom;
        const currentBiome = this.map.biomeAt(
            this.player.x, this.player.y);
        const newBgColor = BIOMES[currentBiome].sky;

        this.terminal.clearScreen();
        this.terminal.setBackground(newBgColor);

        const w = this.terminal.width;
        const h = this.terminal.height;
        const ww = Math.floor(w / 2);
        const hh = Math.floor(h / 2);
        // adjust player coords to be divisible by scale
        const px = Math.floor(this.player.x / scale) * scale;
        const py = Math.floor(this.player.y / scale) * scale;

        const buffer = [];

        // render the terrain / world / map
        for(let y = 0; y < h; y++){
            buffer[h - y - 1] = [];
            for(let x = 0; x < w; x++){
                const dxmid = (w/2-x)*scale;
                const dymid = (h/2-y)*scale;
                if (dxmid*dxmid+dymid*dymid*2 > 200000) continue;

                // find world coords of block to display
                // but the coordinates have to be divisible by scale
                const worldx = px - Math.floor(w/2)*scale + x*scale;
                const worldy = py - Math.floor(h/2)*scale + y*scale;

                if (this.settings.s['chunkbordervisible'] &&
                    (worldx % 256 === 0 || worldy % 256 === 0)) {
                    buffer[h - y - 1][x] = {char: '▖', color: '#f00'};
                    continue;
                }

                const block = this.map.getBlock(worldx, worldy);
                const biome = this.map.biomeAt(worldx, worldy);

                if (block[0] === '#') { // hidden in overview
                    continue;
                }
                else if (block === 'B' || block === 'd') {
                    const wx = x + Math.floor(px/scale);
                    const wy = y + Math.floor(py/scale);
                    const which_B = ((wx*
                        4250351207
                    )%
                        51283
                        +(wy*
                            4190530093
                        )%
                        49811
                    )%8;

                    buffer[h - y - 1][x] = BIOME_SPRITES[block+Math.abs(which_B)][biome];
                }
                else if (block[0] === '=') {
                    const trimBlock = block.substr(0,3);
                    let sprite = BIOME_SPRITES[trimBlock][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if(BIOME_SPRITES[block]){ // fails for air
                    let sprite = BIOME_SPRITES[block][biome];
                    if (sprite.rainbow) {
                        const which_rainbow = (worldx%8+8)%8;
                        sprite = BIOME_SPRITES[block+which_rainbow][biome];
                    }
                    buffer[h - y - 1][x] = sprite;
                }
            }
        }

        // render other players
        for (const key of Object.keys(this.player.positionList)) {
            const p = this.player.positionList[key];
            const sx = Math.floor((p.x - px) / scale) + ww;
            const sy = Math.floor((p.y - py) / scale) + hh;
            if (sx>=0 && sx<w && sy>=0 && sy<h
                && p.x < this.player.x+512 && p.x > this.player.x-512
                && p.y < this.player.y+384 && p.y > this.player.y-384) {
                buffer[h - sy - 1][sx] = {char:'P', color:'#'+p.c};
            }
        }

        // render my player
        {
            buffer[h - hh - 1][ww] = {char:'P', color:'#'+this.player.color};
        }

        // display all of this on screen

        for(let y = 0; y < h; y++){
            for(let x = 0; x < w; x++){
                if(buffer[y][x]){
                    this.terminal.displayCharacter(
                        x,
                        y,
                        buffer[y][x].char,
                        buffer[y][x].color);
                }
            }
        }
    }

    focus(){
        // nothing to do, the gui is just a display and does not require focus for anything
    }

    blur(){
        // nothing to do, the gui is just a display and does not require focus for anything
    }
}
