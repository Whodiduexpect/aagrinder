// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

class GuiAnimation{
    constructor(terminal, map){
        this.map = map;
        this.terminal = terminal;
        this.frame = 0;
        this.framecnt = 0;
        this.interval = undefined;
    }

    ghostAnimation(x1, y1, x2, y2) {
        this.frame = 0;
        this.framecnt = 40;
        return new Promise(resolve => {
            this.interval = setInterval(() => {
                this.ghostAnimationTick(x1, y1, x2, y2, this.frame);
                this.frame++;
                if (this.frame >= this.framecnt) {
                    // end animation
                    clearInterval(this.interval);
                    resolve(true);
                }
            }, 30);
        });
    }

    ghostAnimationTick(x1, y1, x2, y2, frame) {
        this.terminal.clearScreen();

        { // first position
            const centerx = x1;
            const centery = y1;

            const w = this.terminal.width;
            const h = this.terminal.height;
            const left = centerx - Math.floor(w / 2);
            const top = centery - Math.ceil(h / 2) + 1;

            // render the terrain
            for(let y = 0; y < h; y++){
                for(let x = 0; x < w; x++){
                    const radHere = Math.pow((left+x-centerx)*this.terminal.characterAspectRatio(),2)+Math.pow(top+y-centery,2);
                    if(radHere > 200) continue;
                    const block = this.map.getBlock(left + x, top + y);
                    let sprite = false;

                    if (block === '#') { // priority because this is not a normal sprite
                        let shade = Math.floor(100 - ((y-h/2)*(y-h/2)+(x-w/2)*(x-w/2))*0.09);
                        if (shade < 0) shade = 0;
                        let color = shade.toString(16);
                        if (color.length === 1) color = '0' + color;
                        color = '#' + color + color + color;
                        sprite = {
                            char: '#',
                            color: color,
                        };
                    }
                    else if(SPRITES[block]){ // fails for air and player
                        sprite = SPRITES[block];
                    }
                    else if(block[0] === 'P'){
                        sprite = {
                            char: 'P',
                            color: '#' + block.substring(1)
                        };
                    }

                    if (sprite) {
                        const angle = (Math.pow(frame+4,2)-16)*0.3*radHere*0.0002;
                        this.terminal.displayCharacterOffsetPolar(
                            x, h - y - 1, angle, (this.framecnt-frame)/this.framecnt, sprite.char, sprite.color);
                    }


                }
            }
        }
        { // second position
            const centerx = x2;
            const centery = y2;

            const w = this.terminal.width;
            const h = this.terminal.height;
            const left = centerx - Math.floor(w / 2);
            const top = centery - Math.ceil(h / 2) + 1;

            // render the terrain
            for(let y = 0; y < h; y++){
                for(let x = 0; x < w; x++){
                    const radHere = Math.pow((left+x-centerx)*this.terminal.characterAspectRatio(),2)+Math.pow(top+y-centery,2);
                    if(radHere > 50) continue;
                    const block = this.map.getBlock(left + x, top + y);
                    let sprite = false;

                    if (block === '#') { // priority because this is not a normal sprite
                        let shade = Math.floor(100 - ((y-h/2)*(y-h/2)+(x-w/2)*(x-w/2))*0.09);
                        if (shade < 0) shade = 0;
                        let color = shade.toString(16);
                        if (color.length === 1) color = '0' + color;
                        color = '#' + color + color + color;
                        sprite = {
                            char: '#',
                            color: color,
                        };
                    }
                    else if(SPRITES[block]){ // fails for air and player
                        sprite = SPRITES[block];
                    }
                    else if(block[0] === 'P'){
                        sprite = {
                            char: 'P',
                            color: '#' + block.substring(1)
                        };
                    }

                    if (sprite) {
                        const angle = -Math.pow(this.framecnt-frame,2)*0.3*radHere*0.0002;
                        this.terminal.displayCharacterOffsetPolar(
                            x, h - y - 1, angle, frame/this.framecnt, sprite.char, sprite.color);
                    }

                }
            }
        }

    }

}
