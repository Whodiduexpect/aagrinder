// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


// Wrapper for terminal
// Displays a static image

"use strict";

class GuiStatic {
    constructor(terminal){
        this.terminal = terminal;
        this.time = 0;
        this.xpos = 0;
        this.ypos = 0;
        this.leaves = [];
    }

    clearScreen(){
        this.terminal.clearScreen();
    }

    displayWithWatermark(content, xpos = 0, ypos = 0, watermark, watermark2){
        this.time++;
        this.xpos = xpos;
        this.ypos = ypos;
        const h = this.terminal.height;
        const w = this.terminal.width;
        const top = Math.max(this.ypos,0);
        const left = Math.max(this.xpos,0);
        const bottom = Math.min(this.ypos+content.length, h);
        const right = Math.min(this.xpos+content[0].length, w);

        const wtop = this.terminal.height - watermark.length;
        const ww = Math.max(...watermark.map(a=>a.length));


        // render the terrain
        for(let y = top; y < bottom; y++){
            for(let x = left; x < right; x++){
                if(x > ww || y < wtop){
                    const block = content[y-this.ypos][x-this.xpos];
                    if(BIOME_SPRITES[block]){ // fails for air and player
                        if (block === 'B') {
                            // miscolored B
                            // openssl prime -generate -bits 16


                            const which_B = ((x*
                                4250351207
                            )%
                                51283
                                +(y*
                                    4190530093
                                )%
                                49811
                            )%8;

                            // const sprite_B = BIOME_SPRITES['B'+which_B]['A'];
                            this.terminal.displayCharacter(
                                x,
                                y,
                                BIOME_SPRITES[block+which_B]['A'].char,
                                BIOME_SPRITES[block+which_B]['A'].color);
                        }
                        else if (block === 'Aa') {

                            // animated leaves
                            this.terminal.displayCharacter(
                                x
                                +Math.sin(x/10+y/10+this.time*0.03+1)*0.15
                                +Math.sin(x/3+y/3+this.time*0.1+2)*0.05
                                ,y
                                +Math.sin(x/10+this.time*0.03+2)*0.08
                                +Math.sin(x/3+this.time*0.1+3)*0.02
                                ,'A',
                                BIOME_SPRITES[block]['A'].color);
                        }
                        else {
                            let ypos = y;
                            if (typeof BIOME_SPRITES[block]['A'].dy !== 'undefined') {
                                ypos -= BIOME_SPRITES[block]['A'].dy ;
                            }
                            this.terminal.displayCharacter(
                                x,
                                ypos,
                                BIOME_SPRITES[block]['A'].char,
                                BIOME_SPRITES[block]['A'].color);
                        }
                    }
                }
            }
        }

        for (let i = 0; i < 4; i++){ // try spawning new leaf particle
            const x = Math.floor(Math.random()*w);
            const y = Math.floor(Math.random()*h);
            if(x > ww || y < wtop){
                const block = content[y-this.ypos][x-this.xpos];
                if (block === 'Aa') {
                    this.leaves.push({
                        x: x,
                        y: y,
                        age: 0,
                        rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                    });
                }
            }
        }

        for (const leaf of this.leaves) {
            this.terminal.displayCharacterRotated(
                leaf.x + leaf.age*-0.2,
                leaf.y + leaf.age*0.05 + Math.sin(leaf.age*0.2)*0.2,
                leaf.age*-0.1*leaf.rot,
                (1-leaf.age/100)*0.8,
                'A',
                BIOME_SPRITES['Aa']['A'].color
            );
        }
        this.leaves = this.leaves.map(
            l=>{l.age++; return l;}).filter(l=>l.age<100);

        // render the watermark
        for(let y = 0; y < watermark.length; y++){
            for(let x = 0; x < watermark[y].length; x++){
                const character = watermark[y][x];
                this.terminal.displayCharacter(
                    x,
                    wtop + y,
                    watermark[y][x],
                    y>0 ? '#1037ff' : '#7f7f7f'); // ugly color fix
            }
        }

        // render the watermark2
        for(let y = 0; y < watermark2.length; y++){
            for(let x = 0; x < watermark2[y].length; x++){
                const character = watermark2[y][x];
                this.terminal.displayCharacter(
                    x,
                    wtop + y,
                    watermark2[y][x],
                    y>0 ? '#1037ff' : '#7f7f7f'); // ugly color fix
            }
        }
    }


    charToCoords(content, x, y) {
        return {x:x-this.xpos, y:y-this.ypos};
    }

}
