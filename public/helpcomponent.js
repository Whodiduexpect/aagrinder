// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A different representation of data in invcomponent,
 * to be able to display a fully featured hotbar in the UI.
 * Additionally, display hunger
 */

"use strict";


class HelpComponent extends UiComponent {
    constructor(){
        super();
        this.helpvisible = 0;
    }

    static GUI_HELP_LEFT_COLOR = '#aaaa88';
    static GUI_HELP_RIGHT_COLOR = '#aa88aa';

    static HELP_1 =
        [
            ['move '+PLAYER_STRING, 'wasd'],
            ['break', 'left mouse'],
            ['place', 'right mouse'],
            ['sprint', 'shift'],
            ['inventory', 'e'],
            ['crafting', 'c'],
            ['delete items', 'x'],
            ['chat', 'enter'],
            ['map', 'm'],
            ['zoom map', '+   -'],
            ['more help', 'h'],
        ];

    static HELP_2 =
        [
            ['move '+PLAYER_STRING, 'wasd'],
            ['move cursor', '←↑→↓'],
            ['break', 'r  left mouse'],
            ['place', 'q  right mouse'],
            ['interact', 'q  right mouse'],
            ['pick block', 'middle mouse'],
            ['scroll hotbar', ',  .'],
            ['jump', 'space  w'],
            ['sprint', 'shift'],
            ['inventory', 'e  i'],
            ['crafting', 'c'],
            ['delete items', 'x'],
            ['focus chat', 'enter'],
            ['map', 'm'],
            ['zoom map', '+  =  -'],
            ['list commands', '/help'],
            ['map keys', '/map'],
            ['settings', '/set'],
            ['watch credits', '/credits'],
        ];

    getFramePos() {
        if (this.helpvisible === 2) {
            return {
                x: Math.floor(this.w/2) - 17,
                y: Math.floor(this.h/2) - 9,
            };
        }
        return {
            x: Math.floor(this.w/2) - 13,
            y: Math.floor(this.h/2) - 4,
        };
    }

    getFrame(time) {

        let content;
        let firstLine;
        let leftEmpty;
        let rightEmpty;
        let lastLine;
        if (this.helpvisible === 1) {
            content = HelpComponent.HELP_1;
            firstLine = '┌─────────────────────────┐';
            leftEmpty =  '              '            ;
            rightEmpty =               '           ' ;
            lastLine =  '└─────────────────────────┘';
        }
        else if(this.helpvisible === 2){
            content = HelpComponent.HELP_2;
            firstLine = '┌────────────────────────────────┐';
            leftEmpty =  '                 '                ;
            rightEmpty =                  '               ' ;
            lastLine =  '└────────────────────────────────┘';
        }
        // Gui.drawFrame(buffer, ["P = "+YOU_STRING], frameX + 8, frameY + 1, '#'+this.player.color);
        const frame = [firstLine];
        for (const h of content) {
            const left = '\\['+HelpComponent.GUI_HELP_LEFT_COLOR+'\\]'
                +h[0]+'\\[/\\]'
                +leftEmpty.substr(h[0].length);
            const right = '\\['+HelpComponent.GUI_HELP_RIGHT_COLOR+'\\]'
                +h[1]+'\\[/\\]'
                +rightEmpty.substr(h[1].length);
            frame.push('│'+left+right+'│');
        }
        frame.push(lastLine);
        return {f: frame, e: []};
    }

    isShown() {
        return this.helpvisible > 0;
    }
}
