// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A different representation of data in invcomponent,
 * to be able to display a fully featured hotbar in the UI.
 * Additionally, display hunger
 */

"use strict";

class HotbarComponent extends UiComponent {
    constructor(hotbarInvComponent, player){
        super();
        this.hotbarInvComponent = hotbarInvComponent;
        this.player = player;
    }

    getFramePos() {
        return {
            x: Math.floor(this.w/2) - 6,
            y: Math.floor(this.h) - 4,
        };
    }

    isClickWithin(x, y) {
        const local = this.screenToLocal(x, y);
        if (local.x >= 0 && local.x < 12
            && local.y >= 0 && local.y < 4) {
            return true;
        }
        return false;
    }

    // click is in character coordinates
    handleClick(x, y) {
        const local = this.screenToLocal(x, y);
        if (local.x < 1) return false;
        if (local.x > this.hotbarInvComponent.slots.length) return false;
        if (local.y < 1) return false;
        if (local.y > 3) return false;
        this.hotbarInvComponent.cursor = local.x - 1;
        return true;
    }
    makeHotbarSprite(item) {
        if (item === 'E') {
            const blinkblock = 'E'+this.player.detectorBlinkSpeed;
            return BIOME_SPRITES[blinkblock]['A'];
        }
        if (item === 'Wo') {
            const blinkblock = 'Wo'+this.player.followBlinkSpeed;
            return BIOME_SPRITES[blinkblock]['A'];
        }
        const block = Inventory.item2blockPlain(item);
        return BIOME_SPRITES_NO_EFFECT[block]['A'];
    }

    getFrame(time) {
        const extraSprites = [];

        const slotNumber = this.hotbarInvComponent.cursor;
        const selected = this.hotbarInvComponent.getSelected();
        let text = '    ';

        if (this.player.inventory.hasItem(selected)
            && selected[0] === 'G') {
            // hotbar with durability
            const index = this.hotbarInvComponent.getCursorItemSlotIndex();
            const dur = this.player.inventory.state[selected].d[index].d;
            const max = Inventory.item_data_templates[selected].d;
            const percent = dur === max ? ''
                : Math.floor(dur/max*100)+'%';
            text = ' '+percent;
        } else if (selected !== null) {
            // hotbar with item amount
            const amount = this.player.inventory.numItems(selected);
            if (amount === 0) {
                text = ' -';
            } else if (amount > 999) {
                text = '999+';
            } else {
                text = ' '+amount.toString();
            }
        }

        if (selected !== null) {
            extraSprites.push({
                dx: 1,
                dy: 1,
                sprite: this.makeHotbarSprite(selected),
            });
        }

        const lineLeft = Array(slotNumber).fill('─').join('');
        const lineRight = Array(9-slotNumber).fill('─').join('');

        const frame =
            [
                '┌─────┐@@@@@',
                '│ '+(text+'    ').substring(0, 4)+'└────┐',
                '│          │',
                '└'+lineLeft+'┴'+lineRight+'┘',
            ];

        for (let i = 0; i < this.hotbarInvComponent.slots.length; i++) {
            const item_code = this.hotbarInvComponent.slots[i];
            if (item_code === null) continue;
            const item = this.hotbarInvComponent.slots[i];
            extraSprites.push({
                dx: 1+i,
                dy: 2,
                sprite: this.makeHotbarSprite(item_code),
            });
        }

        // render hunger above hotbar
        let shade = Math.sin(time*0.3)*0.3+0.7;
        const baseColor = '#000000';
        const addColor = '#cf1005';
        const newColor = MIX_COLORS_HEX(baseColor, addColor, shade);
        if (this.player.food === 0) {
            for (let i = 0; i < 5; i++) {
                extraSprites.push({
                    dx: 7+i,
                    dy: 0,
                    sprite: {char: 'x', color: newColor},
                });
            }
        }
        for (let i = 0; i < Math.ceil(this.player.food/PLAYER_MAXFOOD*5); i++) {
            const sprite = {char: 'a', color: addColor};
            if (5*this.player.food < (i+0.2)*PLAYER_MAXFOOD) {
                sprite.color = newColor;
            }
            extraSprites.push({
                dx: 7+i,
                dy: 0,
                sprite: sprite,
            });
        }

        return {f: frame, e: extraSprites};
    }

}
