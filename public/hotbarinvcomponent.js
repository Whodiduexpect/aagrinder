// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A modified version of InventoryComponent which
 * stores slots separately, to allow arbitrary item order,
 * and only displays item types (no amounts).
 * Used for hotbar management
 */

"use strict";

class HotbarInventoryComponent extends InventoryComponent {
    constructor(xpos=0){
        super(undefined, "hotbar", xpos);
        this.slots = Array(10).fill(null);
        this.cursor = 0;
    }

    updateSlots() {
    }

    getVisualHeight() {
        return this.slots.length;
    }

    getSelected() {
        return this.slots[this.cursor];
    }

    useHotkey(nkey) {
        this.cursor = (nkey+9)%10;
    }

    getFrame() {
        const frame = [
            InventoryComponent.TITLES[this.name][this.compact ? 0 : 1],
        ]
        const extraSprites = [];

        // add recipe list
        for (let i = 0; i < this.slots.length; i++) {
            const slotHotkeyNumber = (i+1)%10;
            const item_code = this.slots[i];
            const item_name = item_code === null ? ''
                : Inventory.item_short_names[item_code];
            const line = '│' + slotHotkeyNumber + '>   '
                + (this.compact ? '' : (item_name+'               ')
                    .substring(0, 11)
                ) + '│';
            frame.push(line);
            if (item_code !== null) {
                extraSprites.push({
                    dx: 4,
                    dy: i+1,
                    sprite: BIOME_SPRITES_NO_EFFECT[Inventory.item2blockPlain(item_code)]['A'],
                });
            }
        }
        frame.push(this.compact ? '└─────┘':'└────────────────┘');
        return {f: frame, e: extraSprites};
    }

    getCursorScreenPos() {
        const pos = super.getCursorScreenPos();
        pos.x += 3;
        return pos;
    }

    getHoverScreenPos(x, y) {
        const pos = super.getHoverScreenPos(x, y);
        if (pos === null) return pos;
        pos.x += 3;
        return pos;
    }

    getCursorItemSlotIndex() {
        // TODO: THIS IS TEMPORARY.
        // Actually, HotbarInventoryComponent should remember the slot index
        // of every data item in the hotbar, which is used to recognize which
        // item data to use/display; and also, it is used to see when an item
        // disappears whether or not it was the item that is in the hotbar;
        // so we can determine whether the item should disappear from hotbar
        // too, or alternatively only update its slot number
        return 0;
    }
}
