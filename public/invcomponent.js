// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A class for holding client-side-specific inventory data.
 * This is all for the UI. It caches a slots representation
 * (which does not exist on the server), and also a cursor.
 * Both keyboard and mouse controls for navigating inventory
 * are handled here in the end.
 */

/*
 * Please note that the Inventory class has the getNumUsedSlots()
 * method which is used by both client and server and must match
 * this class by functionality.
 */

"use strict";

class InventoryComponent extends UiComponent {
    constructor(inventory, name, xpos=0){
        super();
        this.inventory = inventory;
        this.slots = [];
        this.pretendOffset = {};
        this.cursor = 0;
        this.scrollPos = 0;
        this.shown = false;
        this.focused = false;
        this.name = name;
        this.xpos = xpos;
        this.compact = false;
        this.updateSlots();
    }

    // update representation as array of slots
    updateSlots() {
        this.slots = [];

        // get the item codes in inventory (arbitrary order)
        const sorted_codes = Object.keys(this.inventory.state)
        // find item index for each item code
            .map(k => Inventory.index_by_code[k])
        // sort the indices (this makes display order consistent)
            .sort((a,b)=>a-b)
        // make it back into item codes
            .map(i => Inventory.item_definitions[i].c);

        for (const item_code of sorted_codes) {
            let how_many = this.inventory.state[item_code].c;
            if (this.pretendOffset[item_code] !== undefined) {
                how_many += this.pretendOffset[item_code];
                if (how_many < 1) continue;
            }
            const stack_size = Inventory.stack_sizes[item_code];
            while (how_many > stack_size) {
                this.slots.push({i:item_code, c:stack_size});
                how_many -= stack_size;
            }
            this.slots.push({i:item_code, c:how_many});
        }

        this.fixInvalidCursor();
    }

    fixInvalidCursor() {
        if (this.cursor >= this.slots.length) {
            this.cursor = this.slots.length - 1;
        }
        if (this.cursor < 0) {
            this.cursor = 0;
        }
    }

    getSelected() {
        if (this.slots.length < 1) return null;
        if (this.cursor < 1 || this.cursor >= this.slots.length) {
            console.log('ERROR: out of bounds cursor '+this.cursor);
        }
        return this.slots[this.cursor].i;
    }

    getYpos() {
        let ypos = Math.floor(this.h/2) - 11;
        if (ypos < 6) ypos = 6;
        if (this.h < 15) ypos = this.h - 9;
        if (ypos < 0) ypos = 0;
        return ypos;
    }

    getFramePos() {
        let xoffset = this.xpos;
        if (this.w < 42) {
            if (xoffset < 0) {
                xoffset += 12;
            }
            else {
                xoffset -= 1;
            }
            this.compact = true;
        }
        else {
            this.compact = false;
        }
        return {
            x: Math.floor(this.w/2) - 9 + xoffset,
            y: this.getYpos(),
        }
    }

    getVisualHeight() {
        const ypos = this.getYpos();
        const tslots = this.inventory.total_slots;
        const nslots = this.slots.length;
        let visualHeight = this.h - ypos - 7;
        if (visualHeight < 5) visualHeight = 5;
        if (this.h < 9) visualHeight = this.h - ypos - 4;
        if (visualHeight < 1) visualHeight = 1;
        if (visualHeight > tslots) visualHeight = tslots;
        return visualHeight;
    }

    // converts from global inventory slot index
    // to index within the slots of this same item.
    // Important for items with additional data.
    getItemSlotIndex(index) {
        // find first slot with this item
        let i = 0;
        while (i < this.slots.length) {
            if (this.slots[i].i === this.slots[index].i) {
                // found the first slot with this item!
                // calculate relative index
                return index - i;
            }
            i++;
        }

        // didn't find any slots with this item
        return -1;
    }

    // important:
    // returns the index of the selected slot
    // WITHIN THE SLOTS OF THIS SAME ITEM
    getCursorItemSlotIndex() {
        if (this.cursor < 0) return -1;
        return this.getItemSlotIndex(this.cursor);
    }

    // returns number of items in selected slot
    curSlotAmount() {
        if (this.cursor < 0) return 0;
        const slot = this.slots[this.cursor];
        return slot.c;
    }

    // returns number of items of a specified type
    numItems(item_code) {
        let amount = this.inventory.numItems(item_code);
        if (this.pretendOffset[item_code] !== undefined) {
            amount += this.pretendOffset[item_code];
        }
        return amount;
    }

    pretendGainNonDataItem(item_code, amount) {
        if (this.pretendOffset[item_code] === undefined) {
            this.pretendOffset[item_code] = 0;
        }
        this.pretendOffset[item_code] += amount;
        if (this.pretendOffset[item_code] === 0) {
            delete this.pretendOffset[item_code];
        }
    }

    clearPretend() {
        delete this.pretendOffset;
        this.pretendOffset = {};
    }

    cursorSet(cursor) {
        if (cursor >= -1 && cursor < this.slots.length) {
            this.cursor = cursor;
        }
    }

    // with 0,0 at start of canvas
    isClickWithin(x, y) {
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        if (local.x >= 0 && local.x < 2+lineWidth
            && local.y >= 0 && local.y < 2+this.getVisualHeight()) {
            return true;
        }
        return false;
    }

    // click is in character coordinates
    handleClick(x, y) {
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        const visualHeight = this.getVisualHeight();
        if (local.x > 0 && local.x < 1+lineWidth
            && local.y > 0 && local.y < 1+visualHeight
            && local.y-1+this.scrollPos < this.slots.length) {
            this.cursor = local.y-1+this.scrollPos;
            return true;
        }
        return false;
    }

    cursorUp(){
        this.cursor--;
        this.fixInvalidCursor()
        this.scrollToEnsureCursorVisible();
    }

    cursorDown(){
        this.cursor++;
        this.fixInvalidCursor()
        this.scrollToEnsureCursorVisible();
    }

    cursorUpWrap() {
        this.cursor = (this.cursor + this.slots.length - 1)
            % this.slots.length;
        this.fixInvalidCursor()
        this.scrollToEnsureCursorVisible();
    }

    cursorDownWrap() {
        this.cursor = (this.cursor + 1) % this.slots.length;
        this.fixInvalidCursor()
        this.scrollToEnsureCursorVisible();
    }

    scrollToEnsureCursorVisible() {
        if (this.cursor === -1) {
            this.scrollPos = 0;
        }
        else if (this.cursor < this.scrollPos) {
            this.scrollPos = this.cursor;
        }
        else if (this.cursor > this.scrollPos + this.getVisualHeight() - 1) {
            this.scrollPos = this.cursor - this.getVisualHeight() + 1;
        }
    }

    getMaxScrollPos() {
        const visualHeight = this.getVisualHeight();
        let maxScrollPos = this.inventory.total_slots - visualHeight;
        if (this.slots.length > this.inventory.total_slots) {
            // there are more slots than inventory capacity.
            // Allow all slots of this overfull inventory to display:
            maxScrollPos = this.slots.length - visualHeight;
        }
        // Never allow scrolling above the top item in inventory:
        if (maxScrollPos < 0) maxScrollPos = 0;
        return maxScrollPos;
    }

    scrollUp() {
        this.scrollPos--;
        if (this.scrollPos < 0) {
            this.scrollPos = 0;
        }
    }

    scrollDown() {
        this.scrollPos++;
        const maxScrollPos = this.getMaxScrollPos();
        if (this.scrollPos > maxScrollPos) {
            this.scrollPos = maxScrollPos;
        }
    }

    // used for "pick block" feature (middle mouse button)
    selectItemById(item) {
        for (let i = 0; i < this.slots.length; i++) {
            if (this.slots[i].i === item) {
                // found a slot with the requested item!
                this.cursor = i;
                return true;
            }
        }
        return false;
    }

    focus() {
        this.focused = true;
    }

    blur() {
        this.focused = false;
    }

    show() {
        this.shown = true;
    }

    hide() {
        this.shown = false;
        this.focused = false;
    }

    isShown() {
        return this.shown;
    }

    static TITLES = {
        'inventory': ['┌─Inv─┐', '┌─Inventory──────┐'],
        'chest':     ['┌Chest┐', '┌─Chest──────────┐'],
        'crafting':  ['┌Craft┐', '┌─Crafting───────┐'],
        'delete':    ['┌DELET┐', '┌─!DELETE!───────┐'],
        'hotbar':    ['┌Hotbr┐', '┌─Hotbar─────────┐'],
    }

    getFrame() {
        const visualHeight = this.getVisualHeight();
        const extraSprites = [];

        // top
        const frame = [
            InventoryComponent.TITLES[this.name][this.compact ? 0 : 1],
        ]

        const total_slots = this.inventory.total_slots;
        const displayedHeight = Math.min(total_slots, visualHeight);
        const maxScrollPos = this.getMaxScrollPos();
        const isInvFull = this.slots.length >= total_slots;
        const isInvOverfull = this.slots.length > total_slots;
        const isInventoryScrollable = maxScrollPos > 0;
        const areAllAvailableSlotsVisible = total_slots > displayedHeight;
        const isScrolledToEnd = this.scrollPos === maxScrollPos;
        const BAR = isInventoryScrollable ? '╵' : '│';
        const LINE = (areAllAvailableSlotsVisible
            && !isScrolledToEnd) ? ' ' : '─';
        for (let i = 0; i < displayedHeight; i++) {
            if (i+this.scrollPos >= this.slots.length) {
                // no more items to show
                if (this.slots.length === 0 && i === 0) {
                    // actually no items at all
                    if (this.compact) {
                        frame.push('│     │');
                        frame.push('│ no  │');
                        frame.push('│items│');
                        frame.push('│     │');
                        i+=3;
                    } else {
                        if (this.name === 'chest') {
                            frame.push('│                │');
                            frame.push('│  use spacebar  │');
                            frame.push('│  to send item  │');
                            frame.push('│                │');
                            i+=3;
                        } else if (this.name === 'crafting') {
                            frame.push('│                │');
                            frame.push('│  use spacebar  │');
                            frame.push('│  for crafting  │');
                            frame.push('│                │');
                            i+=3;
                        } else {
                            frame.push('│                │');
                            frame.push('│    no items    │');
                            frame.push('│                │');
                            i+=3;
                        }
                    }
                }
                else {
                    if (this.compact) {
                        frame.push(BAR+'     '+BAR);
                    } else {
                        const isInventoryEmpty = this.slots.length === 0;
                        const beyondEnd = this.scrollPos + i >= total_slots;
                        const emptyChar = isInventoryEmpty || beyondEnd
                            ? ' ' : '-';
                        frame.push(BAR+'  '+emptyChar+'             '+BAR);
                    }
                }
            }
            else {
                // there is a slot to show
                const item_code = this.slots[i+this.scrollPos].i;
                const number = this.nthSlotNumber(i+this.scrollPos);
                const item_name = Inventory.item_short_names[item_code];
                const line = BAR+'  ' +
                    (this.compact ? '' : (item_name+'               ')
                        .substring(0, 11)
                    )+
                    number+'   '.substring(0, 3-AASTRING_LENGTH(number)) + BAR;
                frame.push(line);
                extraSprites.push({
                    dx: 1,
                    dy: i+1,
                    sprite: BIOME_SPRITES_NO_EFFECT[Inventory.item2blockPlain(item_code)]['A'],
                });
            }
        }

        if (isInvOverfull) {
            frame.push(this.compact ? '└!OVF!┘' : '└───!OVERFULL!───┘');
        } else {
            frame.push('└─'+LINE.repeat(this.compact ? 3 : 14)+'─┘');
        }

        return {f: frame, e: extraSprites};

    }

    nthSlotNumber(n) {
        const item_code = this.slots[n].i;
        if (Inventory.isDataItem(item_code)) {
            if (item_code[0] === 'G') {
                // grinder. Display durability
                const dur = this.inventory.state[item_code]
                    .d[this.getItemSlotIndex(n)].d;
                const max = Inventory.item_data_templates[item_code].d;
                if (dur === max) {
                    // can't display '100%', too many characters
                    return '';
                }
                const percent = Math.floor(dur/max*100);
                const str = percent.toString()+'%';
                let color = '#7a7';
                if (percent < 70) color = '#897';
                if (percent < 40) color = '#987';
                if (percent < 10) color = '#955';
                return '\\['+color+'\\]'+str+'\\[/\\]';
            } else {
                // some nyi item
                return '\\[#f00\\]BUG\\[/\\]';
            }
        }
        return this.slots[n].c.toString();
    }

    getCursorScreenPos() {
        if (this.cursor === -1) {
            return null;
        }
        if (this.cursor < this.scrollPos) {
            // not visible because cursor is outside displayed area
            return null;
        }
        const visualHeight = this.getVisualHeight();
        if (this.cursor >= this.scrollPos + visualHeight) {
            // not visible because cursor is outside displayed area
            return null;
        }
        return this.localToScreen(1, 1+this.cursor-this.scrollPos);
    }

    getHoverScreenPos(x, y) {
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        const visualHeight = this.getVisualHeight();
        if (local.x >= 0 && local.x < 2+lineWidth
            && local.y >= 0
            && local.y < visualHeight
            && local.y + this.scrollPos < this.slots.length) {
            return this.localToScreen(1, 1+local.y);
        }
        return null;
    }

}
