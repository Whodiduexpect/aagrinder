// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const LAYOUTS = {

    qwerty: {
        9: 'tablist',
        13: 'chat',
        16: 'sprint',
        27: 'escape',
        32: 'jump',
        37: 'cursorleft',
        38: 'cursorup',
        39: 'cursorright',
        40: 'cursordown',
        48: 'hotkey0',
        49: 'hotkey1',
        50: 'hotkey2',
        51: 'hotkey3',
        52: 'hotkey4',
        53: 'hotkey5',
        54: 'hotkey6',
        55: 'hotkey7',
        56: 'hotkey8',
        57: 'hotkey9',
        61: 'zoomin',
        65: 'left',
        67: 'craft',
        68: 'right',
        69: 'inventory',
        72: 'help',
        73: 'inventory',
        77: 'togglemap',
        81: 'place',
        82: 'dig',
        83: 'down',
        87: 'up',
        88: 'delete',
        109: 'zoomout',
        173: 'zoomout',
        107: 'zoomin',
        171: 'zoomin',
        187: 'zoomout',
        189: 'zoomin',
        191: 'command',
        188: 'hotbarnext',
        190: 'hotbarprev',
        2001: 'dig', // lmb
        2002: 'place', // rmb
        2003: 'pick', // mmb
    },

    emptylayout: {
        13: 'chat',
        27: 'escape',
    },

};


const ACTIONS = [
    'chat',
    'command',
    'craft',
    'cursordown',
    'cursorleft',
    'cursorright',
    'cursorup',
    'hotbarnext',
    'hotbarprev',
    'hotkey0',
    'hotkey1',
    'hotkey2',
    'hotkey3',
    'hotkey4',
    'hotkey5',
    'hotkey6',
    'hotkey7',
    'hotkey8',
    'hotkey9',
    'dig',
    'down',
    'escape',
    'help',
    'inventory',
    'jump',
    'left',
    'pick',
    'place',
    'right',
    'sprint',
    'tablist',
    'togglemap',
    'up',
    'delete',
    'zoomin',
    'zoomout',
];


class Layout {


    constructor() {
        // try loading the layout
        if (!this.tryLoadLayout()) {
            // use default qwerty because loading failed
            this.changeLayout('qwerty');
        }
    }

    changeLayout(name) {
        // quit if invalid layout name
        if (!LAYOUTS[name]) return;
        this.mappings = {};
        for (const key in LAYOUTS[name]) {
            if (!LAYOUTS[name].hasOwnProperty(key)) continue;
            this.mappings[key] = LAYOUTS[name][key];
        }
    }

    tryLoadLayout() {
        // try loading
        const str = localStorage.getItem('keys');
        // success?
        if(str) {
            this.mappings = JSON.parse(str);
            // verify
            for (const key in this.mappings) {
                if (!this.mappings.hasOwnProperty(key)) continue;
                if (!ACTIONS.includes(this.mappings[key])) {
                    console.log('invalid layout in localstorage.');
                    localStorage.removeItem('keys');
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    saveLayout() {
        if(Object.keys(this.mappings).length === 0){
            localStorage.removeItem('keys');
        }
        else{
            localStorage.setItem('keys', JSON.stringify(this.mappings));
        }
    }

    keyCanMap(key) {
        if (key === 13 || key === 27) {
            // this key cannot be changed
            return false;
        }
        return true;
    }

    mapKey(key, action) {
        this.mappings[key] = action;
    }

    unmapKey(key) {
        delete this.mappings[key];
    }

}
