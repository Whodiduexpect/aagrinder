// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * The login screen.
 * When the player logs in, a Game object is created.
 * When the player logs out, the Game object is disabled.
 *
 * This class also contains the logic for the registration "program".
 */

"use strict";

const WATERMARK = [
    'Copyright 2018-2022  MRAAGH',
    'gitlab.com/MRAAGH/aagrinder',
    '',
];
const WATERMARK2 = [
    '',
    '___________________________',
    '',
];

class Login{
    constructor(cli, guiterminal, socket, keys){
        this.cli = cli;
        this.guiterminal = guiterminal;
        this.keys = keys;
        this.inGame = false;
        this.guiStatic = new GuiStatic(guiterminal);
        this.socket = socket;
        this.lastBlock = 'B';
        this.settings = SETTINGS;
        this.cli.setLineLimit(this.settings.eqs['maxchatlines']);
        this.layout = new Layout();
        this.music = new Music(this.cli, this.settings);
        this.commands = new Commands(
            null,
            this.cli,
            this.settings,
            this.layout,
            this.music
        );
        this.socket.on('connect', data=>this.onSocketConnect(data, socket));
        this.socket.on('disconnect', data=>this.onSocketDisconnect(data, socket));
        this.socket.on('loginsuccess', data=>this.onSocketLoginSuccess(data, socket));
        this.socket.on('loginerror', data=>this.onSocketLoginError(data, socket));
        this.socket.on('FORCERELOAD', data=>this.onSocketForceReload(data, socket));
        this.waitingForMusic = true;
        this.AAGRINDER_WELCOME_IMAGE =
            deserialize(256,256,AAGRINDER_WELCOME_IMAGE_COMPRESSED).terrain;
        this.AAGRINDER_WELCOME_IMAGE.reverse();

        this.guiterminal.addResizeEventListener((w,h)=>{
            if (this.loginScreenInterval) {
                this.displayStatic();
            }
        });
    }

    onSocketConnect(data){
        this.waitingForMusic = true;
        this.tryPlayingLoginMusic();
        this.welcome();
        this.login();
    }

    onSocketDisconnect(data){
        this.cli.abort();
        this.cli.println('\\[#733\\]disconnected.');
        this.inGame = false;
        this.gameStop();
    }

    onSocketLoginSuccess(data){
        console.log('login');
        this.cli.println('login successful');
        this.gameStart();
    }

    onSocketLoginError(data){
        this.cli.println('\\[#733\\]'+data.message);
        this.login();
    }

    onSocketForceReload(data){
        this.cli.println('\\[#f00\\][SERVER]: New version! Refresh page!');
        setInterval(()=> {
            this.cli.println('\\[#f00\\][SERVER]: New version! Refresh page!');
        } , 10000);
        // this is force reload. Deactivated now.
        // window.location.reload(true);
    }


    gameStart(){

        // be gone, login picture
        if(this.loginScreenInterval){
            clearInterval(this.loginScreenInterval);
        }
        // no more watermark, no need for this any more
        window.removeEventListener('mousedown', this.mouseDownHandler);

        // enter game
        this.inGame = true;
        this.music.graduallyStopMusic();
        this.music.startMusicAfterSeconds(60);
        this.keys.clearFresh();
        this.game = new Game(
            this.cli,
            this.guiterminal,
            this.socket,
            this.keys,
            this.settings,
            this.layout,
            this.music,
            this.commands
        );
        this.game.active = true;
        // make sure there's not already an interval before creating a new one
        if(this.gameTickInterval){
            clearInterval(this.gameTickInterval);
        }
        this.gameTickInterval = setInterval(()=>this.game.gameTick(), 100);

        // commands get access to this new game object
        this.commands.game = this.game;

    }

    gameStop(){

        // stop game tick
        if(this.gameTickInterval){
            clearInterval(this.gameTickInterval);
        }

        // kill game
        if(this.game){
            this.game.active = false;
            this.game.kill();
            this.cli.abort();
        }

        // commands retain access to the game.
        // I could set this.commands.game to null here
        // if I didn't want that
        // but I don't want unnecessary restrictions.
    }

    handleKeyDown(keyCode){
        this.tryPlayingLoginMusic();
        if(this.game){
            this.game.handleKeyDown(keyCode);
        }
    }

    tryPlayingLoginMusic() {
        if (this.waitingForMusic) {
            this.music.loginMusic();
            if (this.music.isPaused()) {
                console.log('AAGRINDER is not allowed to play music!');
                console.log('You need to enable Autoplay permission!');
            }
            else {
                this.waitingForMusic = false;
            }
        }
    }

    handleKeyUp(keyCode){
        if(this.game){
            this.game.handleKeyUp(keyCode);
        }
    }

    handleMouseDown(e){
        this.tryPlayingLoginMusic();
        const xy = this.guiterminal.pixelToChar(e.clientX, e.clientY, false);

        // was link clicked?
        if(xy.y===this.guiterminal.height-2 && xy.x>=0 && xy.x<WATERMARK[1].length){
            let win = window.open('https://gitlab.com/MRAAGH/aagrinder','_blank');
            win.focus();
        }

        const coords = this.guiStatic.charToCoords(this.AAGRINDER_WELCOME_IMAGE,xy.x,xy.y);
        const here = this.AAGRINDER_WELCOME_IMAGE[coords.y][coords.x];
        if (e.buttons === 1 && here !== ' ') {
            this.AAGRINDER_WELCOME_IMAGE[coords.y][coords.x] = ' ';
            this.lastBlock = here;
        }
        else if (e.buttons === 2 && here === ' ') {
            this.AAGRINDER_WELCOME_IMAGE[coords.y][coords.x] = this.lastBlock;
        }
    }

    focus(){ // window focused
        if(this.inGame){
            this.game.focus();
        }
        else{
            this.cli.focus();
        }
    }

    blur(){ // window unfocused
        this.keys.clearAll();
        this.cli.blur();
        if(this.game){
            this.game.blur();
        }
    }

    async login(){ // whole login procedure
        this.cli.abort(); // cancel existing prompt if one was open. This is more important.
        this.cli.focus();
        let nameChosen = false;
        let passwordChosen = false;
        let typedName;
        let loginName;
        let loginPassword;
        let insecureMode = false;
        while(!nameChosen){ // loop for choosing name
            typedName = await this.cli.prompt(
                'your name: ', false, 0, [], ' (type here)');
            if(/^\//.test(typedName)){
                await this.commands.executeCommand(typedName);
                continue;
            }
            const responseName = await this.ajaxVerifyUsername(typedName);
            console.log('responsename', responseName);
            // result must be "Username taken"
            // unless ... the server is in insecure mode
            if(responseName.insecure){
                // welp, insecure mode it is.
                insecureMode = true;
                loginName = responseName.name;
            }
            else if(responseName.message === "Username taken"){
                // okay so it's a good name then
                nameChosen = true;
                // but we will use the name specified by the server
                // (which may or may not have a different capitalization)
                loginName = responseName.name;
                this.cli.println('Hello, \\[#'+responseName.color+'\\]'+loginName+'\\[/\\]!');
            }
            else if(responseName.message === 'OK'){
                // is a non-registered name
                const regres = await this.register(typedName)
                if(regres.success){
                    // registration was successful.
                    // use this data for login.
                    nameChosen = true;
                    loginName = typedName;
                    passwordChosen = true;
                    loginPassword = regres.password;
                }
                else {
                    // failed to register
                    continue;
                }
            }
            else{
                // something else?
                console.log(responseName.message);
                this.cli.println('\\[#733\\]'+responseName.message)
                continue;
            }
            nameChosen = true;
        }

        // prompt for password if it hasn't been chosen yet 

        if(!passwordChosen){
            loginPassword = insecureMode ? ''
                : await this.cli.promptPassword('password for '+loginName+': ');
            if (loginPassword === '/prompt' || loginPassword === '/') {
                loginPassword = prompt('password');
            }
        }

        // try to login
        this.socket.emit('login', {
            username: loginName,
            password: loginPassword,
        });

        playerName = loginName;
    }

    welcome(){
        this.cli.println(AAGRINDER_STRING+'\\[n\\]');
        if (IS_DEBUGLOG_CLIENT) {
            this.cli.println('\\[#733\\]DEBUG CLIENT\\[/\\]\\[n\\]/log is supported.');
        }
        // display nice picture and watermark
        // (first make sure there's not already a running interval)
        if(this.loginScreenInterval){
            clearInterval(this.loginScreenInterval);
        }
        this.loginScreenInterval = setInterval(()=>this.displayStatic(), 100);
        // also allow user to click link in watermark
        window.addEventListener('mousedown', this.mouseDownHandler = e=>this.handleMouseDown(e), false);
    }

    displayStatic(){
        const w = this.guiterminal.width
        const h = this.guiterminal.height
        const focusy = 149;
        const focusx = 125;
        const screeny = Math.floor(h/2);
        const screenx = Math.floor(w*2/3);
        const posy = screeny - focusy;
        const posx = screenx - focusx;

        this.guiStatic.clearScreen();
        this.guiStatic.displayWithWatermark(this.AAGRINDER_WELCOME_IMAGE,posx,posy,WATERMARK,WATERMARK2);
    }

    async register(registerName){
        const rawYes = await this.cli.prompt('I don\'t know '+registerName+'.\\[n\\]Create player '+registerName+'? (Yes/no) ', true);
        const registerYes = rawYes === '' || rawYes[0] === 'y' || rawYes[0] === 'Y';
        if(!registerYes){
            this.cli.println('\\[#733\\]cancelled.');
            return {success: false};
        }
        let registerPassword = '';
        let askPassword2 = true;
        while (registerPassword === '') {
            askPassword2 = true;
            registerPassword = await this.cli.promptPassword('choose password: ');
            if (registerPassword === '/prompt' || registerPassword === '/') {
                registerPassword = prompt('password');
                askPassword2 = false;
            }
        }
        if (askPassword2) {
            const registerPassword2 = await this.cli.promptPassword('repeat password: ');
            if(registerPassword !== registerPassword2){
                this.cli.println('\\[#733\\]passwords do not match.');
                return {success: false};
            }
        }
        let registerColor = '';
        let loopQuit = false;
        let registerOk = false;
        while (!loopQuit) {
            const rawColor = await this.cli.prompt('what is your favorite color? ');
            registerColor = parseColor(rawColor);
            const rawOk = await this.cli.prompt(
                'color \\[#'+registerColor+'\\]#'+registerColor+'\\[/\\]? (Yes/no/cancel) ',
                true,
            );
            registerOk = rawOk === '' || rawOk[0] === 'y' || rawOk[0] === 'Y';
            loopQuit = registerOk || rawOk[0] === 'c' || rawOk[0] === 'C';
        }
        if(!registerOk){
            this.cli.println('\\[#733\\]cancelled.');
            return {success: false};
        }
        this.cli.println('requesting new user');
        const response = await this.ajaxRegister(registerName, registerPassword, registerColor);
        if (response.success) {
            // yey
            this.cli.println('\\[#373\\]'+response.message);
            return {success: true, password: registerPassword};
        }
        else {
            // ney
            this.cli.println('\\[#733\\]'+response.message);
            return {success: false};
        }
    }

    async ajaxVerifyUsername(name){
        const rawdata = {
            name: name,
        };
        const url = '/api/users/checkname';
        return ajax(rawdata, url);
    }

    async ajaxRegister(name, password, color){
        const rawdata = {
            name: name,
            password: password,
            color: color,
        };
        const url = '/api/users/register';
        return ajax(rawdata, url);
    }


}
