// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class Music{
    constructor(cli, settings){
        this.cli = cli;
        this.settings = settings;

        this.songList = [
            {
                file: 'audio/music/ga01_O_er_Yonder.ogg',
                title: 'O\'er Yonder',
                artist: 'Garrett Aldridge',
            },
            {
                file: 'audio/music/ga02_I_ll_Go_Where_You_Go.ogg',
                title: 'I\'ll Go Where You Go',
                artist: 'Garrett Aldridge',
            },
            {
                file: 'audio/music/ga05_Era_On_Repeat.ogg',
                title: 'Era On Repeat',
                artist: 'Garrett Aldridge',
            },
            {
                file: 'audio/music/sa01_A_Magic_World.ogg',
                title: 'A Magic World',
                artist: '[BC]afGun',
            },
            {
                file: 'audio/music/sa02_Probe_Away.ogg',
                title: 'Probe Away',
                artist: '[BC]afGun',
            },
            {
                file: 'audio/music/sa03_Lunar_Eclipse.ogg',
                title: 'Lunar Eclipse',
                artist: '[BC]afGun',
            },
        ];

        this.loginSong = {
            file: 'audio/music/ga04_Haydn_Seek.ogg',
            title: 'Haydn Seek',
            artist: 'Garrett Aldridge',
        },

        this.prevsong = -1;
        this.creepySongList = [
            {
                file: 'audio/music/ga03_Followed.ogg',
                title: 'Followed',
                artist: 'Garrett Aldridge',
            },
        ];
        this.musicinterval = undefined;
        this.transitionTimeout = undefined;
    }

    startMusicAfterSeconds(s) {
        // check if music is enabled in settings
        if (this.settings.s['music']) {
            // start playing music after that many seconds
            this.musictimeout = setTimeout(
                () => this.startMusicIfNotPlaying(), s*1000);
        }
    }

    startMusicIfNotPlaying() {
        if (this.musicinterval !== undefined) {
            return;
        }
        this.startMusic();
    }

    resetVolume() {
        if (this.transitionTimeout !== undefined) {
            clearTimeout(this.transitionTimeout);
        }
        this.audio_mus.volume = 0.01 * this.settings.eqs['music'];
    }

    startMusic() {
        this.stopMusic();
        this.nextSong();
        this.resetVolume();
        this.musicinterval = setInterval(()=>this.nextSong(), 10*60*1000);
        this.musictimeout = undefined;
    }

    stopMusic() {
        if (this.musictimeout !== undefined) {
            clearTimeout(this.musictimeout);
        }
        if (this.musicinterval !== undefined) {
            clearInterval(this.musicinterval);
        }
        if (this.audio_mus) {
            this.audio_mus.pause();
        }
    }

    loginMusic() {
        this.stopMusic();
        // don't play login music if music disabled
        if (!this.settings.s['music']) return;
        this.audio_mus = new Audio(this.loginSong.file);
        this.resetVolume();
        this.audio_mus.play();
    }

    isPaused() {
        if (!this.audio_mus) return false;
        return this.audio_mus.paused;
    }

    isPlaying() {
        if (!this.audio_mus) return false;
        return !this.audio_mus.paused;
    }

    graduallyStopMusic(volume=1) {
        if (volume < 0.01) {
            if (this.isPlaying()) {
                this.audio_mus.pause();
            }
            return;
        }
        if (this.audio_mus) {
            this.audio_mus.volume = volume * 0.01 * this.settings.eqs['music'];
        }
        this.transitionTimeout = setTimeout(
            ()=>this.graduallyStopMusic(volume-0.01), 100);
    }

    nextSong() {
        if (this.audio_mus) {
            this.audio_mus.pause();
        }
        let chosenSong = -1;
        do {
            chosenSong = Math.floor(Math.random()*this.songList.length);
        } while (chosenSong === this.prevsong);
        this.prevsong = chosenSong;
        this.audio_mus = new Audio(this.songList[chosenSong].file);
        if (this.settings.s.listmusic) {
            this.cli.println('*Now playing: '+this.songList[chosenSong].title+' by '+this.songList[chosenSong].artist+'*');
        }
        this.audio_mus.volume = 0.01 * this.settings.eqs['music'];
        this.audio_mus.play();
    }

}
