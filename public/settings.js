// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Settings class.
 * The s member is an object containing the states of all boolean settings.
 * The eqs member is an object containing the states of all other settings.
 * There is a predefined list of settings and aliases.
 * The set method sets a setting, and also returns the name and value
 * of the settig that was set, if any.
 */

"use strict";

class Settings{
    static OBJECT_N = 0;
    constructor(){

        // my cheap Singleton implementation.
        if (this.constructor.OBJECT_N !== 0) {
            throw "TOO MANY SETTINGS OBJECTS";
        }
        this.constructor.OBJECT_N = 1;

        this.SETTING_LIST = [
            {names: ['music', 'musi', 'mus', 'mu', 'm'], default: true},
            {names: ['listmusic', 'listmusi', 'listmus', 'listmu', 'listm'], default: false},
            {names: ['chunkbordervisible', 'cbv'], default: false},
            {names: ['???'], default: false},
            {names: ['twirt', 'twi', 't', 'blip', 'cricket'], default: true},
            {names: ['gap', 'gaps', 'ga', 'g'], default: true},
        ];
        this.EQ_SETTING_LIST = [
            {names: ['music', 'mus', 'mu', 'm'], default: 50, type:'int', min:0, max:100},
            {names: ['maxchatlines'], default: 100, type:'int', min:5, max:10000},
            {names: ['spectatorspeed', 'ss'], default: 8, type:'int', min:2, max:256},
        ];

        this.s = {};
        this.eqs = {};

        // try loading the settings
        this.tryLoadSettings();
        // use default settings for everything that wasn't loaded
        this.setAllDefaultSettings();

    }

    tryLoadSettings() {
        // try loading
        const str = localStorage.getItem('s');
        const eqstr = localStorage.getItem('eqs');
        // success?
        if(str && eqstr) {
            this.s = JSON.parse(str);
            this.eqs = JSON.parse(eqstr);
            return true;
        }
        return false;
    }

    setAllDefaultSettings() {
        for (const se of this.SETTING_LIST) {
            if (!this.s.hasOwnProperty(se.names[0])) {
                this.s[se.names[0]] = se.default;
            }
        }
        for (const se of this.EQ_SETTING_LIST) {
            if (!this.eqs.hasOwnProperty(se.names[0])) {
                this.eqs[se.names[0]] = se.default;
            }
        }
    }

    saveSettings() {
        localStorage.setItem('s', JSON.stringify(this.s));
        localStorage.setItem('eqs', JSON.stringify(this.eqs));
    }

    clearLocalStorage() {
        localStorage.removeItem('s');
        localStorage.removeItem('eqs');
    }

    set(args) {

        // parse args
        // first check if it is an eq setting (with "=")
        const iseq = args[1].indexOf('=') > -1;
        // the name of the setting (beyond the = is not included)
        const searchedname = iseq ? args[1].substr(0,args[1].indexOf('=')) : args[1];
        // everything after the =
        const rawvalue = iseq ? args[1].substr(args[1].indexOf('=')+1) : '';
        // list of possible settings (or eq settings)
        const relevantlist = iseq ? this.EQ_SETTING_LIST : this.SETTING_LIST;

        let yesfound = false;
        let yesvalideq = false;
        let parsedeqvalue = undefined;
        let found = undefined;
        let negfound = false;
        let queryfound = false;
        for (const se of relevantlist) {
            for (const name of se.names) {
                if (name === searchedname) {
                    // found the requested setting
                    yesfound = true;
                    found = se.names[0];
                    negfound = false;
                    if (iseq) {
                        // it is an eq setting (assignment with =)
                        if (rawvalue === '?') {
                            // actually it is a query of an eq setting
                            // (assignment with =?)
                            queryfound = true;
                            break;
                        }
                        if (rawvalue.length < 1 || isNaN(rawvalue)) {
                            // not a valid number
                            yesvalideq = false;
                            break;
                        }
                        parsedeqvalue = parseInt(rawvalue);
                        if (parsedeqvalue < se.min || parsedeqvalue > se.max) {
                            yesvalideq = false;
                        }
                        else {
                            yesvalideq = true;
                        }
                    }
                    break;
                }
                else if (!iseq && 'no' + name === searchedname) {
                    // found the requested setting (but negated)
                    yesfound = true;
                    found = se.names[0];
                    negfound = true;
                    break;
                }
                else if (!iseq && name + '?' === searchedname) {
                    // found the requested setting (but queried)
                    yesfound = true;
                    found = se.names[0];
                    negfound = false;
                    queryfound = true;
                    break;
                }
            }
            if (yesfound) {
                // already found
                break;
            }
        }
        if (yesfound) {
            if (iseq) {
                if (queryfound) {
                    // only return result of query, don't change anything
                    return {eq: true, name: found, value: this.eqs[found], q: true};
                }
                if (yesvalideq) {
                    // valid assignment
                    this.eqs[found] = parsedeqvalue;
                    return {eq: true, name: found, value: this.eqs[found], q: false};
                }
                else {
                    // error
                    return false;
                }
            }
            else {
                if (queryfound) {
                    // only return result of query, don't change anything
                    return {eq: false, name: found, value: this.s[found], q: true};
                }
                this.s[found] = !negfound;
                return {eq: false, name: found, value: this.s[found], q: false};
            }
        }

        // not found!
        return false;
    }

    printhelp(cli) {
        cli.println('Available settings:'
            + this.SETTING_LIST.map(se=>
                '\\[n\\]/set '+se.names[0]
                +'\\[n\\]/set no'+se.names[0]).join('')
            + this.EQ_SETTING_LIST.map(se=>{
                switch (se.type) {
                    case 'int':
                        return '\\[n\\]/set '+se.names[0]+'=['+se.min+'-'+se.max+']';
                        break;
                }
                return 'ERR: unk type '+se.names;
            }).join(''))
    }

}

const SETTINGS = new Settings();
