// Copyright (C) 2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class TablistComponent extends UiComponent {
    constructor(player, prefillChatbox) {
        super();
        this.player = player;
        this.prefillChatbox = prefillChatbox;
        this.shown = false;
    }

    static CENTERING(text, len) {
        const padTotal = len-text.length;
        const padLeft = Math.floor(padTotal/2);
        const padRight = padTotal-padLeft;
        const spaces = '                                              ';
        const spacesLeft = spaces.substr(0, padLeft);
        const spacesRight = spaces.substr(0, padRight);
        return spacesLeft + text + spacesRight;
    }

    getFramePos() {
        return {
            x: Math.floor(this.w/2) - 15,
            y: 2,
        };
    }

    // click is in character coordinates
    handleClick(x, y) {
        const local = this.screenToLocal(x, y);
        if (local.y < 1 || local.y >= this.player.friends.requests.length+1) {
            return false;
        }
        const clickedFriend = this.player.friends.requests[local.y-1];
        if (local.x >= 14 && local.x < 22) {
            this.prefillChatbox('/friend add '+clickedFriend);
            return true;
        }
        if (local.x >= 22 && local.x < 30) {
            this.prefillChatbox('/friend remove '+clickedFriend);
            return true;
        }
        return false;
    }

    show() {
        this.shown = true;
    }

    hide() {
        this.shown = false;
    }

    isShown() {
        return this.shown;
    }

    getFrame() {
        const curTime = Math.floor((new Date()).getTime() / 1000);

        const frame =
            [
                '┌─────────Friends─────────────┐',
            ];

        // iterate over incoming friend requests
        for (const friend of this.player.friends.requests) {
            frame.push('│' + TablistComponent.CENTERING(friend, 12)
                + '| \\[#373\\]ACCEPT  \\[#733\\]REJECT\\[/\\] │');
        }

        // iterate over real friends
        const sortedFriends = this.player.friends.friends.slice();
        sortedFriends.sort((a,b)=>{
            const aonline = a.name in this.player.positionList;
            const bonline = b.name in this.player.positionList;
            if (aonline !== bonline) return bonline;
            return a.seen < b.seen;
        })
        for (const friend of sortedFriends) {
            // check if online
            if (friend.name in this.player.positionList) {
                // friend is online!
                const friendPos = this.player.positionList[friend.name];
                let coordText = 'x:'+friendPos.x+' y:'+friendPos.y;
                if (coordText.length > 16) {
                    // use shorter version
                    coordText = friendPos.x+','+friendPos.y;
                }
                if (coordText.length > 16) {
                    // use even shorter version
                    coordText = Math.floor(friendPos.x/1000)+'k,'
                        +Math.floor(friendPos.y/1000)+'k';
                }
                if (coordText.length > 16) {
                    // use EVEN SHORTER version
                    coordText = Math.floor(friendPos.x/1000000)+'M,'
                        +Math.floor(friendPos.y/1000000)+'M';
                }
                frame.push('│\\[#'+friendPos.c+'\\]'
                    + TablistComponent.CENTERING(friend.name, 12)
                    + '\\[/\\]|'
                    + TablistComponent.CENTERING(coordText, 16) + '│'
                );
            } else {
                // friend is offline.
                const seenAgo = curTime - friend.seen;
                let when;
                if (seenAgo < 0) {
                    when = 'in the future??';
                }
                else if (seenAgo < 60) {
                    when = seenAgo+'s ago';
                }
                else if (seenAgo < 3600) {
                    when = Math.floor(seenAgo/60)+' min ago';
                }
                else if (seenAgo < 172800) {  // < 2 days
                    when = Math.floor(seenAgo/3600)+' h ago';
                }
                else if (seenAgo < 8640000) {
                    when = Math.floor(seenAgo/86400)+' days ago';
                }
                else if (seenAgo < 86400000) {
                    when = Math.floor(seenAgo/86400)+'days ago';
                }
                else {
                    when = Math.floor(seenAgo/86400)+'daysago';
                }
                frame.push('│\\[#'+friend.color+'\\]'
                    + TablistComponent.CENTERING(friend.name, 12)
                    + '\\[/\\]|\\['+GUI_SLIGHT_COLOR+'\\]'
                    + TablistComponent.CENTERING('seen '+when, 16)
                    + '\\[/\\]│'
                );
            }
        }

        // iterate over pending friends (outgoing requests)
        for (const friend of this.player.friends.pending) {
            frame.push('│' + TablistComponent.CENTERING(friend, 12)
                + '|    \\[#337\\]PENDING\\[/\\]     │');
        }


        if (frame.length < 2) {
            frame.push('│                             │');
            frame.push('│   use  /friend add "name"   │');
            frame.push('│                             │');
        }
        frame.push('└─────────────────────────────┘');

        return {f: frame, e: []};

    }

    getHoverScreenPos(x, y) {
        const local = this.screenToLocal(x, y);
        if (local.y < 1 || local.y >= this.player.friends.requests.length+1) {
            return null;
        }
        if (local.x >= 14 && local.x < 22) {
            return this.localToScreen(15, local.y);
        }
        if (local.x >= 22 && local.x < 30) {
            return this.localToScreen(23, local.y);
        }
        return null;
    }

}
