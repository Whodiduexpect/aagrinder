// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


// Wrapper for canvas, allows drawing text from an array of strings

"use strict";

const PADDING = 5;
const LINE_SPACING = 20;
const CHAR_WIDTH = 11.7;

class Terminal {
    constructor(canvas, font, width, height, xpos, ypos = 0){
        this.canvas = canvas;
        this.font = font;
        this.width = width;
        this.height = height;
        this.xpos = xpos;
        this.ypos = ypos;
        this.ctx = canvas.getContext('2d');
        this.resizeEventHandlers = [];
    }

    addResizeEventListener(handler) {
        this.resizeEventHandlers.push(handler);
        // also invoke immediately
        // see this as "resizing to starting size"
        handler(this.width, this.height);
    }

    resize(w, h, xpos, ypos = 0){
        this.width = w;
        this.height = h;
        this.xpos = xpos;
        this.ypos = ypos;
        for (const handler of this.resizeEventHandlers) {
            handler(w, h);
        }
    }

    displayScreen(lines){
        this.clearScreen();
        for(let i = 0; i < lines.length; i++){
            for(let j = 0; j < lines[i].raw.length; j++){
                this.ctx.fillStyle = lines[i].color[j];
                this.ctx.fillText(
                    lines[i].raw[j],
                    PADDING + (j + this.xpos) * CHAR_WIDTH,
                    PADDING + (i + this.ypos+1) * LINE_SPACING
                );
            }
        }
    }

    clearScreen(){
        this.ctx.clearRect(
            this.xpos * CHAR_WIDTH,
            this.ypos * LINE_SPACING,
            4 * PADDING + this.width * CHAR_WIDTH,
            4 * PADDING + this.height * LINE_SPACING
        );
        this.ctx.font = this.font;
    }

    setBackground(color) {
        this.ctx.beginPath();
        this.ctx.fillStyle = color;
        this.ctx.rect(
            this.xpos * CHAR_WIDTH,
            this.ypos * LINE_SPACING,
            4 * PADDING + this.width * CHAR_WIDTH,
            4 * PADDING + this.height * LINE_SPACING
        );
        this.ctx.fill();
    }

    displayCharacter(x, y, char, color){
        this.ctx.fillStyle = color;
        this.ctx.fillText(
            char,
            PADDING + (this.xpos + x) * CHAR_WIDTH,
            PADDING + (y + this.ypos + 1) * LINE_SPACING
        );
    }

    displayCharacterScaled(x, y, scale, char, color){
        this.ctx.fillStyle = color;
        this.ctx.save();
        this.ctx.scale(scale, scale);
        this.ctx.fillText(
            char,
            PADDING + (this.xpos + x) / scale * CHAR_WIDTH,
            PADDING + (y + this.ypos + 1) / scale * LINE_SPACING
        );
        this.ctx.restore();
    }

    displayCharacterSafe(x, y, char, color){
        if (x < -0.3) {
            return;
        }
        this.ctx.fillStyle = color;
        this.ctx.fillText(
            char,
            PADDING + (this.xpos + x) * CHAR_WIDTH,
            PADDING + (y + this.ypos + 1) * LINE_SPACING
        );
    }

    displayCharacterAlpha(x, y, alpha, char, color){
        if (x < 0) {
            return;
        }
        this.ctx.fillStyle = color;
        this.ctx.save();
        this.ctx.globalAlpha = alpha;
        this.ctx.fillText(
            char,
            PADDING + (this.xpos + x) * CHAR_WIDTH,
            PADDING + (y + this.ypos + 1) * LINE_SPACING
        );
        this.ctx.restore();
    }

    displayCharacterRotated(x, y, angle, alpha, char, color){

        if (x < 2) {
            alpha *= (x+0.3)/2;
        }

        if (x < -0.3) {
            return;
        }

        this.ctx.fillStyle = color;

        this.ctx.save();

        this.ctx.globalAlpha = alpha;
        this.ctx.rotate(angle);
        const a = this.xpos*CHAR_WIDTH+(x+1)*CHAR_WIDTH;
        const b = this.ypos*LINE_SPACING+(y+1)*LINE_SPACING;
        this.ctx.translate(-a, -b);
        const fixx = b * Math.sin(angle) + a * Math.cos(angle);
        const fixy = -b * Math.cos(angle) + a * Math.sin(angle);

        this.ctx.fillText(
            char,
            PADDING + (this.xpos + x) * CHAR_WIDTH + fixx,
            PADDING + (y + this.ypos + 1) * LINE_SPACING - fixy
        );

        this.ctx.restore();
    }

    displayCharacterOffsetPolar(x, y, angle, alpha, char, color){

        this.ctx.fillStyle = color;

        this.ctx.save();

        this.ctx.globalAlpha = alpha;
        this.ctx.rotate(angle);
        const a = this.xpos*CHAR_WIDTH+this.width/2*CHAR_WIDTH;
        const b = this.ypos*LINE_SPACING+this.height/2*LINE_SPACING;
        this.ctx.translate(-a, -b);
        const fixx = b * Math.sin(angle) + a * Math.cos(angle);
        const fixy = -b * Math.cos(angle) + a * Math.sin(angle);

        this.ctx.fillText(
            char,
            PADDING + (this.xpos + x) * CHAR_WIDTH + fixx,
            PADDING + (y + this.ypos + 1) * LINE_SPACING - fixy
        );

        this.ctx.restore();
    }


    pixelToChar(pixelX, pixelY, centered=true){
        const w = this.width;
        const h = this.height;

        const x = Math.floor((pixelX - PADDING) / CHAR_WIDTH) - this.xpos;
        const y = Math.floor((pixelY - PADDING) / LINE_SPACING - this.ypos);

        const charX = x - Math.floor(w / 2);
        const charY = h - Math.ceil(h / 2) - y;
        return centered ? {x:charX, y:charY} : {x:x, y:y};
    }

    characterAspectRatio() {
        return CHAR_WIDTH / LINE_SPACING ;
    }

}
