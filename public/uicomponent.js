// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * An "abstract" base class for all UI components.
 */

"use strict";

class UiComponent {
    constructor(){
        this.w = 0;
        this.h = 0;
    }

    resizeTerminal(w, h) {
        this.w = w;
        this.h = h
    }

    // with 0,0 at start of canvas
    isClickWithin(x, y) {
        return false;
    }

    // click is in character coordinates
    handleClick(x, y) {
        return false;
    }

    isShown() {
        return true;
    }

    getFrame() {
        const extraSprites = [];
        const frame = ['@'];
        return {f: frame, e: extraSprites};
    }

    getFramePos() {
        return {x: 0, y: 0};
    }

    screenToLocal(x, y) {
        const framePos = this.getFramePos();
        const xlocal = x - framePos.x;
        const ylocal = y - framePos.y;
        return {x: xlocal, y: ylocal};
    }

    localToScreen(x, y) {
        const framePos = this.getFramePos();
        const xlocal = x + framePos.x;
        const ylocal = y + framePos.y;
        return {x: xlocal, y: ylocal};
    }

    drawOnBuffer(buffer, time) {
        const fe = this.getFrame(time);
        const framePos = this.getFramePos();
        const frame = fe.f;
        const extraSprites = fe.e;

        Gui.drawFrame(buffer, frame,
            framePos.x, framePos.y, GUI_FRAME_COLOR);
        Gui.drawSpritesRelative(buffer,
            framePos.x, framePos.y, extraSprites);
    }

}
