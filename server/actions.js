// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * some Player actions and server actions.
 *
 * this is the game logic, determines how things move and interact.
 *
 * with the abstractions provided by the Syncher class, there is no need
 * to worry about client-server synchronization, order of events,
 * missing pieces of terrain etc.
 * In this class we work on a high level.
 *
 * Notice "sharedActionFunctions". That's code that comes from a shared file
 * and it determines those parts of the game logic that the client also uses.
 * This way, I literally use the same code on the server and the client.
 * It is more maintainable this way.
 */

"use strict";

const sharedActionFunctions = require('../shared/gamelogic').sharedActionFunctions;
const sharedTickLogic = require('../shared/ticklogic').sharedTickLogic;
const Inventory = require('../shared/inventory').Inventory;

class Actions {

    constructor(map, syncher, subscribe, spawn, playerData, props) {
        this.map = map;
        this.syncher = syncher;
        this.subscribe = subscribe;
        this.spawn = spawn;
        this.actionFunctions = sharedActionFunctions;
        this.tickLogic = sharedTickLogic;
        this.playerData = playerData;
        this.props = props;
    }

    login(player) {

        const spawnSpot = this.spawn.choosePlayerSpawnSpot(player);

        // we assume this is a good spot and no checks need to be performed.

        player.visiblePlayers = [];
        this.syncher.movePlayer(player, spawnSpot.x, spawnSpot.y);

        // verify that player color is ok
        if (!/^[0-9a-f]{6}$/.test(player.color)) {
            player.color = 'ffffff';
        }

        this.subscribe.unsubscribeAll(player);
        this.subscribe.resubscribe(player, true);
        this.syncher.detectors.recalculateVisibleDetectors(player);
        this.syncher.ghosts.recalculateVisibleGhosts(player);

        player.digtime = 0;  // reset this (just in case)

        player.changedx = true;
        player.changedy = true;
        player.changedFood = true;
        player.changedReach = true;
        player.changedMaxjump = true;
        player.changedGamemode = true;
        player.changedColor = true;
        player.changedFriends = true;
        player.changedInventory = true;
        player.changedDetectorBlink = true;
        player.changedFollowBlink = true;
        player.unsubscriptions = [];
        player.justLoggedIn = true;

        player.debugLog = [];  // Clear the debug log when logging in

        // welcome
        if (this.props.welcome_message !== "") {
            player.announcement = this.props.welcome_message;
        }

        // also announce this login
        for(const anyplayer of this.playerData.onlinePlayers) {
            anyplayer.socket.emit('chat', {
                message: '\\[#'+player.color+'\\]'+player.name+'\\[/\\] joined the game',
            });
        }
        LOG.log(player.name + ' logged in from '
            + (player.socket.handshake.headers['x-forwarded-for']
            || player.socket.handshake.address));

    }

    logout(player) {

        this.syncher.deletionFromPositionLists(player);
        this.subscribe.unsubscribeAll(player);


        // also announce this logout
        for(const anyplayer of this.playerData.onlinePlayers) {
            anyplayer.socket.emit('chat', {
                message: '\\[#'+player.color+'\\]'+player.name+'\\[/\\] left the game',
            });
        }
        LOG.log(player.name + ' left the game');

        // update last seen time
        ACCOUNTS.accounts[player.name].seen =
            Math.floor((new Date()).getTime() / 1000);
        // all friends need to get this update
        for (const friendName of ACCOUNTS.accounts[player.name].friends) {
            const friendPlayer =
                this.playerData.onlinePlayerByName(friendName);
            // skip offline friends
            if (friendPlayer === null) continue;
            // mark that friend list needs an update
            friendPlayer.changedFriends = true;
        }
        ACCOUNTS.save();
    }

    tick(player, tick, eventId) {
        // flush previous events to client
        // (it is important to do this before changing lastEventId)
        const wasSent = this.syncher.sendUpdatesToClient(player);
        if (wasSent) {
            // an update was just sent to this client. So maybe we don't
            // need to send another update at the next timer cycle.
            player.skipNextTimerUpdate = true;
        }
        // execute the tick
        const tickstring1 = JSON.stringify(tick);
        if (this.props.enable_debug_log === true) {
            player.debugLog.push('['+eventId+'] '+tickstring1);
        }
        const wasfiltered = this.tickLogic(tick, (actionName, data) => {
            // check if action by this name exists
            if (!this.actionFunctions[actionName]) return false;
            // execute action of tick
            const view = this.syncher.createView(player)
            const success = this.actionFunctions[actionName](view, data) !== false;
            view.apply(eventId);
            this.subscribe.resubscribe(player);
            return success;
        });
        if (wasfiltered && this.props.is_debug_server) {
            const tickstring2 = JSON.stringify(tick);
            player.socket.emit('chat', {
                message: '\\[#f00\\]E: '+tickstring1+'->'+tickstring2,
            });
            LOG.log('!!!DEBUG ERROR by '+player.name+': '+tickstring1+'->'+tickstring2)
        }
        // update eventId
        player.lastEventId = eventId;
        if(this.props.is_debug_server === true){
            this.syncher.sendDebugEventToPlayer(player);
        }
    }

    inventoryEvent(player, x, y, item_code, count, data_index) {
        if (x > player.x + 10
            || x < player.x - 10
            || y > player.y + 10
            || y < player.y - 10) {
            // too far. Out of reach.
            // ignore this event.
            return false;
        }
        const tiles = this.map.getTileEntityAt(x, y);
        if (tiles.length < 1) {
            // no tile entities
            return false;
        }
        const tile = tiles[0];
        if (tile.t !== 'chest') {
            // this is not a chest
            return false;
        }

        const move_successful = player.inventory.moveItemsTo(tile.inventory, item_code, count, data_index);

        if (move_successful) {
            player.changedInventory = true;
            this.syncher.updateTileEntity(tile);
            return true;
        }

        return false;
    }

    itemDeletion(player, data) {
        for (const item_code in data) {
            const amount = data[item_code].c;
            if (amount <= 0) continue; // someone is trying to hack
            if (!player.inventory.canGain(item_code, -amount)) continue;
            player.inventory.gainItem(item_code, -amount);
        }
        player.changedInventory = true;
        this.syncher.broadcastItemDeletion(player.x, player.y, data);
        this.syncher.sendUpdatesToClient(player);
        return true;
    }
}

exports.Actions = Actions;
