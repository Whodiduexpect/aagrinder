// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * This file contains the API for all user account operations
 * that should be available without being logged in, and operations that
 * we want to potentially be available through other interfaces (not game)
 */

"use strict";


const express = require('express');
const bcrypt = require('bcryptjs');
const router = express.Router();

const SALT_WORK_FACTOR = 10;

router.post('/checkname', (req, res)=>{
    if(typeof(req.body.name) !== 'string'){
        res.json({ message: 'Missing form data', success: false});
        return;
    }
    if(!/^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$/.test(req.body.name)){
        res.json({ message: 'Username invalid \
^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$', success: false });
        return;
    }
    const name = ACCOUNTS.findMatchingName(req.body.name);
    if (name !== null) {
        res.json({ message: 'Username taken',
            success: false,
            name: name,
            color: ACCOUNTS.accounts[name].color,
            insecure: ACCOUNTS.accounts[name].hash === '' });
        return;
    }
    if (SERVER_ALLOW_REGISTRATION === false) {
        res.json({ message: 'Unknown name. \
Contact the administrator for an account.'
            , success: false });
        return;
    }
    res.json({ message: 'OK', success: true });
});

router.post('/checkpass', (req, res)=>{
    if(typeof(req.body.name) !== 'string'
        || typeof(req.body.password) !== 'string'){
        res.json({ message: 'Missing form data', success: false});
        return;
    }
    if(!/^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$/.test(req.body.name)){
        res.json({ message: 'Username invalid \
^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$', success: false });
        return;
    }
    if (!(req.body.name in ACCOUNTS.accounts)) {
        res.json({ message: 'User does not exist', success: false });
        return;
    }
    const hash = ACCOUNTS.accounts[req.body.name].hash;
    if (hash === '') {
        // no password set for this account. Automatically succeed
        res.json({ message: 'OK', success: true });
        return;
    }
    if(
        !/^.{1,72}$/.test(req.body.password)
    ){
        res.json({ message: 'Password invalid ^.{1,72}$', success: false });
        return;
    }
    const isMatch = bcrypt.compareSync(req.body.password, hash);
    if(!isMatch){
        res.json({ message: 'Wrong password', success: false });
    }
    else {
        res.json({ message: 'OK', success: true });
    }
});

router.post('/changepass', (req, res)=>{
    if(typeof(req.body.name) !== 'string'
        || typeof(req.body.oldpassword) !== 'string'
        || typeof(req.body.newpassword) !== 'string'){
        res.json({ message: 'Missing form data', success: false});
        return;
    }
    if(!/^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$/.test(req.body.name)){
        res.json({ message: 'Username invalid \
^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$', success: false });
        return;
    }
    if(
        !/^.{1,72}$/.test(req.body.newpassword)
    ){
        res.json({ message: 'Password invalid ^.{1,72}$', success: false });
        return;
    }
    if (!(req.body.name in ACCOUNTS.accounts)) {
        res.json({ message: 'User does not exist', success: false });
        return;
    }
    const oldhash = ACCOUNTS.accounts[req.body.name].hash;
    // check if there is a previous password
    if (oldhash !== '') {
        // there is a password set. Check if user knows it
        if(
            !/^.{1,72}$/.test(req.body.oldpassword)
        ){
            res.json({ message: 'Password invalid ^.{1,72}$', success: false });
            return;
        }
        const isMatch = bcrypt.compareSync(req.body.oldpassword, oldhash);
        if(!isMatch){
            res.json({ message: 'Wrong password', success: false });
            return;
        }
    }
    // password matches. Now change it.
    const salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);
    const newhash = bcrypt.hashSync(req.body.newpassword, salt);
    ACCOUNTS.accounts[req.body.name].hash = newhash;
    res.json({ message: 'Password changed.', success: true });
    LOG.log('Password changed for: '+req.body.name);
    // Immediately update accounts on disk
    ACCOUNTS.save();
});

router.post('/register', (req, res) => {
    if (SERVER_ALLOW_REGISTRATION === false) {
        res.json({ message: 'Registration is not allowed on this server.\
Please contact the administrator.'
            , success: false });
        return;
    }
    if(
        typeof(req.body.name) !== 'string'
        || typeof(req.body.password) !== 'string'
        || typeof(req.body.color) !== 'string'
    ){
        res.json({ message: 'Missing form data', success: false });
        return;
    }

    if(
        !/^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$/.test(req.body.name)
    ){
        res.json({ message: 'Username invalid \
^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$', success: false });
        return;
    }

    if(
        !/^.{1,72}$/.test(req.body.password)
    ){
        res.json({ message: 'Password invalid ^.{1,72}$', success: false });
        return;
    }

    if(
        !/^[0-9a-f]{6}$/.test(req.body.color)
    ){
        res.json({ message: 'Color invalid ^[0-9a-f]{6}$', success: false });
        return;
    }

    {
        // check the color
        const r = parseInt(req.body.color.substring(0, 2), 16) / 255;
        const g = parseInt(req.body.color.substring(2, 4), 16) / 255;
        const b = parseInt(req.body.color.substring(4, 6), 16) / 255;

        const luminance = 0.2126 * r + 0.7152 * g + 0.0722 * b;

        if (luminance < 0.2) {
            res.json({ message: 'Color too dark', success: false });
            return;
        }
    }

    const existingName = ACCOUNTS.findMatchingName(req.body.name);
    if (existingName !== null) {
        res.json({ message: 'Username taken', success: false });
        return;
    }

    const salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);
    const hash = bcrypt.hashSync(req.body.password, salt);
    ACCOUNTS.accounts[req.body.name] = {
        hash: hash,
        color: req.body.color,
        friends: [],
        pending: [],
        requests: [],
        seen: Math.floor((new Date()).getTime() / 1000),
    };
    res.json({ message: 'User created', success: true });
    console.log('User created: '+req.body.name);
    // Immediately update accounts on disk
    ACCOUNTS.save();
});

module.exports = router;
