// Copyright (C) 2018-2021 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Block update allows blocks to immediately respond
 * to changes in surrounding blocks.
 *
 * Whenever a block is modified,
 * updateAt should be called.
 * Then, syncher.applyServerBlockUpdates()
 * should be called.
 */

"use strict";

class BlockUpdate{

    constructor(map){
        this.map = map;
        this.visited = {};
        this.dfsStack = [];
        this.blockList = [];
    }

    updateAt(x, y, forceContinue = false) {
        this.dfsStack.push({x:x, y:y, f:forceContinue});
    }

    cascadeUpdates() {
        let wereChanges = false;
        while (this.dfsStack.length > 0) {
            const pos = this.dfsStack.pop();
            const x = pos.x;
            const y = pos.y;
            const forceContinue = pos.f;

            const blockHere = this.map.getBlock(x, y);

            let continueUpdate = forceContinue;

            if (blockHere[0] === 'A'){
                this.blockList.push({
                    x: x,
                    y: y,
                    block: 'Wa'+blockHere[1],
                });
                continueUpdate = true;
                wereChanges = true;
            }
            else if (blockHere === 'w') {
                // would stage a water update now
                // without actually making any changes
            }

            // continue updating?
            if (continueUpdate) {
                for (const dxy of [
                    {dx: 1, dy:0},
                    {dx: 0, dy:1},
                    {dx: -1, dy:0},
                    {dx: 0, dy:-1},
                ]) {
                    const nextx = x + dxy.dx;
                    const nexty = y + dxy.dy;
                    if (this.visited[nexty]) {
                        if (this.visited[nexty][nextx]) {
                            // already visited.
                            continue;
                        }
                    }
                    else {
                        this.visited[nexty] = {};
                    }
                    // mark as visited.
                    this.visited[nexty][nextx] = true;
                    // recursion
                    // (or at least it used to be recursion)
                    // (now it's a normal stack)
                    this.updateAt(nextx, nexty);
                }
            }

        }

        return wereChanges;
    }
}
exports.BlockUpdate = BlockUpdate;
