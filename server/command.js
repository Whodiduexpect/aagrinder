// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * subclass sandbox for server commands.
 * Gives access to various components.
 * Still, map should not be modified directly, but rather through the syncher.
 * This way, there's no need to worry about synchronization and order of events.
 *
 */

"use strict";

class Command {
    name = null;
    aliases = [];
    isAdmin = true;  // restricted by default, to avoid security issues

    constructor(server) {
        this.server = server;
        this.map = server.map;
        this.syncher = server.syncher;
        this.subscribe = server.subscribe;
        this.spawn = server.spawn;
        this.playerData = server.playerData;
        this.plant = server.plant;
        this.props = server.props;
    }

    parseRelative(playerCoord, arg) {
        if (arg.length < 1) return false;
        const isRelative = arg[0] === '~';
        if (isRelative) {
            arg = arg.substring(1);
            if (arg === '') {
                return playerCoord;
            }
        }
        if (isNaN(arg)) {
            return false;
        }
        let intArg = parseInt(arg);
        if (isRelative) intArg += playerCoord;
        return intArg;
    }


}

module.exports = Command;
