// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * collection of server-side commands (including admin commands).
 */

"use strict";

const fs = require('fs');
const commandNames = fs.readdirSync('./server/commands')
    .map(fname => fname.slice(0, -3));

// extra commands that we want to display in /help:
// - aliases which are important enough to be displayed anyway
// - client-side commands
// - the /help command itself
const extraCommandsInHelp = [
    '/rainbowme',
    '/map',
    '/unmap',
    '/unmapallkeys',
    '/layout',
    '/hello',
    '/pos',
    '/pop',
    '/password',
    '/prompt',
    '/set',
    '/resetallsettings',
    '/credits',
    '/bye',
    '/clear',
    '/copylast',
    '/copyall',
    '/verify',
    '/log',
    '/help',
];

class Commands {

    constructor(server) {
        this.playerData = server.playerData;

        this.commands = commandNames.map(name => {
            // Load command class
            const commandClass = require('./commands/'+name);
            // Instantiate command once
            const command = new commandClass(server);
            // Add "name" property to command object
            command.name = '/'+name;
            return command;
        });

        // make them indexed by alias
        this.commandByAlias = {};
        for (const command of this.commands) {
            // save it by its main name
            this.commandByAlias[command.name] = command;
            // save it by every alias
            for (const alias of command.aliases) {
                this.commandByAlias[alias] = command;
            }
        }
    }


    executeCommand(player, typed) {

        let args = typed.split(/ +(?=(?:(?:[^"]*"){2})*[^"]*$)/g);
        args = args.map(a=>a.replace(/"/g,''));

        if (args[0] !== '/whisper' && args[0] !== '/w') {
            // is a command that's not a whisper, so logging
            LOG.log(player.name+': '+typed);
        }

        // check if player admin
        const isAdmin = this.playerData.isPlayerAdminByName(player.name);

        if (['/?', '/h', '/help'].includes(args[0])) {
            // available: either all commands or only non-admin
            const availCommands = isAdmin ? this.commands
                : this.commands.filter(c => !c.isAdmin);
            const nameList = availCommands.map(c => c.name);
            const fullNameList = nameList.concat(extraCommandsInHelp);
            const message = fullNameList.sort().join(' ');
            player.socket.emit('chat', { message: message });
            return;
        }

        // remove blank arguments
        args = args.filter(a => a.length > 0);

        // check if exists
        const command = this.commandByAlias[args[0]];
        if (command === undefined) {
            LOG.log('invalid command: '+args[0]);
            player.socket.emit('chat', {message:
                'unknown command: '+args[0]+'. Try /help'});
            return;
        }

        if (command.isAdmin && !isAdmin) {
            // non-admin player tried to run admin command!
            LOG.log('ACCESS DENIED : '+player.name+' : '+typed);
            player.socket.emit('chat', {message:
                'access denied for admin command. Try /help'});
            return;
        }

        command.execute(player, args);
    }

}

exports.Commands = Commands;
