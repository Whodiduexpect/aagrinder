// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = true;

    execute(player, args) {

        const USAGE = 'Usage: /claim <x1> <y1> <x2> <y2> [account]';

        let accountName;
        if (args.length === 5) {
            accountName = player.name;
        }
        else if (args.length === 6) {
            accountName = ACCOUNTS.findMatchingName(args[5]);
            if (accountName === null) {
                player.socket.emit('chat', {message:
                    'can\'t find account '+args[5]});
                return;
            }
        }
        else {
            player.socket.emit('chat', {message: USAGE});
            return;
        }

        let x1 = this.parseRelative(player.x, args[1]);
        let y1 = this.parseRelative(player.y, args[2]);
        let x2 = this.parseRelative(player.x, args[3]);
        let y2 = this.parseRelative(player.y, args[4]);
        if (x1 === false || y1 === false || x2 === false || y2 === false) {
            player.socket.emit('chat', {message: USAGE});
            return;
        }

        if (x1 > x2) [x1, x2] = [x2, x1];
        if (y1 > y2) [y1, y2] = [y2, y1];
        this.syncher.addClaim(accountName, x1, y1, x2, y2);
        player.socket.emit('chat', {message:
            'Claimed '+(x2-x1+1)*(y2-y1+1)+' blocks by '+accountName});

    }

}
