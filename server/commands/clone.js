// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = true;

    execute(player, args) {

        if (args.length !== 7) {
            player.socket.emit('chat', {message:
                'usage: /clone <x1> <y1> <x2> <y2> <xtarget> <ytarget>'});
            return;
        }

        // calculate coordinates
        let areax = this.parseRelative(player.x, args[1]);
        let areay = this.parseRelative(player.y, args[2]);
        let areax2 = this.parseRelative(player.x, args[3]);
        let areay2 = this.parseRelative(player.y, args[4]);
        let targetx = this.parseRelative(player.x, args[5]);
        let targety = this.parseRelative(player.y, args[6]);

        // validate coordinates
        if (areax === false || areay === false
            || areax2 === false || areay2 === false
            || targetx === false || targety === false) {
            player.socket.emit('chat', {message:
                'usage: /clone <x1> <y1> <x2> <y2> <xtarget> <ytarget>'});
            return;
        }

        // swap if needed
        if (areax > areax2) [areax, areax2] = [areax2, areax];
        if (areay > areay2) [areay, areay2] = [areay2, areay];

        const w = areax2 - areax;
        const h = areay2 - areay;

        // check overlap
        if (Math.abs(areax-targetx)<=w && Math.abs(areay-targety)<=h) {
            player.socket.emit('chat', {message:
                'source and destination are overlapping!'});
            return;
        }

        // apply
        for (let dy = 0; dy <= h; dy++) {
            for (let dx = 0; dx <= w; dx++) {
                const block = this.map.getBlock(areax+dx, areay+dy);
                this.syncher.serverChangeBlock(
                    targetx+dx, targety+dy, block);
            }
        }

        player.socket.emit('chat', {
            message: 'cloned '+
            (areax2-areax+1) * (areay2-areay+1)
            +' blocks'});

    }

}
