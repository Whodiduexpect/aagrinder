// Copyright (C) 2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = false;

    execute(player, args) {
        let amount;
        let itemInput;
        // different combinations of arguments are allowed:
        // /craft item
        // /craft amount item
        // /craft item amount
        if (args.length === 2) {
            // /craft item
            amount = 1;
            itemInput = args[1];
        } else if (args.length === 3) {
            // /craft amount item
            // or
            // /craft item amount
            if (!isNaN(args[1])) {
                // /craft amount item
                amount = args[1];
                itemInput = args[2];
            } else {
                // /craft item amount
                amount = args[2];
                itemInput = args[1];
            }
        } else {
            player.socket.emit('chat', {message:
                'usage: /craft [amount] <item>'});
            return;
        }

        // parse the specified item
        const item = Inventory.humanInput2itemCode(itemInput);
        if (item === null) {
            player.socket.emit('chat', {message:
                'unknown item: '+itemInput});
            return;
        }

        if (isNaN(amount) || amount <= 0) {
            player.socket.emit('chat', {message:
                'invalid amount: '+amount});
            return;
        }

        const intAmount = parseInt(amount);
        const itemName = Inventory.item_names[item];
        let numCrafted = 0;
        for (let r = 0; r < Inventory.recipes.length; r++) {
            if (Inventory.recipes[r].get.item !== item) continue;
            while (player.inventory.canCraft(r, 1) && numCrafted < intAmount) {
                player.inventory.craft(r, 1);
                numCrafted += Inventory.recipes[r].get.amount;
            }
            if (numCrafted >= intAmount) break;
        }
        if (numCrafted > 0) {
            player.socket.emit('chat', {message:
                'crafted '+numCrafted+' of '+itemName});
        } else {
            player.socket.emit('chat', {message:
                'no craftable recipes for '+itemName});
        }
        //update properly
        player.changedInventory = true;
    }

}
