// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = false;

    execute(player, args) {
        if (args.length < 2 || isNaN(args[1])) {
            player.socket.emit('chat', {message:
                'usage: /di <count>'});
            return;
        }
        const count = parseInt(args[1]);
        if (count > 21) {
            player.socket.emit('chat', {message:
                count+' is a tad too many, don\'t you think?'});
            return;
        }
        let str = '';
        for(let i = 0; i < count; i++) {
            str += 'di';
        }
        player.socket.emit('chat', {message: str});
    }

}
