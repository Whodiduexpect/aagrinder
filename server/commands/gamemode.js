// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    aliases = ['/mode', '/g', '/m'];
    isAdmin = true;

    execute(player, args) {
        if (args.length < 2 || args.length > 3) {
            player.socket.emit('chat', {message:
                'usage: /gamemode <0-2> [player]'});
            return;
        }

        const gPlayerName = args.length === 3 ? args[2] : player.name;
        const gPlayer = this.playerData.onlinePlayerByName(gPlayerName);

        let g = args[1];

        if (isNaN(g) || !gPlayer || parseInt(g) < 0 || parseInt(g) > 2) {
            player.socket.emit('chat', {message:
                'usage: /gamemode <0-2> [player]'});
            return;
        }

        gPlayer.gamemode = parseInt(g);
        gPlayer.changedGamemode = true;
    }

}
