// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = true;

    execute(player, args) {
        let recvPlayerName;
        let amount;
        let itemInput;
        // different combinations of arguments are allowed:
        // /give item
        // /give amount item
        // /give item amount
        // /give player item
        // /give item player
        // /give player amount item
        // /give amount item player
        if (args.length === 2) {
            // /give item
            recvPlayerName = player.name;
            amount = 1;
            itemInput = args[1];
        } else if (args.length === 3) {
            // /give amount item
            // or
            // /give player item
            // or
            // /give item amount
            // or
            // /give item player
            if (!isNaN(args[1])) {
                // /give amount item
                recvPlayerName = player.name;
                amount = args[1];
                itemInput = args[2];
            } else if(Inventory.humanInput2itemCode(args[1]) === null) {
                // /give player item
                recvPlayerName = args[1];
                amount = 1;
                itemInput = args[2];
            } else if (!isNaN(args[2])) {
                // /give item amount
                recvPlayerName = player.name;
                amount = args[2];
                itemInput = args[1];
            } else {
                // /give item player
                recvPlayerName = args[2];
                amount = 1;
                itemInput = args[1];
            }
        } else if (args.length === 4) {
            // /give player amount item
            // or
            // /give amount item player
            if (isNaN(args[1])) {
                // /give player amount item
                recvPlayerName = args[1];
                amount = args[2];
                itemInput = args[3];
            } else {
                // /give amount item player
                recvPlayerName = args[3];
                amount = args[1];
                itemInput = args[2];
            }
        } else {
            player.socket.emit('chat', {message:
                'usage: /give [player] [amount] <item>'});
            return;
        }

        // find target player
        const recvPlayer = this.playerData.onlinePlayerByName(recvPlayerName);

        if (recvPlayer === null) {
            player.socket.emit('chat', {message:
                'player not found: '+recvPlayerName});
            return;
        }

        // parse the specified item
        const item = Inventory.humanInput2itemCode(itemInput);
        if (item === null) {
            player.socket.emit('chat', {message:
                'unknown item: '+itemInput});
            return;
        }

        // it is possible for a non-number to sneak through
        if (isNaN(amount)) {
            player.socket.emit('chat', {message:
                'invalid amount: '+amount});
            return;
        }

        // nope didn't fail
        const itemName = Inventory.item_names[item];
        if (amount >= 0) {
            player.socket.emit('chat', {message:
                'giving '+amount+' '+itemName+' to '+recvPlayerName});
        } else {
            player.socket.emit('chat', {message:
                'removing '+amount * -1+' '+itemName+' from '+recvPlayerName});
        }
        const intAmount = parseInt(amount);
        if (recvPlayer.inventory.state[item] &&
            recvPlayer.inventory.state[item].c + intAmount < 0) {
            // can't go negative
            recvPlayer.inventory.spendAllItems(item);
        }
        else{
            // no checks. Allowing items to be given beyond limit
            recvPlayer.inventory.gainMany(item, intAmount);
        }
        //update properly
        recvPlayer.changedInventory = true;
    }

}
