// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = false;

    execute(player, args) {
        if (args.length < 2) {
            player.socket.emit('chat', {message:
                'usage: /me <message>\\[n\\]example: /me jumps'});
            return;
        }
        const message = '*\\[#'+player.color+'\\]'+player.name+'\\[/\\] '+args.slice(1).join(' ');
        for(let i = 0; i < this.playerData.onlinePlayers.length; i++){
            const sendPlayer = this.playerData.onlinePlayers[i];
            sendPlayer.socket.emit('chat', {
                message: message,
                notify: sendPlayer.name !== player.name,
                timestamp: true,
            });
        }
    }

}
