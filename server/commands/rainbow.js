// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

function rainbow(len, i) {
    const frequency = 9 / len;
    const pos = frequency * i;
    let r = 0, g = 0, b = 0;
    if (pos < Math.PI) r = Math.cos(pos)/2+0.5;
    if (pos > Math.PI*2) r = -Math.cos(pos)/2+0.5;
    if (pos < Math.PI*2) g = -Math.cos(pos)/2+0.5;
    if (pos > Math.PI) b = Math.cos(pos)/2+0.5;
    r = Math.floor(Math.sqrt(r)*255);
    g = Math.floor(Math.sqrt(g)*255);
    b = Math.floor(Math.sqrt(b)*255);
    const colorHere = '#'+('00' + r.toString(16)).slice(-2)
        + ('00' + g.toString(16)).slice(-2)
        + ('00' + b.toString(16)).slice(-2);
    return colorHere;
}

module.exports = class extends Command {
    aliases = ['/rainbowme'];
    isAdmin = false;

    execute(player, args) {
        if (args.length < 2) {
            if (args[0] === '/rainbowme') {
                player.socket.emit('chat', {message:
                    'usage: /rainbowme <message>\\[n\\]example: /rainbowme jumps'});
            }
            else {
                player.socket.emit('chat', {message:
                    'usage: /rainbowme <message>\\[n\\]example: /rainbow meow'});
            }
            return;
        }
        // format the player name to display in color
        const nameInColor = '\\[#'+player.color+'\\]'+player.name+'\\[/\\]';
        // determine prefix before the rainbow text
        const prefix = args[0] === '/rainbowme'
            ? '*'+nameInColor+' ' : nameInColor+': ';
        // determine text that will be made into rainbow color
        const textToRainbow = args.slice(1).join(' ');
        // length of rainbow
        const len = textToRainbow.length;
        // prefix each character with some color code
        const rainbowText = textToRainbow.split('')
            .map((ch,i)=>'\\['+rainbow(len,i)+'\\]'+ch).join('');
        // combine into whole message
        const message = prefix + rainbowText;
        // send to all online players
        for(let i = 0; i < this.playerData.onlinePlayers.length; i++){
            const sendPlayer = this.playerData.onlinePlayers[i];
            sendPlayer.socket.emit('chat', {
                message: message,
                notify: sendPlayer.name !== player.name,
                timestamp: true,
            });
        }

    }

}
