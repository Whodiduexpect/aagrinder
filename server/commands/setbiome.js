// Copyright (C) 2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

// @ts-ignore
String.prototype.replaceAt = function(index, newchar) {
    return this.substring(0, index) + newchar + this.substring(index+1);
}


module.exports = class extends Command {
    isAdmin = true;

    KNOWN_BIOMES = [

        ['a', 'apple', 'apple'],
        ['A', 'apleforst', 'apple forest'],
        ['j', 'aplejungl', 'apple jungle'],
        ['p', 'apricot', 'apricot'],
        ['P', 'apriforst', 'apricot forest'],
        ['J', 'jungle', 'jungle'],
        ['t', 'taiga', 'taiga'],
        ['T', 'snowtaiga', 'snowy taiga'],
        ['d', 'desert', 'desert'],
        ['r', 'mountain', 'mountain'],
        ['i', 'icemntn', 'ice mountain'],
        ['m', 'mixforest', 'mixed forest'],
        ['C', 'tentislnd', 'tentacle island'],
        ['y', 'yayisland', 'yay island'],
        ['f', 'flwrislnd', 'flower island'],
        ['u', 'undergrnd', 'underground'],
        ['c', 'spookcave', 'cave'],
        ['I', 'ice cave', 'ice cave'],
        ['o', 'ocean', 'ocean'],
        ['s', 'sky', 'sky'],
        ['R', 'rainbow', 'rainbow'],
        ['W', 'warp', 'warp'],

    ];

    execute(player, args) {

        if (args.length !== 2 && args.length !== 4) {
            player.socket.emit('chat', {message:
                'usage: /setbiome [x] [y] <biome>'});
            return;
        }

        // calculate coordinates
        let x;
        let y;
        let biomeInput;
        if (args.length === 4) {
            x = this.parseRelative(player.x, args[1]);
            y = this.parseRelative(player.y, args[2]);
            biomeInput = args[3];
        }
        else {
            x = player.x;
            y = player.y;
            biomeInput = args[1];
        }

        // validate coordinates
        if (x === false || y === false) {
            player.socket.emit('chat', {message:
                'usage: /setbiome [x] [y] <biome>'});
            return;
        }

        // validate biome
        let biome = null;
        let biomeName = null;
        for (const validNames of this.KNOWN_BIOMES) {
            if (validNames.includes(biomeInput)) {
                biome = validNames[0];
                biomeName = validNames[2];
                break;
            }
        }
        if (biome === null) {
            player.socket.emit('chat', {message:
                'unknown biome: ' + biomeInput
                + '\\[n\\]available biomes: '
                + this.KNOWN_BIOMES.map(b=>b[2]).join(', ')});
            return;
        }

        // apply
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = ((x%256+256)%256)>>2;
        const subchunky = ((y%256+256)%256)>>3;
        if (this.map.chunks[chunky] === undefined
            || this.map.chunks[chunky][chunkx] === undefined) {
            player.socket.emit('chat', {
                message: 'not loaded. Unable to set biome'});
            return;
        }
        this.map.chunks[chunky][chunkx].biomes =
            this.map.chunks[chunky][chunkx].biomes
            .replaceAt(64*subchunky+subchunkx, biome);
        this.map.chunks[chunky][chunkx].dirty = true;
        player.socket.emit('chat', {
            message: 'set biome to '+biomeName+' (relog to see)'});

    }

}
