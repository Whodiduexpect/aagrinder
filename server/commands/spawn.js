// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    aliases = ['/stuck', '/unstuck'];
    isAdmin = false;

    execute(player, args) {
        if (args[0] !== '/spawn') {
            const lines = [
                'You should have thought about it BEFORE you got in this situation.',
                'It\'s not like I *can\'t* help you, I just won\'t',
                'Keep telling yourself that',
                'Alright whatever',
                'Alright, but only this time',
                'Look, I\'m busy.',
                'You got yourself into this, and now you\'re going to get yourself out.',
                'You can\'t just go around asking strangers for help.',
                'Yes, you are.',
                'Call Superman.',
            ];
            const line = lines[Math.floor(Math.random()*lines.length)];
            player.socket.emit('chat', {message: line});
            console.log(player.name + ' got: ' + line);
            if (line[0] !== 'A') {
                return;
            }
        }

        const spawnSpot = this.spawn.choosePlayerSpawnSpot(player, true);

        this.syncher.movePlayer(player, spawnSpot.x, spawnSpot.y);
        this.subscribe.resubscribe(player, true);

        player.socket.emit('chat', {message: 'Position reset.'});
    }

}
