// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = true;

    execute(player, args) {

        if (args.length !== 2 && args.length !== 4) {
            player.socket.emit('chat', {message:
                'usage: /summon <creature> [x] [y]'});
            return;
        }

        const creature = args[1];
        let x = player.x;
        let y = player.y;

        if (args.length === 4) {

            x = this.parseRelative(player.x, args[2]);
            y = this.parseRelative(player.y, args[3]);
            if (x === false || y === false) {
                player.socket.emit('chat', {message:
                    'usage: /summon <creature> [x] [y]'});
                return;
            }

        }

        switch (creature) {

            case 'ghost':
                this.plant.ghosts.spawn(x, y, true);
                return;

            default:
                player.socket.emit('chat', {message:
                    'unknown creature: '+creature});
                return;
        }

        player.socket.emit('chat', {message:
            'summoned '+creature+' at '+x+', '+y});
    }

}
