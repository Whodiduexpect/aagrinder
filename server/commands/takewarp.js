// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = false;

    execute(player, args) {
        if (args.length < 2) {
            player.socket.emit('chat', {message:
                'usage: /takewarp <code>'});
            return;
        }
        const code = args[1];
        const warp = this.map.getCustomWarpByCode(code);
        if (warp === null || warp.p !== player.name) {
            player.socket.emit('chat', {message:
                'not found. Try /mywarps'});
            return;
        }
        if (Math.abs(warp.x - player.x) > 10
            || Math.abs(warp.y - player.y) > 10) {
            player.socket.emit('chat', {message:
                'too far to take warp'});
            return;
        }
        if (!player.inventory.canGain('WV')) {
            player.socket.emit('chat', {message:
                'no inventory space to take warp'});
            return;
        }
        // delete warp from map
        this.map.deleteCustomWarp(code);
        // gain the warp item
        player.inventory.gainItem('WV');
        //update properly
        player.changedInventory = true;

        player.socket.emit('chat', {message: 'warp taken.'});
    }

}
