// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');
const BlockProps = require('../../shared/blockprops').BlockProps;

module.exports = class extends Command {
    isAdmin = false;

    execute(player, args) {
        if (args.length < 2) {
            player.socket.emit('chat', {message:
                'usage: /warp <code>'});
            return;
        }
        const code = args[1];
        const warp = this.map.getCustomWarpByCode(code);
        if (warp === null
            || warp.x > player.x+5000 || warp.x < player.x-5000
            || warp.y > player.y+5000 || warp.y < player.y-5000) {
            player.socket.emit('chat', {message:
                'code not found within range'});
            return;
        }
        if ((warp.s === 'o' && warp.p !== player.name)
            || (warp.s === 'f' && !ACCOUNTS.accounts[player.name]
                .friends.includes(warp.p))) {
            player.socket.emit('chat', {message:
                'permission denied'});
            return;
        }
        const blockThere = this.map.getBlock(warp.x, warp.y);
        if (!BlockProps.isEnterable(blockThere)) {
            player.socket.emit('chat', {message:
                'warp obstructed'});
            return;
        }
        this.syncher.movePlayer(player, warp.x, warp.y);
        this.subscribe.resubscribe(player, true);
        player.socket.emit('chat', {message: 'warping to '+warp.c});
    }

}
