// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = true;

    execute(player, args) {
        const foundTiles = [];
        for(const chunky in this.map.chunks){
            for(const chunkx in this.map.chunks[chunky]){
                const chunk = this.map.chunks[chunky][chunkx];
                const tileEntities = chunk.tileEntities;
                // iterate all tile entities per loaded chunk
                for (const tile of tileEntities) {
                    // skip if not warp tile (portal)
                    if (tile.t !== 'portal') continue;
                    foundTiles.push(tile);
                }
            }
        }
        player.socket.emit('chat', {
            message: foundTiles
            .map(w => '[x'+w.x+',y'+w.y+']->[x'+w.d.tx+',y'+w.d.ty+']')
            .join(', '),
        });
    }

}
