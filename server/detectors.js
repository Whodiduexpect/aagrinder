// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const SPAWN_RADIUS = 200;  // Square range around player where it can spawn
const MIN_PLAYER_DISTANCE = 50;  // How far from players it must spawn
const MIN_DETECTOR_DISTANCE = 50;  // How far from detectors it must spawn
const MIN_WARP_DISTANCE = 400;  // How far from warps it must spawn
const DESPAWN_RADIUS = 600;  // How far do all players need to be
const VISIBLE_RADIUS = 40;  // How close does the player need to be
const LIFETIME = 300;  // Live for 10 minutes with no players nearby

class Detectors {

    constructor(map, playerData) {
        this.map = map;
        this.playerData = playerData;
        this.detectors = [];  // [{x:int, y:int, life:int}]
    }

    loadFromJSON(object) {
        if (typeof object === 'undefined') return;
        this.detectors = object;
    }

    saveToJSON() {
        return this.detectors;
    }

    detectorTick() {
        if (this.playerData.onlinePlayers.length === 0) {
            // no players online. Don't do unnecessary processing, just quit
            return;
        }

        const playerCount = this.playerData.onlinePlayers.length;
        if (this.detectors.length < playerCount + 1) {
            this.trySpawnDetector();
        }

        // remove detectors that are not near any players for a while
        for (let i = this.detectors.length - 1; i >= 0; i--) {
            for (const player of this.playerData.onlinePlayers) {
                const dx = player.x - this.detectors[i].x;
                const dy = player.y - this.detectors[i].y;
                const sqdist = dx*dx + 4*dy*dy;
                if (sqdist < DESPAWN_RADIUS*DESPAWN_RADIUS) {
                    // detector is near one of the players. Keep
                    this.detectors[i].life = LIFETIME;
                }
            }
            if (this.detectors[i].life < 1) {
                this.detectors.splice(i, 1);
                continue;
            }
            this.detectors[i].life--;
        }

        for (const player of this.playerData.onlinePlayers) {
            this.recalculateVisibleDetectors(player);
        }
    }

    trySpawnDetector() {
        // abort if no players (safety check)
        if (this.playerData.onlinePlayers.length === 0) return false;
        // who will it spawn near?
        const victim = this.playerData.onlinePlayers[
            Math.floor(Math.random()*this.playerData.onlinePlayers.length)];
        // where to spawn exactly?
        const dx = Math.floor(Math.random()*2*SPAWN_RADIUS)-SPAWN_RADIUS;
        const dy = Math.floor(Math.random()*2*SPAWN_RADIUS)-SPAWN_RADIUS;
        const x = victim.x + dx;
        const y = victim.y + dy;

        for (const player of this.playerData.onlinePlayers) {
            const dx = player.x - x;
            const dy = player.y - y;
            const sqdist = dx*dx + 4*dy*dy;
            if (sqdist < MIN_PLAYER_DISTANCE*MIN_PLAYER_DISTANCE) {
                // too close to one of the players. abort
                return false;
            }
        }

        for (const otherDetector of this.detectors) {
            const dx = otherDetector.x - x;
            const dy = otherDetector.y - y;
            const sqdist = dx*dx + 4*dy*dy;
            if (sqdist < MIN_DETECTOR_DISTANCE*MIN_DETECTOR_DISTANCE) {
                // too close to another detector. abort
                return false;
            }
        }

        // abort if inside a claimed area (would be unfair)
        if (this.map.getClaim(x, y) !== null) {
            return false;
        }

        // abort if not 3 by 3 air space
        for (let dy = -1; dy <= 1; dy++) {
            for (let dx = -1; dx <= 1; dx++) {
                if (this.map.getBlockWithoutLoading(
                    x+dx, y+dy, 'unloaded') !== ' ') {
                    return false;
                }
            }
        }

        // abort if near a warp
        const chunkx = x >> 8;
        const chunky = y >> 8;
        for (let dcy = -1; dcy <= 1; dcy++) {
            for (let dcx = -1; dcx <= 1; dcx++) {
                const chunk = this.map.getChunk(chunkx+dcx, chunky+dcy);
                for (const tile of chunk.tileEntities) {
                    if (tile.t !== 'portal') continue;
                    const dx = tile.x - x;
                    const dy = tile.y - y;
                    const sqdist = dx*dx + 4*dy*dy;
                    if (sqdist < MIN_WARP_DISTANCE*MIN_WARP_DISTANCE) {
                        // too close to a warp. abort
                        return false;
                    }
                }
            }
        }

        // everything is ok. Spawn detector
        this.detectors.push({x:x, y:y, life:LIFETIME});

        // check if warp nearby (by checking tile entities)
        return true;
    }

    iClickedDetector(player, x, y, follow) {
        if (Math.abs(player.x - x) > player.reach
            || Math.abs(player.y - y) > player.reach){
            // should not be able to reach!
            return false;
        }
        if (follow) {
            // abort if not have item
            if (!player.inventory.hasItem('Wo')) return false;
            player.socket.emit('chat', {message:
                'This part of the game is coming soon!'});
            return;
            return false;
        }
        // abort if no inventory space
        if (!player.inventory.canGain('E')) return false;
        // find the detector with these coordinates
        for (let i = this.detectors.length - 1; i >=0; i--) {
            if (this.detectors[i].x !== x
                || this.detectors[i].y !== y) continue;
            // found detector at this position
            this.detectors.splice(i, 1);
            player.inventory.gainItem('E');
            player.changedInventory = true;
            for (const player of this.playerData.onlinePlayers) {
                this.recalculateVisibleDetectors(player);
            }
            return true;
        }
        // no such detector
        // (probably because two players clicked it at the same time)
        return false;
    }

    recalculateVisibleDetectors(player) {
        // hide previous detectors
        if (player.visibleDetectors.length > 0){
            player.visibleDetectors = [];
            player.changedDetectors = true;
        }
        let mindist = 10000;
        for (const detector of this.detectors) {
            const dx = player.x - detector.x;
            const dy = player.y - detector.y;
            const sqdist = dx*dx + 4*dy*dy;
            if (sqdist < VISIBLE_RADIUS*VISIBLE_RADIUS) {
                // detector is within visible range. Show
                player.visibleDetectors.push(detector);
                player.changedDetectors = true;
            }
            if (sqdist < mindist) {
                mindist = sqdist;
            }
        }
        // do not send additional info if not have FOLLOW
        if (!player.inventory.hasItem('Wo')) {
            if (player.followBlinkSpeed !== 0) {
                player.followBlinkSpeed = 0;
                player.changedFollowBlink = true;
            }
            return;
        }
        let followBlinkSpeed = 0;
        if (mindist < 100*100) followBlinkSpeed = 1;
        if (mindist < 70*70) followBlinkSpeed = 2;
        if (mindist < 40*40) followBlinkSpeed = 3;
        if (mindist < 20*20) followBlinkSpeed = 4;
        if (player.followBlinkSpeed !== followBlinkSpeed) {
            player.followBlinkSpeed = followBlinkSpeed;
            player.changedFollowBlink = true;
        }
    }

}

exports.Detectors = Detectors;
