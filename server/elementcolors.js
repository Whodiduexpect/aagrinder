/*
 * Copyright 2016 OpenMarket Ltd
 * Copyright 2019, 2020 The Matrix.org Foundation C.I.C.
 *
 * This code is from matrix-react-sdk
 *
 * https://github.com/matrix-org/matrix-react-sdk/blob/f53451df654ac4958264578470965cbe2fa8ed5f/src/utils/FormattingUtils.ts#L76-L94
 *
 * https://github.com/matrix-org/matrix-react-sdk/blob/b8f6d2926cffd56d5cf3b6faf0b6acb25e81588f/res/themes/light/css/_light.scss#L207-L214
 *
 * This code is used to match Matrix username colors from Element.
 */

"use strict";

class ElementColors {

    static hashCode(str) {
        let hash = 0;
        let i;
        let chr;
        if (str.length === 0) {
            return hash;
        }
        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return Math.abs(hash);
    }

    static getUserNameColor(userId) {
        const colorNumber = this.hashCode(userId) % 8;
        return this.mx_colors[colorNumber];
    }


    static mx_colors = [
        '#368bd6',
        '#ac3ba8',
        '#0DBD8B',
        '#e64f7a',
        '#ff812d',
        '#2dc2c5',
        '#5c56f5',
        '#74d12c',
    ];

}

exports.ElementColors = ElementColors;
