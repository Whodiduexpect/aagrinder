// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const Inventory = require('../shared/inventory').Inventory;

const MAX_GHOSTS = 5;
const GHOST_ATTACK_SIZE = 5;
const GHOST_ATTACK_TRIGGER = 6;
const GHOST_ATTACK_REACH = 9.5;
const GHOST_TELEPORT_DISTANCE = 400;
const SPOOKY_VANISH_SIZE = 5;
const SPOOKY_VANISH_DEPTH = 5;
const SAFE_AREA_RADIUS = 30;
const DESPAWN_RADIUS = 200;
const SPAWN_RADIUS = 200;
const VISIBLE_RADIUS = 40;  // How close does the player need to be
const CAPTIVE_RANGE = 10;
const HAPPY_RANGE = 10;
const AVOID_RADIUS = 50;

class Ghosts {

    constructor(map, playerData, subscribe, syncher, props) {
        this.map = map;
        this.playerData = playerData;
        this.subscribe = subscribe;
        this.syncher = syncher;
        this.ghosts = [];
        this.props = props;
        this.positions = {
            avoid: [],
            captive: [],
            happy: [],
        };
    }

    loadFromJSON(object) {
        this.ghosts = object.map(o=>{
            const g = new Ghost(
                this.positions,
                this.map,
                this.playerData,
                this.subscribe,
                this.syncher
            );
            g.x = o.x;
            g.y = o.y;
            g.speedx = o.speedx;
            g.speedy = o.speedy;
            g.state = o.state;
            g.type = o.type;
            return g;
        });
    }

    saveToJSON() {
        return this.ghosts.map(g=>({
            x: g.x,
            y: g.y,
            speedx: g.speedx,
            speedy: g.speedy,
            state: g.state,
            type: g.type,
        }));
    }

    ghostTick() {

        if (this.playerData.onlinePlayers.length === 0) {
            // ghosts are paused when there are no players
            return;
        }

        // recalculate the tiles that ghosts react to.
        // (a possible optimization is to call this more rarely,
        // because it is not so important and tiles don't change often,
        // but this is currently not a bottleneck)
        this.recalculateRelevantTiles();

        // tick the ghosts
        for (const ghost of this.ghosts) {
            ghost.tick();
        }

        // filter the ghosts
        this.ghosts = this.ghosts.filter(g=>g.state!=='completelygone');

        // spawning a ghost?
        const numghost = this.ghosts.filter(g=>g.type!=='captivated').length;
        if (this.props.spawn_monsters === true && numghost < MAX_GHOSTS
            && numghost < Math.floor(this.playerData.onlinePlayers.length/2)+2) {
            // not too many ghosts already
            const g = new Ghost(
                this.positions,
                this.map,
                this.playerData,
                this.subscribe,
                this.syncher
            );
            if (g.spawnSomewhere()) {
                // it worked!
                this.ghosts.push(g);
            }
        }

        for (const player of this.playerData.onlinePlayers) {
            this.recalculateVisibleGhosts(player);
        }
        this.recalculateDetectorBlocks();
    }

    spawn(x, y, force = false) {
        const g = new Ghost(
            this.positions,
            this.map,
            this.playerData,
            this.subscribe,
            this.syncher
        );
        if (g.spawn(x, y, force)) {
            // it worked!
            this.ghosts.push(g);
        }
    }

    recalculateVisibleGhosts(player) {
        // hide previous ghosts
        if (player.visibleGhosts.length > 0) {
            player.visibleGhosts = [];
            player.changedGhosts = true;
        }
        // abort if doesn't have detector
        if (!player.inventory.hasItem('E')) {
            if (player.detectorBlinkSpeed !== 0) {
                player.detectorBlinkSpeed = 0;
                player.changedDetectorBlink = true;
            }
            return;
        }
        let mindist = 10000;
        for (const ghost of this.ghosts) {
            const dx = player.x - ghost.x;
            const dy = player.y - ghost.y;
            const sqdist = dx*dx + 4*dy*dy;
            if (sqdist < VISIBLE_RADIUS*VISIBLE_RADIUS) {
                // ghost is within visible range. Show
                player.visibleGhosts.push({x:ghost.x, y:ghost.y});
                player.changedGhosts = true;
            }
            if (sqdist < mindist) {
                mindist = sqdist;
            }
        }
        let detectorBlinkSpeed = 0;
        if (mindist < 100*100) detectorBlinkSpeed = 1;
        if (mindist < 70*70) detectorBlinkSpeed = 2;
        if (mindist < 40*40) detectorBlinkSpeed = 3;
        if (mindist < 20*20) detectorBlinkSpeed = 4;
        if (player.detectorBlinkSpeed !== detectorBlinkSpeed) {
            player.detectorBlinkSpeed = detectorBlinkSpeed;
            player.changedDetectorBlink = true;
        }
    }

    recalculateRelevantTiles() {
        // iterate all loaded chunks
        this.positions.avoid = [];
        this.positions.captive = [];
        this.positions.happy = [];
        for(const chunky in this.map.chunks){
            for(const chunkx in this.map.chunks[chunky]){
                const chunk = this.map.chunks[chunky][chunkx];
                const tileEntities = chunk.tileEntities;
                // iterate all tile entities per loaded chunk
                for (const tile of tileEntities) {
                    if (tile.t === 'grinder') {
                        const grinderBlock = this.map.getBlock(tile.x, tile.y);
                        if (grinderBlock === 'G#') {
                            this.positions.captive.push({x:tile.x, y:tile.y});
                        }
                        else if (grinderBlock === 'Gy') {
                            this.positions.happy.push({x:tile.x, y:tile.y});
                        }
                    }
                    // avoided tile entities
                    else if (tile.t === 'sspooky'
                        || tile.t === 'portal'
                        || tile.t === 'vvarp') {
                        this.positions.avoid.push({x:tile.x, y:tile.y});
                    }
                }
            }
        }
    }

    iClickedSspooky(player, x, y) {
        if (Math.abs(player.x - x) > player.reach
            || Math.abs(player.y - y) > player.reach){
            // should not be able to reach!
            return false;
        }
        // abort if not have detector to spend
        if (!player.inventory.canSpend('E')) return false;
        // abort if no inventory space
        if (!player.inventory.canGain('Ws')) return false;
        // find the ghost with these coordinates
        for (let i = this.ghosts.length - 1; i >=0; i--) {
            if (this.ghosts[i].x !== x
                || this.ghosts[i].y !== y) continue;
            // found ghost at this position
            // can not click happy ghost
            if (this.ghosts[i].type === 'happy') return false;
            // trigger clear
            this.ghosts[i].clear();
            // remove ghost
            this.ghosts.splice(i, 1);
            player.inventory.gainItem('Ws');
            player.inventory.spendItem('E');
            player.changedInventory = true;
            for (const player of this.playerData.onlinePlayers) {
                this.recalculateVisibleGhosts(player);
            }
            this.sspookyAnimationAt(x, y);
            return true;
        }
        // no such ghost
        // (probably the ghost already moved due to network delay)
        return false;
    }

    sspookyAnimationAt(x, y) {
        for (const ani of Ghosts.SSPOOKY_ANIMATION) {
            this.syncher.serverBroadcastAnimation({
                x: x + ani.x,
                y: y + ani.y,
                b: ani.b,
                a: ani.a,
                d: ani.d,
            });
        }
    }

    static SSPOOKY_ANIMATION = [  // constant
        {x:0, y:0, b:'E0', a:5},

        {x:1, y:0, b:'#s', a:3, d:0},
        {x:-1, y:0, b:'#s', a:3, d:0},

        {x:2, y:0, b:'#s', a:4, d:1},
        {x:-2, y:0, b:'#s', a:4, d:1},
        {x:1, y:1, b:'#s', a:4, d:1},
        {x:-1, y:-1, b:'#s', a:4, d:1},

        {x:2, y:1, b:'#s', a:5, d:2},
        {x:-2, y:-1, b:'#s', a:5, d:2},
        {x:1, y:2, b:'#s', a:5, d:2},
        {x:-1, y:-2, b:'#s', a:5, d:2},
        {x:0, y:2, b:'#s', a:5, d:2},
        {x:0, y:-2, b:'#s', a:5, d:2},

        {x:0, y:3, b:'#s', a:6, d:3},
        {x:0, y:-3, b:'#s', a:6, d:3},
        {x:-1, y:2, b:'#s', a:6, d:3},
        {x:1, y:-2, b:'#s', a:6, d:3},

        {x:0, y:4, b:'#s', a:7, d:4},
        {x:0, y:-4, b:'#s', a:7, d:4},
        {x:-1, y:3, b:'#s', a:7, d:4},
        {x:1, y:-3, b:'#s', a:7, d:4},

        {x:-1, y:4, b:'#s', a:8, d:5},
        {x:1, y:-4, b:'#s', a:8, d:5},
        {x:-2, y:3, b:'#s', a:8, d:5},
        {x:2, y:-3, b:'#s', a:8, d:5},

    ]

    recalculateDetectorBlocks() {
        // iterate all loaded chunks
        for(const chunky in this.map.chunks){
            for(const chunkx in this.map.chunks[chunky]){
                const chunk = this.map.chunks[chunky][chunkx];
                const tileEntities = chunk.tileEntities;
                // iterate all tile entities per loaded chunk
                for (const tile of tileEntities) {
                    if (tile.t !== 'detector') continue;
                    let mindist = 10000;
                    // find nearest ghost to this detector
                    for (const ghost of this.ghosts) {
                        const dx = tile.x - ghost.x;
                        const dy = tile.y - ghost.y;
                        const sqdist = dx*dx + 4*dy*dy;
                        if (sqdist < mindist) {
                            mindist = sqdist;
                        }
                    }
                    let detectorBlinkSpeed = 0;
                    if (mindist < 100*100) detectorBlinkSpeed = 1;
                    if (mindist < 70*70) detectorBlinkSpeed = 2;
                    if (mindist < 40*40) detectorBlinkSpeed = 3;
                    if (mindist < 20*20) detectorBlinkSpeed = 4;
                    const newBlock = 'E'+detectorBlinkSpeed;
                    const blockHere = this.map.getBlock(tile.x, tile.y);
                    if (blockHere !== newBlock) {
                        this.syncher.serverChangeBlock(tile.x, tile.y, newBlock);
                    }
                }
            }
        }
    }

}

class Ghost {

    static isStealable(block) {
        if (typeof block !== 'string') return false;
        if (block[0] === 't') return false;
        if (block[0] === '*') return false;
        return true;
    }

    constructor(positions, map, playerData, subscribe, syncher){
        this.positions = positions;
        this.map = map;
        this.playerData = playerData;
        this.subscribe = subscribe;
        this.syncher = syncher;
        this.x = 0;
        this.y = 0;
        this.speedx = 0;
        this.speedy = 0;
        this.state = 'active';
        this.type = 'normal';
    }

    tick() {

        // check if area is loaded
        if (!this.map.isBlockLoaded(this.x-5, this.y-5)
            || !this.map.isBlockLoaded(this.x+5, this.y+5)
            || !this.map.isBlockLoaded(this.x+5, this.y-5)
            || !this.map.isBlockLoaded(this.x-5, this.y+5)) {
            // something is not loaded
            // time for vanish
            this.state = 'completelygone';
            return;
        }

        // check if any players nearby
        let yesThereIsNearbyPlayer = false;
        for (const anyplayer of this.playerData.onlinePlayers) {
            const dx = anyplayer.x - this.x;
            const dy = anyplayer.y - this.y;
            const sqdist = dx*dx + dy*dy;
            if (sqdist < DESPAWN_RADIUS*DESPAWN_RADIUS) {
                yesThereIsNearbyPlayer = true;
                break;
            }
        }
        if (!yesThereIsNearbyPlayer
            && this.type !== 'captivated') {
            this.state = 'completelygone';
            return;
        }

        // whole ghost tick
        switch (this.state) {
            case 'active':
                const value = Math.random();
                if (value < 0.01) {
                    this.state = 'sleeping';
                }
                else if (value < 0.1) {
                    this.clear();
                }
                else if (value < 0.5) {
                    // the attack is different per ghost type
                    switch (this.type) {
                        case 'normal':
                            if (!this.tryAttack()) {
                                this.spookySpawn(1);
                            }
                            break;
                        case 'captivated':
                            // captivated ghost spawns spooky more rarely
                            if (Math.random() < 0.1) {
                                this.spookySpawn();
                            }
                            break;
                        case 'happy':
                            this.spookySpawn(1,'#y');
                            break;
                    }
                }
                else {
                    // the remaining 50% is for movement
                    if (this.move()) {
                        // after successful movement, type might change
                        this.updateType();
                    }
                    // sometimes change speed
                    if (value < 0.7) {
                        this.changeSpeed();
                    }
                }
                break;
            case 'attacking':
                // the previous action was "triggerAttack()"
                this.executeAttack();
                // clean the mess from the trigger
                this.clear();
                this.type = 'completelygone';
            case 'sleeping':
                this.wakeUpMaybe();
                break;
        }
    }

    clear() {
        // they disappear in chain
        // DFS over all the #s nearby
        const queue = [{x:this.x, y:this.y, depth:0}];
        let head = 0;

        let spookyWasDespawned = false;
        while (head < queue.length) {
            const cur = queue[head];
            head++;
            for (let dx = -SPOOKY_VANISH_SIZE; dx <= SPOOKY_VANISH_SIZE; dx++) {
                for (let dy = -SPOOKY_VANISH_SIZE; dy <= SPOOKY_VANISH_SIZE; dy++) {
                    const blockThere = this.map.getBlockWithoutLoading(
                        cur.x+dx, cur.y+dy);
                    // skip if not spooky there
                    if (blockThere[0] !== '#') continue;
                    // despawn this spooky
                    spookyWasDespawned = true;
                    this.syncher.serverChangeBlock(cur.x+dx, cur.y+dy, ' ');
                    const delay = Math.floor(Math.random()*8);
                    this.syncher.serverBroadcastAnimation(
                        {x: cur.x+dx, y:cur.y+dy, b: blockThere, a: delay});
                    if (cur.depth < SPOOKY_VANISH_DEPTH) {
                        queue.push({
                            x:cur.x+dx,
                            y:cur.y+dy,
                            depth:cur.depth+1
                        });
                    }
                }
            }
        }
    }

    wakeUpMaybe() {
        // wake up if there is a block in this position
        // otherwise, small chance of waking up
        if (this.map.getBlockWithoutLoading(this.x, this.y) !== ' '
            || Math.random() < 0.05
        ) {
            // WAKE UP
            this.state = 'active';
            return true;
        }
        else {
            return false;
        }
    }

    move() {
        // choose spot to move to
        let dx = Math.floor(Math.random()*7) - 3 + this.speedx;
        let dy = Math.floor(Math.random()*7) - 3 + this.speedy;
        // clamp (max movement is 3)
        if (dx < -3) dx = -3;
        if (dx > +3) dx = +3;
        if (dy < -3) dy = -3;
        if (dy > +3) dy = +3;
        if (this.type === 'captivated') {
            // only small movement in this case
            dx = Math.floor(Math.random()*3) - 1;
            dy = Math.floor(Math.random()*3) - 1;
        }
        const targetx = this.x + dx;
        const targety = this.y + dy;
        const blockThere = this.map.getBlockWithoutLoading(
            targetx, targety, 'B');
        // abort if target is not air
        if (blockThere !== ' ') return false;
        // check if target is close to something ghost avoids
        for (const aPos of this.positions.avoid) {
            const dx = aPos.x - targetx;
            const dy = aPos.y - targety;
            const sqdist = dx*dx + dy*dy;
            if (sqdist < AVOID_RADIUS*AVOID_RADIUS) {
                // target pos is too close to something ghost avoids
                // but while we're at it, check for current pos
                const dx = aPos.x - this.x;
                const dy = aPos.y - this.y;
                const cursqdist = dx*dx + dy*dy;
                if (sqdist < cursqdist) {
                    // now it is also confirmed that the new pos
                    // is closer to the avoided entity than the
                    // current pos, so this move should be aborted!
                    return false;
                }
            }
        }
        // execute the jump
        this.x = targetx;
        this.y = targety;
        return true;
    }

    changeSpeed() {
        if (this.type === 'captivated') {
            this.speedx = 0;
            this.speedy = 0;
            return;
        }
        this.speedx += Math.round(Math.random()*4-2);
        this.speedy += Math.round(Math.random()*4-2);
        //clamp
        if (this.speedx > 2) this.speedx = 2;
        if (this.speedx < -2) this.speedx = -2;
        if (this.speedy > 2) this.speedy = 2;
        if (this.speedy < -2) this.speedy = -2;
    }

    spawnSomewhere() {
        if (this.playerData.onlinePlayers.length === 0) {
            // abort
            return false;
        }
        // who?
        const victim = this.playerData.onlinePlayers[
            Math.floor(Math.random()*this.playerData.onlinePlayers.length)];
        // where?
        const dx = Math.floor(Math.random()*2*SPAWN_RADIUS)-SPAWN_RADIUS;
        const dy = Math.floor(Math.random()*2*SPAWN_RADIUS)-SPAWN_RADIUS;
        return this.spawn(victim.x+dx, victim.y+dy);
    }

    spawn(x, y, force = false) {

        const biome = this.map.getBiome(x, y);
        if (!force) {
            // check this spot for okay-ness
            // This used to be a complicated check of surrounding blocks.
            // Now we're just checking for the correct biome.
            if (biome !== 's'
                && biome !== 'R'
                && biome !== 'c'
                && biome !== 'I'
                && biome !== 'y') {
                return false;
            }
            // ghost needs 5 by 5 air
            for (let dy = -2; dy <= 2; dy++) {
                for (let dx = -2; dx <= 2; dx++) {
                    if (this.map.getBlockWithoutLoading(
                        x+dx, y+dy, 'unloaded') !== ' ') {
                        return false;
                    }
                }
            }
            // and of course ghost spawns in an air block
            // also check for nearby players
            for (const anyplayer of this.playerData.onlinePlayers) {
                const dx = anyplayer.x - x;
                const dy = anyplayer.y - y;
                const sqdist = dx*dx + dy*dy;
                if (sqdist < SAFE_AREA_RADIUS*SAFE_AREA_RADIUS) {
                    // abort because too close to this player
                    return false;
                }
            }
            // check if spawn pos is close to something ghost avoids
            for (const aPos of this.positions.avoid) {
                const dx = aPos.x - x;
                const dy = aPos.y - y;
                const sqdist = dx*dx + dy*dy;
                if (sqdist < AVOID_RADIUS*AVOID_RADIUS) {
                    // abort because too close to something ghost avoids
                    return false;
                }
            }
        }

        // spawn the ghost
        this.x = x;
        this.y = y;
        this.updateType();
        this.spookySpawn(2);
        return true;
    }

    spookySpawn(n = 1, block = '#s') {
        // create some spooky around here
        for (let i = 0; i < n; i++) {
            const dx = Math.round(Math.random()*6)-3;
            const dy = Math.round(Math.random()*6)-3;
            // skip this exact spot
            if (dx === 0 && dy === 0) continue;
            if (this.map.getBlockWithoutLoading(this.x+dx, this.y+dy, 'B') === ' ') {
                this.syncher.serverChangeBlock(this.x+dx, this.y+dy, block);
            }
        }
    }

    tryAttack() {
        for (const player of this.playerData.onlinePlayers) {

            // no attack if too far
            if (Math.abs(player.x - this.x) >= GHOST_ATTACK_TRIGGER
                || Math.abs(player.y - this.y) >= GHOST_ATTACK_TRIGGER) continue;

            // any spooky in area?
            let spooky = false;
            for (let i = -10; i < 10; i++) {
                for (let j = -10; j < 10; j++) {
                    if (this.map.getBlock(player.x+i, player.y+j)[0] === '#') {
                        spooky = true;
                    }
                }
            }
            // no attack if no spooky in area
            if (!spooky) continue;

            // attack! Maybe not specifically player,
            // but the player that will be nearest at next tick
            this.triggerAttack();
            return true;
        }
    }

    triggerAttack() {
        // this is the "warmup" for the attack
        // create a lot of spooky around here
        const intReach = Math.floor(GHOST_ATTACK_REACH);
        for (let dx = -intReach; dx <= intReach; dx++) {
            for (let dy = -intReach; dy <= intReach; dy++) {
                const sqdist = dx*dx + dy*dy;
                // skip if outside radius
                if (sqdist > GHOST_ATTACK_REACH*GHOST_ATTACK_REACH) continue;
                const blockThere = this.map.getBlockWithoutLoading(
                    this.x+dx, this.y+dy);
                const time = Math.abs(dx) + Math.abs(dy);
                // always do a pre-animation (even if wall)
                this.syncher.serverBroadcastAnimation({
                    x: this.x + dx,
                    y: this.y + dy,
                    b: '#s',
                    a: time,
                    d: time-1,
                });
                if (blockThere === ' ') {
                    // create real # but animated
                    this.syncher.serverChangeBlock(this.x+dx, this.y+dy, '#s');
                    this.syncher.serverBroadcastAnimation({
                        x: this.x + dx,
                        y: this.y + dy,
                        b: ' ',
                        a: time+3,
                    });
                }
            }
        }
        this.state = 'attacking';
    }

    executeAttack() {
        for (const player of this.playerData.onlinePlayers) {
            const dx = player.x - this.x;
            const dy = player.y - this.y;
            const sqdist = dx*dx + dy*dy;
            // skip if outside radius
            if (sqdist > GHOST_ATTACK_REACH*GHOST_ATTACK_REACH) continue;
            if (this.attackPlayer(player)) {
                // After attacking one player, abort
                // because there's no good solution to attacking multiple
                return true;
            }
        }
        this.state = 'normal';
        return false;
    }

    attackPlayer(player) {
        // find a good target position
        let targetx;
        let targety;
        let i = 0;
        while (i < 10) {
            // choose a target position
            const distance = Math.random()*200 + GHOST_TELEPORT_DISTANCE-100;
            const angle = Math.random()*Math.PI*2;
            targetx = player.x + Math.floor(Math.cos(angle)*distance);
            targety = player.y + Math.floor(Math.sin(angle)*distance);
            // check if spot is clear
            let isclear = true;
            for (let ix = -GHOST_ATTACK_SIZE-2; ix <= GHOST_ATTACK_SIZE+2; ix++) {
                for (let iy = -GHOST_ATTACK_SIZE-2; iy <= GHOST_ATTACK_SIZE+2; iy++) {
                    if (this.map.getBlock(targetx+ix, targety+iy) !== ' ') {
                        isclear = false;
                        break;
                    }
                }
                if (!isclear) {
                    break;
                }
            }
            if (isclear) {
                // yes spot is clear, it's ok to teleport here!
                break;
            }
            i++;
        }
        if (i >= 10) {
            // you got lucky this time
            console.log('ghost failed to attack');
            return false;
        }

        // found target position successfully
        // delete all #'s from inventory, hehehe
        player.inventory.spendAllItems('#s');
        player.inventory.spendAllItems('#y');
        player.changedInventory = true;
        // teleport the surrounding area to the target area
        const changes = [];
        for (let ix = -GHOST_ATTACK_SIZE-2; ix <= GHOST_ATTACK_SIZE+2; ix++) {
            for (let iy = -GHOST_ATTACK_SIZE-2; iy <= GHOST_ATTACK_SIZE+2; iy++) {
                const ir = Math.sqrt(ix*ix+iy*iy) // distance from center
                if (ir >= GHOST_ATTACK_SIZE + 2) continue;
                if (ir >= GHOST_ATTACK_SIZE) {
                    // not within the inner radius, but in the ring itself.
                    // place a stone
                    changes.push({x:targetx+ix, y:targety+iy, block:'B'});
                    continue;
                }

                // otherwise it's inside the radius.
                const blockHere = this.map.getBlock(player.x+ix, player.y+iy);
                let wasBlockTeleported = false;
                // a few blocks that can't be teleported by ghost.
                // I didn't put boulder in this list
                // because I do not think this can be exploited.
                // (this is the only way of moving a boulder block)
                if (blockHere !== ' '
                    && blockHere[0] !== '#'
                    && blockHere !== 'WW'
                    && blockHere !== 'WA'
                ) {
                    // teleport this block to the new area
                    changes.push({x:targetx+ix, y:targety+iy, block:blockHere});
                    wasBlockTeleported = true;
                }
                // steal a block
                let blockStolen = Inventory.item_codes[
                    Math.floor(Math.random()*Inventory.item_codes.length)];
                if (Ghost.isStealable(blockStolen)
                    && player.inventory.canGain(blockStolen, -1)) {
                    // hehehehe
                    if (Inventory.isDataItem(blockStolen)) {
                        // data item
                        const d = player.inventory
                            .state[blockStolen].d[0];
                        player.inventory.gainItem(blockStolen, -1, 0);
                        const tile = {
                            t: 'grinder',
                            x: player.x+ix,
                            y: player.y+iy,
                            d: d,
                        };
                        this.syncher.createTileEntity(tile);
                    } else {
                        // non-data item
                        player.inventory.gainItem(blockStolen, -1);
                    }
                    blockStolen = Inventory.item2blockPlain(blockStolen);
                    changes.push({x:player.x+ix, y:player.y+iy, block:blockStolen});
                }
                else if (wasBlockTeleported) {
                    // didn't steal a block.
                    // but because there was a block here earlier,
                    // it still needs to be removed,
                    // otherwise this would be a duplication device.
                    changes.push({x:player.x+ix, y:player.y+iy, block:' '});
                }
            }
        }
        // remember to actually teleport the player too!
        this.syncher.movePlayer(player, targetx, targety);
        this.subscribe.resubscribe(player, true);
        this.syncher.serverChangeBlocks(changes);
        // trigger animation
        player.animation = 'ghost';

        // announce
        const message = '\\[#'+player.color+'\\]'+player.name
            +' \\[#aa3\\]didn\'t believe in ghosts, until they got smacked by one';
        LOG.log(message);
        for (const otherplayer of this.playerData.onlinePlayers) {
            otherplayer.socket.emit('chat', {
                message: message,
            });
        }
        this.state = 'completelygone';
        return true;
    }

    updateType() {
        // check if biome is yay
        const biome = this.map.getBiome(this.x, this.y);
        if (biome === 'y') {
            // become or stay happy
            this.type = 'happy';
            return;
        }

        for (const cPos of this.positions.captive) {
            const dx = cPos.x - this.x;
            const dy = cPos.y - this.y;
            if (Math.abs(dx) < CAPTIVE_RANGE
                && Math.abs(dy) < CAPTIVE_RANGE) {
                // become or stay captivated
                this.type = 'captivated';
                return;
            }
        }

        for (const hPos of this.positions.happy) {
            const dx = hPos.x - this.x;
            const dy = hPos.y - this.y;
            if (Math.abs(dx) < CAPTIVE_RANGE
                && Math.abs(dy) < CAPTIVE_RANGE) {
                // become or stay happy
                this.type = 'happy';
                return;
            }
        }

        // nothing special in range. Be normal
        this.type = 'normal';
    }

}

exports.Ghosts = Ghosts;
exports.Ghost = Ghost;
