// Copyright (C) 2018-2022 MRAAGH and contributors

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

// Contains all hopper-related logic

"use strict";

class Hopper {

    constructor(map, syncher) {
        this.map = map;
        this.syncher = syncher;
        this.interval = undefined;
        this.chestRelations = [];   // x1: Source X, y1: Source Y, x2: Dest X, y2: Dest Y
    }

    setLoop(multiplier) {

        // stop previous loop if exists
        if (this.interval !== undefined) {
            clearInterval(this.interval);
        }

        if (multiplier < 0.01) {
            // don't start the interval at all
            return;
        }

        // start interval
        this.interval = setInterval(() => {
            this.doHopperTick();
        }, 200 / multiplier);

    }

    sortRelations() {
        this.chestRelations.sort((rel1, rel2) => {
            // figure out what is vertical
            const rel1vert = rel1.x1 === rel1.x2;
            const rel2vert = rel2.x1 === rel2.x2;
            // is only the first one vertical?
            if (rel1vert && !rel2vert) return -1;
            // is only the second one vertical?
            if (!rel1vert && rel2vert) return 1;
            // are they both vertical?
            if (rel1vert && rel2vert) return rel1.y1 - rel2.y1;
            // No, none are vertical.

            // figure out what is left
            const rel1left = rel1.x1 > rel1.x2;
            const rel2left = rel2.x1 > rel2.x2;
            // is only the first one left?
            if (rel1left && !rel2left) return 1;
            // is only the second one left?
            if (!rel1left && rel2left) return -1;
            // are they both left?
            if (rel1left && rel2left) return rel1.x1 - rel2.x1;
            // No, none are left.

            // both are right, then.
            return rel2.x1 - rel1.x1;
        });
    }

    followTrail(block, x, y, dx, dy, max_depth=20) {
        // Follow trail of blocks starting at x y
        // returns block at the end (first block to not match) as well as it's coords
        // if the max depth is reached that block will be returned regardless

        let block_here = block;
        let i = 0;

        while (block_here === block) {
            i++;

            block_here = this.map.getBlock(x + i * dx, y + i * dy);
            if (i > max_depth) break;
        }

        const block_x = x + i * dx;
        const block_y = y + i * dy;
        return {b:block_here, x:block_x, y:block_y};
    }

    processHopper(x, y, block) {
        // Update from placing a hopper
        // this handles if the block passed is not a hopper
        // but it is recommended to avoid calling this method in the first place
        if (block[0] === 'p' && block !== 'pv') {
            // Horizontal Hoppers
            const block_right = this.map.getBlock(x + 1, y);
            const block_left = this.map.getBlock(x - 1, y);

            // Let's first check if our placement even makes sense
            const our_direction = block[1];

            if ( !( block_left[0] === 'h' || block_left === block ) ) return;
            if ( !( block_right[0] === 'h' || block_right === block ) ) return;

            // it's plausible that we just connected two chests together
            const rr = this.followTrail(block, x, y, 1, 0);
            const rl = this.followTrail(block, x, y, -1, 0);

            if ( !(rr.b[0] === 'h' && rl.b[0] === 'h') ) return;
            if ( Math.abs(rr.x - rl.x) > 22 ) return;

            // Make the relationship
            if (our_direction === '<') {
                // Were pointing left, so left is the destination
                this.chestRelations.push({x1: rr.x, y1: y, x2: rl.x, y2: y, active: true});
                this.sortRelations();
            }
            else if (our_direction === '>') {
                // Were pointing right, so right is the destination
                this.chestRelations.push({x1: rl.x, y1: y, x2: rr.x, y2: y, active: true})
                this.sortRelations();
            }
        }
        else if (block === 'pv') {
            // Vertical Hoppers
            // Doing additional checks here doesn't do much, so directly check
            const rt = this.followTrail(block, x, y, 0, 1);
            const rb = this.followTrail(block, x, y, 0, -1);

            if ( !(rt.b[0] === 'h' && rb.b[0] === 'h') ) return;
            if ( Math.abs(rt.y - rb.y) > 22 ) return;

            // Make the relationship
            this.chestRelations.push({x1: rt.x, y1: rt.y, x2: rb.x, y2: rb.y, active: true});
            this.sortRelations();
        }
    }

    doHopperTick() {
        // Chest Relationship Tick
        let dirty = false;

        for (let i = 0; i < this.chestRelations.length; i++) {
            const rel = this.chestRelations[i];
            if (rel === undefined) {
                delete this.chestRelations[i];
                dirty = true;
                console.error("Undefined chest relationship")
                continue;
            }

            if (rel.active === false) continue;

            let sourceTile = this.map.getTileEntityAt(rel.x1, rel.y1);

            if (sourceTile.length > 0) {

                const source = sourceTile[0].inventory;

                if (Object.getOwnPropertyNames(source.state).length < 1) {
                    rel.active = false;
                    continue;
                }

                // We have things to move, load the destination chest
                let destTile = this.map.getTileEntityAt(rel.x2, rel.y2);
                const dest = destTile[0].inventory;

                for (let item in source.state) {
                    source.moveItemsTo(dest, item, 1, 0);
                    this.syncher.updateTileEntity(sourceTile[0]);
                    this.syncher.updateTileEntity(destTile[0]);
                    break;
                }
            } else {
                console.error("No tile entity at chest spot. This is probably because of a bug in the chest relationship update logic.");
                delete this.chestRelations[i];
                dirty = true;
                continue;
            }
        }
        // Re-write the array without deleted spots
        if (dirty) {
            this.chestRelations = this.chestRelations.filter(function(element){return element !== undefined;});
        }
    }

    loadFromJSON(object){
        if (object) this.chestRelations = object;
    }

    saveToJSON(){
        return this.chestRelations;
    }
}

exports.Hopper = Hopper;
