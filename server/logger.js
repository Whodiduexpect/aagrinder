// Copyright (C) 2018-2021 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const fs = require('fs');

class Logger {

    getDateTime() {
        var date = new Date();
        var hour = date.getHours();
        // @ts-ignore
        hour = (hour < 10 ? "0" : "") + hour;
        var min  = date.getMinutes();
        // @ts-ignore
        min = (min < 10 ? "0" : "") + min;
        var sec  = date.getSeconds();
        // @ts-ignore
        sec = (sec < 10 ? "0" : "") + sec;
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        // @ts-ignore
        month = (month < 10 ? "0" : "") + month;
        var day  = date.getDate();
        // @ts-ignore
        day = (day < 10 ? "0" : "") + day;
        return year + "-" + month + "-" + day + "--" + hour + "-" + min + "-" + sec;
    }

    constructor() {
        this.logfile = null;
        this.logStart = this.getDateTime()
        this.enable_server_log = false;
    }

    initialize(enable_server_log) {
        this.enable_server_log = enable_server_log;
        if (!this.enable_server_log) return;
        // make log dir
        if(!fs.existsSync('logs')){
            fs.mkdirSync('logs');
        }
        this.logfile = 'logs/'+this.getDateTime()+'.log';
        console.log('log file: '+this.logfile);
        this.logonly('log begins at '+this.logStart);
    }

    log(content) {
        content = '['+this.getDateTime()+'] '+content;
        console.log(content);
        this.logonly(content);
    }

    logonly(content) {
        if (!this.enable_server_log) {
            return;
        }
        if (typeof(content) !== 'string') {
            content = JSON.stringify(content);
        }
        fs.appendFile(this.logfile, content + '\n', err=>{if(err) throw err;});
    }

}

exports.Logger = Logger;
