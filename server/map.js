// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Map is the class holding everything about the world that needs to get saved to disk
 * and loaded from disk.
 * 
 * It also acts as an abstraction over chunks,
 * so that other classes do not need to worry about whether chunks exist or don't exist.
 * Calling the getChunk method always works.
 * If the chunk does not exist it is automatically generated on the fly.
 * 
 * A similar abstraction exists over players and can be found in PlayerData.js.
 */

"use strict";

const fs = require('fs');
const { promisify } = require('util');
const existsAsync = promisify(fs.exists);
const writeFileAsync = promisify(fs.writeFile);
const readFileAsync = promisify(fs.readFile);
const mkdirAsync = promisify(fs.mkdir);

const Chunk = require('../shared/chunk').Chunk;
const child_process = require('child_process');
const execFileSync = child_process.execFileSync;
const execFile = child_process.execFile;
const execFileAsync = promisify(child_process.execFile);

const Inventory = require('../shared/inventory').Inventory;

const NEWEST_CHUNK_VERSION = 3;

class Map {
    constructor(props, enqueueWater) {
        this.props = props;
        this.worldSeed = props.level_seed;
        this.chunks = {};
        this.customWarps = [];
        this.spawnareax = undefined;
        this.spawnareay = undefined;
        this.chunkDir = './saves/' + props.level_name + '/chunks/';
        this.halfStagedChunksForUnload = [];
        this.stagedChunksForUnload = [];
        if (props.enable_unloading) {
            this.unloadInterval = setInterval(() => this.unloadChunks(), 10000);
        }
        this.enqueueWater = enqueueWater;
        this.generatorExecutable = './aagrinder-terrain/aagrinderterrain';
    }

    getChunk(chunkx, chunky){
        if(!this.chunks[chunky]){
            // make row
            this.chunks[chunky] = {};
        }
        if(!this.chunks[chunky][chunkx]){
            // load or generate it
            this.loadManyChunksSync([{x: chunkx, y: chunky}]);
        }
        return this.chunks[chunky][chunkx];
    }

    prepareRows(coords) {
        for (const {x, y} of coords) {
            if(!this.chunks[y]){
                this.chunks[y] = {};
            }
        }
    }

    makeGenerationArgs(coords) {
        const args = [
            '-s',
            '--',
        ];
        for (const {x, y} of coords) {
            args.push(x.toString());
            args.push(y.toString());
        }
        // insert world seed
        args.splice(4, 0, this.worldSeed.toString());
        return args;
    }

    callGeneratorSync(coords) {
        return execFileSync(
            this.generatorExecutable,
            this.makeGenerationArgs(coords),
            {encoding:'utf8', maxBuffer: 1024*1024}
        );
    }

    async callGeneratorAsync(coords) {
        return (await execFileAsync(
            this.generatorExecutable,
            this.makeGenerationArgs(coords),
            {encoding:'utf8', maxBuffer: 1024*1024}
        )).stdout;
    }

    makeChunkFilename(chunkx, chunky) {
        return this.chunkDir+'chunk_'+chunkx+'_'+chunky;
    }

    async createSaveDirs() {
        if (!await existsAsync('./saves')) {
            await mkdirAsync('./saves');
        }
        if (!await existsAsync('./saves/'+this.props.level_name)) {
            await mkdirAsync('./saves/'+this.props.level_name);
        }
        if (!await existsAsync('./saves/'+this.props.level_name+'/chunks')) {
            await mkdirAsync('./saves/'+this.props.level_name+'/chunks');
        }
    }

    unpackDiskChunk(chunkx, chunky, loadedString) {
        let tileEntities;
        let biomes;
        let compressedTerrain;
        let claims;
        let dirty = false;
        // check if it has the new format
        if (loadedString[0] === '{') {
            // it has the new format!
            // (because it's clearly a JSON object)
            const loadedObject = JSON.parse(loadedString);
            let chunkVersion = loadedObject.v;
            if (typeof chunkVersion !== 'number') {
                // default chunk version is 0
                chunkVersion = 0;
            }
            if (chunkVersion > NEWEST_CHUNK_VERSION) {
                console.log('WARNING: downgrading is not supported!!');
            }
            compressedTerrain = loadedObject.t;
            if (chunkVersion < 2) {
                // need to convert wires to 4-char format
                compressedTerrain = compressedTerrain.replace(
                    /=../g, match => match+'R');
            }
            if (chunkVersion < 3) {
                // need to convert crossing to 2-char format
                compressedTerrain = compressedTerrain.replace(
                    /%../g, '%0');
            }
            tileEntities = loadedObject.e.map(Map.loadEntity);
            biomes = loadedObject.b;
            // ensure claims field exists (missing in old format)
            claims = loadedObject.c ? loadedObject.c : [];
        }
        else {
            // using an old format.
            // try to load anyway.
            compressedTerrain = loadedString;
            tileEntities = [];
            biomes = Array(128*64+1).join('A');
            claims = [];
        }
        // ensure row exists
        if(!this.chunks[chunky]){
            this.chunks[chunky] = {};
        }
        this.chunks[chunky][chunkx] = new Chunk();
        const err = this.chunks[chunky][chunkx].setCompressed(compressedTerrain);
        if (err !== 0) {
            LOG.log('Corrupted ('+err+') terrain string in loaded chunk_'+chunkx+'_'+chunky);
        }
        this.chunks[chunky][chunkx].tileEntities = tileEntities;
        this.chunks[chunky][chunkx].biomes = biomes;
        this.chunks[chunky][chunkx].claims = claims;
        this.chunks[chunky][chunkx].dirty = dirty;
        Map.postProcessChunk(this.chunks[chunky][chunkx]);
    }

    static postProcessChunk(chunk) {
        for (const tile of chunk.tileEntities) {
            if (tile.t === 'chest') {
                // determine chest slot number based on block type
                const subchunkx = (tile.x%256+256)%256;
                const subchunky = (tile.y%256+256)%256;
                const block = chunk.terrain[subchunky][subchunkx];
                const total_slots = block === 'hh' ? 10 : 20;
                tile.inventory.total_slots = total_slots;
            }
        }
    }

    static loadEntity(entity) {
        if (entity.t === 'chest') {
            // chest gets loaded with inventory instance
            if (!entity.d.n) {
                // legacy loader
                const inv = {};
                for (const item_code in entity.d) {
                    const value = entity.d[item_code];
                    if (Inventory.isDataItem(item_code)) {
                        // start at default data
                        inv[item_code] = {
                            c: value,
                            d: [ Inventory.makeItemData(item_code) ]
                        };
                    } else {
                        inv[item_code] = {c:value};
                    }
                }
                return {
                    t: 'chest', // type is chest
                    x: entity.x, // global x coordinate
                    y: entity.y, // global y coordinate
                    inventory: new Inventory(inv, 10),
                    subscribers: [], // no subscribers
                }
            }
            else {
                return {
                    t: 'chest', // type is chest
                    x: entity.x, // global x coordinate
                    y: entity.y, // global y coordinate
                    inventory: new Inventory(entity.d.s, entity.d.n),
                    subscribers: [], // no subscribers
                }
            }
        }
        // other entities are raw data.
        // delete the legacy "hidden" field though.
        delete entity.h;
        // and the "subscribe" field is not needed.
        delete entity.s;
        return entity;
    }

    static unloadEntity(entity) {
        if (entity.t === 'chest') {
            // chest gets saved without inventory instance
            return {
                t: 'chest', // type is chest
                x: entity.x, // global x coordinate
                y: entity.y, // global y coordinate
                d: entity.inventory.saveToJSON() // extract from inv object
            }
        }
        // other entities are unchanged.
        return entity;
    }

    unpackGeneratedChunks(generatedJSONstring) {
        const generatedMany = JSON.parse(generatedJSONstring);
        for (const generated of generatedMany) {
            const chunkx = generated.x;
            const chunky = generated.y;
            // ensure row exists
            if(!this.chunks[chunky]){
                this.chunks[chunky] = {};
            }
            this.chunks[chunky][chunkx] = new Chunk();
            const err = this.chunks[chunky][chunkx].setCompressed(generated.t);
            this.chunks[chunky][chunkx].biomes = generated.b;
            this.chunks[chunky][chunkx].tileEntities = generated.e.map(Map.loadEntity);
            if (err !== 0) {
                LOG.log('Corrupted ('+err+') generated terrain string for chunk '+chunkx+', '+chunky);
            }
            this.chunks[chunky][chunkx].claims = [];
            // save new chunks. Even if they were not modified.
            // because disk is cheaper than cpu, hihi
            this.chunks[chunky][chunkx].dirty = true;
            // water updates can be generated
            for (const w of generated.w) {
                // this is an external function call
                this.enqueueWater(w.x, w.y);
            }
        }
    }

    loadManyChunksSync(coords) {

        this.prepareRows(coords);

        const gencoords = [];

        for (const {x, y} of coords) {
            // Skip if chunk already in array.
            if (this.chunks[y][x]) continue;
            // No chunk in array. Try loading it.
            const filename = this.makeChunkFilename(x, y);
            if (fs.existsSync(filename)) {
                console.log('load chunk: ' + x + ' ' + y);
                let loadedString = fs.readFileSync(filename, 'utf8');
                this.unpackDiskChunk(x, y, loadedString);
            }
            else {
                gencoords.push({x:x, y:y});
            }
        }

        if (gencoords.length < 1) return;

        // Remaining chunks do not exist, not even on disk. Generate them.
        for (let i = 0; i < gencoords.length; i+=this.props.multithread) {
            const section = gencoords.slice(i, i+this.props.multithread);
            LOG.log('generate chunks: '+section.map(a=>'['+a.x+' '+a.y+']').join(', '));
            const generatedJSONstring = this.callGeneratorSync(section);
            this.unpackGeneratedChunks(generatedJSONstring);
        }
    }

    async loadManyChunksAsync(coords) {

        this.prepareRows(coords);

        const gencoords = [];

        for (const {x, y} of coords) {
            const filename = this.makeChunkFilename(x, y);
            if (await existsAsync(filename)) {
                LOG.log('load chunk: ' + x + ' ' + y);
                let loadedString = await readFileAsync(filename, 'utf8');
                this.unpackDiskChunk(x, y, loadedString);
            }
            // Skip if chunk already in array
            if (this.chunks[y][x]) continue;
            // No chunk in array. Try loading it.
            if (fs.existsSync(filename)) {
                LOG.log('load chunk: ' + x + ' ' + y);
                let loadedString = fs.readFileSync(filename, 'utf8');
                this.unpackDiskChunk(x, y, loadedString);
            }
            else {
                gencoords.push({x:x, y:y});
            }
        }

        if (gencoords.length < 1) return;

        // Remaining chunks do not exist, not even on disk. Generate them.
        for (let i = 0; i < gencoords.length; i+=this.props.multithread) {
            const section = gencoords.slice(i, i+this.props.multithread);
            LOG.log('generate chunks: '+section.map(a=>'['+a.x+' '+a.y+']').join(', '));
            const generatedJSONstring = await this.callGeneratorAsync(section);
            this.unpackGeneratedChunks(generatedJSONstring);
        }
    }

    getBlock(x, y){
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = (x%256+256)%256;
        const subchunky = (y%256+256)%256;
        const chunk = this.getChunk(chunkx, chunky);
        const block = chunk.terrain[subchunky][subchunkx];
        return block;
    }

    getBlockWithoutLoading(x, y, defaul=' '){
        if (!this.isBlockLoaded(x, y)) {
            return defaul;
        }
        return this.getBlock(x, y);
    }

    setBlock(x, y, block){
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = (x%256+256)%256;
        const subchunky = (y%256+256)%256;
        const chunk = this.getChunk(chunkx, chunky);
        chunk.terrain[subchunky][subchunkx] = block;
        chunk.dirty = true; // needs saving
    }

    setDirtyAtXY(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = (x%256+256)%256;
        const subchunky = (y%256+256)%256;
        const chunk = this.getChunk(chunkx, chunky);
        chunk.dirty = true;
    }

    isChunkLoaded(chunkx, chunky) {
        if (!this.chunks[chunky]){
            return false;
        }
        if (!this.chunks[chunky][chunkx]){
            return false;
        }
        return true;
    }

    isBlockLoaded(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        return this.isChunkLoaded(chunkx, chunky);
    }

    getTileEntityAt(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const chunk = this.getChunk(chunkx, chunky);
        return chunk.tileEntities.filter(a=>a.x===x&&a.y===y);
    }

    createTileEntity(data) {
        const chunkx = data.x >> 8;
        const chunky = data.y >> 8;
        const chunk = this.getChunk(chunkx, chunky);
        chunk.tileEntities.push(data);
        chunk.dirty = true; // needs saving
        // return the chunk
        // because in most cases we need it.
        // and this way we can avoid looking it up twice.
        return chunk;
    }

    deleteTileEntityAt(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const chunk = this.getChunk(chunkx, chunky);
        chunk.tileEntities = chunk.tileEntities
            .filter(a=>a.x!==x||a.y!==y);
        chunk.dirty = true; // needs saving
        // return the chunk
        // because in most cases we need it.
        // and this way we can avoid looking it up twice.
        return chunk;
    }

    getCustomWarpByCode(code) {
        for (const w of this.customWarps) {
            if (w.c === code) {
                return w;
            }
        }
        return null;
    }

    getCustomWarpsByPlayer(playername) {
        return this.customWarps
            .filter(w=>w.p===playername);
    }

    createCustomWarp(x, y, code, playername, scope) {
        this.customWarps.push({
            x: x,
            y: y,
            c: code,
            p: playername,
            s: scope,
        });
    }

    deleteCustomWarp(code) {
        this.customWarps = this.customWarps
            .filter(w=>w.c!==code);
    }

    // TODO: this is needed due to an incompatibility
    // because diggrinderlogic.js wants to access this directly
    // (which is a bad idea in the first place)
    biomeAt(x, y) {
        return this.getBiome(x, y);
    }

    getBiome(x, y){
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = ((x%256+256)%256)>>2;
        const subchunky = ((y%256+256)%256)>>3;

        if(!this.chunks[chunky]){
            return ' ';
        }
        if(!this.chunks[chunky][chunkx]){
            return ' ';
        }
        const chunk = this.chunks[chunky][chunkx];
        const biome = chunk.biomes[64*subchunky+subchunkx];
        if (biome === undefined) {
            console.log(chunk.biomes);
        }
        return biome;
    }

    getBiomeWithoutLoading(x, y, defaul='A'){
        if (!this.isBlockLoaded(x, y)) {
            return defaul;
        }
        return this.getBiome(x, y);
    }

    addClaim(accountName, x, y, x2, y2) {
        const chunkCoordList = Map.getChunksInClaim(x, y, x2, y2);
        const claim = {x:x, y:y, x2:x2, y2:y2, p:accountName};
        for (const {x, y} of chunkCoordList) {
            const chunk = this.getChunk(x, y);
            // it is ok to re-use the same claim object many times
            // because it will never be modified
            chunk.claims.push(claim);
            chunk.dirty = true;  // needs saving
        }
        return chunkCoordList;
    }

    removeClaimAt(x, y) {
        const claim = this.getClaim(x, y);
        if (claim === null) return;
        const chunkCoordList = Map.getChunksInClaim(
            claim.x, claim.y, claim.x2, claim.y2);
        for (const {x, y} of chunkCoordList) {
            const chunk = this.getChunk(x, y);
            chunk.claims = chunk.claims.filter(
                c => !(c.x === claim.x
                    && c.x2 === claim.x2
                    && c.y === claim.y
                    && c.y2 === claim.y2
                    && c.p === claim.p));
            chunk.dirty = true;  // needs saving
        }
        return chunkCoordList;
    }

    // Determine who (if any) owns a claim at a specific position
    getClaim(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const chunk = this.getChunk(chunkx, chunky);
        for (const claim of chunk.claims) {
            if (claim.x <= x && x <= claim.x2
                && claim.y <= y && y <= claim.y2) {
                return claim;
            }
        }
        return null;
    }

    // Determine which chunks overlup a claimed area
    static getChunksInClaim(x, y, x2, y2) {
        const chunkCoordList = [];
        for (let chunky = y >> 8; chunky <= y2 >> 8; chunky++) {
            for (let chunkx = x >> 8; chunkx <= x2 >> 8; chunkx++) {
                chunkCoordList.push({x: chunkx, y: chunky});
            }
        }
        return chunkCoordList;
    }

    async unloadChunks() {
        const promises = [];
        if (this.stagedChunksForUnload.length > 0) {
            await this.createSaveDirs();
        }
        for (const {x, y} of this.stagedChunksForUnload) {
            if (typeof(this.chunks[y]) === 'undefined') {
                // huh, row is missing
                continue;
            }
            if (typeof(this.chunks[y][x]) === 'undefined') {
                // huh, column is missing
                continue;
            }
            const chunk = this.chunks[y][x];
            if (chunk.subscribers.length > 0) {
                // huh, someone is still looking at this
                continue;
            }
            if (chunk.dirty) {
                const filename = this.makeChunkFilename(x, y);
                console.log('save',filename);
                const savingObject = {
                    v: NEWEST_CHUNK_VERSION,
                    t: chunk.getCompressed(),
                    e: chunk.tileEntities.map(Map.unloadEntity),
                    b: chunk.biomes,
                    c: chunk.claims,
                }
                const savingString = JSON.stringify(savingObject);
                chunk.dirty = false;
                const p = writeFileAsync(filename, savingString);
                promises.push(p);
            }
            delete this.chunks[y][x];
            if (Object.keys(this.chunks[y]).length === 0) {
                delete this.chunks[y];
            }
        }
        this.stagedChunksForUnload = [];
        // from half stage to stage:
        for (const {x, y} of this.halfStagedChunksForUnload) {
            if (typeof(this.chunks[y]) === 'undefined') {
                // huh, row is missing
                continue;
            }
            if (typeof(this.chunks[y][x]) === 'undefined') {
                // huh, column is missing
                continue;
            }
            const chunk = this.chunks[y][x];
            if (chunk.subscribers.length > 0) {
                // huh, someone is still looking at this
                continue;
            }
            this.stagedChunksForUnload.push({x:x, y:y});
        }
        // clear half stage:
        this.halfStagedChunksForUnload = [];
        await Promise.all(promises);
    }

    async flushChunks() {
        const promises = [];
        for (const y in this.chunks){
            for (const x in this.chunks[y]){
                const chunk = this.chunks[y][x];
                if (chunk.dirty) {
                    const filename = this.makeChunkFilename(x, y);
                    const savingObject = {
                        v: NEWEST_CHUNK_VERSION,
                        t: chunk.getCompressed(),
                        e: chunk.tileEntities.map(Map.unloadEntity),
                        b: chunk.biomes,
                        c: chunk.claims,
                    }
                    const savingString = JSON.stringify(savingObject);
                    chunk.dirty = false;
                    const p = writeFileAsync(filename, savingString);
                    promises.push(p);
                }
            }
        }
        await Promise.all(promises);
    }

    saveToJSON(){
        return {
            seed: this.worldSeed,
            spawn: {x: this.spawnareax, y: this.spawnareay},
            customWarps: this.customWarps,
        };
    }

    loadFromJSON(object){
        this.worldSeed = object.seed;
        this.spawnareax = object.spawn.x;
        this.spawnareay = object.spawn.y;
        if (typeof object.customWarps !== 'undefined') {
            this.customWarps = object.customWarps;
        }
    }

}

exports.Map = Map;
