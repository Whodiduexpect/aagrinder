// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A function that runs every 10 ms
 * A function that runs every 200 ms
 * and a function that runs every 2000 ms.
 *
 * Takes care of events that happen rarely and spontaneously
 * (such as plants for example)
 * 
 */

"use strict";

const Ghosts = require('./ghost').Ghosts;
const Detectors = require('./detectors').Detectors;
const Tree = require('./tree').Tree;
const BP = require('../shared/blockprops').BlockProps;

const dirs4 = [
    {dx:1, dy:0},
    {dx:0, dy:1},
    {dx:-1, dy:0},
    {dx:0, dy:-1},
];

const dirs4d = [
    {dx:1, dy:1},
    {dx:-1, dy:1},
    {dx:-1, dy:-1},
    {dx:1, dy:-1},
];

const dirs8 = [
    {dx:1, dy:0},
    {dx:1, dy:1},
    {dx:0, dy:1},
    {dx:-1, dy:1},
    {dx:-1, dy:0},
    {dx:-1, dy:-1},
    {dx:0, dy:-1},
    {dx:1, dy:-1},
];

// tree growth probability for each tree type, for each biome
// 0: 0%
// 1: 100%
const treeProbabilities = {

    //     appl  pine  almo  apri  avoc  bush  cran  blub

    'a': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // apple
    'A': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // apple forest
    'j': { ta:1, ti:1, tl:1, tp:1, tv:1, tb:1, tr:1, tu:1 },  // apple jungle
    'p': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // apricot
    'P': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // apricot fore
    'J': { ta:1, ti:1, tl:1, tp:1, tv:1, tb:1, tr:1, tu:1 },  // jungle
    't': { ta:1, ti:1, tl:0, tp:1, tv:0, tb:1, tr:1, tu:1 },  // taiga
    'T': { ta:0, ti:1, tl:0, tp:0, tv:0, tb:1, tr:1, tu:0 },  // snowy taiga
    'd': { ta:0, ti:0, tl:1, tp:0, tv:0, tb:1, tr:0, tu:0 },  // desert
    'r': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:0, tu:0 },  // mountain
    'i': { ta:0, ti:1, tl:0, tp:0, tv:0, tb:1, tr:1, tu:0 },  // ice mountain
    'm': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // mixed forest
    'C': { ta:0, ti:0, tl:1, tp:0, tv:0, tb:1, tr:0, tu:0 },  // tentacle isl
    'y': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // yay island
    'f': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // flower islan
    'u': { ta:0, ti:0, tl:0, tp:0, tv:0, tb:1, tr:0, tu:0 },  // underground
    'c': { ta:0, ti:0, tl:0, tp:0, tv:0, tb:1, tr:0, tu:0 },  // cave
    'I': { ta:0, ti:0, tl:0, tp:0, tv:0, tb:1, tr:0, tu:0 },  // ice cave
    'o': { ta:0, ti:0, tl:1, tp:0, tv:0, tb:1, tr:0, tu:0 },  // ocean
    's': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // sky
    'R': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1 },  // rainbow
    'W': { ta:0, ti:0, tl:0, tp:0, tv:0, tb:0, tr:0, tu:0 },  // warp

}

const CORAL_LOGIC = {
    'X0': {minwater: 4, mincoral:2, maxcoral:10, bfslimit:20},
    'X1': {minwater: 6, mincoral:0, maxcoral:6, bfslimit:20},
    'X2': {minwater: 6, mincoral:0, maxcoral:5, bfslimit:20},
    'X3': {minwater: 9, mincoral:2, maxcoral:3, bfslimit:8},
    'X4': {minwater: 6, mincoral:3, maxcoral:10, bfslimit:20},
    'X5': {minwater: 10, mincoral:0, maxcoral:6, bfslimit:20},
    'X6': {minwater: 4, mincoral:2, maxcoral:10, bfslimit:20},
    'X7': {minwater: 1, mincoral:7, maxcoral:30, bfslimit:-1},
    'X8': {minwater: 10, mincoral:0, maxcoral:6, bfslimit:-1},
}

class Plant{

    constructor(map, playerData, subscribe, syncher, water, props){
        this.map = map;
        this.subscribe = subscribe;
        this.syncher = syncher;
        this.ghosts = new Ghosts(map, playerData, subscribe, syncher, props);
        this.detectors = new Detectors(map, playerData);
        this.tree = new Tree(map, playerData, syncher);
        this.water = water;
        this.props = props;
        this.interval = undefined;
        this.tickcounter = 0;
        this.currentMultiplier = 1;
        // set up profiler
        setInterval(() => {
            // no profiling for low tickspeed
            if (this.currentMultiplier < 50) return;
            // no output if tickspeed is above 80% of expected
            if (this.tickcounter > 0.8*this.currentMultiplier) return;
            console.log('running at tickspeed '+this.tickcounter);
            this.tickcounter = 0;
        }, 2000);
    }

    setLoop(multiplier){

        // stop previous loop if exists
        if (this.interval !== undefined) {
            clearInterval(this.interval);
        }

        if (multiplier < 0.01) {
            // don't start the interval at all
            return;
        }

        // start interval
        this.interval = setInterval(() => {
            for (let i = 0; i < 200; i++){
                this.plantUpdate10();
            }
            for (let i = 0; i < 10; i++){
                this.plantUpdate200();
            }
            this.plantUpdate2000();
            this.tickcounter++;
        }, 2000 / multiplier);

        this.currentMultiplier = multiplier;
    }

    plantUpdate10() {
        const subchunkx = Math.floor(Math.random()*256);
        const subchunky = Math.floor(Math.random()*256);
        for(const chunky in this.map.chunks){
            for(const chunkx in this.map.chunks[chunky]){
                // For increased speed, not using map.getBlock here
                const chunk = this.map.chunks[chunky][chunkx];
                const blockHere = chunk.terrain[subchunky][subchunkx];

                if (blockHere[0] === 't'){
                    const x = 256*parseInt(chunkx) + subchunkx;
                    const y = 256*parseInt(chunky) + subchunky;
                    // tiny tree must be on dirt
                    // (or on stone, if it is type tb)
                    const blockBelow = this.map.getBlockWithoutLoading(
                        x, y-1, 'unloaded');
                    if (blockBelow !== 'd'
                        && !(blockHere[1] === 'b' && blockBelow === 'B'))
                        continue;
                    if (blockHere[2] === '0'){
                        // always goes from stage 0 to stage 1
                        this.syncher.serverChangeBlock(x, y,
                            blockHere.substring(0,2)+'1');
                    }
                    else if (blockHere[2] === '1'){
                        // before stage 2, check biome
                        const biomeHere = this.map.getBiome(x, y);
                        if (treeProbabilities[biomeHere]
                            ['t'+blockHere[1]] > 0) {
                            // success!
                            this.syncher.serverChangeBlock(x, y,
                                blockHere.substring(0,2)+'2');
                        }
                        else {
                            // DEATH
                            this.syncher.serverChangeBlock(x, y, 'Wt');
                        }
                    }
                    else if (blockHere[2] === '2'){
                        // okay tree attempt
                        let type = '???';
                        if (blockHere[1] === 'a') {
                            type = 'apple';
                        }
                        else if (blockHere[1] === 'i') {
                            type = 'pine';
                        }
                        else if (blockHere[1] === 'l') {
                            type = 'almond';
                        }
                        else if (blockHere[1] === 'p') {
                            type = 'apricot';
                        }
                        else if (blockHere[1] === 'v') {
                            type = 'avocado';
                            // or even super avocado
                            if (Math.random() > 0.7) {
                                // wow
                                type = 'avocadosuper';
                            }
                        }
                        else if (blockHere[1] === 'b') {
                            type = 'bush';
                        }
                        else if (blockHere[1] === 'r') {
                            type = 'cranberry';
                        }
                        else if (blockHere[1] === 'u') {
                            type = 'blueberry';
                        }
                        this.tree.treeAttempt(x, y, type);
                    }
                }

                else if (blockHere === 'Wt') {
                    const x = 256*parseInt(chunkx) + subchunkx;
                    const y = 256*parseInt(chunky) + subchunky;
                    // dead tree disappears or turns to dirt
                    if (Math.random() < 0.5) {
                        this.syncher.serverChangeBlock(x, y, 'd');
                    }
                    else {
                        this.syncher.serverChangeBlock(x, y, ' ');
                    }
                }

                else if (blockHere === 'WA') {
                    const x = 256*parseInt(chunkx) + subchunkx;
                    const y = 256*parseInt(chunky) + subchunky;
                    // must be on sand
                    const blockBelow = this.map.getBlockWithoutLoading(
                        x, y-1, 'unloaded');
                    if (blockBelow !== 'S') continue;
                    // must be in correct biome
                    const biomeHere = this.map.getBiome(x, y);
                    if (biomeHere !== 'C'
                        && biomeHere !== 'W') continue;
                    // attempt tentacle growth
                    this.tentacleAttempt(x, y);
                }

                else if (blockHere[0] === '#') {
                    const x = 256*parseInt(chunkx) + subchunkx;
                    const y = 256*parseInt(chunky) + subchunky;
                    // ghost trail (smoke)
                    // despawn it
                    this.syncher.serverChangeBlock(x, y, ' ');
                }

                else if (blockHere[0] === 'W') {
                    if (blockHere[1] === 'a') {
                        const x = 256*parseInt(chunkx) + subchunkx;
                        const y = 256*parseInt(chunky) + subchunky;
                        // decaying leaf
                        this.despawnLeaf(x, y, blockHere);
                    }
                }

                else if (blockHere[0] === 'X' && blockHere !== 'Xx') {
                    const x = 256*parseInt(chunkx) + subchunkx;
                    const y = 256*parseInt(chunky) + subchunky;
                    // coral death
                    if (this.checkCoralDeath(x, y)) {
                        this.syncher.serverChangeBlock(x, y, 'Xx');
                    }
                    else if (blockHere === 'X6'
                        || blockHere === 'X7'
                        || blockHere === 'X8') {
                        // these three coral colors grow much faster
                        const dir = dirs8[Math.floor(Math.random()*8)];
                        if (this.checkCoralGrowth(
                            x+dir.dx, y+dir.dy, blockHere)) {
                            this.syncher.serverChangeBlock(
                                x+dir.dx, y+dir.dy, blockHere);
                        }
                        // also gets a chance of spontaneous death, hehe
                        else if (Math.random() < 0.01) {
                            if (this.checkCoralSpontaneousDeath(x, y)) {
                                this.syncher.serverChangeBlock(x, y, 'Xx')
                            }
                        }
                    }
                }

                else if (blockHere === 'w') {
                    // this water might freeze into ice
                    // (actually ice forms on water and not inside it)
                    // cancel if wrong biome
                    // For increased speed, not using map.getBiome here
                    const biomeHere = chunk.biomes[
                        64*(subchunky>>3)+(subchunkx>>2)];
                    if (biomeHere !== 'i'
                        && biomeHere !== 'I'
                        && biomeHere !== 't'
                        && biomeHere !== 'T') continue;
                    const x = 256*parseInt(chunkx) + subchunkx;
                    const y = 256*parseInt(chunky) + subchunky;
                    const blockAbove = this.map.getBlockWithoutLoading(
                        x, y+1, 'B');
                    // cancel if not air above
                    if (blockAbove !== ' ') continue;
                    const blockLeft = this.map.getBlockWithoutLoading(
                        x-1, y, ' ');
                    const blockRight = this.map.getBlockWithoutLoading(
                        x+1, y, ' ');
                    const blockUpLeft = this.map.getBlockWithoutLoading(
                        x-1, y+1, ' ');
                    const blockUpRight = this.map.getBlockWithoutLoading(
                        x+1, y+1, ' ');
                    // must be adjacent to solid block or ice
                    if (!BP.isSolidBlock(blockLeft)
                        && !BP.isSolidBlock(blockRight)
                        && blockUpLeft !== 'Wi'
                        && blockUpRight !== 'Wi'
                    ) continue;
                    // okay freeze
                    this.syncher.serverChangeBlock(x, y+1, 'Wi');
                    this.water.enqueue(x, y)
                }

                else if (blockHere === 'sn') {
                    const x = 256*parseInt(chunkx) + subchunkx;
                    const y = 256*parseInt(chunky) + subchunky;
                    // this snow might spread
                    // (to fill all the accessible corners)
                    const dx = Math.floor(Math.random()*5-2);
                    const dy = Math.floor(Math.random()*5-2);
                    const blockThere = this.map.getBlockWithoutLoading(
                        x+dx, y+dy, 'B');
                    // only if it is air there
                    if (blockThere !== ' ') continue;
                    // this air might become snow
                    const blockUnderThere = this.map.getBlockWithoutLoading(
                        x+dx, y+dy-1, ' ');
                    // cancel if air below
                    if (blockUnderThere === ' ') continue;
                    // cancel if wrong biome
                    const biomeThere = this.map.getBiome(x+dx, y+dy);
                    if (biomeThere !== 'i'
                        && biomeThere !== 'I'
                        && biomeThere !== 'T') continue;
                    // cancel if not on solid block
                    if (!BP.isSolidBlock(blockUnderThere)) continue;
                    // okay become snow
                    this.syncher.serverChangeBlock(x+dx, y+dy, 'sn');
                }

            }
        }
    }

    despawnLeaf(x, y, type) {
        // check if wood is nearby
        const wood = 'H' + type[2];
        for (let dx = -5; dx <= 5; dx++) {
            for (let dy = -2; dy <= 0; dy++) {
                if (this.map.getBlockWithoutLoading(
                    x+dx, y+dy, wood) === wood) {
                    // found wood. abort
                    return;
                }
            }
        }
        // there was no wood
        // despawn leaf
        this.syncher.serverChangeBlock(x, y, ' ');
        // despawn snow on top
        if (this.map.getBlockWithoutLoading(x, y+1) === 'sn') {
            this.syncher.serverChangeBlock(x, y+1, ' ');
        }
    }


    checkGrowVineDown(x, y) {
        // above the vine, there needs to be
        // a row of existing vines
        // and then a solid block
        for (let i = 1; i < 6; i++) {
            const blockThere = this.map.getBlockWithoutLoading(x, y+i);
            if (BP.isSolidBlock(blockThere)) {
                // found a solid block on top
                return true;
            }
            // otherwise, it has to be a vine
            if (blockThere !== '-a') {
                // well it's not
                return false;
            }
        }
        // can't grow because it is too long.
        return false;
    }

    checkGrowStalagmite(x, y) {
        // below the stalactite, there needs to be a sequence of air
        // and then stone (B)
        for (let i = 1; i < 32; i++) {
            const blockThere = this.map.getBlockWithoutLoading(x, y-i);
            if (blockThere === 'B') {
                // found bottom
                return i-1;
            }
            if (blockThere !== ' ' && blockThere !== 'sn') {
                // found ungrowable spot
                return -1
            }
        }
        // can't grow because it is too far.
        return -1;
    }

    checkGrowVineUp(x, y) {
        // below the vine, there needs to be
        // a row of existing vines
        // and then a solid block
        for (let i = 1; i < 6; i++) {
            const blockThere = this.map.getBlockWithoutLoading(x, y-i);
            if (BP.isSolidBlock(blockThere)) {
                // found a solid block on bottom
                return true;
            }
            // otherwise, it has to be a vine
            if (blockThere !== '-a') {
                // well it's not
                return false;
            }
        }
        // can't grow because it is too long.
        return false;
    }

    checkCoralDeath(x, y) {
        let adjacentDead = false;
        for (const dir of dirs8) {
            const b = this.map.getBlockWithoutLoading(x+dir.dx, y+dir.dy, 'w');
            if (b === 'w') {
                // can't die with adjacent water
                return false;
            }
            else if (b === 'Xx') {
                adjacentDead = true;
            }
        }
        if (adjacentDead) {
            // die
            return true;
        }
        for (const dir of dirs4) {
            const b = this.map.getBlockWithoutLoading(x+dir.dx, y+dir.dy, 'w');
            if (b === ' ') {
                // die in this case, too
                // because air
                return true;
            }
        }
        return false;
    }

    recognizeTentacleStage(x, y) {
        // we require the surrounding area to be loaded
        if(!this.map.isBlockLoaded(x-2, y)) return -1;
        if(!this.map.isBlockLoaded(x+2, y)) return -1;
        if(!this.map.isBlockLoaded(x-2, y+4)) return -1;
        if(!this.map.isBlockLoaded(x+2, y+4)) return -1;
        // check if stage 0
        if (
            this.map.getBlock(x, y+1) === ' '
            && this.map.getBlock(x+1, y) === ' '
            && this.map.getBlock(x-1, y) === ' '
        ) {
            return 0;
        }
        // check if stage 1
        if (
            this.map.getBlock(x, y+1) === 'WA'
            && this.map.getBlock(x+1, y) === ' '
            && this.map.getBlock(x-1, y) === ' '
            && this.map.getBlock(x+1, y+1) === ' '
            && this.map.getBlock(x-1, y+1) === ' '
            && this.map.getBlock(x, y+2) === ' '
        ) {
            return 1;
        }
        // check if stage 2
        if (
            this.map.getBlock(x, y+2) === ' '
            && this.map.getBlock(x, y+3) === ' '
            && this.map.getBlock(x+1, y+1) === ' '
            && this.map.getBlock(x-1, y+1) === ' '
            && this.map.getBlock(x+1, y+2) === ' '
            && this.map.getBlock(x-1, y+2) === ' '
            && this.map.getBlock(x+1, y+3) === ' '
            && this.map.getBlock(x-1, y+3) === ' '
            && this.map.getBlock(x, y+1) === 'WA'
            && this.map.getBlock(x+1, y) === 'WA'
            && this.map.getBlock(x-1, y) === 'WA'
            && this.map.getBlock(x+2, y) === ' '
            && this.map.getBlock(x-2, y) === ' '
            && this.map.getBlock(x+2, y+1) === ' '
            && this.map.getBlock(x-2, y+1) === ' '
        ) {
            return 2;
        }
        // check if stage 3
        if (
            this.map.getBlock(x, y+3) === ' '
            && this.map.getBlock(x+1, y+2) === ' '
            && this.map.getBlock(x-1, y+2) === ' '
            && this.map.getBlock(x, y+1) === 'WA'
            && this.map.getBlock(x, y+2) === 'WA'
            && this.map.getBlock(x+1, y) === 'WA'
            && this.map.getBlock(x-1, y) === 'WA'
            && this.map.getBlock(x+1, y+1) === 'WA'
            && this.map.getBlock(x-1, y+1) === 'WA'
            && this.map.getBlock(x+2, y) === ' '
            && this.map.getBlock(x-2, y) === ' '
            && this.map.getBlock(x+2, y+1) === ' '
            && this.map.getBlock(x-2, y+1) === ' '
            && this.map.getBlock(x+2, y+2) === ' '
            && this.map.getBlock(x-2, y+2) === ' '
            && this.map.getBlock(x+2, y+3) === ' '
            && this.map.getBlock(x-2, y+3) === ' '
            && this.map.getBlock(x+2, y+4) === ' '
            && this.map.getBlock(x-2, y+4) === ' '
        ) {
            // might be one of the stage 2 tentacles
            if (this.map.getBlock(x-1, y+3) === ' '
                && this.map.getBlock(x+1, y+3) === 'WA') {
                return 3;
            }
            if (this.map.getBlock(x-1, y+3) === 'WA'
                && this.map.getBlock(x+1, y+3) === ' ') {
                return 3;
            }
        }
        // it is not a valid stage.
        return -1;
    }

    tentacleAttempt(x, y) {

        const stage = this.recognizeTentacleStage(x, y);
        if (stage === -1) return;

        if (stage === 0) {
            this.syncher.serverChangeBlock(x, y+1, 'WA');
            return;
        }

        if (stage === 1) {
            this.syncher.serverChangeBlock(x+1, y, 'WA');
            this.syncher.serverChangeBlock(x-1, y, 'WA');
            return;
        }

        if (stage === 2) {
            this.syncher.serverChangeBlock(x+1, y+1, 'WA');
            this.syncher.serverChangeBlock(x-1, y+1, 'WA');
            this.syncher.serverChangeBlock(x, y+2, 'WA');
            if (Math.random() < 0.5) {
                this.syncher.serverChangeBlock(x-1, y+3, 'WA');
            }
            else {
                this.syncher.serverChangeBlock(x+1, y+3, 'WA');
            }
            return;
        }

        if (stage === 3) {
            // becomes proper tentacle
            // (using the terrain generator)
            this.tree.treeAttempt(x, y, 'tentacle');
        }

    }


    tryGrowBerry(x, y, block) {
        // we require the surrounding area to be loaded
        if(!this.map.isBlockLoaded(x-2, y+1)) return false;
        if(!this.map.isBlockLoaded(x+2, y-1)) return false;
        if(!this.map.isBlockLoaded(x-2, y+1)) return false;
        if(!this.map.isBlockLoaded(x+2, y-1)) return false;
        for (let dy = -1; dy <= 1; dy++) {
            for (let dx = -2; dx <= 2; dx++) {
                const blockThere = this.map.getBlock(x+dx, y+dy);
                if (blockThere === block) {
                    // this other berry is too close
                    return false;
                }
            }
        }
        this.syncher.serverChangeBlock(x, y, block);
    }


    plantUpdate200(){

        // for speed, only generate one pair of subchunk coordinates
        // and use this in all chunks.
        // it's really not important enough for randomizing in each separately
        // and this needs to be super fast!
        const subchunkx = Math.floor(Math.random()*256)
        const subchunky = Math.floor(Math.random()*256)

        for(const chunky in this.map.chunks){
            for(const chunkx in this.map.chunks[chunky]){
                const x = 256*parseInt(chunkx) + subchunkx;
                const y = 256*parseInt(chunky) + subchunky;

                const blockHere = this.map.getBlock(x, y);

                if (blockHere === 'DD'){
                    // found a diamond!
                    // try to find nearby DD or R0 n times
                    for (let i = 0; i < 8; i++) {
                        // choose a nearby spot
                        const dx = Math.round(Math.random()*6)-3;
                        const dy = Math.round(Math.random()*6)-3;
                        if (dx === 0 && dy === 0) continue;
                        const secondBlock = this.map.getBlockWithoutLoading(
                            x+dx, y+dy);
                        if (secondBlock === 'DD') {
                            // found another diamond nearby!
                            // and it's not the same one twice.
                            // 2 diamonds in the same area means
                            // we can convert nearby
                            // ruby stone (pink B) to ruby!
                            // unless there is already ruby in this area
                            let isAlreadyRuby = false;
                            for (let rdx = -7; rdx <= 7; rdx++) {
                                for (let rdy = -7; rdy <= 7; rdy++) {
                                    if (this.map.getBlockWithoutLoading(
                                        x+rdx, y+rdy, 'R1') === 'R1') {
                                        isAlreadyRuby = true;
                                        break;
                                    }
                                }
                                if (isAlreadyRuby) break;
                            }
                            if (isAlreadyRuby) continue;
                            // this loops many times to compensate
                            // for the fact that we had to randomly
                            // find the two diamonds (which is rare)
                            for (let i = 0; i < 10; i++) {
                                const dx2 = Math.round(Math.random()*6)-3;
                                const dy2 = Math.round(Math.random()*6)-3;
                                if (this.map.getBlockWithoutLoading(
                                    x+dx2, y+dy2) !== 'R0') continue;
                                this.syncher.serverChangeBlock(x+dx2, y+dy2, 'R1');
                            }
                        }
                        else if (secondBlock === 'B' && i < 3) {
                            // convert this stone to ruby stone (pink B)
                            this.syncher.serverChangeBlock(x+dx, y+dy, 'R0');
                        }
                    }
                }

                else if (blockHere === 'R0'){
                    // ruby stone devolves back to stone.  This
                    // prevents diamond from getting surrounded.
                    this.syncher.serverChangeBlock(x, y, 'B');
                }

                else if (blockHere === 'WW'){
                    // found a warp!
                    // check 1 surrounding block
                    {
                        const dx = Math.round(Math.random()*6)-3;
                        const dy = Math.round(Math.random()*6)-3;
                        const secondBlock = this.map.getBlockWithoutLoading(
                            x+dx, y+dy, 'WB');
                        if (secondBlock === 'WB') {
                            continue;
                        }
                    }
                    {
                        const dx = Math.round(Math.random()*6)-3;
                        const dy = Math.round(Math.random()*6)-3;
                        const secondBlock = this.map.getBlockWithoutLoading(
                            x+dx, y+dy);
                        if (secondBlock === 'B') {
                            // found a nearby B
                            this.syncher.serverChangeBlock(x+dx, y+dy, 'WB');
                        }
                    }
                }

                else if (blockHere === 'ar'
                    || blockHere === 'Ar'
                    || blockHere === 'War') {
                    const biomeHere = this.map.getBiome(x, y);
                    if (biomeHere !== 't'
                        && biomeHere !== 'T'
                        && biomeHere !== 'i'
                        && Math.random() < 0.5) {
                        continue;
                    }
                    this.tryGrowBerry(x, y, 'fr');
                }


                else if (blockHere === 'au'
                    || blockHere === 'Au'
                    || blockHere === 'Wau') {
                    const biomeHere = this.map.getBiome(x, y);
                    if (biomeHere !== 'A'
                        && biomeHere !== 'm'
                        && Math.random() < 0.5) {
                        continue;
                    }
                    this.tryGrowBerry(x, y, 'fu');
                }


                else if (blockHere === '-a') {
                    // vine growing down
                    if (this.map.getBlockWithoutLoading(
                        x, y-1, 'H') === ' ') {
                        if (this.checkGrowVineDown(x, y)) {
                            this.syncher.serverChangeBlock(x, y-1, '-2');
                        }
                    }
                    // vine growing up
                    if (this.map.getBlockWithoutLoading(
                        x, y+1, 'H') === 'w') {
                        if (this.checkGrowVineUp(x, y)) {
                            this.syncher.serverChangeBlock(x, y+1, '-8');
                        }
                    }
                }

                else if (blockHere === '-2') {
                    // vine growing down
                    if (this.checkGrowVineDown(x, y)) {
                        this.syncher.serverChangeBlock(x, y, '-a');
                    }
                }

                else if (blockHere === '-8') {
                    // vine growing up
                    if (this.map.getBlockWithoutLoading(
                        x, y+1, 'H') === 'w') {
                        if (this.checkGrowVineUp(x, y)) {
                            this.syncher.serverChangeBlock(x, y, '-a');
                        }
                    }
                }

                else if (blockHere[0] === 'X' && blockHere !== 'Xx') {
                    // coral, but not dead coral
                    const dir = dirs8[Math.floor(Math.random()*8)];
                    if (this.checkCoralGrowth(
                        x+dir.dx, y+dir.dy, blockHere)) {
                        this.syncher.serverChangeBlock(
                            x+dir.dx, y+dir.dy, blockHere);
                    }
                    // also gets a chance of spontaneous death, hehe
                    else if (Math.random() < 0.01) {
                        if (this.checkCoralSpontaneousDeath(x, y)) {
                            this.syncher.serverChangeBlock(x, y, 'Xx')
                        }
                    }
                }

                else if (blockHere === ' ') {
                    // this air might become snow
                    const blockBelow = this.map.getBlockWithoutLoading(
                        x, y-1, ' ');
                    // cancel if air below (this usually happens)
                    if (blockBelow === ' ') continue;
                    // cancel if wrong biome
                    const biomeHere = this.map.getBiome(x, y);
                    if (biomeHere !== 'i'
                        && biomeHere !== 'I'
                        && biomeHere !== 'T') continue;
                    const blockAbove = this.map.getBlockWithoutLoading(
                        x, y+1, 'B');
                    // cancel if not on solid block
                    if (!BP.isSolidBlock(blockBelow)) continue;
                    // check some blocks up
                    const ny = biomeHere === 'I' ? 5 : 10;
                    let yay = true;  // TODO: this code can be organized better
                    for (let dy = 1; dy <= ny; dy++) {
                        const someBlockAbove = this.map.getBlockWithoutLoading(
                            x, y+dy, 'B');
                        // cancel if some block above is bad
                        if (someBlockAbove !== ' '
                            && someBlockAbove !== 'sn'
                            && someBlockAbove[0] !== 'A'
                            && someBlockAbove[0] !== 'a'
                            && !(someBlockAbove[0] === 'W'
                                && someBlockAbove[1] === 'A')){
                            yay = false;
                            break;
                        };
                    }
                    if (!yay) continue;
                    // okay become snow
                    this.syncher.serverChangeBlock(x, y, 'sn');
                }

            }
        }

    }

    plantUpdate2000(){

        // for speed, only generate one pair of subchunk coordinates
        // and use this in all chunks.
        // it's really not important enough for randomizing in each separately
        // and this needs to be super fast!
        const subchunkx = Math.floor(Math.random()*256)
        const subchunky = Math.floor(Math.random()*256)

        for(const chunky in this.map.chunks){
            for(const chunkx in this.map.chunks[chunky]){
                const x = 256*parseInt(chunkx) + subchunkx;
                const y = 256*parseInt(chunky) + subchunky;

                const blockHere = this.map.getBlock(x, y);
                const biomeHere = this.map.getBiome(x, y);

                if (blockHere === 'Xx') {
                    if (this.checkCoralStoning(x, y)) {
                        this.syncher.serverChangeBlock(x, y, 'B')
                    }
                }

                else if (blockHere === 'WT') {
                    // might spawn stalagmite
                    if (Math.random() > 0.5) continue;
                    // ok try
                    let distance = this.checkGrowStalagmite(x, y);
                    if (distance <= 2) continue;
                    this.syncher.serverChangeBlock(x, y-distance, 'WI')
                }

                else if (blockHere === 'Wl') {
                    // icice might grow
                    // cancel if wrong biome
                    const biomeHere = this.map.getBiome(x, y);
                    if (biomeHere !== 'i'
                        && biomeHere !== 'I'
                        && biomeHere !== 'T') continue;
                    // cancel if no space for next icicle
                    if (this.map.getBlockWithoutLoading(x, y-1, '..')
                        !== ' ') continue;
                    // cancel if already enough icicles
                    if (this.map.getBlockWithoutLoading(x, y+2, 'Wl')
                        === 'Wl') continue;
                    // cancel if already enough icicles
                    if (this.map.getBlockWithoutLoading(x, y+1, 'Wl')
                        === 'Wl') continue;
                    this.syncher.serverChangeBlock(x, y-1, 'Wl')
                }

            }
        }

        if (this.props.spawn_monsters === true) {
            this.ghosts.ghostTick();
        }

        this.detectors.detectorTick();

    }

    checkCoralGrowth(x, y, type) {
        if (this.map.getBlockWithoutLoading(x, y) !== 'w') {
            // not water here. Nothing to grow.
            return;
        }
        // count nearby waters,
        // count nearby corals
        let waters = 0;
        let corals = 0;
        for (let dx = -2; dx <= 2; dx++) {
            for (let dy = -2; dy <= 2; dy++) {
                const b = this.map.getBlockWithoutLoading(x+dx, y+dy, 'X?');
                if (b === 'w') {
                    waters++;
                }
                else if (b[0] === 'X' && b !== 'Xx') {
                    corals++;
                }
            }
        }
        // lower limit for number of nearby waters
        if (waters < CORAL_LOGIC[type].minwater) return false;
        // lower limit for number of nearby corals
        if (corals < CORAL_LOGIC[type].mincoral) return false;
        // upper limit for number of nearby corals
        if (corals > CORAL_LOGIC[type].maxcoral) return false;
        // bfs
        const limit = CORAL_LOGIC[type].bfslimit;
        if (limit < 0) return true; // special case, skip bfs
        let numfound = -1;
        const visited = [];
        for (let i = 0; i < 41; i++) {
            visited.push(Array(41).fill(false));
        }
        const queue = [{dx:20, dy:20}];
        visited[20][20] = true;
        while (queue.length > 0) {
            // next in queue
            const v = queue.shift();
            numfound++;
            if (numfound > limit) return false;
            const dx = v.dx;
            const dy = v.dy;
            // consider enqueuing each neighbor
            for (let ddy = -1; ddy <= 1; ddy++) {
                for (let ddx = -1; ddx <= 1; ddx++) {
                    // not current block, only its neighbors
                    if (ddx === 0 && ddy === 0) continue;
                    // if outside of bounds, just abort this
                    // because we can't know what's out there.
                    if (dx+ddx<0 || dy+ddy<0
                        || dx+ddx>=visited[0].length
                        || dy+ddy>=visited.length) return false;
                    // not blocks that were already visited
                    if (visited[dy+ddy][dx+ddx]) continue;
                    // not blocks that are not matching
                    if (this.map.getBlockWithoutLoading(
                        x+dx-20+ddx,y+dy-20+ddy) !== type) continue;
                    // ok let's enqueue this
                    visited[dy+ddy][dx+ddx] = true;
                    queue.push({dx:dx+ddx,dy:dy+ddy});
                }
            }
        }
        // got to the end with no problems!
        // accept it
        return true
    }

    checkCoralSpontaneousDeath(x, y) {
        for (let dx = -2; dx <= 2; dx++) {
            for (let dy = -2; dy <= 2; dy++) {
                const b = this.map.getBlockWithoutLoading(x+dx, y+dy, 'w');
                if (b === 'w') {
                    // can't die near water
                    return false;
                }
            }
        }
        return true;
    }

    checkCoralStoning(x, y) {
        for (let dx = -2; dx <= 2; dx++) {
            for (let dy = -2; dy <= 2; dy++) {
                const b = this.map.getBlockWithoutLoading(x+dx, y+dy, 'w');
                if (b !== 'B' && b !== 'Xx') {
                    // needs only dead coral and stone adjacent
                    return false;
                }
            }
        }
        return true;
    }

    loadFromJSON(object){
        this.ghosts.loadFromJSON(object.ghosts);
        this.detectors.loadFromJSON(object.detectors);
    }

    saveToJSON(){
        return {
            ghosts: this.ghosts.saveToJSON(),
            detectors: this.detectors.saveToJSON(),
        };
    }

}
exports.Plant = Plant;
