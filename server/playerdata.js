// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * PlayerData is the class holding everything about the players that needs to get saved to disk
 * and loaded from disk.
 * 
 * It also acts as an abstraction over players,
 * so that new players are handled gracefully (so long as they are registered)
 * If the player does not exist in this world, they are added at coordinates null, null
 * with empty inventory.
 */

"use strict";

const Player = require('./player').Player;
const Inventory = require('../shared/inventory').Inventory;
const bcrypt = require('bcryptjs');
const fs = require('fs');

class PlayerData {
    constructor(props) {
        this.onlinePlayers = [];
        this.knownPlayers = [];
        this.admins = [];
        this.sudos = [];
        this.props = props;
        this.reloadAdmins();
    }

    saveToJSON(){
        return this.knownPlayers.map(player => player.saveToJSON());
    }

    loadFromJSON(object){
        this.knownPlayers = object.map(ob => {
            const pl = new Player(
                ob.x,
                ob.y,
                ob.name,
                null, // socket
                ob.color,
                ob.gamemode,
                ob.food,
                ob.reach
            );
            pl.homex = ob.homex;
            pl.homey = ob.homey;
            // convert inventory to new format if old
            const inv = {};
            for (const item_code in ob.inventory) {
                const value = ob.inventory[item_code];
                if (typeof value === 'number') {
                    // is legacy format.
                    if (Inventory.isDataItem(item_code)) {
                        // start at default data
                        inv[item_code] = {
                            c: value,
                            d: [ Inventory.makeItemData(item_code) ]
                        };
                    } else {
                        inv[item_code] = {c:value};
                    }
                } else {
                    // is new format
                    // (value is a proper object with c and maybe d too)
                    inv[item_code] = value;
                }
            }
            pl.inventory.state = inv;
            // remove 0s (they can come from legacy format)
            pl.inventory.fix();
            pl.inventory.errorCheck();
            if (ob.hotbar) {
                pl.hotbar = ob.hotbar;
            }
            return pl;
        });
    }

    reloadAdmins(){
        fs.exists('./admins.txt', exists=>{
            if(exists){
                fs.readFile('./admins.txt', (err, data)=>{
                    if(err){
                        console.log('error while reading admins.txt:', err);
                    }
                    else{
                        this.admins = data.toString().split('\n');
                        console.log('admins.txt has been reloaded');
                    }
                });
            }
            else{
                console.log('admins.txt missing, creating empty');
                const emptyStream = fs.createWriteStream('./admins.txt');
                emptyStream.end();
                this.admins = [];
            }
        });
    }

    login(name, password, socket){
        // this is my injection guard
        if(
            !/^[A-Za-z]([A-Za-z0-9 ]{0,10}[A-Za-z0-9])?$/.test(name)
        ){
            return {success: false, message: 'Invalid username. Up to 12 alphanumeric.'};
        }

        if (!(name in ACCOUNTS.accounts)) {
            return {success: false, message: 'User does not exist.'};
        }
        const account = ACCOUNTS.accounts[name];
        // check if password is set
        if (account.hash !== '') {
            // verify password
            const isMatch = bcrypt.compareSync(password, account.hash);
            if(!isMatch){
                return {success: false, message: 'Wrong password'};
            }
        }

        // okay password is ok (or didn't need to check)
        // does this player exist in this world?
        const knownPlayer = this.knownPlayerByName(name);

        if (knownPlayer){
            // player exists. Is this player already online?

            if (this.onlinePlayerByName(name)){
                return {success: false, message: 'Already online'};
            }
            else{
                // looks good
                knownPlayer.socket = socket;
                knownPlayer.color = account.color;
                this.onlinePlayers.push(knownPlayer);
                return {success: true, player: knownPlayer};
            }
        }
        else{
            // player does not exist and needs to be added.

            const newPlayer = new Player(null, null, name, socket,
                account.color, this.props.gamemode);
            this.knownPlayers.push(newPlayer);
            this.onlinePlayers.push(newPlayer);
            return {success: true, player: newPlayer};
        }
    }

    logout (socket){
        for(let i = 0; i < this.onlinePlayers.length; i++){
            if(this.onlinePlayers[i].socket.id === socket.id){
                const player = this.onlinePlayers[i];
                this.onlinePlayers.splice(i, 1);
                return player;
            }
        }
        return null;
    }

    knownPlayerByName(name){
        for(let i = 0; i < this.knownPlayers.length; i++){
            if(this.knownPlayers[i].name === name){
                return this.knownPlayers[i];
            }
        }
        return null;
    }

    onlinePlayerByName(name){
        for(let i = 0; i < this.onlinePlayers.length; i++){
            if(this.onlinePlayers[i].name.toUpperCase() === name.toUpperCase()){
                return this.onlinePlayers[i];
            }
        }
        return null;
    }

    onlinePlayerByXY(x, y){
        for(let i = 0; i < this.onlinePlayers.length; i++){
            if(this.onlinePlayers[i].x === x && this.onlinePlayers[i].y === y){
                return this.onlinePlayers[i];
            }
        }
        return null;
    }

    onlinePlayerBySocket(socket){
        for(let i = 0; i < this.onlinePlayers.length; i++){
            if(this.onlinePlayers[i].socket.id === socket.id){
                return this.onlinePlayers[i];
            }
        }
        return null;
    }

    isPlayerAdminByName(name){
        if(this.props.authorization === false){
            // no authorization
            return true;
        }
        for(const admin of this.admins){
            if(admin.toUpperCase() === name.toUpperCase()){
                return true;
            }
        }
        // not found, so deny
        return false;
    }

    getPositionList() {
        return this.onlinePlayers.map(a=>({
            x:a.x,
            y:a.y,
            n:a.name,
            c:a.color,
        }));
    }

}

exports.PlayerData = PlayerData;
