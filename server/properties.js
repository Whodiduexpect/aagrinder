// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * DEFAULT_PROPERTIES defines which fields should appear in the file config.json
 * and their default values of course. When properties are being loaded,
 * these default values are used for all missing fields in the file,
 * and then the file is also updated to include these fields.
 *
 * If the config.json file does not exist at all,
 * it is created with all defaults, and those defaults are also loaded.
 *
 * However, if the legacy server-properties.json file exists,
 * it is used instead.
 */

"use strict";

const fs = require('fs');

const loadServerProperties = function(){

    const DEFAULT_PROPERTIES = {
        level_name: 'world',
        level_seed: Math.floor(Math.random() * 65536),
        generate_structures: true,
        spawn_monsters: true,
        load_distance: 1,
        enable_unloading: true,
        unload_distance: 2,
        gamemode: 0,
        allow_home: true,
        server_port: 8080,
        restrict_to_localhost: false,
        allow_registration: true,
        enable_autosave: true,
        autosave_minutes: 120,
        max_players: 42,
        matrix_room_id: '',
        matrix_homeserver_url: '',
        matrix_bot_enabled: false,
        matrix_bot_access_token: '',
        matrix_appservice_enabled: false,
        matrix_appservice_id: 'aagrinder',
        matrix_appservice_url: 'http://localhost:8433',
        matrix_appservice_domain: 'example.org',
        matrix_appservice_bindaddress: 'localhost',
        matrix_appservice_port: '8433',
        authorization: true,
        enable_server_log: true,
        is_debug_server: false,
        enable_debug_log: false,
        enable_autologin: false,
        autologin_name: 'user',
        multithread: 4,
        welcome_message: '',
    };

    //Check if file exists
    let bad_file_format = false;
    let loaded_properties;
    let config_filename = 'config.json';
    let config_exists = fs.existsSync(config_filename);
    if (!config_exists && fs.existsSync('server-properties.json')) {
        // use legacy file instead
        console.log('server-properties.json was detected and will be used instead of config.json');
        config_filename = 'server-properties.json';
        config_exists = true;
    }

    if (config_exists) {
        const loadedString = fs.readFileSync(config_filename).toString('utf8');
        loaded_properties = JSON.parse(loadedString);
        for(const property in DEFAULT_PROPERTIES){
            if(DEFAULT_PROPERTIES.hasOwnProperty(property)){
                if(loaded_properties[property] === undefined
                    || typeof(loaded_properties[property]) !== typeof(DEFAULT_PROPERTIES[property])){
                    // this property is missing in the file!
                    // or has wrong type
                    // use default
                    loaded_properties[property] = DEFAULT_PROPERTIES[property];
                    // mark file as bad
                    bad_file_format = true;
                }
            }
        }
    }

    else {
        // file doesn't exist. Just use all defaults
        loaded_properties = DEFAULT_PROPERTIES;
        bad_file_format = true;
    }

    // anything wrong with this file?
    if (bad_file_format) {
        // yes. rewrite it
        const corrected_properties_string = JSON.stringify(loaded_properties, null, 4);
        fs.writeFileSync(config_filename, corrected_properties_string);
        console.log('config.json was missing or incomplete. Default values were added to the file. You should open it and check if everything is okay.');
    }
    return Object.freeze(loaded_properties);
}

exports.load = loadServerProperties;
