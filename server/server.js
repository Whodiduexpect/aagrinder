// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * This used to all be merged with index.js but now it's here.
 * Just turned the server into a class.
 * It is a collection of all the server components,
 * and also links them to sockets.
 */

"use strict";

const SERVER_VERSION = '1.0.0';

const fs = require('fs');
const { promisify } = require('util');
const existsAsync = promisify(fs.exists);
const writeFileAsync = promisify(fs.writeFile);
const readFileAsync = promisify(fs.readFile);

const { AASTRING_RAW } = require('../shared/aastring');

const { Player } = require('./player');
const { Map } = require('./map');
const { BlockUpdate } = require('./blockupdate');
const { PlayerData } = require('./playerdata');
const { Spawn } = require('./spawn');
const { Syncher } = require('./syncher');
const { Subscribe } = require('./subscribe');
const { Actions } = require('./actions');
const { Commands } = require('./commands');
const { Plant } = require('./plant');
const { Wires } = require('./wires');
const { Water } = require('./water');
const { Hopper } = require('./hopper');
const { AagrinderMatrixBot } = require('./matrixbot');
const { AagrinderMatrixAppservice } = require('./matrixappservice');

class Server{

    constructor(props){
        this.props = props;
        this.map = new Map(props, (x,y)=>this.water.enqueue(x,y));
        this.playerData = new PlayerData(props);
        this.blockUpdate = new BlockUpdate(this.map);
        this.connectedSockets = [];
        this.syncher = new Syncher(
            this.map,
            this.blockUpdate,
            this.playerData,
            undefined,
            undefined,
            undefined,
            undefined,
            props
        );
        this.spawn = new Spawn(this.map, this.syncher, props);
        this.subscribe = new Subscribe(this.map, props);
        this.water = new Water(
            this.map,
            this.playerData,
            this.subscribe,
            this.syncher,
        );
        this.plant = new Plant(
            this.map,
            this.playerData,
            this.subscribe,
            this.syncher,
            this.water,
            props,
        );
        this.wires = new Wires(
            this.map,
            this.syncher,
            this.playerData,
        );
        this.hopper = new Hopper(
            this.map,
            this.syncher
        )
        // now fix the missing references in syncher:
        this.syncher.wires = this.wires;
        this.syncher.hopper = this.hopper;
        this.syncher.water = this.water;
        this.syncher.detectors = this.plant.detectors;
        this.syncher.ghosts = this.plant.ghosts;
        this.actions = new Actions(
            this.map,
            this.syncher,
            this.subscribe,
            this.spawn,
            this.playerData,
            props,
        );
        // commands gets reference to whole server.
        // commands have access to everything.
        this.commands = new Commands(this);

        this.aagrinderMatrixBot = new AagrinderMatrixBot(props, this.playerData);
        this.aagrinderMatrixAppservice = new AagrinderMatrixAppservice(props, this.playerData);

        if(!fs.existsSync('./aagrinder-terrain')){
            console.error('You need the terrain generator! Get it here: https://gitlab.com/MRAAGH/aagrinder-terrain and put it in your aagrinder folder.');
            process.exit(1);
        }
        if(!fs.existsSync('./aagrinder-terrain/aagrinderterrain')){
            console.error('You need to compile the terrain generator! Go to the aagrinder-terrain folder and run this command: make');
            process.exit(1);
        }

    }

    command(c){
        switch(c){
            case 'save':
                this.saveToFile(false);
                break;
            case 'stop':
                this.saveToFile(true);
                break;
            case 'kill':
                this.stopServer();
                break;
            case 'admin':
                this.playerData.reloadAdmins();
                break;
            case 'forcereload':
                LOG.log('force reloading all clients!');
                for (const socket of this.connectedSockets) {
                    socket.emit('FORCERELOAD', { FORCE: 'RELOAD' });
                }
                break;
            case 'players':
                console.log(this.playerData.onlinePlayers.map(p=>p.name).join(', '));
                break;
            case 'chat':
                console.log('Usage: chat <message>');
                break;
            case 'log':
                console.log('Usage: log <player>');
                break;
            case 'clearlog': case 'logclear':
                for (const player of this.playerData.onlinePlayers) {
                    player.debugLog = [];
                }
                console.log('Cleared all debug logs.');
                break;
            case 'help':
                console.log('available commands: save, stop, kill, players, admin, forcereload, log, clearlog, help, chat');
                break;
            default:

                if (c.length > 4 && c.substring(0,5) === 'chat ') {
                    const message = '\\[#fff\\][SERVER]\\[/\\]: ' + c.substring(5);
                    LOG.log(message);

                    for (const sendPlayer of this.playerData.onlinePlayers) {
                        sendPlayer.socket.emit('chat', {
                            message: message,
                            notify: true,
                            timestamp: true,
                        });
                    }
                    break;
                }

                if (c.length > 3 && c.substring(0,4) === 'log ') {
                    if (this.props.enable_debug_log !== true) {
                        console.log('debug log not enabled in config.json');
                        break;
                    }
                    const name = c.substring(4);
                    const player = this.playerData.onlinePlayerByName(name);
                    if (player === null) {
                        console.log('No log for player: '+name);
                        break;
                    }
                    const log = player.debugLog.join('\n');
                    const fname = './'+name+'.debuglog';
                    writeFileAsync(fname, log);
                    console.log('Log saved to '+fname);
                    break;
                }

                console.log('unknown command: '+c+' (type "help" for help)');
        }
    }

    async saveToFile(quit) {
        await this.map.createSaveDirs();
        const savingObjectMap = this.map.saveToJSON();
        const savingObjectPlayerData = this.playerData.saveToJSON();
        const savingObjectPlantInfo = this.plant.saveToJSON();
        const savingObjectWaterUpdates = this.water.saveToJSON();
        const savingObjectChestRelations = this.hopper.saveToJSON();
        const savingObject = {
            playerData: savingObjectPlayerData,
            plantInfo: savingObjectPlantInfo,
            waterUpdates: savingObjectWaterUpdates,
            map: savingObjectMap,
            chestRelations: savingObjectChestRelations,
        };
        const savingString = JSON.stringify(savingObject, null, 2);
        await writeFileAsync('./saves/' + this.props.level_name + '/level.json',
            savingString);
        await this.map.flushChunks();
        console.log('saved map to saves/' + this.props.level_name + '/');
        if (quit) {
            this.stopServer();
        }
    }

    stopServer() {
        // update everyone's last seen time
        const currentTime = Math.floor((new Date()).getTime() / 1000);
        for (const player of this.playerData.onlinePlayers) {
            ACCOUNTS.accounts[player.name].seen = currentTime;
        }
        ACCOUNTS.save();
        console.log('STOPPING SERVER');
        process.exit();
    }

    async loadFromFileOrConvert() {
        //Check if dir exists
        if (await existsAsync('./saves/'+this.props.level_name)) {
            await this.loadFromFile();
        }
        // Checkif old format exists
        else if (await existsAsync('./saves/' + this.props.level_name + '.txt')) {
            // save exists in old format!!!
            await this.loadOldFormatAndConvert();
            await this.loadFromFile();
        }
    }

    async loadFromFile() {
        const loadedString = await readFileAsync(
            './saves/' + this.props.level_name + '/level.json');
        const loadedObject = JSON.parse(loadedString.toString('utf8'));
        this.playerData.loadFromJSON(loadedObject.playerData);
        this.plant.loadFromJSON(loadedObject.plantInfo);
        this.water.loadFromJSON(loadedObject.waterUpdates);
        this.map.loadFromJSON(loadedObject.map);
        this.hopper.loadFromJSON(loadedObject.chestRelations);
    }

    async loadOldFormatAndConvert() {
        LOG.log('CONVERTING FROM OLD FORMAT');
        const loadedString = await readFileAsync(
            './saves/' + this.props.level_name + '.txt');
        const loadedObject = JSON.parse(loadedString.toString('utf8'));
        await this.map.createSaveDirs();
        const savingObject = {
            playerData: loadedObject.playerData,
            plantInfo: loadedObject.plantInfo,
            waterUpdates: loadedObject.waterUpdates,
            map: {
                seed: loadedObject.map.seed,
                spawn: loadedObject.map.spawn,
            },
        };
        const savingString = JSON.stringify(savingObject, null, 2);
        await writeFileAsync('./saves/' + this.props.level_name + '/level.json', savingString);
        const promises = [];
        for (const chunk of loadedObject.map.chunks) {
            const filename = this.map.chunkDir + 'chunk_'+chunk.x+'_'+chunk.y;
            console.log('create',filename);
            const p = writeFileAsync(filename, chunk.terrain);
            promises.push(p);
        }
        await Promise.all(promises);
        LOG.log('FINISHED CONVERTING');
    }

    async prepareSpawnArea() {
        console.log('Preparing spawn area');
        await this.spawn.prepareSpawnArea();
    }

    startGameLoops() {
        this.plant.setLoop(1);
        this.water.setLoop(1);
        this.wires.setLoop(1);
        this.hopper.setLoop(1);
    }

    onClientConnect(socket) {
        console.log('Client connected: ' + socket.id);
        this.connectedSockets.push(socket);
        if (this.props.enable_autologin === true) {
            const result = this.onLogin({
                username: this.props.autologin_name,
                password: '',
            }, socket);
            if (!result) {
                console.log('Autologin failed. Ensure the password hash for '
                    +this.props.autologin_name+' is empty.');
            }
        }
    }

    onLogin(data, socket) {
        //Username sent?
        if (
            typeof(data.username) !== 'string'
            || typeof(data.password) !== 'string'
        ){
            socket.emit('loginerror', {message: 'incomplete data'});
            return false;
        }
        const result = this.playerData.login(data.username, data.password, socket);
        if (!result.success) {
            socket.emit('loginerror', {message: result.message});
            return false;
        }
        this.actions.login(result.player);
        socket.emit('loginsuccess', {});
        return true;
    }

    onClientDisconnect(data, socket) {
        console.log('Client disconnected: ' + socket.id);
        const player = this.playerData.logout(socket);
        // could be a player disconnecting
        // or just someone who closed the login screen.
        // check here:
        if(player !== null){
            // oki it's a logged in player
            this.actions.logout(player);
        }
        this.connectedSockets = this.connectedSockets.filter(
            s => s.id !== socket.id);
    }

    onChat(data, socket) {
        // needs to be a message
        if (typeof(data.message) !== 'string') return;
        // Chat is completely independent from the rest of the game.
        // You just need to be logged in.

        // who speaks?
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        const typed = data.message.substr(0,10000);
        this.handleChat(player, typed);
    }

    handleChat(player, typed) {

        // can't be empty message
        if (typed === '') return;

        const iscommand = /^\//.test(typed);

        if(iscommand){
            // is in fact a server-side command
            // this is where chat does affect the game

            this.commands.executeCommand(player, typed);
        } else {
            // it is not a command, only chat
            // prepend player name
            const message = '\\[#'+player.color+'\\]'+player.name +
                '\\[/\\]: ' + typed;
            LOG.log(AASTRING_RAW(message));

            for (const sendPlayer of this.playerData.onlinePlayers) {
                sendPlayer.socket.emit('chat', {
                    message: message,
                    notify: sendPlayer.name !== player.name,
                    timestamp: true,
                });
            }

            this.aagrinderMatrixBot.handleLocalChat(player, typed);
            this.aagrinderMatrixAppservice.handleLocalChat(player, typed);
        }
    }

    onTick(data, socket) {
        if (typeof(data.t) !== 'object') return;
        if (typeof(data.i) !== 'number') return;
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.actions.tick(player, data.t, data.i);
    }

    // server-side inventory events
    onInventory(data, socket) {
        if (typeof(data.x) !== 'number') return;
        if (typeof(data.y) !== 'number') return;
        if (typeof(data.i) !== 'string') return;
        if (typeof(data.n) !== 'number') return;
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.actions.inventoryEvent(player, data.x, data.y, data.i, Math.floor(data.n), data.d);
    }

    // server-side item deletion
    onItemDeletion(data, socket) {
        if (typeof(data) !== 'object') return;
        if (Object.keys(data).length > 5) return;
        for (const key in data) {
            if (typeof(key) !== 'string') return;
            if (typeof(data[key]) !== 'object') return;
            if (typeof(data[key].c) !== 'number') return;
        }
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.actions.itemDeletion(player, data);
    }

    onHotbar(data, socket) {
        if (typeof(data) !== 'object') return;
        if (data.length !== 10) return;
        for (const entry of data) {
            if (entry === null) continue;
            if (typeof entry !== 'string') return;
            if (entry.length > 20) return;
        }
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        player.hotbar = data;
    }

    onIClickedDetector(data, socket) {
        if (typeof(data.x) !== 'number') return;
        if (typeof(data.y) !== 'number') return;
        if (typeof(data.follow) !== 'boolean') return;
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.plant.detectors.iClickedDetector(player, data.x, data.y, data.follow)
    }

    onIClickedSspooky(data, socket) {
        if (typeof(data.x) !== 'number') return;
        if (typeof(data.y) !== 'number') return;
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.plant.ghosts.iClickedSspooky(player, data.x, data.y)
    }

    listen(http, io, port){
        io.on('connection', socket=>{
            this.onClientConnect(socket);
            socket.on('login', data=>this.onLogin(data, socket));
            socket.on('disconnect', data=>this.onClientDisconnect(data, socket));
            socket.on('chat', data=>this.onChat(data, socket));
            socket.on('t', data=>this.onTick(data, socket));
            socket.on('i', data=>this.onInventory(data, socket));
            socket.on('del', data=>this.onItemDeletion(data, socket));
            socket.on('h', data=>this.onHotbar(data, socket));
            socket.on('iclickeddetector', data=>this.onIClickedDetector(data, socket));
            socket.on('iclickedsspooky', data=>this.onIClickedSspooky(data, socket));
        });
        if (this.props.restrict_to_localhost) {
            http.listen(port, 'localhost');
            console.log('NOTE: access is restricted to localhost!');
        }
        else {
            http.listen(port);
        }
    }
}

exports.Server = Server;
