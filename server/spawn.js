// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * A class to handle the spawn area logic
 * and the logic for actually spawning players
 * when they log in for the first time
 * or when they log in but their position is obstructed.
 *
 * The current logic breaks sometimes.
 * TODO: change it to something more simple and robust.
 */

"use strict";

const PREPARE_DIST = 2;
const PREPARE_CHUNK_COUNT = (2 * PREPARE_DIST + 1) * (2 * PREPARE_DIST + 1);
const MAX_SPAWN_ATTEMPTS = 100;

class Spawn {
    constructor(map, syncher, props) {
        this.map = map;
        this.syncher = syncher;
        this.props = props;
    }

    // choose a good area for spawn
    // and load surrounding chunks
    async prepareSpawnArea() {

        if (this.map.spawnareax === undefined) {
            let seed = this.props.level_seed + 0.1;
            let iterations = 0;
            do {
                // pick spawn area
                this.map.spawnareax = Math.floor(Math.sin(seed++)*1024);
                this.map.spawnareay = Math.floor(Math.sin(seed++)*1024);
                iterations++;
                if (iterations > 100) {
                    // force override
                    // because it's taking too long
                    this.map.spawnareax = 100;
                    this.map.spawnareay = 100;
                    break;
                }
                // generate area needed for checking
                const chunkx = Math.floor(this.map.spawnareax / 256);
                const chunky = Math.floor(this.map.spawnareay / 256);
                const subchunkx = (this.map.spawnareax%256+256)%256;
                const subchunky = (this.map.spawnareay%256+256)%256;
                const coords = [];
                // can't think of a more concise way to express this
                coords.push({x: chunkx, y: chunky});
                if (subchunkx < 32) {
                    // left
                    coords.push({x: chunkx-1, y: chunky});
                    if (subchunky < 32) {
                        // bottom left
                        coords.push({x: chunkx-1, y: chunky-1});
                    }
                    else if (subchunky > 256-32) {
                        // top left
                        coords.push({x: chunkx-1, y: chunky+1});
                    }
                }
                if (subchunkx > 256-32) {
                    // right
                    coords.push({x: chunkx+1, y: chunky});
                    if (subchunky < 32) {
                        // bottom right
                        coords.push({x: chunkx+1, y: chunky-1});
                    }
                    else if (subchunky > 256-32) {
                        // top right
                        coords.push({x: chunkx+1, y: chunky+1});
                    }
                }
                if (subchunky < 32) {
                    // bottom
                    coords.push({x: chunkx, y: chunky-1});
                }
                else if (subchunky > 256-32) {
                    // top
                    coords.push({x: chunkx, y: chunky+1});
                }
                await this.map.loadManyChunksAsync(coords);
                // check if it is an okay area
            } while (this.evaluateSpawnArea() === false);
            LOG.log('Picked spawn area ('+iterations+' attempts)');
        }

        const spawnchunkx = Math.floor(this.map.spawnareax / 256);
        const spawnchunky = Math.floor(this.map.spawnareay / 256);

        // make list of spawn chunks
        const coords = [];
        for (let y = -PREPARE_DIST; y <= PREPARE_DIST; y++) {
            for (let x = -PREPARE_DIST; x <= PREPARE_DIST; x++) {
                coords.push({x: spawnchunkx+x, y: spawnchunky+y});
            }
        }
        await this.map.loadManyChunksAsync(coords);

        // make spawn chunks persistent (never unloads)
        for (let y = -PREPARE_DIST; y <= PREPARE_DIST; y++) {
            for (let x = -PREPARE_DIST; x <= PREPARE_DIST; x++) {
                this.map.getChunk(spawnchunkx+x,
                    spawnchunky+y).persistent = true;
            }
        }

    }

    static canSpawnInside(block) {
        return block === ' '
            || block === '~'
            || block === 'sn'
            || block === '..'
            || block === '*|'
            || block === 'si'
            || block[0] === 'A';
    }

    static canSpawnOn(block) {
        return block !== ' '
            && block !== 'sn'
            && block !== '..'
            && block !== '~'
            && block !== 'si'
            && block !== 'WS'
            && block !== 'w'
            && block[0] !== '>'
            && block[0] !== '-'
            && block[0] !== '*'
            && block[0] !== 'A';
    }

    // decide if the current spawn area is good
    evaluateSpawnArea() {
        // count number of good spawning spaces in the area:
        let good = 0;
        for (let dx = -32; dx < 32; dx++) {
            for (let dy = -31; dy < 32; dy++) {
                const x = this.map.spawnareax+dx;
                const y = this.map.spawnareay+dy;
                const here = this.map.getBlock(x, y);
                const below = this.map.getBlock(x, y-1);

                // bad spot if not space
                if (!Spawn.canSpawnInside(here)) continue;

                // bad spot if not ground
                if (!Spawn.canSpawnOn(below)) continue;

                // bad spot if wrong biome
                const biome = this.map.getBiome(x, y);
                if (
                    biome === 's'
                    || biome === 'c'
                    || biome === 'I'
                    || biome === 'u'
                    || biome === 'f'
                    || biome === 'o'
                    || biome === 'C'
                    || biome === 'R'
                    || biome === 'W'
                ) continue;
                //is a good spot!
                good++;
                if (good > 100) {
                    return true;
                }
            }
        }
        return false;
    }

    choosePlayerSpawnSpot(player, force = false){
        // does the player have a position?
        if(player.x !== null && player.y !== null && !force){
            // yes. Spawn at that position, or at the first unobstructed space above.
            const x = player.x;
            let y = player.y;
            return({x: x, y: y});
        }
        else{
            // spawn the player somewhere around world spawn instead.

            let sx, sy;

            for (let attempts = 1; attempts <= MAX_SPAWN_ATTEMPTS; attempts++) {
                sx = this.map.spawnareax + Math.floor(Math.random()*64)-32;
                sy = this.map.spawnareay + Math.floor(Math.random()*64)-32;

                for (let j = 0; j < 5; j++) {
                    const blockHere = this.map.getBlock(sx, sy);
                    const blockUnder = this.map.getBlock(sx, sy-1);

                    if (!Spawn.canSpawnInside(blockHere)) {
                        // obstructed, try higher.
                        sy++;
                        continue;
                    }

                    if (!Spawn.canSpawnOn(blockUnder)) {
                        // no ground underneath, try lower.
                        sy--;
                        continue;
                    }

                    // just fine! spawn here!
                    LOG.log('spawned player '+player.name+' ('
                        +attempts+' attempts)');
                    return({x: sx, y: sy});
                }
            }

            // did not manage to find a spot AFTER MAX ATTEMPTS!
            // just use the last attempt from earlier
            // and force-spawn here
            LOG.log('force-spawning player '+player.name
                +' ('+MAX_SPAWN_ATTEMPTS+' failed attempts)');
            return({x: sx, y: sy});
        }
    }

}

exports.Spawn = Spawn;
