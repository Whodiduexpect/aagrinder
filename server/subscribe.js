// Copyright (C) 2018-2021 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Makes sure that the clients are getting updates for exactly those chunks they are supposed to.
 *
 * The logic is simple, really.
 * Keep the 3 by 3 area of chunks around the player loaded,
 * and unload chunks if they go beyond the 5 by 5 area.
 *
 * And we only check each time when a player has moved over 128 blocks.
 * Checking at every movement would be overkill.
 *
 * Also keeps a collection of all chunks that are subscribed by any player
 * (this is needed for plant updates)
 *
 * Also marks chunks for unloading
 */

"use strict";

class Subscribe{

    constructor(map, props){
        this.map = map;
        this.playerLists = {};
        this.loadDistance = props.load_distance; // chunks of at most this distance away from the player's chunk are loaded.
        this.unloadDistance = props.unload_distance; // chunks further than this distance away from the player's chunk are unloaded.
        // fix silly mistakes:
        if(this.loadDistance < 1) this.loadDistance = 1;
        if(this.unloadDistance < 1) this.unloadDistance = 1;
    }

    subscribe(player, chunkx, chunky){
        player.subscriptions.push({x:chunkx, y:chunky});
        player.chunkUpdates.push({x:chunkx, y:chunky});
        const chunk = this.map.getChunk(chunkx, chunky); // and generate if necessary
        chunk.subscribers.push(player);
    }

    unsubscribe(player, chunkx, chunky){
        for(let i = 0; i < player.subscriptions.length; i++){
            if(player.subscriptions[i].x === chunkx && player.subscriptions[i].y === chunky){
                this.unsubscribeIndex(player, i);
                break;
            }
        }
    }

    unsubscribeIndex(player, index){
        const chunkx = player.subscriptions[index].x;
        const chunky = player.subscriptions[index].y;
        player.subscriptions.splice(index, 1);
        player.unsubscriptions.push({x:chunkx, y:chunky});
        const chunk = this.map.getChunk(chunkx, chunky);
        for(let i = 0; i < chunk.subscribers.length; i++){
            if(chunk.subscribers[i].name === player.name){
                chunk.subscribers.splice(i, 1);
                break;
            }
        }
        if (chunk.subscribers.length === 0 && !chunk.persistent) {
            // nobody is looking at this chunk any more.
            // unload.
            this.map.halfStagedChunksForUnload.push({
                x: chunkx,
                y: chunky,
            });
        }
    }

    resubscribe(player, force = false){
        const distance = Math.abs(player.resubscribePosition.x - player.x) + Math.abs(player.resubscribePosition.y - player.y);
        if(distance < 128 && !force){
            // The player has hardly moved, no point in resubscribing now
            return;
        }
        // Update position of last resubscribe
        player.resubscribePosition.x = player.x;
        player.resubscribePosition.y = player.y;

        // Determine which chunk the player is in:
        const chunkPosX = Math.floor(player.x / 256);
        const chunkPosY = Math.floor(player.y / 256);

        // Unsubscribe from chunks which are too far:
        for (let c = player.subscriptions.length - 1; c >= 0; c--){
            const chunkx = player.subscriptions[c].x;
            const chunky = player.subscriptions[c].y;
            if(chunkx < chunkPosX-this.unloadDistance
                || chunkx > chunkPosX+this.unloadDistance
                || chunky < chunkPosY-this.unloadDistance
                || chunky > chunkPosY+this.unloadDistance){
                // this chunk is too far away
                this.unsubscribeIndex(player, c);
            }
        }

        // Subscribe to chunks which are by the player
        // (unless already subscribed)
        const coords = [];
        for (let i = chunkPosY-this.loadDistance; i <= chunkPosY+this.loadDistance; i++){
            for (let j = chunkPosX-this.loadDistance; j <= chunkPosX+this.loadDistance; j++){
                // check if this subscription exists already
                let exists = false;
                for (const s of player.subscriptions){
                    if(s.x === j && s.y === i){
                        exists = true;
                        break;
                    }
                }
                if(exists){
                    continue;
                }
                // subscription does not exist. Add to list:
                coords.push({x: j, y: i});
            }
        }
        // generate them first (all at once)
        this.map.loadManyChunksSync(coords);
        // now subscribe
        for (const {x, y} of coords) {
            this.subscribe(player, x, y);
        }

    }

    unsubscribeAll(player){
        for(let i = player.subscriptions.length-1; i>=0; i--){
            this.unsubscribeIndex(player, i);
        }
    }

}


exports.Subscribe = Subscribe;
