// Copyright (C) 2018-2021 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/* This class invokes the program aagrindertree,
 * deserializes the result and attempts to
 * insert the tree into the existing world.
 */

"use strict";


const execFileSync = require('child_process').execFileSync;
const deserialize = require('../shared/chunk').deserialize;

class Tree{

    constructor(map, playerData, syncher){
        this.map = map;
        this.playerData = playerData;
        this.syncher = syncher;
    }

    treeAttempt(x, y, type){
        // generate a tree!
        const treeSeed = Math.floor(Math.random()*65536);
        const treeString = execFileSync(
            './aagrinder-terrain/aagrindertree',
            ['-s', '--', type, treeSeed],
            {encoding:'utf8'}
        );
        const tree = deserialize(40, 39, treeString).terrain;

        const changes = [];
        for (let i = 0; i < 40; i++) {
            for (let j = 0; j < 39; j++) {
                if (tree[i][j] === ' ') continue; // ok
                const blockHere = this.map.getBlockWithoutLoading(
                    x+j-18, y+i-1,'B');
                if (type === 'tentacle' && (blockHere === 'S'
                    || blockHere === 'WA')) continue; // ok
                // check collision
                // (note: do not check collision with my own sapling)
                if (!(i === 1 && j === 18)
                    && blockHere !== ' '
                    && blockHere !== '~'
                    && blockHere[0] !== '-') {
                    // collision with a block other than air, cloud or grass!
                    if (type === 'bush' || type === 'cranberry'
                        || type === 'blueberry') {
                        // for these types, collision is skipped
                        // but does not abort tree growth
                        continue;
                    }
                    // collision!
                    // abort abort abort
                    return false;
                }
                changes.push({x:x+j-18, y:y+i-1, block:tree[i][j]});
            }
        }

        // success!
        // but tentacle requires more cleanup
        if (type === 'tentacle') {
            for (let dx = -1; dx <= 1; dx++) {
                for (let dy = 0; dy <= 3; dy++) {
                    const blockHere = this.map.getBlockWithoutLoading(
                        x+dx, y+dy, ' ')
                    if (blockHere !== 'WA') continue;
                    const newBlockHere = tree[dy][dx+18];
                    if (newBlockHere === 'WA') continue;
                    if (newBlockHere === 'WO') continue;
                    this.syncher.serverChangeBlock(x+dx, y+dy, ' ');
                }
            }
        }

        this.syncher.serverChangeBlocks(changes);
        return true;
    }

}
exports.Tree = Tree;
