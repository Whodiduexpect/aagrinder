// Copyright (C) 2018-2021 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

const levelByCode = Object.freeze({
    'w': 32,
    '>v': 31,
    '>u': 30,
    '>t': 29,
    '>s': 28,
    '>r': 27,
    '>q': 26,
    '>p': 25,
    '>o': 24,
    '>n': 23,
    '>m': 22,
    '>l': 21,
    '>k': 20,
    '>j': 19,
    '>i': 18,
    '>h': 17,
    '>g': 16,
    '>f': 15,
    '>e': 14,
    '>d': 13,
    '>c': 12,
    '>b': 11,
    '>a': 10,
    '>9': 9,
    '>8': 8,
    '>7': 7,
    '>6': 6,
    '>5': 5,
    '>4': 4,
    '>3': 3,
    '>2': 2,
    '>1': 1,
    '>0': 0,
});

const nextLevel = Object.freeze({
    'w': '>v',
    '>v': '>u',
    '>u': '>t',
    '>t': '>s',
    '>s': '>r',
    '>r': '>q',
    '>q': '>p',
    '>p': '>o',
    '>o': '>n',
    '>n': '>m',
    '>m': '>l',
    '>l': '>k',
    '>k': '>j',
    '>j': '>i',
    '>i': '>h',
    '>h': '>g',
    '>g': '>f',
    '>f': '>e',
    '>e': '>d',
    '>d': '>c',
    '>c': '>b',
    '>b': '>a',
    '>a': '>9',
    '>9': '>8',
    '>8': '>7',
    '>7': '>6',
    '>6': '>5',
    '>5': '>4',
    '>4': '>3',
    '>3': '>2',
    '>2': '>1',
    '>1': '>0',
});

const codeByLevel = [
    '>0',
    '>1',
    '>2',
    '>3',
    '>4',
    '>5',
    '>6',
    '>7',
    '>8',
    '>9',
    '>a',
    '>b',
    '>c',
    '>d',
    '>e',
    '>f',
    '>g',
    '>h',
    '>i',
    '>j',
    '>k',
    '>l',
    '>m',
    '>n',
    '>o',
    '>p',
    '>q',
    '>r',
    '>s',
    '>t',
    '>u',
    '>v',
];

function isFlowSpace(block) {
    return block === ' '
    || block === '~'
    || block === 'w'
    || block === 'sn'
    || block === 'Wi'
    || block[0] === '='
    || (block[0] === '-' && block !== '-8' && block !== '-a')
    || block[0] === '>';
}

function isDominatedFlowSpace(block, waterBlock) {
    if (block === ' '
    || block === '~'
    || block === 'sn'
    || block === 'Wi'
    || block[0] === '='
    || (block[0] === '-' && block !== '-8' && block !== '-a')
    ) {
        return true;
    }
    else if (block[0] === '>') {
        // it is another flowing block
        // this one is only dominated if has higher level
        const myLevel = levelByCode[waterBlock];
        const otherLevel = levelByCode[block];
        if (myLevel > otherLevel) return true;
    }
    return false;
}


class Water{

    constructor(map, playerData, subscribe, syncher){
        this.map = map;
        this.subscribe = subscribe;
        this.syncher = syncher;
        this.interval = undefined;
        this.updates = [];
    }

    setLoop(multiplier){

        // stop previous loop if exists
        if (this.interval !== undefined) {
            clearInterval(this.interval);
        }

        if (multiplier < 0.01) {
            // don't start the interval at all
            return;
        }

        // start interval
        this.interval = setInterval(() => {
            this.waterUpdate100();
        }, 100 / multiplier);

    }


    waterUpdate100(){

        const newUpdates = []
        for (const u of this.updates) {
            // surroundings unloaded?
            if (
                !this.map.isBlockLoaded(u.x+1, u.y+2)
                || !this.map.isBlockLoaded(u.x+1, u.y-2)
                || !this.map.isBlockLoaded(u.x-1, u.y+2)
                || !this.map.isBlockLoaded(u.x-1, u.y-2)
            ) {
                // just skip it for now,
                // we'll deal with it when it's loaded
                newUpdates.push(u);
                continue;
            }
            // from here on it is guaranteed that
            // surrounding 5x3 area is loaded.
            let blockHere = this.map.getBlock(u.x, u.y);
            // cancel if it is not a water block
            if (blockHere !== 'w' && blockHere[0] !== '>') continue;

            // get all surrounding blocks
            const blockAbove = this.map.getBlock(u.x, u.y+1);
            const blockBelow = this.map.getBlock(u.x, u.y-1);
            const blockLeft = this.map.getBlock(u.x-1, u.y);
            const blockRight = this.map.getBlock(u.x+1, u.y);

            // DRAINING

            // flowing water block can change level or disappear:
            if (blockHere[0] === '>') {
                // get all water levels
                const levelHere = levelByCode[blockHere];
                let levelAbove = levelByCode[blockAbove];
                let levelLeft = levelByCode[blockLeft];
                let levelRight = levelByCode[blockRight];
                if (levelAbove === undefined) levelAbove = -1;
                if (levelLeft === undefined) levelLeft = -1;
                if (levelRight === undefined) levelRight = -1;
                // get max
                const levelMax = Math.max(levelAbove, levelLeft, levelRight);
                // level here should be 1 less than that
                const should = levelMax - 1;
                // if level should be less than 0, become air
                if (should < 0) {
                    this.syncher.serverChangeBlock(u.x, u.y, ' ');
                    // update surrounding blocks
                    // because they might disappear too
                    newUpdates.push({ x: u.x, y: u.y-1 });
                    newUpdates.push({ x: u.x+1, y: u.y });
                    newUpdates.push({ x: u.x-1, y: u.y });
                    // now cancel to prevent it from also flowing
                    // (it can't flow if it completely disappeared)
                    continue;
                }
                // otherwise become or stay a flowing water block
                // (possibly different than current)
                else if (should !== levelHere) {
                    // level will change
                    // BUT with this exception:
                    // if there is still water above, the level
                    // here can only increase, not decrease.
                    // This gives a nicer pattern to draining.
                    if (should < levelHere
                        && levelAbove > -1
                        && levelAbove > levelHere-8) {
                        // skip decreasing because there's water above
                    }
                    else {
                        // ok level really will change
                        const shouldBlock = codeByLevel[should];
                        this.syncher.serverChangeBlock(u.x, u.y, shouldBlock);
                        blockHere = shouldBlock;
                        // update surrounding blocks
                        // because they might change too
                        newUpdates.push({ x: u.x, y: u.y-1 });
                        newUpdates.push({ x: u.x+1, y: u.y });
                        newUpdates.push({ x: u.x-1, y: u.y });
                    }
                }
            }

            // FLOWING

            // cancel if end of flowing water
            if (blockHere === '>0') continue;

            // what is the block code of the next step
            const nextBlock = nextLevel[blockHere];
            // decide where this block will flow.
            // check if flow down:

            if (isFlowSpace(blockBelow)) {
                // definitely flow down, if anywhere at all
                if (isDominatedFlowSpace(blockBelow, blockHere)) {
                    // yes, flow down
                    // but also possible infinite water spring
                    if (blockHere === 'w'
                    && ((this.map.getBlock(u.x-1, u.y-1) === 'w')
                        || this.map.getBlock(u.x+1, u.y-1) === 'w')) {
                        // infinite water spring
                        this.syncher.serverChangeBlock(u.x, u.y-1, 'w');
                    }
                    else {
                        // just normal spread
                        this.syncher.serverChangeBlock(u.x,u.y-1, nextBlock);
                    }
                    newUpdates.push({ x: u.x, y: u.y-1 });
                }
            }

            if (!isFlowSpace(blockBelow) || blockHere === 'w') {
                // going left and right instead.
                // because it's blocked below
                // or because this is the source block

                // left?
                if (isDominatedFlowSpace(blockLeft, blockHere)) {
                    // yes, flow left
                    // but also possible infinite water spring
                    if (blockHere === 'w'
                    && ((this.map.getBlock(u.x-2, u.y) === 'w')
                        || this.map.getBlock(u.x-1, u.y-1) === 'w'
                        || this.map.getBlock(u.x-1, u.y+1) === 'w')) {
                        // infinite water spring
                        this.syncher.serverChangeBlock(u.x-1, u.y, 'w');
                    }
                    else {
                        // just normal spread
                        this.syncher.serverChangeBlock(u.x-1,u.y, nextBlock);
                    }
                    newUpdates.push({ x: u.x-1, y: u.y });
                }

                // right?
                const blockRight = this.map.getBlock(u.x+1, u.y);
                if (isDominatedFlowSpace(blockRight, blockHere)) {
                    // yes, flow right
                    // but also possible infinite water spring
                    if (blockHere === 'w'
                    && ((this.map.getBlock(u.x+2, u.y) === 'w')
                        || this.map.getBlock(u.x+1, u.y-1) === 'w'
                        || this.map.getBlock(u.x+1, u.y+1) === 'w')) {
                        // infinite water spring
                        this.syncher.serverChangeBlock(u.x+1, u.y, 'w');
                    }
                    else {
                        // just normal spread
                        this.syncher.serverChangeBlock(u.x+1,u.y, nextBlock);
                    }
                    newUpdates.push({ x: u.x+1, y: u.y });
                }

            }
        }
        this.updates = newUpdates;
    }

    enqueue(x, y) {
        this.updates.push({
            x: x,
            y: y,
        });
    }

    loadFromJSON(object){
        this.updates = object;
    }

    saveToJSON(){
        return this.updates;
    }

}
exports.Water = Water;
