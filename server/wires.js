// Copyright (C) 2018-2021 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * the whole cable + wire + motor + flying mechanics
 *
 * Note that all terrain changes MUST go through the syncher,
 * otherwise clients would not get updates.
 *
 */

"use strict";

const SPRITES = require('../shared/sprites').SPRITES;

// @ts-ignore
String.prototype.replaceAt = function(index, newchar) {
    return this.substring(0, index) + newchar + this.substring(index+1);
}

const cableDirections = Object.freeze({
    '0': [ true,false, true,false],
    '1': [ true,false,false,false],
    '2': [false, true,false,false],
    '3': [ true, true,false,false],
    '4': [false,false, true,false],
    '5': [ true,false, true,false],
    '6': [false, true, true,false],
    '7': [ true, true, true,false],
    '8': [false,false,false, true],
    '9': [ true,false,false, true],
    'a': [false, true,false, true],
    'b': [ true, true,false, true],
    'c': [false,false, true, true],
    'd': [ true,false, true, true],
    'e': [false, true, true, true],
    'f': [ true, true, true, true],
});

const int2hex = '0123456789abcdef';
const hex2int = Object.freeze({
    '0': 0,
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    'a': 10,
    'b': 11,
    'c': 12,
    'd': 13,
    'e': 14,
    'f': 15,
});

const DIRS = [
    {dx:1, dy:0, dir:0, invdir:2},
    {dx:0, dy:1, dir:1, invdir:3},
    {dx:-1, dy:0, dir:2, invdir:0},
    {dx:0, dy:-1, dir:3, invdir:1},
];

class Wires {

    constructor(map, syncher, playerData) {
        this.map = map;
        this.syncher = syncher;
        this.playerData = playerData;
        this.queue = [];
        this.nextqueue = [];
        this.interval = undefined;
    }

    setLoop(multiplier) {

        // stop previous loop if exists
        if (this.interval !== undefined) {
            clearInterval(this.interval);
        }

        if (multiplier < 0.01) {
            // don't start the interval at all
            return;
        }

        // start interval
        this.interval = setInterval(() => {
            this.doWireTick();
        }, 200 / multiplier);

    }

    enqueue(x, y) {
        this.queue.push({x:x, y:y});
    }

    pointingDirections(block) {
        if (block[0] === '=') {
            return cableDirections[block[1]];
        }
        if (block[0] === '%') {
            return [true, true, true, true];
        }
    }

    directionalPower(block, direction) {
        const dir = DIRS[direction];

    }

    findStrongestPowerSource(x, y, dirs = DIRS,
        usePotentialDrop = false) {
        let maxPower = 0;
        let char = 'R';
        for (const dir of dirs) {
            const otherBlock = this.map.getBlockWithoutLoading(
                x + dir.dx, y + dir.dy);
            if (['R1', 's1', 'O1'].includes(otherBlock)) {
                // UNLIMITED POWER!!
                maxPower = 15;
                if (otherBlock === 's1') char = 'S';
                if (otherBlock === 'O1') char = 'O';
                break;
            }
            if (otherBlock[0] === ':') {
                const power = otherBlock[1];
                if ((dir.dir === 1 || dir.dir === 3)
                    && (power === '0' || power === '2')) {
                    // Powered by the not gate
                    maxPower = 15;
                    char = otherBlock[2];
                    break;
                }
            }
            if (otherBlock === 'sC' && dir.dir === 3) {
                // Maybe powered by the aascanner
                const scanBlock = this.map.getBlockWithoutLoading(x, y-2);
                const scanCode = Wires.blockToScanCode(scanBlock);
                if (scanCode !== null) {
                    maxPower = 15;
                    char = scanCode;
                    break;
                }
            }
            if (otherBlock[0] === '=') {
                if (
                    // checking if the other wire is pointing at me,
                    // because then, it should be powering me.
                    cableDirections[otherBlock[1]][dir.invdir]
                ) {
                    // the other wire is pointing at me.
                    let power = hex2int[otherBlock[2]];
                    if (usePotentialDrop) {
                        power -= 1;
                    }
                    if (power > maxPower) {
                        maxPower = power;
                        char = otherBlock[3];
                    }
                }
            }
            if (otherBlock[0] === '%') {
                // possible that power goes 2 spaces
                const otherOtherBlock = this.map.getBlockWithoutLoading(
                    x + 2*dir.dx, y + 2*dir.dy);
                if (otherOtherBlock[0] === '=') {
                    if (
                        // checking if the other wire is pointing at me,
                        // because then, it should be powering me.
                        cableDirections[otherOtherBlock[1]][dir.invdir]
                    ) {
                        // the other wire is pointing at me.
                        let power = hex2int[otherOtherBlock[2]];
                        if (usePotentialDrop) {
                            power -= 1;
                        }
                        if (power > maxPower) {
                            maxPower = power;
                            char = otherOtherBlock[3];
                        }
                    }
                }
            }
        }
        return {power:maxPower, char:char};
    }

    enqueueSurroundings(x, y, dirs = DIRS) {
        for (const dir of dirs) {
            const ox = x + dir.dx;
            const oy = y + dir.dy;
            const otherBlock = this.map.getBlockWithoutLoading(ox, oy);
            if (
                otherBlock === 'st'
                || otherBlock[0] === '='
                || otherBlock === 'sT'
                || otherBlock === 'WO'
                || otherBlock === 'WD'
                || otherBlock === 'Wp'
                || otherBlock === 'sc'
                || otherBlock === 'sC'
                || otherBlock[0] === '%'
            ) {
                this.queue.push({x:ox, y:oy});
            }
            if (otherBlock[0] === '%') {
                this.queue.push({x:x+dir.dx*2, y:y+dir.dy*2});
            }
            else if (otherBlock[0] === ':') {
                this.nextqueue.push({x:ox, y:oy});
            }
            // else if (otherBlock === ...)
        }
    }

    static blockToScanCode(b) {
        // special scan codes
        if (b[0] === '-') return '-';
        if (b[0] === '=') return '-';
        if (b[0] === ':') return ':';
        // find sprite for this block
        const sprite = SPRITES[b];
        // abort if sprite not exist
        if (!sprite) return null;
        // char of sprite becomes the scan code
        const scanCode = sprite.char;
        // validate scan code length
        if (scanCode.length !== 1) return null;
        // convert special scan codes
        if (scanCode === '╳') return 'X';
        if (scanCode === '▁') return '_';
        if (scanCode === '”') return '"';
        // remaining scan codes are allowed only if matching ? exists
        if (!SPRITES['?'+scanCode]) return null;
        return scanCode;
    }

    doWireTick() {

        for (let i = 0; i < this.queue.length; i++) {
            const pos = this.queue[i];
            const block = this.map.getBlockWithoutLoading(pos.x, pos.y);
            if (block[0] === '=') {
                // it's a cable
                // find my strongest power source
                // but only from the directions where this wire is pointing
                const max = this.findStrongestPowerSource(pos.x, pos.y,
                    DIRS.filter(dir=>cableDirections[block[1]][dir.dir]), true);
                // propagated power decreases by 1 if coming from other wire
                const maxPower = max.power;
                const char = max.char;
                // now I know the strongest power powering me.
                const actualPowerHere = hex2int[block[2]];
                if (maxPower > actualPowerHere) {
                    // you power me uuuuuup
                    const newBlock = block.substr(0,2) + int2hex[maxPower] + char;
                    this.syncher.serverChangeBlock(pos.x, pos.y, newBlock);
                    // and send updates to all blocks I'm pointing at
                    this.enqueueSurroundings(pos.x, pos.y,
                        DIRS.filter(dir=>cableDirections[block[1]][dir.dir]));
                }
                else if (maxPower < actualPowerHere) {
                    // my max power is actually LOWER than my current power.
                    // the way we deal with this, is become zero.
                    const newBlock = block.replaceAt(2, '0');
                    this.syncher.serverChangeBlock(pos.x, pos.y, newBlock);
                    // and send updates to all blocks I'm pointing at.
                    this.enqueueSurroundings(pos.x, pos.y,
                        DIRS.filter(dir=>cableDirections[block[1]][dir.dir]));
                    // also update myself later, because turning to zero
                    // was probably not completely correct.
                    this.queue.push({x:pos.x, y:pos.y});
                }
                // there's also the case in which this wire is powered
                // exactly the way it's supposed to be.
                // We do nothing in that case.
            }
            else if (block[0] === '%') {
                // it's a wire crossing
                // find strongest wire power around this block
                // (only for visualization)
                let maxPower = 0;
                for (const dir of DIRS) {
                    const otherBlock = this.map.getBlockWithoutLoading(
                        pos.x + dir.dx, pos.y + dir.dy);
                    if (otherBlock[0] !== '=') continue;
                    if (!cableDirections[otherBlock[1]][dir.invdir]) continue;
                    const potentialPower = hex2int[otherBlock[2]];
                    if (potentialPower <= maxPower) continue;
                    maxPower = potentialPower;
                }
                // The crossing lights up with power 1 less than the wire
                let powerHere = maxPower - 1;
                if (powerHere < 0) powerHere = 0;
                const newBlock = '%' + int2hex[powerHere];
                if (newBlock !== block) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, newBlock);
                }
            }
            else if (block[0] === ':') {
                // it's a gate
                const powerLeft = this.findStrongestPowerSource(pos.x, pos.y, [DIRS[2]]);
                const powerRight = this.findStrongestPowerSource(pos.x, pos.y, [DIRS[0]]);
                let newBlock;
                if (powerLeft.power === 0 && powerRight.power === 0) {
                    // gate with both inputs off
                    newBlock = ':0:';
                }
                else if (powerLeft.power > 0 && powerRight.power > 0
                    && powerLeft.char === powerRight.char) {
                    // gate with both inputs on and same type
                    newBlock = ':2' + powerLeft.char;
                }
                else {
                    // all other cases
                    newBlock = ':1:';
                }
                if (newBlock !== block) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, newBlock);
                    this.enqueueSurroundings(pos.x, pos.y, [DIRS[1], DIRS[3]]);
                }
            }
            else if (block === 'st' || block === 'sT') {
                // it's a trapdoor
                const maxPower = this.findStrongestPowerSource(pos.x, pos.y).power;
                if (maxPower > 0) {
                    if (block === 'st')
                        this.syncher.serverChangeBlock(pos.x, pos.y, 'sT');
                }
                else {
                    if (block === 'sT')
                        this.syncher.serverChangeBlock(pos.x, pos.y, 'st');
                }
            }
            else if (block === 'WO') {
                // it's a grabber
                const maxPower = this.findStrongestPowerSource(pos.x, pos.y).power;
                if (maxPower > 0) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'WD');
                }
            }
            else if (block === 'WD') {
                // it's a disabled grabber
                const maxPower = this.findStrongestPowerSource(pos.x, pos.y).power;
                if (maxPower > 0) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'WO');
                }
            }
            else if (block === 'Wp') {
                // it's an aaprinter
                const max = this.findStrongestPowerSource(pos.x, pos.y);
                const maxPower = max.power;
                const char = max.char;
                if (maxPower > 0) {
                    for (const dir of DIRS) {
                        const ox = pos.x + dir.dx;
                        const oy = pos.y + dir.dy;
                        const otherBlock = this.map.getBlockWithoutLoading(ox, oy);
                        if (otherBlock[0] === '?') {
                            this.syncher.serverChangeBlock(ox, oy, '?'+char);
                        }
                    }
                }
            }
            else if (block === 'sc') {
                // it's an unpowered aascanner
                const max = this.findStrongestPowerSource(pos.x, pos.y, [DIRS[0], DIRS[2]]);
                const maxPower = max.power;
                if (maxPower > 0) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'sC');
                    this.nextqueue.push({x:pos.x, y:pos.y+1});
                }
            }
            else if (block === 'sC') {
                // it's a powered aascanner
                const max = this.findStrongestPowerSource(pos.x, pos.y, [DIRS[0], DIRS[2]]);
                const maxPower = max.power;
                if (maxPower < 1) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'sc');
                    this.nextqueue.push({x:pos.x, y:pos.y+1});
                }
            }
        }

        // swap the queues
        this.queue = this.nextqueue;
        this.nextqueue = [];
    }
}

exports.Wires = Wires;
