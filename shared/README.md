# /shared

Shared client & server logic in Javascript.

The AAGRINDER server expects this code to be running on the client.
If the client is not running this code (or equivalent),
the server and client will go out of synch.

All game logic that requires synchronization belongs here.
