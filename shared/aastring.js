// Copyright (C) 2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * SHARED FILE
 *
 * Class for working with aagrinder markup and control characters
 */

function AASTRING_RAW(str) {
    return str.replace(/\\\[[^\\]*\\\]/g,'');
}

function AASTRING_DECODE(str, defaultColor='') {
    let color = [];
    let currentColor = defaultColor;
    const lines = [];
    let lastNewline = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === '\\') {
            // might be a control code
            const sub = str.substr(i);
            const match = sub.match(/^\\\[([^\\]*)\\\]/);
            if (match !== null) {
                const controlseq = match[0];
                i += controlseq.length-1;  // skip parsing control code
                const code = match[1];
                if (code === '/') {  // clear formatting
                    currentColor = defaultColor;
                }
                else if (code[0] === '#') {  // set color
                    currentColor = code;
                }
                else if (code === 'n') {  // newline
                    lines.push({
                        raw: AASTRING_RAW(str.substring(lastNewline, i+1)),
                        color: color,
                    });
                    color = [];
                    lastNewline = i+1;
                }
                continue;
            }
        }
        color.push(currentColor);
    }
    lines.push({
        raw: AASTRING_RAW(str.substring(lastNewline)),
        color: color,
    });
    return lines;
}

function AASTRING_DECODE_WITH_WRAPPING(str, width, defaultColor='') {
    const linesNoWrap = AASTRING_DECODE(str, defaultColor);
    const linesWrap = [];
    for (const lineNoWrap of linesNoWrap) {
        while (lineNoWrap.raw.length > width) {
            const spaceIndex = lineNoWrap.raw.lastIndexOf(' ', width);
            const cutIndex = spaceIndex > 0 ? spaceIndex : width;
            linesWrap.push({
                raw: lineNoWrap.raw.substring(0, cutIndex),
                color: lineNoWrap.color.slice(0, cutIndex),
            });
            lineNoWrap.raw = lineNoWrap.raw.substring(cutIndex);
            lineNoWrap.color = lineNoWrap.color.slice(cutIndex);
        }
        linesWrap.push(lineNoWrap);
    }
    return linesWrap;
}

function AASTRING_LENGTH(str) {
    return AASTRING_RAW(str).length;
}

function AASTRING_SANITIZE(str) {
    let len = str.length;
    let len_prev;
    do {
        str = str.replace(/\\\[|\\\]/g,'');
        len_prev = len;
        len = str.length;
    } while (len < len_prev);
    return str;
}

if (typeof exports !== 'undefined') {
    exports.AASTRING_RAW = AASTRING_RAW;
    exports.AASTRING_DECODE = AASTRING_DECODE;
    exports.AASTRING_LENGTH = AASTRING_LENGTH;
    exports.AASTRING_SANITIZE = AASTRING_SANITIZE;
}
