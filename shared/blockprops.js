// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Common functions for determining properties of specific blocks.
 * Used in shared client&server game logic.
 * Also used for various server-only mechanics.
 */

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

class BlockProps {

    static isEnterable(block) {
        // blocks the player can move into
        return block === ' '
            || block === '~'
            || block === 'w'
            || block === '..'
            || block === 'sn'
            || block === 'si'
            || block === 'WA'
            || block === 'WO'
            || block === 'WD'
            || block === 'WS'
            || block === 'Wi'
            || block === 'sB'
            || block === 'st'
            || block === 'sT'
            || block === 'WI'
            || block[0] === 'A'
            || block[0] === 'a'
            || block[0] === 'f'
            || block[0] === 'W' && (block[1] === 'a' || block[1] === 'f')
            || block[0] === '>'
            || block[0] === 'H'
            || block[0] === 'h'
            || block[0] === '#'
            || block[0] === 't'
            || block[0] === '*'
            || block[0] === '-'
            || block[0] === '='
            || block[0] === '%'
            || block[0] === ':'
            || block[0] === '|'
        ;
    }

    static hasOnlyBottomSurface(block) {
        // blocks the player can stand inside
        // without falling out
        // (not climbing)
        return block === 'Wi'
            || block === 'sB'
            || block === 'st'
        ;
    }

    static hasOnlyTopSurface(block) {
        // blocks the player can stand on
        // but can also go inside
        // (not climbing)
        return block === 'sT'
        ;
    }

    static isNotSurface(block) {
        // blocks the player can not stand on
        return block === ' '
            || block === '~'
            || block === 'w'
            || block === '..'
            || block === 'sn'
            || block === 'si'
            || block === 'WO'
            || block === 'WD'
            || block === 'WS'
            || block === 'Wi'
            || block === 'sB'
            || block === 'st'
            || block[0] === 'A'
            || block[0] === 'a'
            || block[0] === 'f'
            || block[0] === 'W' && (block[1] === 'a' || block[1] === 'f')
            || block[0] === '>'
            || block[0] === '#'
            || block[0] === 't'
            || block[0] === '*'
            || block[0] === '-'
            || block[0] === '='
            || block[0] === ':'
            || block[0] === '|'
        ;
    }

    static isUndiggable(block) {
        // blocks that can't be destroyed by player
        return block === ' '
            || block === 'w'
            || block[0] === '>'
            || block === 'b'
            || block === 'WW'
            || block === 'NaN'
            || block === 'undefined';
    }

    static digDuration(block, tool) {
        if (block === 'B') return tool === ' ' ? 10 : 4;
        if (block === 'Dm') return tool === ' ' ? 15 : 5;
        if (block === 'Dg') return tool === ' ' ? 15 : 5;
        if (block === 'WB') return tool === ' ' ? 15 : 5;
        if (block === 'd') return tool === ' ' ? 4 : 3;
        if (block === 'S') return tool === ' ' ? 4 : 3;
        if (block[0] === '?') return tool === ' ' ? 8 : 2;
        if (block === 'si') return 15;
        if (block === 'WS') return 50;
        return 0;
    }

    static isLadder(block) {
        // blocks that act as a climbing surface
        return block[0] === 'H'
            || block[0] === 'h'
            || block[0] === '%'
            || block === 'WI'
            || block === 'WA';
    }

    static isClimbable(block) {
        // blocks that act as a climbing surface
        return block[0] === 'H'
            || block[0] === 'h'
            || block[0] === '%'
            || block === '-2'
            || block === '-a'
            || block === 'WI'
            || block === 'sn'
            || block === 'WA';
    }

    static isClimbSprintable(block) {
        // blocks that act as a climbing surface
        return block[0] === 'H'
            || block === 'WI';
    }

    static isSwimmable(block) {
        return block === 'w'
            || block[0] === '>';
    }

    static isUnstable(block) {
        // blocks that break if the block below disappears
        return block === '*|'
            || block[0] === 't'
            || block === 'Wt'
            || block === 'sn';
    }

    static isSolidBlock(block) {
        const b0 = block[0];
        return (block !== ' ')
        && (b0 !== 'w')
        && (b0 !== '>')
        && (b0 !== '~')
        && (b0 !== 't')
        && (b0 !== 'f')
        && (b0 !== '=')
        && (b0 !== ':')
        && (b0 !== '%')
        && (b0 !== '-')
        && (b0 !== '*')
        && (b0 !== '#')
        && (b0 !== 'p')
        && (b0 !== '|')
        && (block !== 'sn')
        && (block !== 'sB')
        && (block !== 'st')
        && (block !== 'sp')
        && (block !== 'sT')
        && (block !== '..')
        && (block !== 'Wi');
    }

    static validquestions = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~€';

}

if (typeof exports !== 'undefined') {
    exports.BlockProps = BlockProps;
} else {
    BP = BlockProps;
}
