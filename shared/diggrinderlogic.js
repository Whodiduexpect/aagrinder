// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}


// Determines which surrounding leaves will be checked by grinder
const leafSurround = [
    {dx:0, dy:-1, dir:-1},
    {dx:0, dy:-1, dir:+1},
    {dx:-1, dy:0, dir:-1},
    {dx:+1, dy:0, dir:+1},
    {dx:0, dy:+1, dir:-1},
    {dx:0, dy:+1, dir:+1},
    {dx:0, dy:+2, dir:0},
];

// Determines which block types will be dug in a 3x3 pattern with grinder
const diggableStones = {
    'G0': ['B'],
    'G#': ['B'],
    'Gy': ['B','b'],
    'GB': [],
};

// dig a block properly.
// if gain is true, corresponding item will be gained
// if force is true, block will be dug regardless of
// whether it can be gained or not.
function fullDig(x, y, view, gain, force=false) {

    const claim = view.getClaim(x, y);
    if (claim !== null && claim.p !== view.player.name) {
        if (!view.checkIfFriend(claim.p)) {
            view.notifyPrint('Owned by '+claim.p);
            return false;
        }
    }

    // block that will be dug:
    const dug = view.getBlock(x, y);
    // corresponding item that will be gained:
    let gained_item = Inventory.block2item(dug);

    let yes_will_gain = gain;

    if (dug === '*|') {
        // Don't gain flower stem. Gain what's on it, instead.
        const theflower = view.getBlock(x, y+1);
        gained_item = Inventory.block2item(theflower);
    }
    else if (dug === 'Wt'
            || dug === 'sn'
            || dug === 'WS'
            || dug === '~') {
        // don't gain these unobtainable items
        gained_item = null;
        yes_will_gain = false;
    }

    if (yes_will_gain && !view.player.inventory.canGain(gained_item)) {
        // ohwait I can't gain this item because no inv space.
        if (force) {
            // Since you're forcing me, I will dig without gaining.
            // That's all I can do.
            gained_item = null;
            yes_will_gain = false;
        }
        else {
            // I guess no dig after all ...
            return false;
        }
    }

    let didGain = false;
    // dig the block
    view.setBlock(x, y, ' ', true);
    // block above might break
    const above = view.getBlock(x, y+1);
    if (BP.isUnstable(view.getBlock(x, y+1))) {
        // recursively dig the unstable block that's above
        // (this is forced, otherwise it would look real weird)
        fullDig(x, y+1, view, gain, force=true);
    }

    // flower has a special digging procedure
    if (dug[0] === '*') {
        if (dug === '*|') {
            // dug the stem block.
            // there is a flower on it.
            const theflower = view.getBlock(x, y+1);
            if (theflower[0] !== '*') {
                console.log('uhhhhh, so, I found this weird flower: '+theflower);
                return false;
            }
            if (yes_will_gain) {
                view.gainItem(gained_item);
                didGain = true;
            };
            // the flower breaks as well
            view.setBlock(x, y+1, ' ', true);
            // make sure it also updates water
            if (view.isServer) {
                GLS.checkEnqueueWater(x+1, y+1, view);
                GLS.checkEnqueueWater(x-1, y+1, view);
                GLS.checkEnqueueWater(x, y+2, view);
            }
        }
        else {
            // dug the actual flower
            // gain it
            if (yes_will_gain) {
                view.gainItem(gained_item);
                didGain = true;
            };
            // now break the stem
            if (view.getBlock(x, y-1) === '*|') {
                // ok
                view.setBlock(x, y-1, ' ', true);
                // make sure it also updates water
                if (view.isServer) {
                    GLS.checkEnqueueWater(x+1, y-1, view);
                    GLS.checkEnqueueWater(x-1, y-1, view);
                    GLS.checkEnqueueWater(x, y-2, view);
                }
            }
            else {
                console.log('wait so this flower didn\'t have a stem??');
                // in this situation, we do not abort
                // because this is not as critical as the other one.
            }
        }
    }
    else {
        // for all other items, dig and gain normally
        if (yes_will_gain) {
            didGain = true;
            const is_data_item = view.player.inventory.constructor.isDataItem(
                gained_item);
            if (is_data_item) {
                const tiles = view.getTileEntityAt(x, y);
                // safety check for number of tiles
                if (tiles.length === 1) {
                    const tile = tiles[0];
                    view.playerDeleteTileEntityAt(x, y);
                    // tile data (.d) becomes item data
                    view.gainItem(gained_item, 1, tile.d);
                } else {
                    console.log('incorrect number of tile entities at '
                        +x+', '+y+': '+tiles.length);
                    const data = Inventory.makeItemData(gained_item);
                    view.gainItem(gained_item, 1, data);
                }
            }
            else {
                view.gainItem(gained_item);
            }
        }
    }

    // if digging fruit, get the leaf as well
    if (dug[0] === 'f') {
        view.gainItem('A'+dug[1], 1);
    }

    // on the server, need to check for tile entity
    if (view.isServer) {
        const tiles = view.getTileEntityAt(x, y);
        if (tiles.length > 0) {
            // there is a tile entity
            const tile = tiles[0];
            if (tile.t === 'chest') {
                if (!tile.inventory.isEmpty()) {
                    // chest has items in it!
                    // Non-empty chests can't be destroyed.
                    // Of course, the client does not have this information
                    // The client goes "I just broke this block"
                    // and now it's the server's job to respond
                    // "YOU DID NOT JUST BREAK THAT BLOCK"
                    view.surpriseSetBlock(x, y, dug, false);
                    if (didGain) {
                        // lose this item:
                        view.surpriseGainItem(gained_item, -1);
                    }
                }
                else {
                    // okay allow breaking this chest.
                    view.deleteTileEntityAt(x, y);
                    // camo chests:
                    if (dug === 'B'
                        || dug === 'Aa'
                        || dug === 'Ha'
                        || dug === 'DD') {
                        view.surpriseGainItem('hg', 1);
                    }
                }
            }
        }
    }

    // special stuff for sign
    if (dug === 'si') {
        view.playerDeleteTileEntityAt(x, y);
    }

    // special stuff for detector
    if (view.isServer && dug[0] === 'E') {
        view.deleteTileEntityAt(x, y);
    }

    // special stuff for SSPOOKY
    if (view.isServer && dug === 'Ws') {
        view.deleteTileEntityAt(x, y);
    }

    // special stuff for diamond
    if (dug === 'DD') {
        for (let dx = -3; dx <= 3; dx++) {
            for (let dy = -3; dy <= 3; dy++) {
                const otherBlock = view.getBlock(x+dx, y+dy);
                if (otherBlock === 'R0') {
                    // this ruby stone (pink B) disappears
                    view.addAnimation(x+dx, y+dy,
                        Math.abs(dx)/2 + Math.abs(dy)/2);
                    view.setBlock(x+dx, y+dy, 'B', true);
                }
            }
        }
    }

    // special stuff for wires and water
    if (view.isServer) {
        if (dug === 'sw'
            || dug === 's1'
            || dug === 'O0'
            || dug === 'O1'
            || dug === 'Dm'
            || dug === 'DM'
            || dug[0] === 'R'
            || dug[0] === '%'
            || dug[0] === ':'
        ) {
            GLS.enqueueWire4(x, y, view);
        }
        if (dug[0] === '=') {
            GLS.enqueueWire8(x, y, view);
        }
        GLS.checkEnqueueWater(x+1, y, view);
        GLS.checkEnqueueWater(x-1, y, view);
        GLS.checkEnqueueWater(x, y+1, view);
        GLS.checkEnqueueWater(x, y-1, view);
    }

    // Hopper Logic
    // Break relationships when any block in the right coords are broken
    if ( view.isServer && (dug[0] === 'p' || dug[0] === 'h' || dug === "Wp") ) {
        view.hopperInvalidate(x, y);
    }

    return true;
}


function digTree(view, x, y, limit) {
    // recursively dig whole tree
    let numdug = 0

    const visited = [];
    for (let i = 0; i < 4; i++) {
        visited.push(Array(39).fill(false));
    }
    const avisited = [];
    for (let i = 0; i < 4; i++) {
        avisited.push(Array(39).fill(false));
    }

    const queue = [{
        x:14,
        y:1,
        type:view.getBlock(x, y),
    }];
    visited[1][14] = true;
    while (queue.length > 0) {
        // stop if at limit
        if (numdug+1 > limit) return numdug;
        // next in queue
        const v = queue.shift();
        // animated block digging
        view.addAnimation(x+v.x, y+v.y, v.y);
        // break this wood block
        const successful_dig = fullDig(x+v.x, y+v.y, view, true);
        // stop if unsuccessful dig
        if (!successful_dig) return numdug;
        numdug++;

        // break surrounding vines
        let dy = -1;
        while (v.y + dy > 0) {
            const vine = view.getBlock(x+v.x, y+v.y+dy);
            if (vine === '-2' || vine === '-a') {
                // yes, this definitely is a vine
                // stop if at limit
                if (numdug+1 > limit) return numdug;
                // animated block digging
                view.addAnimation(x+v.x, y+v.y+dy, v.y-dy*0.5);
                // break this vine block
                const successful_dig = fullDig(x+v.x, y+v.y+dy, view, true);
                // stop if unsuccessful dig
                if (!successful_dig) return numdug;
                numdug++;
            }
            else {
                // no more vine!
                break;
            }
            dy--;
        }

        // break surrounding leaves
        for (const s of leafSurround) {
            for (let i = 0; i < 6; i++) {
                const xlocal = v.x + s.dx + i*s.dir;
                const ylocal = v.y + s.dy;
                if (xlocal<0 || xlocal>=avisited[0].length
                    || ylocal<0 || ylocal>=avisited.length) {
                    break;
                }
                const xglobal = x + xlocal;
                const yglobal = y + ylocal;
                const blockhere = view.getBlock(xglobal, yglobal);
                if (blockhere[0] === 'A'
                    || blockhere[0] === 'a'
                    || blockhere[0] === 'f'
                    || blockhere.substring(0,2) === 'Wa') {
                    // is leaf or fruit
                    if (!avisited[ylocal][xlocal]) {
                        // not visited yet!
                        // dig!
                        avisited[ylocal][xlocal] = true;
                        // gain only if it's not a fruit
                        // (fruit is lost with grinder)
                        let gain = (blockhere[0] !== 'f');
                        // stop if at limit
                        if (numdug+1 > limit) return numdug;
                        // animated block digging
                        view.addAnimation(xglobal, yglobal, v.y+i);
                        // break this leaf or fruit block
                        const successful_dig = fullDig(xglobal, yglobal, view, gain);
                        // stop if unsuccessful dig
                        if (!successful_dig) return numdug;
                        numdug++;

                        // break surrounding vines
                        let dy = -1;
                        while (ylocal + dy > 0) {
                            const vine = view.getBlock(xglobal, yglobal+dy);
                            if (vine === '-2' || vine === '-a') {
                                // yes, this definitely is a vine
                                // stop if at limit
                                if (numdug+1 > limit) return numdug;
                                // animated block digging
                                view.addAnimation(xglobal, yglobal+dy,
                                    v.y+i-dy*0.5);
                                // break this vine block
                                const successful_dig = fullDig(
                                    xglobal, yglobal+dy, view, true);
                                // stop if unsuccessful dig
                                if (!successful_dig) return numdug;
                                numdug++;
                            }
                            else {
                                // no more vine!
                                break;
                            }
                            dy--;
                        }

                    }
                }
                else {
                    // in all other cases, stop following this path
                    // because it is not connected!
                    break;
                }
            }
        }

        // BFS code
        if (v.y < visited.length-1) {
            for (let dx = -1; dx <= 1; dx++) {
                let blockhere = view.getBlock(v.x+dx+x, v.y+1+y);
                if (blockhere[0] === 'H') {
                    if (!visited[v.y+1][v.x+dx]) {
                        visited[v.y+1][v.x+dx] = true;
                        queue.push({
                            x: v.x+dx,
                            y: v.y+1,
                            type: blockhere,
                        });
                    }
                }
            }
        }
    }

    return numdug;
}

function digLogic(view, data) {
    if (isNaN(data.x)) return false;
    if (isNaN(data.y)) return false;
    const x = view.player.x + parseInt(data.x);
    const y = view.player.y + parseInt(data.y);
    // must be within reach
    if (Math.abs(view.player.x - x) > view.player.reach) return false;
    if (Math.abs(view.player.y - y) > view.player.reach) return false;

    // must be a diggable block
    const dug = view.getBlock(x, y);
    if(BP.isUndiggable(dug)){
        return false;
    }

    let tool = ' ';  // TODO: this implementation of tool is a mess
    if (typeof data.g !== 'undefined' && typeof data.slot !== 'undefined') {
        if (data.g === 'H') tool = 'GH';
        // must have this tool
        if (!view.player.inventory.hasItem(tool)) {
            return false;
        }
    }

    // undiggable blocks in underground
    if (dug === 'B' && view.player.gamemode === 0 && tool === ' ') {
        const biomeHere = view.syncher.map.biomeAt(x, y);
        if (biomeHere === 'u') return false;
    }

    if (view.player.gamemode === 0) {
        const duration = BP.digDuration(dug, tool);
        if (duration > 0) {
            view.player.digduration = duration;
            if (view.player.digx !== x || view.player.digy !== y) {
                view.player.digx = x;
                view.player.digy = y;
                view.player.digtime = 0;
            }
            view.player.digtime++;
            if (view.player.digtime < duration) {
                // not enough digging time yet!
                // but this action was "successful" anyway.
                return true;
            }
        }
    }
    view.player.digtime = 0;

    const success = fullDig(x, y, view, true);
    if (!success) return false;
    // skip durability if tool did not have an advantage
    if (BP.digDuration(dug, tool) === BP.digDuration(dug, ' ')) return success;

    // lose durability on this non-fist tool
    if (view.player.inventory.state[tool].d[data.slot].d-1 < 0) {
        // tool breaks
        view.gainItem(tool, -1, data.slot);
    }
    else {
        // lose durability
        view.player.inventory.state[tool].d[data.slot].d -= 1;
    }
    return success;
}

function digresetLogic(view, data) {
    if (view.player.digtime > 0) {
        view.player.digtime = 0;
        return true;
    }
    return false;
}

function grinderLogic(view, data) {
     // grinder action
    let grinderType = 'G0';
    if (data.g === '#') grinderType = 'G#';
    if (data.g === 'y') grinderType = 'Gy';
    if (data.g === 'B') grinderType = 'GB';
    if (isNaN(data.x)) return false;
    if (isNaN(data.y)) return false;
    const x = view.player.x + parseInt(data.x);
    const y = view.player.y + parseInt(data.y);
    // must be within reach
    if (Math.abs(view.player.x - x) > view.player.reach) return false;
    if (Math.abs(view.player.y - y) > view.player.reach) return false;

    const claim = view.getClaim(x, y);
    if (claim !== null && claim.p !== view.player.name) {
        if (!view.checkIfFriend(claim.p)) {
            view.notifyPrint('Owned by '+claim.p);
            return false;
        }
    }

    // must have a grinder, duh
    if (!view.player.inventory.hasItem(grinderType)) {
        return false;
    }

    // grinder needs specified slot
    // and it needs to be an integer within bounds
    if (
        !Number.isInteger(data.slot)
        || data.slot < 0
        || data.slot > view.player.inventory.state[grinderType].d.length
    ) {
        console.log(grinderType+' used without specified slot');
        return false;
    }

    // must be a diggable block
    // (boulder is an exception)
    const dug = view.getBlock(x, y);
    if(BP.isUndiggable(dug) && dug !== 'b'){
        return false;
    }
    if (dug === 'b' && grinderType === 'GB') {
        // boulder is no longer an exception
        return false;
    }
    let numdug = 0;

    const diggable_stones = diggableStones[grinderType];

    const diggable_vines = ['-2', '-8', '-a'];

    // stone
    if (diggable_stones.includes(dug)) {
        // dig all around
        for (let dx = -1; dx <= 1; dx++) {
            for (let dy = -1; dy <= 1; dy++) {
                const otherBlock = view.getBlock(x+dx, y+dy);
                if (diggable_stones.includes(otherBlock)) {
                    view.addAnimation(x+dx, y+dy, (dx-dy+2)/2);
                    fullDig(x+dx, y+dy, view, false);
                    numdug++;
                    if (otherBlock === 'b') {
                        // normal boulder rules
                        numdug += 99;
                        if (view.player.inventory.canGain('..', 25)) {
                            view.gainItem('..', 25);
                        }
                    }
                }
            }
        }
        if (numdug > 5) {
            if (view.player.inventory.canGain('..', 1)) {
                view.gainItem('..', 1);
            } else {
                view.setBlock(x, y, '..');
            }
        }
    }
    else if (dug[0] === 'H') {
        if (view.player.inventory.canGain(dug)) {
            // dig whole tree!
            numdug = digTree(view, x-14, y-1,
                view.player.inventory.state[grinderType].d[data.slot]);
        }
    }
    else if (diggable_vines.includes(dug)) {
        const gained_item = '-';
        if (!view.player.inventory.canGain(gained_item)) {
            console.log('nogain');
            return false;
        }
        for (let dy = 0; dy <= 4; dy++) {
            const otherBlock = view.getBlock(x, y+dy);
            if (diggable_vines.includes(otherBlock)) {
                view.addAnimation(x, y+dy, dy/2);
                fullDig(x, y+dy, view, false);
                numdug++;
            } else {
                break;
            }
        }
        for (let dy = -1; dy >= -4; dy--) {
            const otherBlock = view.getBlock(x, y+dy);
            if (diggable_vines.includes(otherBlock)) {
                view.addAnimation(x, y+dy, -dy/2);
                fullDig(x, y+dy, view, false);
                numdug++;
            } else {
                break;
            }
        }
        view.gainItem(gained_item, numdug);
    }
    else if (dug === 'b') {
        if (view.player.inventory.canGain('..')) {
            fullDig(x, y, view, false);
            // a lot of pebbles
            view.gainItem('..', 25);
            // a lot of durability
            numdug = 100;
        }
    }
    else if (dug === 'B' && grinderType === 'GB') {
        fullDig(x, y, view, false);
        numdug = 1;
        const blockLeft = view.getBlock(x-1, y);
        if (blockLeft === 'B') {
            fullDig(x-1, y, view, false);
            numdug = 2;
        }
        // some chance of getting pebble
        const xp = x>0?x:-x;
        const yp = y>0?y:-y;
        const d = view.player.inventory.state[grinderType].d[data.slot].d
        const rand = ((6789*xp)%11987+(9297*yp)%3873)%(((481*xp%941)*(9871*yp%567))%11111*2+1)
        if (rand % 8 === 1) {
            if (view.player.inventory.canGain('..')) {
                view.gainItem('..', 1);
            }
            else {
                view.setBlock(x, y, '..');
            }
        }
    }
    else {
        const success = fullDig(x, y, view, true);
        return success;
    }

    if (numdug > 1) {
        // lose some durability
        if (view.player.inventory.state[grinderType].d[data.slot].d-numdug < 0) {
            // grinder breaks
            view.gainItem(grinderType, -1, data.slot);
        }
        else {
            // lose this much durability
            view.player.inventory.state[grinderType].d[data.slot].d -= numdug;
        }
    }

    return true;
}


if (typeof exports !== 'undefined') {
    exports.digLogic = digLogic;
    exports.grinderLogic = grinderLogic;
    exports.digresetLogic = digresetLogic;
}
