// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
A collection of common fields and functions
needed exclusively in the shared gamelogic action functions
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
}

class GameLogicStandard {

    static checkStandSpot(view, x, y) {
        if (BP.hasOnlyBottomSurface(view.getBlock(x, y))) return true;
        return !BP.isNotSurface(view.getBlock(x, y-1));
    }

    static checkEnterableFromBelow(view, x, y) {
        const blockBelow = view.getBlock(x, y-1);
        if (BP.hasOnlyTopSurface(blockBelow)) return false;
        const blockHere = view.getBlock(x, y);
        if (BP.hasOnlyBottomSurface(blockHere)) return false;
        return BP.isEnterable(blockHere);
    }

    static checkEnterableFromAbove(view, x, y) {
        const blockHere = view.getBlock(x, y);
        if (BP.hasOnlyTopSurface(blockHere)) return false;
        const blockAbove = view.getBlock(x, y+1);
        if (BP.hasOnlyBottomSurface(blockAbove)) return false;
        return BP.isEnterable(blockHere);
    }

    static enqueueWire4(x, y, view) {
        view.enqueueWire(x+1, y);
        view.enqueueWire(x, y+1);
        view.enqueueWire(x-1, y);
        view.enqueueWire(x, y-1);
    }

    static enqueueWire5(x, y, view) {
        view.enqueueWire(x+1, y);
        view.enqueueWire(x, y+1);
        view.enqueueWire(x-1, y);
        view.enqueueWire(x, y-1);
        view.enqueueWire(x, y);
    }

    static enqueueWire8(x, y, view) {
        view.enqueueWire(x+1, y);
        view.enqueueWire(x+2, y);
        view.enqueueWire(x, y+1);
        view.enqueueWire(x, y+2);
        view.enqueueWire(x-1, y);
        view.enqueueWire(x-2, y);
        view.enqueueWire(x, y-1);
        view.enqueueWire(x, y-2);
    }

    static checkEnqueueWater(x, y, view) {
        const block = view.getBlock(x, y);
        if (block === 'w' || block[0] === '>') {
            view.enqueueWater(x, y);
        }
    }

}


if (typeof exports !== 'undefined') {
    exports.GameLogicStandard = GameLogicStandard;
} else {
    GLS = GameLogicStandard;
}
