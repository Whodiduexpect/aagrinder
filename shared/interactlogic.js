// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}


// @ts-ignore
String.prototype.replaceAt = function(index, newchar) {
    return this.substring(0, index) + newchar + this.substring(index+1);
}


// determines what happens if you click on grass with grass
const cycleGrass = {
    '1':'2',
    '2':'4',
    '4':'8',
    '8':'1',
    '3':'6',
    '6':'c',
    'c':'9',
    '9':'3',
    '5':'a',
    'a':'5',
    'b':'7',
    '7':'e',
    'e':'d',
    'd':'b',
    '0':'a',
    'f':'b',
};

function isFlowSpace(block) {
    return block === ' '
    || block === '~'
    || block === 'w'
    || block === 'sn'
    || block === 'Wi'
    || block[0] === '='
    || (block[0] === '-' && block !== '-8' && block !== '-a')
    || block[0] === '>';
}


function interactLogic(view, data) {
    let x = view.player.x + data.x;
    let y = view.player.y + data.y;
    if(Math.abs(view.player.x - x) > view.player.reach
        || Math.abs(view.player.y - y) > view.player.reach){
        // should not be able to reach!
        return false;
    }

    const claim = view.getClaim(x, y);
    if (claim !== null && claim.p !== view.player.name) {
        if (!view.checkIfFriend(claim.p)) {
            view.notifyPrint('Owned by '+claim.p);
            return false;
        }
    }

    const space = view.getBlock(x, y);
    // interacting grass on grass (or wire)
    if ((space[0] === '-' || space[0] === '=')
        && space[0] === data.item) {
        if (!view.player.inventory.hasItem(data.item)) {
            return false;
        }
        // cycle
        const previousDirection = space[1];
        const newDirection = cycleGrass[previousDirection];
        view.setBlock(x, y, space.replaceAt(1, newDirection));
        // special stuff for wires (triggering circuits by rotating wires)
        if (view.isServer && space[0] === '=') {
            GLS.enqueueWire5(x, y, view);
        }
        return true;
    }
    if (space[0] === 'h' || space === 'Ha' || space === 'B'
        || space === 'Aa' || space === 'DD') {
        // potentially chest tile entity (hidden from client)
        if (view.isServer) {
            const tiles = view.getTileEntityAt(x, y);
            if (tiles.length > 0) {
                // there is a tile
                const tile = tiles[0];
                if (tile.t === 'chest') {
                    // found chest!
                    view.subscribeChest(tile);
                }
            }
        }
        return true;
    }

    // switch
    if (space === 'sw') {
        view.setBlock(x, y, 's1');
        if (view.isServer) {
            GLS.enqueueWire4(x, y, view);
        }
        return true;
    }
    if (space === 's1') {
        view.setBlock(x, y, 'sw');
        if (view.isServer) {
            GLS.enqueueWire4(x, y, view);
        }
        return true;
    }
    if (space === 'O0') {
        view.setBlock(x, y, 'O1');
        if (view.isServer) {
            GLS.enqueueWire4(x, y, view);
        }
        return true;
    }

    // fruit picking
    if (space[0] === 'f') {
        const item = Inventory.block2item(space);
        if (view.player.inventory.canGain(item, 1)) {
            view.setBlock(x, y, 'a' + space[1]);
            view.gainItem(item, 1);
            return true;
        }
        else {
            return false;
        }
    }

    // trapdoor toggle
    if (space === 'st') {
        view.setBlock(x, y, 'sT');
        return true;
    }
    if (space === 'sT') {
        view.setBlock(x, y, 'st');
        return true;
    }

    // flip hopper
    if (space[0] === 'p') {
        let block = undefined;
        if (space === 'p<') block = 'pv';
        else if (space === 'p>') block = 'p<';
        else if (space === 'pv') block = 'p>';

        view.setBlock(x, y, block);
        if (view.isServer) {
            view.hopperInvalidate(x, y);
            view.processHopper(x, y, block);
        }
        return true;
    }

    // use empty bucket
    if (data.item === 'ue') {
        if (!view.player.inventory.hasItem('ue')) {
            return false;
        }
        let bucketItemToGain = undefined;
        if (space === 'w') {
            bucketItemToGain = 'uw';
        } else if (space === '~') {
            bucketItemToGain = 'u~';
        } else if (space === 'sn' || space === 'sN') {
            bucketItemToGain = 'us';
        } else if (space === 'WS') {
            bucketItemToGain = 'uB';
        } else {
            return false;
        }
        if (!view.player.inventory.canGain(bucketItemToGain, 1)) {
            return false;
        }
        view.setBlock(x, y, ' ');
        view.gainItem(bucketItemToGain, 1);
        view.gainItem('ue', -1);
        if (view.isServer) {
            GLS.checkEnqueueWater(x+1, y, view);
            GLS.checkEnqueueWater(x-1, y, view);
            GLS.checkEnqueueWater(x, y+1, view);
            GLS.checkEnqueueWater(x, y-1, view);
        }
        return true;
    }

    // use water bucket
    if (data.item === 'uw') {
        if (!view.player.inventory.hasItem('uw')) {
            return false;
        }
        if (!view.player.inventory.canGain('ue', 1)) {
            return false;
        }
        if (space[0] === 't') {
            const newTree = space[0] + space[1] + '2';
            view.gainItem('ue', 1);
            view.gainItem('uw', -1);
            view.setBlock(x, y, newTree);
            return true;
        }
        if (!isFlowSpace(space)) {
            return false;
        }
        view.setBlock(x, y, 'w');
        view.gainItem('ue', 1);
        view.gainItem('uw', -1);
        if (view.isServer) {
            GLS.checkEnqueueWater(x, y, view);
            GLS.checkEnqueueWater(x+1, y, view);
            GLS.checkEnqueueWater(x-1, y, view);
            GLS.checkEnqueueWater(x, y+1, view);
            GLS.checkEnqueueWater(x, y-1, view);
        }
        return true;
    }

    // use cloud bucket
    if (data.item === 'u~') {
        if (!view.player.inventory.hasItem('u~')) {
            return false;
        }
        if (!view.player.inventory.canGain('ue', 1)) {
            return false;
        }
        if (space !== ' ') {
            return false;
        }
        view.setBlock(x, y, '~');
        view.gainItem('ue', 1);
        view.gainItem('u~', -1);
        if (view.isServer) {
            GLS.checkEnqueueWater(x+1, y, view);
            GLS.checkEnqueueWater(x-1, y, view);
            GLS.checkEnqueueWater(x, y+1, view);
            GLS.checkEnqueueWater(x, y-1, view);
        }
        return true;
    }

    // use snow bucket
    if (data.item === 'us') {
        if (!view.player.inventory.hasItem('us')) {
            return false;
        }
        if (!view.player.inventory.canGain('ue', 1)) {
            return false;
        }
        if (space !== ' '
            && space !== 'w'
            && space !== '~'
            && space !== 'sn'
            && space[0] !== '>') {
            return false;
        }
        view.setBlock(x, y, 'sN');
        view.gainItem('ue', 1);
        view.gainItem('us', -1);
        if (view.isServer) {
            GLS.checkEnqueueWater(x+1, y, view);
            GLS.checkEnqueueWater(x-1, y, view);
            GLS.checkEnqueueWater(x, y+1, view);
            GLS.checkEnqueueWater(x, y-1, view);
        }
        return true;
    }

    // use quicksand bucket
    if (data.item === 'uB') {
        if (!view.player.inventory.hasItem('uB')) {
            return false;
        }
        if (!view.player.inventory.canGain('ue', 1)) {
            return false;
        }
        if (space !== ' '
            && space !== 'w'
            && space !== '~'
            && space !== 'sn'
            && space[0] !== '>') {
            return false;
        }
        view.setBlock(x, y, 'WS');
        view.gainItem('ue', 1);
        view.gainItem('uB', -1);
        if (view.isServer) {
            GLS.checkEnqueueWater(x+1, y, view);
            GLS.checkEnqueueWater(x-1, y, view);
            GLS.checkEnqueueWater(x, y+1, view);
            GLS.checkEnqueueWater(x, y-1, view);
        }
        return true;
    }

    // if none of above matched, reject
    return false;
}


if (typeof exports !== 'undefined') {
    exports.interactLogic = interactLogic;
}
