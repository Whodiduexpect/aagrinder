// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * How to add a new item:
 *
 * 1. add it to item_definitions
 * 2. optionally add a recipe for it
 * 3. add sprites for it in public/sprites.js
 * 4. optionally add special rules for it in
 *     - stack_sizes
 *     - item_data_templates
 *     - item2block
 *     - item2blockPlain
 *     - block2item
 *     - isGrassAttachment
 *     - isWireAttachment
 *     - shared/chunk.js
 *     - shared/blockprops.js
 *     - shared/placelogic.js
 *     - shared/diggrinderlogic.js
 *     - shared/interactlogic.js
 */

if (typeof exports !== 'undefined') {
    SPRITES = require('./sprites').SPRITES;
}

"use strict";

const NORMAL_STACK_LIMIT = 999;

class Inventory{

    // BEGIN CONSTANT STATIC FIELDS

    static item_definitions = [ // constant
        {c:'B',   s:'stone',       n:'stone'},
        {c:'d',   s:'dirt',        n:'dirt'},
        {c:'S',   s:'sand',        n:'sand'},
        {c:'b',   s:'boulder',     n:'boulder'},
        {c:'Aa',  s:'leaves',      n:'leaves'},
        {c:'Ai',  s:'pine lvs',    n:'pine leaves'},
        {c:'Al',  s:'almond lvs',  n:'almond leaves'},
        {c:'Ap',  s:'aprico lvs',  n:'apricot leaves'},
        {c:'Av',  s:'avocad lvs',  n:'avocado leaves'},
        {c:'Ab',  s:'bush lvs',    n:'bush leaves'},
        {c:'Ar',  s:'cranby lvs',  n:'cranberry leaves'},
        {c:'Au',  s:'blueby lvs',  n:'blueberry leaves'},
        {c:'aa',  s:'leaves ND',   n:'leaves (non-decaying)'},
        {c:'ai',  s:'pine lvs ND', n:'pine leaves (non-decaying)'},
        {c:'al',  s:'almn lvs ND', n:'almond leaves (non-decaying)'},
        {c:'ap',  s:'apri lvs ND', n:'apricot leaves (non-decaying)'},
        {c:'av',  s:'avoc lvs ND', n:'avocado leaves (non-decaying)'},
        {c:'ab',  s:'bush lvs ND', n:'bush leaves (non-decaying)'},
        {c:'ar',  s:'crnb lvs ND', n:'cranberry leaves (non-decaying)'},
        {c:'au',  s:'blub lvs ND', n:'blueberry leaves (non-decaying)'},
        {c:'Waa', s:'leaves D',    n:'leaves (decaying)'},
        {c:'Wai', s:'pine lvs D',  n:'pine leaves (decaying)'},
        {c:'Wal', s:'almn lvs D',  n:'almond leaves (decaying)'},
        {c:'Wap', s:'apri lvs D',  n:'apricot leaves (decaying)'},
        {c:'Wav', s:'avoc lvs D',  n:'avocado leaves (decaying)'},
        {c:'Wab', s:'bush lvs D',  n:'bush leaves (decaying)'},
        {c:'War', s:'crnb lvs D',  n:'cranberry leaves (decaying)'},
        {c:'Wau', s:'blub lvs D',  n:'blueberry leaves (decaying)'},
        {c:'ta',  s:'tiny tree',   n:'tiny tree'},
        {c:'ti',  s:'tiny pine',   n:'tiny pine'},
        {c:'tl',  s:'tiny alm t',  n:'tiny almond tree'},
        {c:'tp',  s:'tiny apr t',  n:'tiny apricot tree'},
        {c:'tv',  s:'tiny avo t',  n:'tiny avocado tree'},
        {c:'tb',  s:'tiny bush',   n:'tiny bush'},
        {c:'tr',  s:'tiny crn b',  n:'tiny cranberry bush'},
        {c:'tu',  s:'tiny blu b',  n:'tiny blueberry bush'},
        {c:'Wt',  s:'dead tree',   n:'dead tree'},
        {c:'fp',  s:'apricot',     n:'apricot'},
        {c:'fl',  s:'almond',      n:'almond'},
        {c:'fv',  s:'avocado',     n:'avocado'},
        {c:'fa',  s:'apple',       n:'apple'},
        {c:'fr',  s:'cranberry',   n:'cranberry'},
        {c:'fu',  s:'blueberry',   n:'blueberry'},
        {c:'Wfp', s:'apricot P',   n:'placed apricot'},
        {c:'Wfl', s:'almond P',    n:'placed almond'},
        {c:'Wfv', s:'avocado P',   n:'placed avocado'},
        {c:'Wfa', s:'apple P',     n:'placed apple'},
        {c:'Wfr', s:'cranb P',     n:'placed cranberry'},
        {c:'Wfu', s:'blueb P',     n:'placed blueberry'},
        {c:'Ha',  s:'wood',        n:'wood'},
        {c:'Hi',  s:'pine wood',   n:'pine wood'},
        {c:'Hl',  s:'almnd wood',  n:'almond wood'},
        {c:'Hp',  s:'apric wood',  n:'apricot wood'},
        {c:'Hv',  s:'avoca wood',  n:'avocado wood'},
        {c:'Hb',  s:'bush wood',   n:'bush wood'},
        {c:'Hr',  s:'cranb wood',  n:'cranberry wood'},
        {c:'Hu',  s:'blueb wood',  n:'blueberry wood'},
        {c:'hh',  s:'chest',       n:'chest'},
        {c:'hg',  s:'chest',       n:'chest'},
        {c:'hH',  s:'camo chest',  n:'camo chest'},
        {c:'hB',  s:'camo chest',  n:'camo chest'},
        {c:'hA',  s:'camo chest',  n:'camo chest'},
        {c:'hD',  s:'camo chest',  n:'camo chest'},
        {c:'p',   s:'hopper',      n:'hopper'},
        {c:'DD',  s:'diamond',     n:'diamond'},
        {c:'R',   s:'ruby',        n:'ruby'},
        {c:'R0',  s:'pink B',      n:'pink B'},
        {c:'Rd',  s:'ruby D',      n:'disabled ruby'},
        {c:'=',   s:'cable',       n:'cable'},
        {c:'%',   s:'crossing',    n:'crossing'},
        {c:':',   s:'gate',        n:'gate'},
        {c:'-',   s:'vine',        n:'vine'},
        {c:'sw',  s:'switch',      n:'switch'},
        {c:'M',   s:'motor',       n:'motor'},
        {c:'O0',  s:'button',      n:'button'},
        {c:'R>',  s:'arrow',       n:'arrow'},
        {c:'G0',  s:'grinder',     n:'grinder'},
        {c:'G#',  s:'GRINDER',     n:'GRINDER'},
        {c:'Gy',  s:'yaygrinder',  n:'pink grinder'},
        {c:'GB',  s:'stongrindr',  n:'stone grinder'},
        {c:'GH',  s:'woodgrindr',  n:'wooden grinder'},
        {c:'#s',  s:'spooky',      n:'spooky'},
        {c:'#y',  s:'yay',         n:'yay'},
        {c:'E',   s:'detector',    n:'detector'},
        {c:'Ws',  s:'SSPOOKY',     n:'SSPOOKY'},
        {c:'Wo',  s:'FOLLOW',      n:'FOLLOW'},
        {c:'X0',  s:'browncoral',  n:'brown coral'},
        {c:'X1',  s:'bluecoral',   n:'blue coral'},
        {c:'X2',  s:'redcoral',    n:'red coral'},
        {c:'X3',  s:'greencoral',  n:'green coral'},
        {c:'X4',  s:'neoncoral',   n:'neon coral'},
        {c:'X5',  s:'orangcoral',  n:'orange coral'},
        {c:'X6',  s:'yellocoral',  n:'yellow coral'},
        {c:'X7',  s:'purplcoral',  n:'purple coral'},
        {c:'X8',  s:'pinkcoral',   n:'pink coral'},
        {c:'Xx',  s:'deadcoral',   n:'dead coral'},
        {c:'w',   s:'water',       n:'water'},
        {c:'WW',  s:'warp',        n:'warp'},
        {c:'WV',  s:'customwarp',  n:'custom warp'},
        {c:'WF',  s:'ancieforge',  n:'ancient forge'},
        {c:'WB',  s:'corrupston',  n:'corrupted stone'},
        {c:' ',   s:'air',         n:'air'},
        {c:'..',  s:'pebble',      n:'pebble'},
        {c:'?',   s:'?',           n:'?'},
        {c:'||',  s:'stick',       n:'stick'},
        {c:'|/',  s:'rstick',      n:'rstick'},
        {c:'|\\', s:'lstick',      n:'lstick'},
        {c:'Fx',  s:'grey block',  n:'grey block'},
        {c:'F0',  s:'brown blk',   n:'brown block'},
        {c:'F1',  s:'blue block',  n:'blue block'},
        {c:'F2',  s:'ored block',  n:'orange red block'},
        {c:'F3',  s:'green blk',   n:'green block'},
        {c:'F4',  s:'neon block',  n:'neon block'},
        {c:'F5',  s:'orange blk',  n:'orange block'},
        {c:'F6',  s:'yellow blk',  n:'yellow block'},
        {c:'F7',  s:'purple blk',  n:'purple block'},
        {c:'F8',  s:'pink block',  n:'pink block'},
        {c:'F9',  s:'cyan block',  n:'cyan block'},
        {c:'Fa',  s:'magent blk',  n:'magenta block'},
        {c:'Fb',  s:'white blk',   n:'white block'},
        {c:'Fc',  s:'gold block',  n:'gold block'},
        {c:'Fd',  s:'red block',   n:'red block'},
        {c:'Fe',  s:'dred block',  n:'dark red block'},
        {c:'si',  s:'sign',        n:'sign'},
        {c:'*0',  s:'yello flwr',  n:'yellow flower'},
        {c:'*1',  s:'blue flowr',  n:'blue flower'},
        {c:'*2',  s:'cyan flowr',  n:'cyan flower'},
        {c:'*3',  s:'magen flwr',  n:'magenta flower'},
        {c:'*4',  s:'pink flowr',  n:'pink flower'},
        {c:'*5',  s:'white flwr',  n:'white flower'},
        {c:'*6',  s:'goldn flwr',  n:'golden flower'},
        {c:'*|',  s:'stem',        n:'flower stem'},
        {c:'WI',  s:'stalagmite',  n:'stalagmite'},
        {c:'WT',  s:'stalactite',  n:'stalactite'},
        {c:'Wl',  s:'icicle',      n:'icicle'},
        {c:'WA',  s:'tentacle',    n:'tentacle'},
        {c:'WO',  s:'grabber',     n:'grabber'},
        {c:'WD',  s:'grabber D',   n:'disabled grabber'},
        {c:'~',   s:'cloud',       n:'cloud'},
        {c:'>',   s:'flowing w',   n:'flowing water'},
        {c:'sn',  s:'snow layer',  n:'snow layer'},
        {c:'sN',  s:'snow block',  n:'snow block'},
        {c:'Dg',  s:'chalk',       n:'chalk'},
        {c:'Dm',  s:'magnetite',   n:'magnetite'},
        {c:'DM',  s:'magnetite D', n:'disabled magnetite'},
        {c:'Wi',  s:'ice',         n:'ice'},
        {c:'Wc',  s:'ice block',   n:'ice block'},
        {c:'sp',  s:'inspector',   n:'inspector'},
        {c:'WS',  s:'quicksand',   n:'quicksand'},
        {c:'sB',  s:'stone slab',  n:'stone slab'},
        {c:'st',  s:'trapdoor',    n:'trapdoor'},
        {c:'sT',  s:'trapdoor O',  n:'trapdoor (open)'},
        {c:'ue',  s:'bucket',      n:'bucket'},
        {c:'uw',  s:'waterbuket',  n:'water bucket'},
        {c:'u~',  s:'cloudbuket',  n:'cloud bucket'},
        {c:'us',  s:'snowbucket',  n:'snow bucket'},
        {c:'uB',  s:'qsandbuket',  n:'quicksand bucket'},
        {c:'Wp',  s:'aaprinter',   n:'aaprinter'},
        {c:'sc',  s:'aascanner',   n:'aascanner'},
        {c:'NaN', s:'NaN (ERR)',   n:'NaN (ERROR!)'},
        {c:'undefined', s:'UDF (ERR)', n:'undefined (ERROR!)'},
    ];

    static item_data_templates = { // constant
        'G0': {d:2000},
        'G#': {d:20000},
        'Gy': {d:2500},
        'GB': {d:1000},
        'GH': {d:500},
    };

    // object with codes for keys and names for values, for easy access
    // constant
    static item_names = Inventory.item_definitions.reduce((acc, cur, i)=>{
        acc[cur.c] = cur.n;
        return acc;
    },{});
    static item_short_names = Inventory.item_definitions.reduce((acc, cur, i)=>{
        acc[cur.c] = cur.s;
        return acc;
    },{});

    // an array of existing item codes
    // (is used for checking for valid items)
    static item_codes = Inventory.item_definitions.map(a=>a.c); // constant

    // object with codes for keys and stack sizes for values
    // constant
    static stack_sizes = Inventory.item_definitions.reduce((acc, cur, i)=>{
        const key = cur.c;
        if (key[0] === 'G') {
            acc[key] = 1;
        }
        else if (key === '#s') {
            acc[key] = 13;
        }
        else if (key === '#y') {
            acc[key] = 17;
        }
        else if (key === 'E' || key === 'Ws') {
            acc[key] = 10;
        }
        else if (key === 'sp') {
            acc[key] = 1;
        }
        else if (key === 'WA' || key === 'WO' || key === 'WB') {
            acc[key] = 666;
        }
        else if (key === 'WV') {
            acc[key] = 1;
        }
        else {
            acc[key] = NORMAL_STACK_LIMIT;
        }
        return acc;
    },{});

    // object with codes for keys and indices for values (used for sorting)
    // constant
    static index_by_code = Inventory.item_definitions.reduce((acc, cur, i)=>{
        acc[cur.c] = i;
        return acc;
    },{});

    static recipes = [ // constant
        {need:[{ item: 'Aa', amount: 20 }], get:{item: 'ta', amount:1}},
        {need:[{ item: 'Ai', amount: 20 }], get:{item: 'ti', amount:1}},
        {need:[{ item: 'Al', amount: 10 }], get:{item: 'tl', amount:1}},
        {need:[{ item: 'Ap', amount: 20 }], get:{item: 'tp', amount:1}},
        {need:[{ item: 'Av', amount: 20 }], get:{item: 'tv', amount:1}},
        {need:[{ item: 'Ab', amount: 5  }], get:{item: 'tb', amount:1}},
        {need:[{ item: 'Ar', amount: 5  }], get:{item: 'tr', amount:1}},
        {need:[{ item: 'Au', amount: 5  }], get:{item: 'tu', amount:1}},
        {need:[{ item: 'Ha', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hi', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hp', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hv', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hl', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hb', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hr', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hu', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Ha', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hi', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hp', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hv', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hl', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hb', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hr', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hu', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'B', amount: 1 }], get:{item: 'hB', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Aa', amount: 1 }], get:{item: 'hA', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'DD', amount: 1 }], get:{item: 'hD', amount:1}},
        {need:[{ item: 'B',  amount: 1  }, { item: '=', amount: 1 }], get:{item: 'O0', amount:1}},
        {need:[{ item: 'O0', amount: 1  }, { item: '=', amount: 2 }], get:{item: 'sw', amount:1}},
        {need:[{ item: '?',  amount: 5  }, { item: '=', amount: 5 }], get:{item: 'Wp', amount:1}},
        {need:[{ item: '?',  amount: 5  }, { item: '=', amount: 5 }], get:{item: 'sc', amount:1}},
        {need:[{ item: 'Dm', amount: 5  }], get:{item: 'p', amount:1}},
        {need:[{ item: '=',  amount: 20 }, { item: 'Dm', amount: 10 }], get:{item: 'M', amount:1}},
        {need:[{ item: 'M',  amount: 1  }, { item: 'DD', amount: 1 }], get:{item: 'G0', amount:1}},
        {need:[{ item: 'G0', amount: 1  }, { item: '#s', amount: 20 }], get:{item: 'G#', amount:1}},
        {need:[{ item: 'G0', amount: 1  }, { item: '#y', amount: 20 }], get:{item: 'Gy', amount:1}},
        {need:[{ item: 'M',  amount: 1  }, { item: 'B', amount: 20 }], get:{item: 'GB', amount:1}},
        {need:[{ item: '-',  amount: 20 }, { item: 'Ha', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20 }, { item: 'Hi', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20 }, { item: 'Hp', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20 }, { item: 'Hv', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20 }, { item: 'Hl', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20 }, { item: 'Hb', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20 }, { item: 'Hr', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20 }, { item: 'Hu', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: 'R',  amount: 1  }, { item: '-', amount: 20 }], get:{item: '=', amount:16}},
        {need:[{ item: '=',  amount: 2  }], get:{item: '%', amount:1}},
        {need:[{ item: '=',  amount: 5  }], get:{item: ':', amount:1}},
        {need:[{ item: '..', amount: 10 }], get:{item: '?', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'Xx', amount: 2 }], get:{item: 'Fx', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X0', amount: 2 }], get:{item: 'F0', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X1', amount: 2 }], get:{item: 'F1', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X2', amount: 2 }], get:{item: 'F2', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X3', amount: 2 }], get:{item: 'F3', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X4', amount: 2 }], get:{item: 'F4', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X5', amount: 2 }], get:{item: 'F5', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X6', amount: 2 }], get:{item: 'F6', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X7', amount: 2 }], get:{item: 'F7', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'X8', amount: 2 }], get:{item: 'F8', amount:1}},
        {need:[{ item: '..', amount: 10 }], get:{item: 'Fx', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: '*0', amount: 2 }], get:{item: 'F6', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: '*1', amount: 2 }], get:{item: 'F1', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: '*2', amount: 2 }], get:{item: 'F9', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: '*3', amount: 2 }], get:{item: 'Fa', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: '*4', amount: 2 }], get:{item: 'F8', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: '*5', amount: 2 }], get:{item: 'Fb', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'fa', amount: 2 }], get:{item: 'Fd', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'fp', amount: 2 }], get:{item: 'F5', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'fl', amount: 2 }], get:{item: 'Fc', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'fv', amount: 2 }], get:{item: 'F4', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'fr', amount: 2 }], get:{item: 'Fe', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'fu', amount: 2 }], get:{item: 'F1', amount:1}},
        {need:[{ item: '..', amount: 10 }, { item: 'WA', amount: 2 }], get:{item: 'F7', amount:1}},
        {need:[{ item: 'B',  amount: 1  }, { item: 'Dg', amount: 5 }], get:{item: 'si', amount:1}},
        {need:[{ item: 'Dm', amount: 1  }], get:{item: 'sp', amount:1}},
        {need:[{ item: 'Wi', amount: 6  }], get:{item: 'Wc', amount:1}},
        {need:[{ item: 'Wl', amount: 10 }], get:{item: 'Wc', amount:1}},
        {need:[{ item: 'B',  amount: 2  }], get:{item: 'sB', amount:1}},
        {need:[{ item: 'Ha', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hi', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hp', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hv', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hl', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hb', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hr', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hu', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'B',  amount: 1  }, {item: '#s',  amount: 1}], get:{item: 'WB', amount:1}},
        {need:[{ item: 'Ws', amount: 10 }], get:{item: 'Wo', amount:1}},
        {need:[{ item: 'Ha', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hi', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hp', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hv', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hl', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hb', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hr', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hu', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'sN', amount: 1  }], get:{item: 'sn', amount:1}},
        {need:[{ item: 'Ha', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hi', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hp', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hv', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hl', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hb', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hr', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hu', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: '||', amount: 1  }], get:{item: '||', amount:1}},
        {need:[{ item: '||', amount: 1  }], get:{item: '|/', amount:1}},
        {need:[{ item: '||', amount: 1  }], get:{item: '|\\', amount:1}},
        {need:[{ item: '|/', amount: 1  }], get:{item: '||', amount:1}},
        {need:[{ item: '|/', amount: 1  }], get:{item: '|/', amount:1}},
        {need:[{ item: '|/', amount: 1  }], get:{item: '|\\', amount:1}},
        {need:[{ item: '|\\', amount: 1  }], get:{item: '||', amount:1}},
        {need:[{ item: '|\\', amount: 1  }], get:{item: '|/', amount:1}},
        {need:[{ item: '|\\', amount: 1  }], get:{item: '|\\', amount:1}},
    ];

    static grassBlob = '0022002244664466002200224466446611331133557755771133113355775577002200224466446600220022446644661133113355775577113311335577557788aa88aacceeccee88aa88aacceeccee99bb99bbddffddff99bb99bbddffddff88aa88aacceeccee88aa88aacceeccee99bb99bbddffddff99bb99bbddffddff0022002244664466002200224466446611331133557755771133113355775577002200224466446600220022446644661133113355775577113311335577557788aa88aacceeccee88aa88aacceeccee99bb99bbddffddff99bb99bbddffddff88aa88aacceeccee88aa88aacceeccee99bb99bbddffddff99bb99bbddffddff'; // constant

    // END CONSTANT STATIC FIELDS

    static isDataItem(item_code) {
        return item_code[0] === 'G';
    }

    static makeItemData(item_code) {
        const data = {};
        Object.assign(data, this.item_data_templates[item_code]);
        return data;
    }

    static isGrassAttachment(block) {
        return block[0] === '-';
    };

    static isWireAttachment(block) {
        return block[0] === '='
            || block[0] === '%'
            || block[0] === ':'
            || block[0] === 'G'
            || block === 'R1'
            || block === 'Rd'
            || block === 'st'
            || block === 'sw'
            || block === 's1'
            || block === 'Dm'
            || block === 'DM'
            || block === 'O0'
            || block === 'O1'
            || block === 'sT'
            || block === 'WO'
            || block === 'WD'
            || block === 'Wp'
            || block === 'sc'
            || block === 'sC'
        ;
    };

    // Convert item to block (for the purpose of placing into the world)
    static item2block(item_code, relativeCheck) {
        if (typeof item_code !== "string") {
            console.error("item2block for non-string "+item_code);
        }
        if (item_code === 'p') {
            // Hopper Direction check
            // The priortization of the "right" direction is intentional

            const block_here_right = relativeCheck(1, 0)
            const block_here_left = relativeCheck(-1, 0)
            const block_here_top = relativeCheck(0, 1)
            const block_here_bottom = relativeCheck(0, -1)

            // Always take the hint from neighboring hoppers first
            if (block_here_right === 'p>' || block_here_left === 'p>') return 'p>';
            if (block_here_right === 'p<' || block_here_left === 'p<') return 'p<';
            if (block_here_top === 'pv' || block_here_bottom === 'pv') return 'pv';

            // If there's a chest we should point towards it
            if (block_here_right[0] === 'h') return 'p>';
            if (block_here_left[0] === 'h') return 'p<';
            if (block_here_top[0] === 'h' || block_here_bottom[0] === 'h') return 'pv';

            // Right is the default
            return 'p>';

        }
        if (item_code === '-') {
            const prev_block = relativeCheck(0,0);
            let type_code = 0;
            let power_of_2 = 1;
            for (let dy = -1; dy < 2; dy++) {
                for (let dx = -1; dx < 2; dx++) {
                    // the "-" is there because the logic is back from
                    // when the world coordinates were mirrored
                    const block_here = relativeCheck(dx, -dy);
                    if (prev_block === 'w') {
                        // underwater we only connect up and down
                        if (dx === 0) {
                            if (this.isGrassAttachment(block_here)
                                // more attachemt options underwater
                                || (dy === 1 && (
                                    block_here === 'B'
                                    || block_here === 'b'
                                    || block_here === 'd'
                                ))) {
                                type_code += power_of_2;
                            }
                        }
                    }
                    else {
                        if (this.isGrassAttachment(block_here))
                            type_code += power_of_2;
                    }
                    power_of_2 *= 2;
                }
            }
            const grass_block = '-' + this.grassBlob[type_code];
            return grass_block;
        }
        if (item_code === '=') {
            let type_code = 0;
            let power_of_2 = 1;
            for (let dy = -1; dy < 2; dy++) {
                for (let dx = -1; dx < 2; dx++) {
                    // the "-" is there because the logic is back from
                    // when the world coordinates were mirrored
                    const block_here = relativeCheck(dx, -dy);
                    if (this.isWireAttachment(block_here))
                        type_code += power_of_2;
                    power_of_2 *= 2;
                }
            }
            const wire_block = '=' + this.grassBlob[type_code] + '0R';
            return wire_block;
        }
        if (item_code === '%') return '%0';
        if (item_code === ':') return ':0:';
        if (item_code[0] === 't') return item_code + '0';
        if (item_code === 'R>') return 'R>0';
        if (item_code === 'R') return 'R1';
        if (item_code === 'M') return 'M0';
        // leaves become non-decaying leaves
        if (item_code[0] === 'A') return 'a'+item_code[1];
        // fruit becomes placed fruit
        if (item_code[0] === 'f') return 'W'+item_code;
        // camo chest becomes camo block
        if (item_code === 'hH') return 'Ha';
        if (item_code === 'hB') return 'B';
        if (item_code === 'hA') return 'Aa';
        if (item_code === 'hD') return 'DD';
        if (item_code === '?') return '??';
        if (item_code === '>') return '>0';
        if (item_code === 'E') return 'E0';
        // in other cases, block code is same as item code
        return item_code;
    };

    // Convert item to block (for the purpose of visualizing an item)
    static item2blockPlain(item_code) {
        if (typeof item_code !== "string") {
            console.error("item2blockPlain for non-string "+item_code);
        }
        if (item_code === 'p') return 'p>'
        if (item_code === '-') return '-0';
        // of note: =00 is not a valid block code, it is a sprite
        if (item_code === '=') return '=00';
        if (item_code === '%') return '%0';
        // of note: :0 is not a valid block code, it is a sprite
        if (item_code === ':') return ':0';
        if (item_code === 't') return 't0';
        if (item_code === 'R>') return 'R>0';
        if (item_code === 'R') return 'R1';
        if (item_code === 'M') return 'M0';
        if (item_code === '?') return '??';
        if (item_code === '>') return '>0';
        if (item_code === 'E') return 'E0';
        if (item_code[0] === 't') return item_code.substring(0,2);
        // in other cases, block code is same as item code
        return item_code;
    };

    // Convert a block code to an item code.
    // This function may only be used with valid block codes.
    static block2item(block) {
        if (typeof block !== "string") {
            console.error("block2item for non-string "+block);
        }
        // undeveloped ruby turns to stone
        if (block === 'R0') return 'B';
        // grinder keeps type in item
        if (block[0] === 'G') return block;
        // coral keeps color in item
        if (block[0] === 'X') return block;
        // chest remains chest
        if (block[0] === 'h') return block;
        // any kind of leaves become regular
        // while keeping type ofc
        if (block[0] === 'a') return 'A'+block[1];
        if (block.substring(0,2) === 'Wa') return 'A'+block[2];
        if (block[0] === 'A') return block;
        // wood also keeps type
        if (block[0] === 'H') return block;
        // fruits
        if (block[0] === 'f') return block;
        // frame keeps color in item
        if (block[0] === 'F') return block;
        // flower keeps color in item
        if (block[0] === '*') return block;
        // this should not be possible:
        if (block === '*|') return '*0';
        // specific detector becomes generic detector
        if (block[0] === 'E') return 'E';
        // spooky and yay stay
        if (block[0] === '#') return block;
        // tiny tree keeps type but not growth stage
        if (block[0] === 't') return block.substring(0,2);
        // on switch becomes off switch
        if (block === 's1') return 'sw';
        // on button becomes off button
        if (block === 'O1') return 'O0';
        // open trapdoor becomes closed trapdoor
        if (block === 'sT') return 'st';
        // disabled grabber becomes grabber
        if (block === 'WD') return 'WO';
        // disabled magnetite becomes magnetite
        if (block === 'DM') return 'Dm';
        // disabled ruby becomes ruby
        if (block === 'Rd') return 'R';
        // I started using 2-character blocks
        if (block[0] === 's') return block;
        if (block[0] === 'W') {
            if (block[1] === 'f') {
                return 'f'+block[2];
            }
            return block;
        }
        if (block[0] === 'D') return block;
        if (block[0] === '.') return block;
        if (block[0] === 'O') return block;
        if (block[0] === 'u') return block;
        if (block[0] === '|') return block;
        // NaN and undefined stay their ugly selves
        if (block === 'NaN') return block;
        // other blocks just become their substring
        return block[0];
    };

    static itemCodeExists(item_code) {
        if (this.item_codes.indexOf(item_code) === -1) return false;
        return true;
    };

    static humanInput2itemCode(input) {
        if (this.itemCodeExists(input)) {
            // human entered an item code
            return input;
        }
        for (const def of this.item_definitions) {
            if (def.s === input || def.n === input) {
                // human entered an item name
                return def.c;
            }
        }
        // unable to recognize input
        return null;
    }

    static humanInput2blockCode(input) {
        if (this.blockExists(input)) {
            // human entered a block code
            return input;
        }
        for (const def of this.item_definitions) {
            if (def.s === input || def.n === input) {
                // human entered an item name
                return this.item2block(def.c);
            }
        }
        // unable to recognize input
        return null;
    }

    static blockExists(block) {
        // currently the only way to check if a block code exists
        // is to check if there is a sprite defined for it.
        // note: block2item can not be used for this purpose
        // because it converts a lot of invalid block codes
        // into valid item codes! Example: "wolf"
        if (SPRITES[block] === undefined) return false;
        return true;
    }

    constructor(state={}, total_slots=3) {
        this.state = state;
        this.total_slots = total_slots;
        // Safety check every time when creating a new inventory
        // because data can come from all kinds of sources
        this.errorCheck();
    }

    // remove zeroes from state object
    // this function is not optimized.
    fix() {
        for (const item_code of Inventory.item_codes) {
            const obj = this.state[item_code];
            if (typeof(obj) === 'undefined' || obj === null || obj.c === 0) {
                delete this.state[item_code];
            }
        }
    }

    errorCheck() {
        for (const item_code in this.state) {
            if (!Inventory.itemCodeExists(item_code)) {
                console.log('FOUND AN INEXISTENT ITEM'
                    +' CODE IN AN INVENTORY!'
                    +' THE ITEM CODE IS '+item_code);
            }
        }
    }

    getNumUsedSlots() {
        let used_slots = 0;
        for (const item_code in this.state) {
            const how_many = this.state[item_code].c;
            const stack_size = Inventory.stack_sizes[item_code];
            const num_stacks = Math.ceil(how_many/stack_size);
            used_slots += num_stacks;
        }
        return used_slots;
    }

    isEmpty() {
        return Object.keys(this.state).length === 0;
    }

    numItems(item_code) {
        if (this.state[item_code]) {
            return this.state[item_code].c;
        } else {
            return 0;
        }
    }


    moveItemsTo(invto, item_code, count, data_index) {
        // If count is negative inverse
        let [datainvfrom, datainvto] = count > 0 ? [this, invto] : [invto, this];
        count = Math.abs(count);

        if (!Inventory.itemCodeExists(item_code)) {
            // item type does not exist
            console.error("Item move: Item type does not exist");
            return false;
        }
        if (typeof(count) !== 'number') {
            // not a number
            console.error("Item move: Item count is not a number");
            return false;
        }
        if (count === 0) {
            // so you want to transfer 0 items?
            console.error("Item move: Attempt to move 0 items");
            return false;
        }
        if (!datainvto.canGain(item_code, count)) {
            //console.error("Item move: Destination cannot gain " + count + " " + item_code);
            return false;
        }
        if (!datainvfrom.canGain(item_code, -count)) {
            //console.error("Item move: Source cannot lose " + count + " " + item_code);
            return false;
        }
        if (Inventory.isDataItem(item_code)) {
            // data item
            if ( Math.abs(count) !== 1 ) {
                // bad amount. Can only move 1 at a time
                // because otherwise how would you keep
                // track of which ones were moved?
                console.error("Item move: Attempt to move " + count + " data items (max is 1)");
                return false;
            }
            if (!Number.isInteger(data_index) || data_index < 0) {
                // missing index information
                console.error("Item move: Missing data index information");
                return false;
            }
            if (data_index > datainvfrom.state[item_code].d.length) {
                // too high index
                console.error("Item move: data index too high");
                return false;
            }
            const data = datainvfrom.state[item_code].d[data_index];
            datainvfrom.gainItem(item_code, -1, data_index);
            datainvto.gainItem(item_code, 1, data);
        } else {
            // regular items
            // no additional conditions here.
            datainvfrom.gainItem(item_code, -count);
            datainvto.gainItem(item_code, count);
        }

        return true;
    }

    canGain(item_code, count=1) {
        if (count > 0) {
            // checking against inventory capacity
            let used_slots = this.getNumUsedSlots();
            const how_many = this.numItems(item_code);
            const stack_size = Inventory.stack_sizes[item_code];
            const num_stacks_old = Math.ceil(how_many/stack_size);
            const num_stacks_new = Math.ceil((how_many+count)/stack_size);
            const diff_stacks = num_stacks_new - num_stacks_old;
            used_slots += diff_stacks;
            return used_slots <= this.total_slots;
        }
        else {
            // checking if I have items to spend
            const how_many = this.numItems(item_code);
            return how_many+count >= 0;
            // If, due to a bug, item count is 0 or negative,
            // it still works correctly.
        }
    }

    // Determine max amount of items of this type that can be gained
    howManyCanGain(item_code) {
        // checking against inventory capacity
        const used_slots = this.getNumUsedSlots();
        const vacant_slots = this.total_slots - used_slots;
        const have = this.numItems(item_code);
        const stack_size = Inventory.stack_sizes[item_code];
        const num_stacks = Math.ceil(have/stack_size);
        const vacant_on_last_slot = num_stacks * stack_size - have;
        return vacant_slots * stack_size + vacant_on_last_slot;
    }

    canSpend(item_code, count=1) {
        return this.canGain(item_code, -count);
    }

    hasItem(item_code) {
        return this.canGain(item_code, -1);
    };

    // the parameter data_or_index is an object when gaining a
    // data item. Only one data item can be gained at a time.
    // When spending a data item, the same parameter is used to
    // specify the index of the data that is to be deleted.
    // Only one data item can be spent at once.
    gainItem(item_code, count=1, data_or_index) {
        const is_data_item = Inventory.isDataItem(item_code);
        if (is_data_item) {
            if (count !== 1 && count !== -1) {
                console.log(
                    'Only one data item can be gained or spent at a time.',
                    item_code, count, data_or_index);
                throw 'Only one data item can be gained or spent at a time.';
            }
            if (data_or_index === undefined) {
                console.log(
                    'Data, index or null must be specified for data item.',
                    item_code, count, data_or_index);
                throw 'Data, index or null must be specified for data item.'
            }
        }
        if (this.state[item_code] === undefined) {
            if (is_data_item) {
                // gaining one data item
                this.state[item_code] = {
                    c: 1,
                    d: [data_or_index ? data_or_index
                        : Inventory.makeItemData(item_code)],
                };
            } else {
                this.state[item_code] = {c:count};
            }
        }
        else {
            this.state[item_code].c += count;
            if (is_data_item) {
                if (count > 0) {
                    // gaining one data item
                    this.state[item_code].d.push(data_or_index
                        ? data_or_index : Inventory.makeItemData(item_code));
                } else {
                    // losing one data item
                    // at this index:
                    this.state[item_code].d.splice(data_or_index, 1);
                }
            }
        }
        if (this.state[item_code] === undefined) {
            this.state[item_code] = {c:count};
        }
        if (this.state[item_code].c === 0) {
            delete this.state[item_code];
        }
    }

    // gain items without specifying item data.
    // Only use this if you don't care about item data, obviously
    gainMany(item_code, count=1) {
        const is_data_item = Inventory.isDataItem(item_code);
        if (this.state[item_code] === undefined) {
            if (is_data_item) {
                // gaining data items
                this.state[item_code] = {
                    c: count,
                    d: Array.from({length: count},
                        e => Inventory.makeItemData(item_code)),
                };
            } else {
                // gaining regular items
                this.state[item_code] = {c:count};
            }
        }
        else {
            this.state[item_code].c += count;
            if (is_data_item) {
                if (count > 0) {
                    // gaining data items
                    for (let i = 0; i < count; i++) {
                        this.state[item_code].d.push(
                            Inventory.makeItemData(item_code));
                    }
                } else {
                    // losing data items
                    this.state[item_code].d.splice(0, -count);
                }
            }
        }
        if (this.state[item_code].c === 0) {
            delete this.state[item_code];
        }
    }

    // Completely ignore item data and remove items.
    // Use only if you know what you're doing.
    gainItemNoShenanigans(item_code, count=1) {
        if (this.state[item_code] === undefined) {
            this.state[item_code] = {c:count};
        } else {
            this.state[item_code].c += count;
        }
        if (this.state[item_code].c === 0) {
            delete this.state[item_code];
        }
    }

    spendItem(item_code, count=1) {
        this.gainItem(item_code, -count);
    }

    // This function should be avoided if possible
    setItemAmount(item_code, amount) {
        if (amount === 0) {
            delete this.state[item_code];
        } else {
            this.state[item_code] = {c:amount};
        }
    }

    canCraft(recipe_index, ncrafted) {
        const recipe = Inventory.recipes[recipe_index];
        if (typeof recipe === 'undefined') {
            return false;
        }
        if (!this.canGain(recipe.get.item, recipe.get.amount*ncrafted)) {
            return false;
        }
        for(const r of recipe.need) {
            if (!this.canSpend(r.item, r.amount*ncrafted)) {
                return false;
            }
        }
        return true;
    }

    // Check if have right items without checking amount (used for UI)
    couldCraft(recipe_index) {
        const recipe = Inventory.recipes[recipe_index];
        if (typeof recipe === 'undefined') {
            return false;
        }
        for(const r of recipe.need) {
            if (!this.canSpend(r.item, 1)) {
                return false;
            }
        }
        return true;
    }

    howManyCanCraft(recipe_index) {
        const recipe = Inventory.recipes[recipe_index];
        if (typeof recipe === 'undefined') {
            return 0;
        }
        // How many can we craft, according to available free slots?
        let how_many = Math.floor(
            this.howManyCanGain(recipe.get.item) / recipe.get.amount);
        for(const r of recipe.need) {
            // How many can we craft, according to this recipe part?
            const h = Math.floor(this.numItems(r.item) / r.amount);
            if (h < how_many) {
                how_many = h;
            }
        }
        return how_many;
    }

    craft(recipe_index, ncrafted) {
        const recipe = Inventory.recipes[recipe_index];
        for(const r of recipe.need) {
            this.gainItem(r.item, -r.amount*ncrafted, 0);
        }
        this.gainItem(recipe.get.item, recipe.get.amount*ncrafted, null);
    }

    expendRecipe(recipe_index, ncrafted) {
        const recipe = Inventory.recipes[recipe_index];
        for(const r of recipe.need) {
            this.gainItem(r.item, -r.amount*ncrafted, 0);
        }
    }


    spendAllItems(item_code) {
        delete this.state[item_code];
    }

    saveToJSON() {
        return {
            s: this.state,
            n: this.total_slots,
        }
    }

    loadFromJSON(o) {
        this.state = o.s;
        this.total_slots = o.n;
    }

}

if (typeof exports !== 'undefined') exports.Inventory = Inventory;
