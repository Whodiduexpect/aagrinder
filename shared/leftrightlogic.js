// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}

function leftrightLogic(view, data, dir) {
    const x = view.player.x;
    const y = view.player.y;
    if (isNaN(data.sprint)) return false;
    const speed = parseInt(data.sprint);
    if (speed < 1) return false;
    const blockhere = view.getBlock(x, y);
    let gamemode = view.player.gamemode;
    if (gamemode === 0 && blockhere === 'WO') {
        // stuck on the grabber
        return false;
    }

    switch(gamemode){
        case 0:
            const sprint = speed > 1;
            // sprinting rules in gamemode 0
            if (sprint) {
                // can't sprint in water and quicksand
                if (blockhere === 'w'
                    || blockhere[0] === '>'
                    || blockhere === 'WS'
                ) return false;
                // can't sprint if no food
                if (view.player.food < 1) return false;
            }

            // check if the player is currently in a ladder
            if (BP.isLadder(blockhere)) {
                // it's a ladder!
                // ladder works very similar to gamemode 1
                // but if currently standing on ground
                // and there is a staircase on the side,
                // walk up the staircase anyway.
                if (BP.isEnterable(view.getBlock(x+dir, y+1))
                    && !BP.isEnterable(view.getBlock(x+dir, y))
                    && !BP.isEnterable(view.getBlock(x, y-1))
                    && GLS.checkEnterableFromBelow(view, x, y+1)
                ){
                    // staircase next to climbable block
                    view.player.x += dir;
                    view.player.y += 1;
                    if (sprint) view.player.food--;
                    view.playerPosChanged();
                    return true;
                }
                // there was no special case, so act as gamemode 1:
                // check if can climb sideways
                if(!BP.isEnterable(view.getBlock(x+dir, y))){
                    // no climbing for you
                    return false;
                }
                else{
                    // in all other cases go horizontally
                    view.player.x += dir;
                    if (sprint) view.player.food--;
                    view.playerPosChanged();
                }
                return true;
            }

            // quicksand with no ground underneath?
            if (blockhere === 'WS' && !GLS.checkStandSpot(view, x, y)) {
                // stuck in quicksand
                return false;
            }

            // ladder on side?
            if (BP.isLadder(view.getBlock(x+dir, y))) {
                // yes there is a ladder directly next to the player
                if (GLS.checkEnterableFromBelow(view, x, y+1)
                    && BP.isEnterable(view.getBlock(x+dir, y+1))
                    && !BP.isLadder(view.getBlock(x+dir, y+1))
                ) {
                    // special case with a ladder that can be stepped on
                    view.player.y += 1;
                }
                // move into the ladder (or step on top of it)
                view.player.x += dir;
                if (sprint) view.player.food--;
                view.playerPosChanged();
                return true;
            }

            // check if blocked on side
            if (!BP.isEnterable(view.getBlock(x+dir, y))) {
                // check if blocked overhead
                if (!GLS.checkEnterableFromBelow(view, x, y+1)) {
                    // can't move, due to non-enterable above and on side
                    return false
                }
                if (!BP.isEnterable(view.getBlock(x+dir, y+1))) {
                    // can't move, due to a 2-high solid wall
                    return false;
                }
                // go diagonally, step on this non-enterable block
                view.player.x += dir;
                view.player.y += 1;
                if (sprint) view.player.food--;
                view.playerPosChanged();
                return true;
            }

            // no block on side.
            // is there a nice path down?
            if(
                BP.isEnterable(view.getBlock(x+dir, y))
                && !BP.isClimbable(view.getBlock(x+dir, y))
                && GLS.checkEnterableFromAbove(view, x+dir, y-1)
                && GLS.checkStandSpot(view, x+dir, y-1)
                && GLS.checkStandSpot(view, x, y)
                && !GLS.checkStandSpot(view, x+dir, y)
            ){
                // yes a nice path down
                view.player.x += dir;
                view.player.y += -1;
                if (sprint) view.player.food--;
                view.playerPosChanged();
                return true;
            }

            // in all other cases go horizontally
            view.player.x += dir;
            if (sprint) view.player.food--;
            view.playerPosChanged();
            return true;
        case 1:
            // flight mode
            if(!BP.isEnterable(view.getBlock(x+dir, y))
                && BP.isEnterable(blockhere)){
                // no flying sideways for you
                return false;
            }
            view.player.x += dir;
            view.playerPosChanged();
            break;
        case 2:
            if (speed > 256) return false;
            // spectator mode
            const sspeed = (speed > 1 ? speed-1 : 1);
            view.player.x += dir * sspeed;
            view.playerPosChanged();
            break;
        default:
            console.log('invalid gamemode: ' + view.player.gamemode);
            return false;
    }
    return true;
}

function leftLogic(view, data) {
    return leftrightLogic(view, data, -1);
}

function rightLogic(view, data) {
    return leftrightLogic(view, data, 1);
}


if (typeof exports !== 'undefined') {
    exports.leftLogic = leftLogic;
    exports.rightLogic = rightLogic;
}
