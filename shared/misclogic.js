// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}


function craftLogic(view, data) {
    if(typeof data.r !== 'number'){
        console.log('bad recipe index');
        return false;
    }
    if(typeof data.n !== 'number'){
        console.log('bad craft amount');
        return false;
    }
    const recipe_index = data.r;
    const amount = data.n;
    if (!view.player.inventory.canCraft(recipe_index, amount)) return false;
    view.player.inventory.craft(recipe_index, amount);
    view.refreshInventorySlots();
    return true;
}


function eatLogic(view, data) {
    if (typeof data.item !== 'string') {
        return false;
    }
    const item = data.item;
    // must be a food item
    if (item[0] !== 'f') {
        // you can't eat this
        return false;
    }
    // must have item
    if (!view.player.inventory.hasItem(item)) {
        // you don't even have this item
        return false;
    }
    // can not be full
    if (view.player.food > 800) {
        // can't eat when not hungry at all
        return false;
    }
    view.gainItem(item, -1);
    // round up
    view.player.food = 200*Math.ceil(view.player.food/200+1);
    // safety check
    if (view.player.food > 1000) view.player.food = 1000;
}


function magicallygetitemLogic(view, data) {
    if (typeof data.item !== 'string') {
        return false;
    }
    const item = data.item;
    // must be gamemode 1 or 2
    if (view.player.gamemode === 0) {
        return false;
    }
    // must have space in inventory
    if (!view.player.inventory.canGain(item)) {
        return false;
    }
    view.gainItem(item, 1);
}


function warpLogic(view, data) {
    const x = view.player.x;
    const y = view.player.y;
    const warpblock = view.getBlock(x, y-1);
    if (warpblock !== 'WW') {
        return false;
    }
    if (view.getBlock(x, y+5) !== 'R1') {
        return false;
    }
    const tiles = view.getTileEntityAt(x, y-1);
    if (tiles.length === 0) {
        console.log('missing portal tile entity');
        return false;
    }
    // there is a tile
    const tile = tiles[0];
    // for backwards compatibility, warp tile is named portal
    if (tile.t !== 'portal') {
        console.log('bad portal tile type: ', tile.t);
        return false;
    }
    const targetx = tile.d.tx;
    const targety = tile.d.ty;

    // warp animation is only client-side
    if (!view.isServer) {
        for (let dy = -50; dy <= 50; dy++) {
            for (let dx = -50; dx <= 50; dx++) {
                const dist = Math.abs(dx) + Math.abs(dy);
                if (dist > 70) continue;
                view.player.blockanimations.push({
                    x: targetx+3+dx,
                    y: targety-1+dy,
                    b: view.getBlock(x+dx, y+dy),
                    a: dist * 0.3 + Math.random()*3 - 2
                });
            }
        }
    }

    view.player.x = targetx + 3;
    view.player.y = targety - 1;
    view.playerPosChanged();
    if (view.getBlock(targetx, targety+6) === 'R1') {
        // spend R at exit warp
        view.setBlock(targetx, targety+6, ' ');
    }
    else {
        // huh, it's not there.
        // spend R at entry warp instead
        view.setBlock(x, y+5, ' ');
    }
    return true;
}


if (typeof exports !== 'undefined') {
    exports.craftLogic = craftLogic;
    exports.eatLogic = eatLogic;
    exports.magicallygetitemLogic = magicallygetitemLogic;
    exports.warpLogic = warpLogic;
}
