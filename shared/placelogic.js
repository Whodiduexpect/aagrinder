// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}


// @ts-ignore
String.prototype.replaceAt = function(index, newchar) {
    return this.substring(0, index) + newchar + this.substring(index+1);
}


// Determines how a block changes when it sticks to the block above
const stickTop = {
    '0':'8',
    '1':'9',
    '2':'a',
    '3':'b',
    '4':'c',
    '5':'d',
    '6':'e',
    '7':'f',
};

// Determines how a block changes when it sticks to the block below
const stickBottom = {
    '0':'2',
    '1':'3',
    '4':'6',
    '5':'7',
    '8':'a',
    '9':'b',
    'c':'e',
    'd':'f',
};

// Determines how a block changes when it sticks to the block on the left
const stickLeft = {
    '0':'1',
    '2':'3',
    '4':'5',
    '6':'7',
    '8':'9',
    'a':'b',
    'c':'d',
    'e':'f',
};

// Determines how a block changes when it sticks to the block on the right
const stickRight = {
    '0':'4',
    '1':'5',
    '2':'6',
    '3':'7',
    '8':'c',
    '9':'d',
    'a':'e',
    'b':'f',
};

function placeLogic(view, data) {
    let x = view.player.x + data.x;
    let y = view.player.y + data.y;
    if(Math.abs(view.player.x - x) > view.player.reach
        || Math.abs(view.player.y - y) > view.player.reach){
        // should not be able to reach!
        return false;
    }

    const claim = view.getClaim(x, y);
    if (claim !== null && claim.p !== view.player.name) {
        if (!view.checkIfFriend(claim.p)) {
            view.notifyPrint('Owned by '+claim.p);
            return false;
        }
    }

    const space = view.getBlock(x, y);
    // what to place?
    if(typeof data.item !== 'string'){
        return false;
    }
    const item = data.item;

    // special case when placing WV (not a block; just creating a warp)
    if (item.length > 3
        && item[0] === 'W'
        && item[1] === 'V'
        && view.player.inventory.hasItem('WV')
    ){
        // spend item
        if (view.player.gamemode === 0) {
            view.gainItem('WV', -1);
        }
        const scope = item[2];
        const scopeOk = ['o', 'f', 'a'].includes(scope);
        if (!scopeOk) return false;
        // WV code only exists on server
        if (view.isServer) {
            const code = item.substr(3, 100);
            const success = view.attemptCreateCustomWarp(x, y, code, scope);
            if (success) {
                // notify player
                view.player.socket.emit('chat',
                    {message: 'warp created. /takewarp to remove.'});
            } else {
                // notify player
                view.player.socket.emit('chat',
                    {message: 'warp code unavailable!'});
                // force item back (because on client it was spent)
                if (view.player.gamemode === 0) {
                    view.surpriseGainItem('WV', 1);
                }
            }
        }
        // return true (client has to assume this was successful)
        return true;
    }

    // must be an empty space
    if (space !== ' '
        && space !== 'w'
        && space !== '~'
        && space !== 'sn'
        && space[0] !== '>') {
        // special case when placing ? on ?? (to modify it)
        if (space === '??'
            && item.length === 2
            && item[0] === '?'
            && BP.validquestions.indexOf(item[1]) > -1) {
            view.setBlock(x, y, item, true);
            return true;
        }
        // special case when placing si on si (to modify it)
        if (space === 'si'
            && item.length > 2
            && item[0] === 's'
            && item[1] === 'i'){
            const text = item.substr(2, 100);
            // make sure to delete previous tile entity if exists
            view.playerDeleteTileEntityAt(x, y);
            // create sign tile entity
            view.playerCreateTileEntity({
                t: 'sign',
                x: x,
                y: y,
                d: {t: text},
            });
            return true;
        }
        return false;
    }

    // must have item
    if (!view.player.inventory.hasItem(item)) {
        // you don't even have this item
        return false;
    }

    // grinder needs specified slot
    // and it needs to be an integer within bounds
    if (item[0] === 'G' &&
        (
            !Number.isInteger(data.slot)
            || data.slot < 0
            || data.slot > view.player.inventory.state[item].d.length
        )
    ) {
        return false;
    }

    const relativeCheck = (rx,ry)=>{
        return view.getBlock(x+rx, y+ry);
    }
    const block = Inventory.item2block(item, relativeCheck);

    if (!BP.isEnterable(block)) {
        // must not be a player
        if (view.isPlayer(x,y)) {
            return false;
        }
    }
    // can't place FOLLOW
    if (item === 'Wo') {
        return false;
    }
    // can't place bucket
    if (item[0] === 'u') {
        return false;
    }
    // tiny tree only on dirt
    // (or on stone, if it is type tb)
    if (item[0] === 't' && view.getBlock(x, y-1) !== 'd'
        && !(item[1] === 'b' && view.getBlock(x, y-1) === 'B')) {
        return false;
    }
    // can't place snow layer on non-solid block
    if (item === 'sn' && !BP.isSolidBlock(view.getBlock(x, y-1))) {
        return false;
    }
    // flower has special placement procedure (override)
    if (item[0] === '*') {
        // can't if not on dirt
        if (view.getBlock(x, y-1) !== 'd') {
            return false;
        }
        // can't if no space above
        if (view.getBlock(x, y+1) !== ' '
            && view.getBlock(x, y+1) !== 'w') {
            return false;
        }
        // ok place the flower
        view.setBlock(x, y+1, item, true);
        // and the stem
        view.setBlock(x, y, '*|', true);
        if (view.player.gamemode === 0) {
            view.gainItem(item, -1);
        }
        return true;
    }
    // special stuff for grinder
    // (this depends on previous inventory state so
    // it needs to be done before inventory is modified below)
    if (item[0] === 'G') {
        // grinder has data
        const gdata = view.player.inventory.state[item].d[data.slot]
        view.playerCreateTileEntity({
            t: 'grinder',
            x: x,
            y: y,
            d: gdata,
        });

    }
    // ok place here
    view.setBlock(x, y, block, true);
    if (view.player.gamemode === 0) {
        view.gainItem(item, -1, data.slot);
    }
    // connect surrounding grass or wire to this block
    if (block[0] === '-'
        || block[0] === '='
        || block[0] === '%'
        || block[0] === ':'
        || block[0] === 'G'
        || block === 'R1'
        || block === 'st'
        || block === 'sw'
        || block === 'O0'
        || block === 'Wp'
        || block === 'sc'
        || block === 'sC'
    ) {
        // okay so it IS actually grass
        const stickblock = block[0] === '-' ? '-' : '=';
        { // stick the top grass
            const otherBlock = view.getBlock(x, y+1);
            if (otherBlock[0] === stickblock) {
                const found = stickTop[otherBlock[1]];
                if (found) {
                    view.setBlock(x, y+1, otherBlock.replaceAt(1, found));
                }
            }
        }
        { // stick the bottom grass
            const otherBlock = view.getBlock(x, y-1);
            if (otherBlock[0] === stickblock) {
                const found = stickBottom[otherBlock[1]];
                if (found) {
                    view.setBlock(x, y-1, otherBlock.replaceAt(1, found));
                }
            }
        }
        // unless it's underwater, stick left and right too
        if (space !== 'w') {
            { // stick the left grass
                const otherBlock = view.getBlock(x-1, y);
                if (otherBlock[0] === stickblock) {
                    const found = stickLeft[otherBlock[1]];
                    if (found) {
                        view.setBlock(x-1, y, otherBlock.replaceAt(1, found));
                    }
                }
            }
            { // stick the right grass
                const otherBlock = view.getBlock(x+1, y);
                if (otherBlock[0] === stickblock) {
                    const found = stickRight[otherBlock[1]];
                    if (found) {
                        view.setBlock(x+1, y, otherBlock.replaceAt(1, found));
                    }
                }
            }
        }
    }
    // special stuff for chest
    if (item[0] === 'h') {
        if (view.isServer) {
            // on the server, create the chest.
            const total_slots = item === 'hh' ? 10 : 20;
            view.createTileEntity({
                t: 'chest',
                x: x,
                y: y,
                inventory: new Inventory({}, total_slots),
                subscribers: [],
            });
            // Process adjacent hoppers
            for (let dx = -1; dx < 2; dx++) {
                for (let dy = -1; dy < 2; dy++) {
                    // Don't do diagonals or itself
                    if (Math.abs(dx) === Math.abs(dy) || (dx === 0 && dy === 0)) continue;

                    const block_here = view.getBlock(x + dx, y + dy);
                    if (block_here[0] !== 'p') continue;

                    view.processHopper(x + dx, y + dy, block_here);
                }
            }
        }
    }

    // special stuff for hoppers
    if (view.isServer && (item[0] === 'p') ) {
        if (view.isHopperTrailTooLong(x, y, block)) {
            view.surpriseSetBlock(x, y, ' ', false);
            view.surpriseGainItem(view.player.inventory.block2item(item), 1);
            return false;
        }
        view.processHopper(x, y, block);
    }

    // special stuff for detector
    if (view.isServer && item === 'E') {
        // create tile entity, but only on server
        view.createTileEntity({
            t: 'detector',
            x: x,
            y: y,
        });
    }

    // special stuff for SSPOOKY
    if (view.isServer && item === 'Ws') {
        // create tile entity, but only on server
        view.createTileEntity({
            t: 'sspooky',
            x: x,
            y: y,
        });
    }

    // special stuff for wires and water
    if (view.isServer) {
        if (item[0] === 'S'
            || item[0] === 'O'
            || item[0] === 'R'
            || item === 'Dm'
            || item === 'WO'
            || item === 'st'
            || item === '%'
            || item === ':'
            || item[0] === '=') {
            GLS.enqueueWire5(x, y, view);
        }
        GLS.checkEnqueueWater(x+1, y, view);
        GLS.checkEnqueueWater(x-1, y, view);
        GLS.checkEnqueueWater(x, y+1, view);
        GLS.checkEnqueueWater(x, y-1, view);
    }
    return true;
}


if (typeof exports !== 'undefined') {
    exports.placeLogic = placeLogic;
}
