// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Sprites are pairs of Unicode characters and color codes,
 * with block code strings as identifiers.
 *
 * This should be the only place in the whole codebase
 * fancy characters appear.
 */

"use strict";

const leaf_a = '#1a8239';
const leaf_i = '#016759';
const leaf_l = '#fd88f2';
const leaf_p = '#2e9258';
const leaf_v = '#0a6b00';
const leaf_b = '#308229';
const leaf_r = '#117739';
const leaf_u = '#2e9228';

const SPRITES = {
    'B0': {char: 'B', color: '#7f7f7f', intensity: 1},
    'B1': {char: 'B', color: '#7e7f7f', intensity: 20},
    'B2': {char: 'B', color: '#7f7e7f', intensity: 2},
    'B3': {char: 'B', color: '#7f7f7e', intensity: 3},
    'B4': {char: 'B', color: '#7f7f7d', intensity: 9},
    'B5': {char: 'B', color: '#7f7d7f', intensity: 20},
    'B6': {char: 'B', color: '#7d7f7f', intensity: 20},
    'B7': {char: 'B', color: '#7f7f7f', intensity: 13},

    'd0': {char: 'B', color: '#6b5633', intensity: 5},
    'd1': {char: 'B', color: '#6a5633', intensity: 16},
    'd2': {char: 'B', color: '#6b5533', intensity: 7},
    'd3': {char: 'B', color: '#6b5632', intensity: 8},
    'd4': {char: 'B', color: '#6b5634', intensity: 9},
    'd5': {char: 'B', color: '#6b5733', intensity: 20},
    'd6': {char: 'B', color: '#6c5633', intensity: 21},
    'd7': {char: 'B', color: '#6b5633', intensity: 13},

    'B': {char: 'B', color: '#7f7f7f', intensity: 0},
    'd': {char: 'B', color: '#7b6643', intensity: 5},
    'S': {char: 'B', color: '#d8bf93', intensity: 3},
    'b': {char: 'B', color: '#9f9f9f', intensity: 5},
    'w': {char: 'W', color: '#4549a5', intensity: 6},
    'bubble': {char: 'o', color: '#4549a5', intensity: 6},
    '~': {char: '~', color: '#cccccc', sway:true, intensity: 0},
    'Aa': {char: 'A', color: leaf_a, sway: true, intensity: 14},
    'Ai': {char: 'A', color: leaf_i, sway: true, intensity: 14},
    'Al': {char: 'A', color: leaf_l, sway: true, intensity: 14},
    'Ap': {char: 'A', color: leaf_p, sway: true, intensity: 14},
    'Av': {char: 'A', color: leaf_v, sway: true, intensity: 14},
    'Ab': {char: 'A', color: leaf_b, sway: true, intensity: 14},
    'Ar': {char: 'A', color: leaf_r, sway: true, intensity: 14},
    'Au': {char: 'A', color: leaf_u, sway: true, intensity: 14},
    'aa': {char: 'A', color: leaf_a, sway: true, intensity: 14},
    'ai': {char: 'A', color: leaf_i, sway: true, intensity: 14},
    'al': {char: 'A', color: leaf_l, sway: true, intensity: 14},
    'ap': {char: 'A', color: leaf_p, sway: true, intensity: 14},
    'av': {char: 'A', color: leaf_v, sway: true, intensity: 14},
    'ab': {char: 'A', color: leaf_b, sway: true, intensity: 14},
    'ar': {char: 'A', color: leaf_r, sway: true, intensity: 14},
    'au': {char: 'A', color: leaf_u, sway: true, intensity: 14},
    'Waa': {char: 'A', color: leaf_a, sway: true, intensity: 14},
    'Wai': {char: 'A', color: leaf_i, sway: true, intensity: 14},
    'Wal': {char: 'A', color: leaf_l, sway: true, intensity: 14},
    'Wap': {char: 'A', color: leaf_p, sway: true, intensity: 14},
    'Wav': {char: 'A', color: leaf_v, sway: true, intensity: 14},
    'Wab': {char: 'A', color: leaf_b, sway: true, intensity: 14},
    'War': {char: 'A', color: leaf_r, sway: true, intensity: 14},
    'Wau': {char: 'A', color: leaf_u, sway: true, intensity: 14},
    'Ha': {char: 'H', color: '#91702a', intensity: 9},
    'Hi': {char: 'H', color: '#75542f', intensity: 9},
    'Hl': {char: 'H', color: '#d0c8ce', intensity: 9},
    'Hp': {char: 'H', color: '#91702a', intensity: 9},
    'Hv': {char: 'H', color: '#b1601a', intensity: 9},
    'Hb': {char: 'H', color: '#71502a', intensity: 9},
    'Hr': {char: 'H', color: '#81750a', intensity: 9},
    'Hu': {char: 'H', color: '#61501a', intensity: 9},
    'pv': {char: 'v', color: '#666666', intensity: 5},
    'p<': {char: '<', color: '#666666', intensity: 5},
    'p>': {char: '>', color: '#666666', intensity: 5},
    'ta': {char: 't', color: leaf_a, misplace: 1, intensity: 14},
    'ti': {char: 't', color: leaf_i, misplace: 1, intensity: 14},
    'tl': {char: 't', color: leaf_l, misplace: 1, intensity: 14},
    'tp': {char: 't', color: leaf_p, misplace: 1, intensity: 14},
    'tv': {char: 't', color: leaf_v, misplace: 1, intensity: 14},
    'tb': {char: 't', color: leaf_b, misplace: 1, intensity: 14},
    'tr': {char: 't', color: leaf_r, misplace: 1, intensity: 14},
    'tu': {char: 't', color: leaf_u, misplace: 1, intensity: 14},
    'ta0': {char: 't', color: leaf_a, misplace: 1, intensity: 14},
    'ti0': {char: 't', color: leaf_i, misplace: 1, intensity: 14},
    'tl0': {char: 't', color: leaf_l, misplace: 1, intensity: 14},
    'tp0': {char: 't', color: leaf_p, misplace: 1, intensity: 14},
    'tv0': {char: 't', color: leaf_v, misplace: 1, intensity: 14},
    'tb0': {char: 't', color: leaf_b, misplace: 1, intensity: 14},
    'tr0': {char: 't', color: leaf_r, misplace: 1, intensity: 14},
    'tu0': {char: 't', color: leaf_u, misplace: 1, intensity: 14},
    'ta1': {char: 't', color: leaf_a, misplace: 1, intensity: 11},
    'ti1': {char: 't', color: leaf_i, misplace: 1, intensity: 11},
    'tl1': {char: 't', color: leaf_l, misplace: 1, intensity: 11},
    'tp1': {char: 't', color: leaf_p, misplace: 1, intensity: 11},
    'tv1': {char: 't', color: leaf_v, misplace: 1, intensity: 11},
    'tb1': {char: 't', color: leaf_b, misplace: 1, intensity: 11},
    'tr1': {char: 't', color: leaf_r, misplace: 1, intensity: 11},
    'tu1': {char: 't', color: leaf_u, misplace: 1, intensity: 11},
    'ta2': {char: 't', color: leaf_a, misplace: 1, intensity: 9},
    'ti2': {char: 't', color: leaf_i, misplace: 1, intensity: 9},
    'tl2': {char: 't', color: leaf_l, misplace: 1, intensity: 9},
    'tp2': {char: 't', color: leaf_p, misplace: 1, intensity: 9},
    'tv2': {char: 't', color: leaf_v, misplace: 1, intensity: 9},
    'tb2': {char: 't', color: leaf_b, misplace: 1, intensity: 9},
    'tr2': {char: 't', color: leaf_r, misplace: 1, intensity: 9},
    'tu2': {char: 't', color: leaf_u, misplace: 1, intensity: 9},
    'hh': {char: 'h', color: '#ad8632', intensity: 0},
    'hg': {char: 'h', color: '#ffd700', intensity: 0},
    'hH': {char: 'H', color: '#ffd700', intensity: 0},
    'hB': {char: 'B', color: '#ffd700', intensity: 0},
    'hA': {char: 'A', color: '#ffd700', intensity: 0},
    'hD': {char: 'D', color: '#ffd700', intensity: 0},
    'O0': {char: 'O', color: '#7f7f7f', intensity: 0},
    'O1': {char: 'O', color: '#e0115f', intensity: 0},
    'sw': {char: 'S', color: '#7f7f7f', intensity: 0},
    's1': {char: 'S', color: '#e0115f', intensity: 0},
    'Wp': {char: 'p', color: '#7f7f7f', intensity: 0},
    'sc': {char: 's', color: '#7f7f7f', intensity: 0},
    'sC': {char: 's', color: '#e0115f', intensity: 0},
    'M0': {char: 'M', color: '#9e2929', intensity: 0},
    'G0': {char: 'G', color: '#2faebf', intensity: 0},
    'G#': {char: 'G', color: '#444', intensity: 0},
    'Gy': {char: 'G', color: '#d38afa', intensity: 0},
    'GB': {char: 'G', color: '#7f7f7f', intensity: 3},
    'GH': {char: 'G', color: '#91702a', intensity: 3},
    'DD': {char: 'D', color: '#2faebf', intensity: 3},
    'R0': {char: 'B', color: '#9f5f7f', intensity: 0},
    'R1': {char: 'R', color: '#e0115f', intensity: 0},
    'Rd': {char: 'R', color: '#9f5f7f', intensity: 0},
    '#s': {char: '#', color: '#333333', misplace: true},
    '#y': {char: '#', color: '#d38afa', misplace: true},
    'E': {char: 'æ', color: '#eeeeee', intensity: 0},
    'E0': {char: 'æ', color: '#eeeeee', intensity: 0},
    'E1': {char: 'æ', color: '#eeeeee', blinkspeed: 1, intensity: 0},
    'E2': {char: 'æ', color: '#eeeeee', blinkspeed: 2, intensity: 0},
    'E3': {char: 'æ', color: '#eeeeee', blinkspeed: 3, intensity: 0},
    'E4': {char: 'æ', color: '#eeeeee', blinkspeed: 4, intensity: 0},
    'Ws': {char: '§', color: '#333333', sway: true, intensity: 0},
    'Wo': {char: '¤', color: '#333333', intensity: 0},
    'Wo0': {char: '¤', color: '#333333', intensity: 0},
    'Wo1': {char: '¤', color: '#333333', blinkspeed: 1, intensity: 0},
    'Wo2': {char: '¤', color: '#333333', blinkspeed: 2, intensity: 0},
    'Wo3': {char: '¤', color: '#333333', blinkspeed: 3, intensity: 0},
    'Wo4': {char: '¤', color: '#333333', blinkspeed: 4, intensity: 0},
    '%': {char: '%', color: '#e0115f', intensity: 0},
    'si': {char: '▀', color: '#aaaaaa', intensity: 5},
    'WB': {char: 'B', color: '#666666', intensity: 5},
    'WW': {char: 'W', color: '#444444', intensity: 5},
    'WV': {char: 'W', color: '#eeeeee', intensity: 5},
    'WF': {char: 'F', color: '#eeeeee', intensity: 0},
    'WI': {char: 'I', color: '#727280', intensity: 4},
    'WT': {char: 'T', color: '#727280', intensity: 4},
    'Wt': {char: 't', color: '#5a5219', intensity: 4},
    'WA': {char: 'A', color: '#9d2ccd', intensity: 4},
    'WO': {char: '0', color: '#bd7c3d', intensity: 4},
    'WON': {char: 'N', color: '#bd7c3d', sway: true, intensity: 4},
    'WD': {char: '0', color: '#9d9287', intensity: 4},
    'fp': {char: 'a', color: '#fbae81', intensity: 4},
    'fl': {char: 'a', color: '#d8a34b', intensity: 4},
    'fv': {char: 'a', color: '#34a625', intensity: 4},
    'fa': {char: 'a', color: '#cf1005', intensity: 4},
    'fr': {char: 'b', color: '#a80000', intensity: 4},
    'fu': {char: 'b', color: '#41597c', intensity: 4},
    'Wfp': {char: 'a', color: '#fbae81', intensity: 4},
    'Wfl': {char: 'a', color: '#d8a34b', intensity: 4},
    'Wfv': {char: 'a', color: '#34a625', intensity: 4},
    'Wfa': {char: 'a', color: '#cf1005', intensity: 4},
    'Wfr': {char: 'b', color: '#a80000', intensity: 4},
    'Wfu': {char: 'b', color: '#41597c', intensity: 4},
    '..': {char: '.', color: '#7f7f7f', intensity: 5},
    'sn': {char: '▁', color: '#ffffff', intensity: 3}, // not underscore
    'snsway': {char: '▁', color: '#ffffff', sway: true, intensity: 3},
    'sN': {char: '▇', color: '#ffffff', intensity: 3},
    'Dg': {char: 'c', color: '#cccccc', intensity: 0},
    'Dm': {char: 'm', color: '#684043', intensity: 0},
    'DM': {char: 'm', color: '#9f5f7f', intensity: 0},
    'Wi': {char: '▁', color: '#afd1db', intensity: 0},
    'Wc': {char: '▇', color: '#afd1db', intensity: 0},
    'Wl': {char: 'T', color: '#afd1db', intensity: 0},
    'sp': {char: '♐', color: '#684043', intensity: 0},
    'WS': {char: 'B', color: '#5f3f07', intensity: 3},
    'sB': {char: '▁', color: '#7f7f7f', intensity: 1},
    'st': {char: '▁', color: '#91702a', intensity: 9},
    'sT': {char: '▁', color: '#91702a', intensity: 9, dy: +0.8},
    'ue': {char: 'u', color: '#91702a', intensity: 0},
    'uw': {char: 'u', color: '#91702a', intensity: 0, secondchar: {char: '▆', color: '#5559b5', dx: 0.5, dy: -0.1, scale: 0.5}},
    'u~': {char: 'n', color: '#91702a', intensity: 0, secondchar: {char: '~', color: '#cccccc', dx: 0.45, dy: 0.1, scale: 0.5}},
    'us': {char: 'u', color: '#91702a', intensity: 0, secondchar: {char: '▆', color: '#ffffff', dx: 0.5, dy: -0.1, scale: 0.5}},
    'uB': {char: 'u', color: '#91702a', intensity: 0, secondchar: {char: '▆', color: '#5f3f07', dx: 0.5, dy: -0.1, scale: 0.5}},

    '||': {char: '|', color: '#91702a', intensity: 9},
    '|/': {char: '/', color: '#91702a', intensity: 9},
    '|\\':{char: '\\',color: '#91702a', intensity: 9},
    '|/a': {char: 'a', color: '#91702a', sway: true, intensity: 9},

    '-0': {char: '─', color: '#34962a', intensity: 8},
    '-1': {char: '╶', color: '#34962a', intensity: 8},
    '-2': {char: '╵', color: '#34962a', intensity: 8},
    '-3': {char: '╰', color: '#34962a', intensity: 8},
    '-4': {char: '╴', color: '#34962a', intensity: 8},
    '-5': {char: '─', color: '#34962a', intensity: 8},
    '-6': {char: '╯', color: '#34962a', intensity: 8},
    '-7': {char: '┴', color: '#34962a', intensity: 8},
    '-8': {char: '╷', color: '#34962a', intensity: 8},
    '-9': {char: '╭', color: '#34962a', intensity: 8},
    '-a': {char: '│', color: '#34962a', intensity: 8},
    '-b': {char: '├', color: '#34962a', intensity: 8},
    '-c': {char: '╮', color: '#34962a', intensity: 8},
    '-d': {char: '┬', color: '#34962a', intensity: 8},
    '-e': {char: '┤', color: '#34962a', intensity: 8},
    '-f': {char: '┼', color: '#34962a', intensity: 8},

    '=0': {char: '─', color: '#e0115f', intensity: 0},
    '=1': {char: '╶', color: '#e0115f', intensity: 0},
    '=2': {char: '╵', color: '#e0115f', intensity: 0},
    '=3': {char: '└', color: '#e0115f', intensity: 0},
    '=4': {char: '╴', color: '#e0115f', intensity: 0},
    '=5': {char: '─', color: '#e0115f', intensity: 0},
    '=6': {char: '┘', color: '#e0115f', intensity: 0},
    '=7': {char: '┴', color: '#e0115f', intensity: 0},
    '=8': {char: '╷', color: '#e0115f', intensity: 0},
    '=9': {char: '┌', color: '#e0115f', intensity: 0},
    '=a': {char: '│', color: '#e0115f', intensity: 0},
    '=b': {char: '├', color: '#e0115f', intensity: 0},
    '=c': {char: '┐', color: '#e0115f', intensity: 0},
    '=d': {char: '┬', color: '#e0115f', intensity: 0},
    '=e': {char: '┤', color: '#e0115f', intensity: 0},
    '=f': {char: '┼', color: '#e0115f', intensity: 0},

    ':0': {char: '─', color: '#70082f', intensity: 0, secondchar: {char: ':', color: '#f8c5f5', dx: 0, dy: -0.08, scale: 1}},
    ':1': {char: '─', color: '#c680ad', intensity: 0, secondchar: {char: ':', color: '#70082f', dx: 0, dy: -0.08, scale: 1}},
    ':2': {char: '─', color: '#c680ad', intensity: 0, secondchar: {char: ':', color: '#f8c5f5', dx: 0, dy: -0.08, scale: 1}},

    'Xx': {char: '╳', color: '#7f7f7f', intensity: 5}, // dead (grey)
    'X0': {char: '╳', color: '#8b5b6d', sway: true, intensity: 2}, // brown (dull purple)
    'X1': {char: '╳', color: '#5e9d8d', sway: true, intensity: 2}, // blue (dull green)
    'X2': {char: '╳', color: '#c85b45', sway: true, intensity: 2}, // red (orange)
    'X3': {char: '╳', color: '#428c66', sway: true, intensity: 2}, // green (dull)
    'X4': {char: '╳', color: '#82e85a', sway: true, intensity: 2}, // neon (green)
    'X5': {char: '╳', color: '#eb8432', sway: true, intensity: 2}, // orange (neon)
    'X6': {char: '╳', color: '#fce44e', sway: true, intensity: 2}, // yellow
    'X7': {char: '╳', color: '#834aca', sway: true, intensity: 2}, // purple
    'X8': {char: '╳', color: '#d38afa', sway: true, intensity: 2}, // pink

    'Fx': {char: '▇', color: '#7f7f7f', intensity: 5}, // grey
    'F0': {char: '▇', color: '#8b655b', intensity: 2}, // brown (dull purple)
    'F1': {char: '▇', color: '#3a75dd', intensity: 2}, // blue
    'F2': {char: '▇', color: '#c85b45', intensity: 2}, // red (orange)
    'F3': {char: '▇', color: '#428c66', intensity: 2}, // green (dull)
    'F4': {char: '▇', color: '#82e85a', intensity: 2}, // neon (green)
    'F5': {char: '▇', color: '#eb9432', intensity: 2}, // orange (neon)
    'F6': {char: '▇', color: '#fce44e', intensity: 2}, // yellow
    'F7': {char: '▇', color: '#834aca', intensity: 2}, // purple
    'F8': {char: '▇', color: '#d38afa', intensity: 2}, // pink
    'F9': {char: '▇', color: '#57c2d7', intensity: 2}, // cyan
    'Fa': {char: '▇', color: '#c22378', intensity: 2}, // magenta
    'Fb': {char: '▇', color: '#cbdbdb', intensity: 2}, // white
    'Fc': {char: '▇', color: '#ddc239', intensity: 2}, // gold
    'Fd': {char: '▇', color: '#cf1005', intensity: 2}, // red
    'Fe': {char: '▇', color: '#a80000', intensity: 2}, // dark red

    // flowers
    '*0': {char: '*', color: '#fce44e', intensity: 3, dy: -0.5}, // yellow
    '*1': {char: '*', color: '#4585f5', intensity: 3, dy: -0.5}, // blue
    '*2': {char: '"', color: '#88f0e7', intensity: 3, dy: -0.5}, // cyan
    '*3': {char: '”', color: '#ee31a8', intensity: 3, dy: -0.5}, // magenta
    '*4': {char: '*', color: '#d38afa', intensity: 3, dy: -0.5}, // pink
    '*5': {char: '^', color: '#cbdbdb', intensity: 3, dy: -0.5}, // white
    '*6': {char: '`', color: '#ddc239', intensity: 3, dy: -0.5}, // gold
    '*|': {char: '|', color: '#52af20', intensity: 3}, // stem

    'R>0': {char: '⇒', color: '#e0115f', intensity: 0},
    'R>1': {char: '⇗', color: '#e0115f', intensity: 0},
    'R>2': {char: '⇑', color: '#e0115f', intensity: 0},
    'R>3': {char: '⇖', color: '#e0115f', intensity: 0},
    'R>4': {char: '⇐', color: '#e0115f', intensity: 0},
    'R>5': {char: '⇙', color: '#e0115f', intensity: 0},
    'R>6': {char: '⇓', color: '#e0115f', intensity: 0},
    'R>7': {char: '⇘', color: '#e0115f', intensity: 0},

// → ⇒
// ↗ ⇗
// ↑ ⇑
// ↖ ⇖
// ← ⇐
// ↙ ⇙
// ↓ ⇓
// ↘ ⇘

    '>v': {char: 'W', color: '#4549a5', misplace: 0.2, intensity: 6},
    '>u': {char: 'W', color: '#4549a5', misplace: 0.2, intensity: 6},
    '>t': {char: 'W', color: '#4549a5', misplace:-0.2, intensity: 6},
    '>s': {char: 'W', color: '#4549a5', misplace:-0.2, intensity: 6},
    '>r': {char: 'W', color: '#4549a5', misplace: 0.3, intensity: 6},
    '>q': {char: 'W', color: '#4549a5', misplace: 0.3, intensity: 6},
    '>p': {char: 'W', color: '#4549a5', misplace:-0.3, intensity: 6},
    '>o': {char: 'W', color: '#4549a5', misplace:-0.3, intensity: 6},
    '>n': {char: 'W', color: '#4549a5', misplace: 0.4, intensity: 6},
    '>m': {char: 'W', color: '#4549a5', misplace: 0.4, intensity: 6},
    '>l': {char: 'W', color: '#4549a5', misplace:-0.4, intensity: 6},
    '>k': {char: 'W', color: '#4549a5', misplace:-0.4, intensity: 6},
    '>j': {char: 'W', color: '#4549a5', misplace: 0.5, intensity: 6},
    '>i': {char: 'W', color: '#4549a5', misplace: 0.5, intensity: 6},
    '>h': {char: 'W', color: '#4549a5', misplace:-0.5, intensity: 6},
    '>g': {char: 'W', color: '#4549a5', misplace:-0.5, intensity: 6},
    '>f': {char: 'W', color: '#4549a5', misplace: 0.6, intensity: 6},
    '>e': {char: 'W', color: '#4549a5', misplace: 0.6, intensity: 6},
    '>d': {char: 'W', color: '#4549a5', misplace:-0.6, intensity: 6},
    '>c': {char: 'W', color: '#4549a5', misplace:-0.6, intensity: 6},
    '>b': {char: 'W', color: '#4549a5', misplace: 0.7, intensity: 6},
    '>a': {char: 'W', color: '#4549a5', misplace: 0.7, intensity: 6},
    '>9': {char: 'W', color: '#4549a5', misplace:-0.7, intensity: 6},
    '>8': {char: 'W', color: '#4549a5', misplace:-0.7, intensity: 6},
    '>7': {char: 'W', color: '#4549a5', misplace: 0.8, intensity: 6},
    '>6': {char: 'W', color: '#4549a5', misplace: 0.8, intensity: 6},
    '>5': {char: 'W', color: '#4549a5', misplace:-0.8, intensity: 6},
    '>4': {char: 'W', color: '#4549a5', misplace:-0.8, intensity: 6},
    '>3': {char: 'W', color: '#4549a5', misplace: 0.9, intensity: 6},
    '>2': {char: 'W', color: '#4549a5', misplace: 0.9, intensity: 6},
    '>1': {char: 'W', color: '#4549a5', misplace:-0.9, intensity: 6},
    '>0': {char: 'W', color: '#4549a5', misplace:-0.9, intensity: 6},

    '?': {char: '?', color: '#7f7f7f', intensity: 0},
    '?!': {char:  '!', color: '#7f7f7f', intensity: 0},
    '?"': {char:  '"', color: '#7f7f7f', intensity: 0},
    '?#': {char:  '#', color: '#7f7f7f', intensity: 0},
    '?$': {char:  '$', color: '#7f7f7f', intensity: 0},
    '?%': {char:  '%', color: '#7f7f7f', intensity: 0},
    '?&': {char:  '&', color: '#7f7f7f', intensity: 0},
    '?\'':{char:  '\'',color: '#7f7f7f', intensity: 0},
    '?(': {char:  '(', color: '#7f7f7f', intensity: 0},
    '?)': {char:  ')', color: '#7f7f7f', intensity: 0},
    '?*': {char:  '*', color: '#7f7f7f', intensity: 0},
    '?+': {char:  '+', color: '#7f7f7f', intensity: 0},
    '?,': {char:  ',', color: '#7f7f7f', intensity: 0},
    '?-': {char:  '-', color: '#7f7f7f', intensity: 0},
    '?.': {char:  '.', color: '#7f7f7f', intensity: 0},
    '?/': {char:  '/', color: '#7f7f7f', intensity: 0},
    '?0': {char:  '0', color: '#7f7f7f', intensity: 0},
    '?1': {char:  '1', color: '#7f7f7f', intensity: 0},
    '?2': {char:  '2', color: '#7f7f7f', intensity: 0},
    '?3': {char:  '3', color: '#7f7f7f', intensity: 0},
    '?4': {char:  '4', color: '#7f7f7f', intensity: 0},
    '?5': {char:  '5', color: '#7f7f7f', intensity: 0},
    '?6': {char:  '6', color: '#7f7f7f', intensity: 0},
    '?7': {char:  '7', color: '#7f7f7f', intensity: 0},
    '?8': {char:  '8', color: '#7f7f7f', intensity: 0},
    '?9': {char:  '9', color: '#7f7f7f', intensity: 0},
    '?:': {char:  ':', color: '#7f7f7f', intensity: 0},
    '?;': {char:  ';', color: '#7f7f7f', intensity: 0},
    '?<': {char:  '<', color: '#7f7f7f', intensity: 0},
    '?=': {char:  '=', color: '#7f7f7f', intensity: 0},
    '?>': {char:  '>', color: '#7f7f7f', intensity: 0},
    '??': {char:  '?', color: '#7f7f7f', intensity: 0},
    '?@': {char:  '@', color: '#7f7f7f', intensity: 0},
    '?A': {char:  'A', color: '#7f7f7f', intensity: 0},
    '?B': {char:  'B', color: '#7f7f7f', intensity: 0},
    '?C': {char:  'C', color: '#7f7f7f', intensity: 0},
    '?D': {char:  'D', color: '#7f7f7f', intensity: 0},
    '?E': {char:  'E', color: '#7f7f7f', intensity: 0},
    '?F': {char:  'F', color: '#7f7f7f', intensity: 0},
    '?G': {char:  'G', color: '#7f7f7f', intensity: 0},
    '?H': {char:  'H', color: '#7f7f7f', intensity: 0},
    '?I': {char:  'I', color: '#7f7f7f', intensity: 0},
    '?J': {char:  'J', color: '#7f7f7f', intensity: 0},
    '?K': {char:  'K', color: '#7f7f7f', intensity: 0},
    '?L': {char:  'L', color: '#7f7f7f', intensity: 0},
    '?M': {char:  'M', color: '#7f7f7f', intensity: 0},
    '?N': {char:  'N', color: '#7f7f7f', intensity: 0},
    '?O': {char:  'O', color: '#7f7f7f', intensity: 0},
    '?P': {char:  'P', color: '#7f7f7f', intensity: 0},
    '?Q': {char:  'Q', color: '#7f7f7f', intensity: 0},
    '?R': {char:  'R', color: '#7f7f7f', intensity: 0},
    '?S': {char:  'S', color: '#7f7f7f', intensity: 0},
    '?T': {char:  'T', color: '#7f7f7f', intensity: 0},
    '?U': {char:  'U', color: '#7f7f7f', intensity: 0},
    '?V': {char:  'V', color: '#7f7f7f', intensity: 0},
    '?W': {char:  'W', color: '#7f7f7f', intensity: 0},
    '?X': {char:  'X', color: '#7f7f7f', intensity: 0},
    '?Y': {char:  'Y', color: '#7f7f7f', intensity: 0},
    '?Z': {char:  'Z', color: '#7f7f7f', intensity: 0},
    '?[': {char:  '[', color: '#7f7f7f', intensity: 0},
    '?\\':{char:  '\\',color: '#7f7f7f', intensity: 0},
    '?]': {char:  ']', color: '#7f7f7f', intensity: 0},
    '?^': {char:  '^', color: '#7f7f7f', intensity: 0},
    '?_': {char:  '_', color: '#7f7f7f', intensity: 0},
    '?`': {char:  '`', color: '#7f7f7f', intensity: 0},
    '?a': {char:  'a', color: '#7f7f7f', intensity: 0},
    '?b': {char:  'b', color: '#7f7f7f', intensity: 0},
    '?c': {char:  'c', color: '#7f7f7f', intensity: 0},
    '?d': {char:  'd', color: '#7f7f7f', intensity: 0},
    '?e': {char:  'e', color: '#7f7f7f', intensity: 0},
    '?f': {char:  'f', color: '#7f7f7f', intensity: 0},
    '?g': {char:  'g', color: '#7f7f7f', intensity: 0},
    '?h': {char:  'h', color: '#7f7f7f', intensity: 0},
    '?i': {char:  'i', color: '#7f7f7f', intensity: 0},
    '?j': {char:  'j', color: '#7f7f7f', intensity: 0},
    '?k': {char:  'k', color: '#7f7f7f', intensity: 0},
    '?l': {char:  'l', color: '#7f7f7f', intensity: 0},
    '?m': {char:  'm', color: '#7f7f7f', intensity: 0},
    '?n': {char:  'n', color: '#7f7f7f', intensity: 0},
    '?o': {char:  'o', color: '#7f7f7f', intensity: 0},
    '?p': {char:  'p', color: '#7f7f7f', intensity: 0},
    '?q': {char:  'q', color: '#7f7f7f', intensity: 0},
    '?r': {char:  'r', color: '#7f7f7f', intensity: 0},
    '?s': {char:  's', color: '#7f7f7f', intensity: 0},
    '?t': {char:  't', color: '#7f7f7f', intensity: 0},
    '?u': {char:  'u', color: '#7f7f7f', intensity: 0},
    '?v': {char:  'v', color: '#7f7f7f', intensity: 0},
    '?w': {char:  'w', color: '#7f7f7f', intensity: 0},
    '?x': {char:  'x', color: '#7f7f7f', intensity: 0},
    '?y': {char:  'y', color: '#7f7f7f', intensity: 0},
    '?z': {char:  'z', color: '#7f7f7f', intensity: 0},
    '?{': {char:  '{', color: '#7f7f7f', intensity: 0},
    '?|': {char:  '|', color: '#7f7f7f', intensity: 0},
    '?}': {char:  '}', color: '#7f7f7f', intensity: 0},
    '?~': {char:  '~', color: '#7f7f7f', intensity: 0},
    '?€': {char:  '€', color: '#7f7f7f', intensity: 0},

    'NaN': {char: '▞', color: '#ff00ff', intensity: 0},
    'undefined': {char: '▚', color: '#ff00ff', intensity: 0},

};

if (typeof exports !== 'undefined')
    exports.SPRITES = SPRITES;
