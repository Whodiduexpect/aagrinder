// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
This file determines the order and logic of applying
a set of player actions within the same game tick.

sharedTickLogic gets
- a tick object that needs to be executed
- a callback which is used for executing individual actions
  (multiple actions executed per tick of course)

This same file is used by both the server and the client.
*/

"use strict";

function sharedTickLogic(tick, action) {
    let wasfiltered = false;
    let sprintwasused = false;
    if (tick.u && tick.d) {
        // up and down cancel each other out
        delete tick.u;
        delete tick.d;
        wasfiltered = true;
    }
    if (tick.l && tick.r) {
        // left and right cancel each other out
        delete tick.l;
        delete tick.r;
        wasfiltered = true;
    }
    if (typeof(tick.c) !== "undefined" && typeof(tick.c.r)!== "undefined") {
        if (!action('craft', {r: tick.c.r, n: tick.c.n})) {
            delete tick.c;
            wasfiltered = true;
        }
    }
    let nojump = false;
    if (tick.u) {
        const success = action('up', tick.s ? tick.s : 1);
        if (success) {
            sprintwasused = true;
            nojump = true;
        } else {
            delete tick.u;
            wasfiltered = true;
        }
    }
    if (tick.d) {
        const success = action('down', tick.s ? tick.s : 1);
        if (success) {
            sprintwasused = true;
            nojump = true;
        } else {
            delete tick.d;
            wasfiltered = true;
        }
    }
    if (!nojump) {
        // try to fall down
        // or jump if space is pressed
        // (falljump action conveniently ignores
        // all invalid attempts to fall)
        if (tick.j) {
            if (!action('falljump', {jump: true})) {
                delete tick.j;
                wasfiltered = true;
            }
        }
        else {
            action('falljump', {jump: false});
        }
    }
    if (tick.l) {
        if (!action('left', {sprint: 1})) {
            delete tick.l;
            wasfiltered = true;
        }
        if (tick.s) {
            // another movement
            if (action('left', {sprint: tick.s ? tick.s : 1})) {
                sprintwasused = true;
            }
        }
    }
    if (tick.r) {
        if (!action('right', {sprint: 1})) {
            delete tick.r;
            wasfiltered = true;
        }
        if (tick.s) {
            // another movement
            if (action('right', {sprint: tick.s ? tick.s : 1})) {
                sprintwasused = true;
            }
        }
    }
    if (typeof(tick.I) !== 'undefined') {
        // attempt to interact (fails if air)
        if (!action('interact', {
            x: tick.I.x,
            y: tick.I.y,
            item: tick.I.i,
        })) {
            delete tick.I;
            wasfiltered = true;
        }
    }
    if (typeof(tick.D) !== 'undefined'
        && typeof(tick.D.x) !== 'undefined'
        && typeof(tick.D.y) !== 'undefined') {
        let successD;
        if (tick.D.G === 1) { // with grinder
            successD = action('grinder', {x: tick.D.x, y: tick.D.y, slot: tick.D.s});
        }
        else if (tick.D.G === 2) { // with SUPER GRINDER
            successD = action('grinder', {x: tick.D.x, y: tick.D.y, slot: tick.D.s, g: '#'});
        }
        else if (tick.D.G === 3) { // with yay grinder
            successD = action('grinder', {x: tick.D.x, y: tick.D.y, slot: tick.D.s, g: 'y'});
        }
        else if (tick.D.G === 4) { // with stone grinder
            successD = action('grinder', {x: tick.D.x, y: tick.D.y, slot: tick.D.s, g: 'B'});
        }
        else if (tick.D.G === 5) { // with wooden grinder
            successD = action('dig', {x: tick.D.x, y: tick.D.y, slot: tick.D.s, g: 'H'});
        }
        else { // with fist
            successD = action('dig', {x: tick.D.x, y: tick.D.y});
        }
        if (!successD) {
            delete tick.D;
            // because dig failed, reset digtime
            if (action('digreset')) {
                tick.Dr = true;
            }
            wasfiltered = true;
        }
    } else {
        // because there was no dig, reset digtime
        if (action('digreset')) {
            tick.Dr = true;
        }
    }
    if (typeof(tick.P) !== 'undefined'
        && typeof(tick.P.x) !== 'undefined'
        && typeof(tick.P.y) !== 'undefined'
        && typeof(tick.P.i) !== 'undefined') {
        if (!action('place', {
            x: tick.P.x,
            y: tick.P.y,
            item: tick.P.i,
            slot: tick.P.s,
        })) {
            delete tick.P;
            wasfiltered = true;
        }
    }

    if (typeof(tick.e) !== 'undefined') {
        // attempt to eat
        if (!action('eat', {item: tick.e})) {
            delete tick.e;
            wasfiltered = true;
        }
    }

    if (tick.W) {
        // attempt to warp
        // (this action is freely triggered by the client)
        if (!action('warp', {})) {
            delete tick.W;
            wasfiltered = true;
        }
    }

    if (tick.s && !sprintwasused) {
        // unused sprint
        delete tick.s;
        wasfiltered = true;
    }

    if (typeof(tick.M) !== 'undefined') {
        if (!action('magicallygetitem', {item: tick.M})) {
            delete tick.M;
            wasfiltered = true;
        }
    }

    return wasfiltered;
}


if (typeof exports !== 'undefined')
    exports.sharedTickLogic = sharedTickLogic;
