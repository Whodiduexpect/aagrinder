// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}


function upLogic(view, data) {
    const x = view.player.x;
    const y = view.player.y;
    if (isNaN(data)) return false;
    const speed = parseInt(data);
    if (speed < 1) return false;
    switch(view.player.gamemode){
        case 0:
            if (speed > 2) return false;
            if (view.getBlock(x, y) === 'WO') {
                // stuck on the grabber
                return false;
            }
            if (data === 2) {
                // wants to sprint up
                if (
                    BP.isClimbSprintable(view.getBlock(x, y))
                    && GLS.checkEnterableFromBelow(view, x, y+1)
                    && BP.isClimbSprintable(view.getBlock(x, y+1))
                    && GLS.checkEnterableFromBelow(view, x, y+2)
                    && view.player.food > 0
                ) {
                    // two spaces up!
                    view.player.y += 2;
                    view.player.jump = 1;
                    view.player.food--;
                    view.playerPosChanged();
                    break;
                }
                // failed sprinting up
                // (but can still move up normally)
            }
            // is there a ladder?
            if (
                GLS.checkEnterableFromBelow(view, x, y+1)
                && (
                    BP.isClimbable(view.getBlock(x, y+1))
                    || BP.isClimbable(view.getBlock(x, y))
                )
            ){
                // there is either a ladder here
                // or in the spot we're entering
                // anyway it works.
                view.player.y += 1;
                view.player.jump = 1;
                view.playerPosChanged();
                break;
            }
            return false;
        case 1:
            if (speed > 2) return false;
            // fly up
            if (!GLS.checkEnterableFromBelow(view, x, y+1)
                && BP.isEnterable(view.getBlock(x, y))) {
                return false;
            }
            view.player.y += 1;
            if (data === 2
                && GLS.checkEnterableFromBelow(view, x, y+2)) {
                view.player.y += 1;
            }
            view.playerPosChanged();
            break;
        case 2:
            if (speed > 256) return false;
            // spectator mode
            view.player.y += speed;
            view.playerPosChanged();
            break;
        default:
            console.log('invalid gamemode: ' + view.player.gamemode);
            return false;
    }
    return true;
}


function downLogic(view, data) {
    const x = view.player.x;
    const y = view.player.y;
    if (isNaN(data)) return false;
    const speed = parseInt(data);
    if (speed < 1) return false;
    switch(view.player.gamemode){
        case 0:
            if (speed > 2) return false;
            if (view.getBlock(x, y) === 'WO') {
                // stuck on the grabber
                return false;
            }
            if (data === 2) {
                // wants to sprint down
                if (
                    BP.isClimbSprintable(view.getBlock(x, y))
                    && GLS.checkEnterableFromAbove(view, x, y-1)
                    && BP.isClimbSprintable(view.getBlock(x, y-1))
                    && GLS.checkEnterableFromAbove(view, x, y-2)
                    && view.player.food > 0
                ) {
                    // two spaces down!
                    view.player.y += -2;
                    view.player.food--;
                    view.playerPosChanged();
                    break;
                }
                // failed sprinting down
                // (but can still move down normally)
            }
            // is there a ladder?
            if (
                GLS.checkEnterableFromAbove(view, x, y-1)
                && (
                    BP.isClimbable(view.getBlock(x, y-1))
                    || BP.isClimbable(view.getBlock(x, y))
                )
            ){
                // there is either a ladder here
                // or in the spot we're entering
                // anyway it works.
                view.player.y += -1;
                view.playerPosChanged();
                break;
            }
            // in other cases, can't climb down
            return false;
        case 1:
            if (speed > 2) return false;
            // fly down
            if(
                !GLS.checkEnterableFromAbove(view, x, y-1)
                && BP.isEnterable(view.getBlock(x, y))
            ){
                return false;
            }
            view.player.y += -1;
            if (data === 2
                && GLS.checkEnterableFromAbove(view, x, y-2)
            ) {
                view.player.y += -1;
            }
            view.playerPosChanged();
            break;
        case 2:
            if (speed > 256) return false;
            // spectator mode
            view.player.y -= speed;
            view.playerPosChanged();
            view.playerPosChanged();
            break;
        default:
            console.log('invalid gamemode: ' + view.player.gamemode);
            return false;
    }
    return true;
}


if (typeof exports !== 'undefined') {
    exports.upLogic = upLogic;
    exports.downLogic = downLogic;
}
