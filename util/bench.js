function e(x, y) {
    const chunkx = x >> 8;
    const chunky = y >> 8;
    const subchunkx = (x%256+256)%256;
    const subchunky = (y%256+256)%256;
    return chunkx - chunky + subchunkx - subchunky;
}

function f(x, y) {
    return (x >> 8) - (y >> 8) + (x%256+256)%256 - (y%256+256)%256;
}

const N = 30000

let res = 0;
for (x = 0; x < N; x++)
    for (y = 0; y < N; y++)
        res += e(x, y);

console.log(res);
