#!/usr/bin/python3

# Written by MRAAGH.
# I don't want to copyright this small script.

# This file is in the public domain.

r = 0xe0
g = 0x11
b = 0x5f

r *= 0.5
g *= 0.5
b *= 0.5

for i in range(0,16):
    grad = (i / 16)*0.7+0.3 if i > 0 else 0
    print("'#%02x%02x%02x'," % (
        int(r + grad * (255-r)),
        int(g + grad*0.8 * (255-g)),
        int(b + grad * (255-b)),
        ))
